import { browser, element, by } from 'protractor';

export class BYOThPage {
    navigateTo() {
        return browser.get('/');
    }

    getLoginButtonText() {
        return element(by.css('app-root #login')).getText();
    }
}
