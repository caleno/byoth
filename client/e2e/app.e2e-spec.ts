import { BYOThPage } from './app.po';

describe('BYOTh App', () => {
    let page: BYOThPage;

    beforeEach(() => {
        page = new BYOThPage();
    });

    it('should display login button', async () => {
        await page.navigateTo();
        const text = await page.getLoginButtonText();
        expect(text).toEqual('Logg inn');
    });
});
