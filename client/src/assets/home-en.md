# Welcome to Avhandlingsportalen 

Avhandlingsportalen offers several services for your PhD thesis: 

1.  You can submit your approved thesis for printing and deposit it in the open archive BORA 

1.  You can submit text and a photograph  for the press release promoting your public defence 

1.  You can order hard copies of the thesis for the evaluation committee 

It is mandatory to make printed copies of your  thesis available to the public prior to the public defence.  Additionally, you can  choose to make your thesis accessible in the University open research archive, Bergen Open Research Archive (BORA). 

The relevant forms for the different services are available from  the top bar. You need a UiB username and password in order to log on. You may begin filling in the form, leave it, and continue the process later.  

Below you will find information that will help you in filling in the forms. The advice  may be helpful even if you are not in the process of finishing your degree. For example, it can be well worth making decisions on format early in the writing process. 

## Submit approved thesis for printing 

 To place the order, you need the following information: 

-   The date of your public defence 

-   The number of copies you need for the public defence 

It can also be a benefit to have the following information at hand: 

-   Permission to use published articles in the printed version of your thesis 

-   Abstract of your thesis 

-   If you wish to upload the thesis as one or several files 

-   The approved purchaser at your unit 

-   If you want colour print of relevant pages (extra cost) 

-   If you want foiled cover (extra cost) 

-   If you want a test print (extra cost) 

-   If you want to make your thesis available online  in BORA 

### Placing the order and production   

Choose language: Norwegian or English. Follow the instructions and fill in required fields. You will receive a confirmation after the order is booked 

1.  Forward the confirmation to an approved purchaser at your department. The approved purchaser will place the order through UiB's e-commerce system 

1.  The printing company will not start production until the order is officially placed  

1.  While waiting for an official confirmation in the e-commerce system, the printer will produce a test print or PDF for you to read through 

1.  You look through the test print or PDF and give feedback to the printing company. Once the test print or PDF is approved, the printer starts the production of the thesis and delivers the agreed number of copies to the relevant department at UiB  

1.  8 copies are automatically added to the order. The copies are made available for loan at the relevant library 

1.  The delivery period depends on what kind of files the printing company receives from you, and how fast they receive feedback on the test print/PDF. Production period is 5 workdays from the test print/PDF is approved. A more urgent delivery can be arranged at an extra cost 

The various units have a limit on how many copies they will cover. If you want to print extra copies, you may do this in the portal. Extra copies will come at your expense. 

### Open digital access  

In the same form as you submit your approved thesis for printing, you also have the possibility to choose to make your thesis available online in Bergen Open Research Archive (BORA). BORA is the University of Bergen's open research archive.  

You can choose to put an embargo on when the thesis should be made available, and you have the  chooise between the following licenses:  

-   All rights reserved within the boundaries of [Norwegian copyright law ](https://lovdata.no/dokument/NL/lov/2018-06-15-40). The work can be referenced, linked to, cited, printed and downloaded for private use.  

-   Attribution ( [CC-BY ](https://creativecommons.org/licenses/by/4.0/deed.no)): The work can be shared and modified if it is correctly credited.  

-   Attribution/Non-commercial ( [CC-BY-NC ](https://creativecommons.org/licenses/by-nc/4.0/deed.no)): The work can be shared and modified if it is correctly credited and not used for commercial purposes.  

#### Additional information about monographies 

If you are planning to publish your thesis as a book you should check if it is possible to make the thesis available with the publisher/ what is common within your research field. Some publishers will permit  

#### Additional information about article-based theses 

Articles can be made available in BORA if you choose this and if the publisher gives permission. Most publishers will permit a version of the article to be made available in an open archive. The University Library will clear copyright for articles before they are made available in BORA. 

If you want to make your thesis available in BORA in a complete digital version, we have the following tips: 

1.  Open-Access articles can be made available in the published version. 

1.  Use your  accepted versions of articles in the thesis. Most publishers allow this version to be made available, but not the published version. 

1.  Get permission from your co-authors before you make available manuscripts and preprints. 

1.  If you do not want to make available preprints before publishing, you can choose an embargo that ensures that the article is not available until after it is published. 

## Submit press release  

Avhandlingsportalen offers a form for submitting your press release. All press releases will be published here: <https://www.uib.no/nye-doktorgrader>. Please note that the press release must be written in Norwegian. Ask for help from colleagues or your supervisor regarding translation if you're not fluent in Norwegian. 

A successful submission presupposes the following: 

-   A press release in Norwegian that has been approved by the Faculty 

-   A short and catchy title capturing the essence of the thesis 

-   Personal information (short) 

-   Your contact information (e-mail and phone number) 

-   A picture for the press release, with information on the photographer 

-   Time and place for the public defence 

-   Time and place for the trial lecture 

## Order copies for the evaluation committee 

Avhandlingsportalen offers a form for ordering copies for the evaluation committee. However, it is not obligatory to order these copies through Avhandlingsportalen.     

Some faculties distribute the thesis to the evaluation committee in digital format, only. Hard copies for evaluation purposes is not necessary in these instances. 

## Template for thesis   

A template has been made to assist in the writing of the thesis and ensure that appropriate content elements are included (see below). Not all elements are mandatory. You may use other headings when relevant. 

Most theses are printed in book format 17 x 24 cm., but written in page size A4. To compensate for this the font size of body text is set to 13pt and line spacing is set at 1.5 in the template. This is to ensure that the text is legible when the document later is shrunk from A4 to book format. 

If you want to add page separators (for example between articles) you can indicate this with black banners. 

Chapters, list of content, introduction etc. should start at the right section of the book, i.e. on a page with an odd number. 

Cover, title page and colophon page are automatically generated from the information you enter when placing the order on the webpage of Skipnes. Here is an example of cover, title page and colophon page (OPPRETT DOKUMENT OG LEGG INN HYPELENKE).   

The thesis can be printed in A4 format, if there are scientific arguments for such a format. Please contact us at Skipnes if you have an inquiry on format. 

Please make a thorough proof-read of the text, geometrical figures, pictures etc. before placing your order. The fewer corrections we need to make, the faster we can deliver the thesis. 

Reference handling tools like EndNote, EndNote online and Reference Manager are available for PhD candidates at UiB. Please see the webpage of the UiB Library on this theme. 

## Template for errata   

After submission, the candidate may apply to correct formal errors in the thesis. With the application you need to attach a list (errata list) displaying all the corrections you plan to make. Neither the corrected version nor the errata list should be sent to the committee. 

You can apply to make corrections up until one week after you have received the evaluation report. The errata list can be included as a loose document in the thesis.  

A special text template has been made for the documentation of such corrections.        

## Pictures, graphs and geometrical figures   

Pictures in the thesis must meet certain quality criteria when put on print. When you place your order for printing, an automatic check is done on the quality of your pictures. Pictures of low quality will be refused. Either way, it is important to have a conscious approach to quality before printing. Below is some general advice on picture quality. 

Technical quality in pictures is often measured in ppi (pixels per inch). If you use a picture editing programme you will be able to see ppi for the pictures you want to use. A picture should have at least 200 ppi if you write and edit in A4-format. 

Here are some examples of different picture sizes and the adequate number of pixels in A4-format: 

If you want to fill the whole page (the text area), the picture will measure 16 x 24 cm (w x h). It must then have 1260 x 1890 pixels (w x h). 

If you want to use a smaller picture, for example 5 x 4 cm (w x h), 315 x 395 (w x h) pixels will be sufficient. 

The thesis is scaled down from A4 to book format (17 x 24) when printed. A picture that is 200 ppi in A4 will become approximately 250 ppi in book format. 250 ppi is considered as sufficient quality when printed, but pictures of lower technical quality may also look fine when printed. The low limits for the number of pixels are somewhat flexible. 

Some tips on pictures: Try to avoid enlarging small pictures as this reduces the picture quality. If jpg-pictures are used, one must avoid compressing the file. The size of the file does not in itself tell everything about the picture quality as different file formats may have different degrees of compression. A picture with a sufficient number of pixels may not look optimal. This may occur if the file is too compressed, or if a small picture has been scaled up and added pixels to it. 

Vector graphics: We advise you to save vector graphics in .png-format, as the complexity of line thickness, colour coding, and transparent colours may lead to disappointing results. 

Dots and lines: (for example in figures) should not have thickness less than 0,5 pt. 

Questions regarding these issues may be addressed to the printing company.