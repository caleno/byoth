### Følgende vilkår gjelder for alle lisensvalg:

* Forfatter beholder opphavsretten, og har rett til å trekke tilbake sin doktorgradsavhandling fra BORA.
* Forfatter har ansvar for å innhente tillatelse til digital tilgjengeliggjøring av verk skapt av tredjepart som er brukt i avhandlingen (illustrasjoner, grafer, bilder og lignende).
* UiB har rett til å konvertere filen til andre format enn PDF hvis nødvendig for arkiverings- og tilgangsformål.
* UiB har rett til å overføre og gjøre tilgjengelig avhandlingen i et annet arkiv med samme formål som BORA, enten det er eid eller drevet av Universitetet, eller et tilsvarende nasjonalt arkiv, hvis et slikt arkiv erstatter BORA i fremtiden.
