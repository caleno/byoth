### The following conditions apply to all license options:

* The author keeps the copyright, and retains the right to withdraw their thesis from BORA.
* It's the author's responsibility to obtain permission for digital reproduction av work created by third parties (illustrations, graphs, pictures and similar).
* UiB has the right to convert the file to other formats than PDF, if necessary for archiving or publishing.
* UiB has the right to transfer and publish the thesis in another archive with the same purpose as BORA, either it is run or owned by UiB, or a corresponding national archive, in case such an archive replaces BORA in the future.
