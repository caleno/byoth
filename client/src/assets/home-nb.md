# Velkommen til Avhandlingsportalen  

Avhandlingsportalen tilbyr en rekke tjenester for din doktorgradsavhandling:  

1.  Du kan levere din godkjente avhandling til trykking og til digital tilgjengeliggjøring i BORA  

1.  Du kan levere tekst og bilde til pressemelding på nettsidene til Universitetet i Bergen  

1.  Du kan bestille bedømmelseseksemplarer av din avhandling til komité  

Avhandlingen skal gjøres offentlig tilgjengelig. Minstekravet er at trykte eksemplarer blir tilgjengelig på biblioteket før disputasen og i disputaslokalet under disputasen. I tillegg kan du velge å gjøre tilgjengelig en digital utgave i Universitetets åpne forskningsarkiv Bergen Open Research Archive (BORA). 

Skjema for de ulike tjenestene åpnes i knapperaden øverst på siden. Tilgang til skjemaene krever UiB brukernavn og passord. Det er mulig å påbegynne utfylling av skjema og gå tilbake å fullføre i ettertid.

Under finner du informasjon som hjelper deg gjennom de ulike bestillingsskjemaene. Hvis du ennå ikke er klar for å bruke de ulike skjemaene, vil vi likevel anbefale at du ser gjennom rådene under. Det kan være verdt å gjøre bevisste valg om format tidlig i skriveprosessen.  

## Lever godkjent avhandling

For å kunne gjennomføre bestillingen trenger du følgende informasjon:  

-   Disputasdato
-   Antall eksemplarer som skal trykkes til disputaslokalet

Det kan også være en fordel om du har klarlagt følgende:  

-   Tillatelse til å bruke publiserte artikler i den trykte versjonen av avhandlingen
-   Abstract/sammendrag av avhandlingen på engelsk
-   Om du vil laste opp avhandlingen som én eller flere filer
-   Hvem som er godkjent bestiller ved din enhet
-   Om du ønsker fargetrykk av utvalgte sider (koster ekstra)
-   Om du ønsker foliert omslag (koster ekstra)
-   Om du ønsker prøvetrykk (koster ekstra) og hvor prøvetrykk skal leveres
-   Om du ønsker digital tilgjengeliggjøring  i BORA

### Bestilling og produksjonsprosess

1.  Velg språk: norsk eller engelsk. Følg instruksene og fyll inn alle nødvendige felt. Etter bestillingen er fullført og sendt, vil du motta en ordrebekreftelse.
2.  Du må så selv sende ordrebekreftelsen til ditt institutts bestiller/innkjøp. Innkjøp vil da opprette et bestillingsnummer og sende dette til trykkeriet.   
3.  Trykkeriet starter ikke produksjon før de har mottatt bestillingsnummeret fra UiB.  
4.  Trykkeriet ser igjennom filene og gjør filene trykkeklare. Trykkeriet lager enten en prøvetrykk eller en PDF-fil for gjennomlesing av deg mens de venter på bestillingsnummeret fra UiB.   
5.  Du ser gjennom prøvetrykket eller PDF-fil og sender tilbakemelding til trykkeriet. Straks prøvetrykket/PDF-fil er godkjent av deg, produserer trykkeriet avhandlingen og leverer avtalt antall kopier til avtalt adresse.   
6.  I tillegg legges 8 eksemplarer automatisk til bestillingen. Dette er eksemplarer som skal gå til biblioteket for utlån.   
7.  Leveringstid vil variere ut fra hva slags materiell trykkeriet mottar og hvor fort trykkeriet mottar tilbakemelding på prøvetrykk eller PDF-fil. Produksjonstid fra godkjent prøvetrykk er 5 arbeidsdager. Ved kortere leveringstid tilkommer pristillegg. 

Hvis du ønsker å trykke opp flere eksemplarer enn det obligatoriske antallet til disputaslokalet, kan du gjøre dette i en egen runde i skjema. Ph.d.-kandidaten betaler selv for de ekstra eksemplarene.   

### Digital tilgjengeliggjøring

I skjemaet for innlevering av godkjent avhandling kan du velge å gjøre avhandlingen din tilgjengelig i digital utgave i BORA. BORA er Universitetet i Bergens åpne forskningsarkiv. Fordelen med å gjøre avhandlingen tilgjengelig i BORA er at flere vil kunne få tilgang til å lese og bruke den i digital utgave. Avhandlingen kan legges ut på nett før disputas eller etter en embargoperiode.  

Du har følgende valg:   

-   Om du ønsker å tilgjengeliggjøre avhandlingen i BORA nå eller på et senere tidspunkt  
-   Lisensen du ønsker for digital tilgjengeliggjøring  
-   Om du vil tilgjengeliggjøre hele eller kun enkeltdeler av avhandlingen.  

Universitetsbiblioteket klarerer rettigheter for artikler i avhandlingen før disse blir lagt ut i BORA.   

Hvis du ønsker å tilgjengeliggjøre artikler i den digitale utgaven av avhandlingen er det en fordel om det er din aksepterte fagfellevurderte versjon (postprint) som legges ved. Det er langt flere utgivere som tillater at denne versjonen legges ut enn den publiserte versjonen. Unntaket er Open Access-artikler med en åpen lisens (Creative Commons). Disse kan legges ut i publisert versjon.  

#### Utfyllende informasjon om monografier

Hvis du senere skal publisere avhandlingen som bok bør du sjekke hva som er mulig i forhold til utgiver/hva som er vanlig innenfor ditt fagområde. Mange utgivere vil tillate at avhandlingen er tilgjengelig selv om den senere publiseres. Dette kan avhenge av i hvilken grad den omarbeides fra avhandling til bok.   

#### Utfyllende informasjon om artikkelbaserte avhandlinger

Artikler i artikkelbaserte avhandlinger vil kun gjøres tilgjengelig i BORA hvis du som forfatter godkjenner dette og hvis utgiver gir tillatelse. De fleste utgivere tillater at en versjon av artikkelen gjøres digitalt tilgjengelig i et åpent arkiv.   

Artiklene blir tilgjengelige som egne innførsler i BORA. Tilgjengeliggjøring av fagfellevurderte versjoner av artikkelen (akseptert versjon/publisert versjon) oppfyller krav om åpen tilgang satt av myndigheter og finansierer av forskning, som for eksempel EU og Norges Forskningsråd.  

Hvis du ønsker å tilgjengeliggjøre avhandlingen din komplett i digital utgave har vi følgende tips:  

1.  Open Access-artikler kan legges ut i publisert versjon. 
2.  For andre artikler, legg dine egne aksepterte versjoner av artiklene i avhandlingen. De fleste utgivere tillater at denne versjonen legges ut, men ikke den publiserte. 
3.  Hvis du er bekymret for å legge ut preprint, legg på en embargoperiode som sikrer at artikkelen ikke blir tilgjengelig før etter publisering.  

Les mer om hva som gjelder for digital tilgjengeliggjøring av ulike versjoner av artikler under Opphavsrett og publisering på denne siden. 

Les mer åpen tilgjengeliggjøring av din avhandling på Universitetsbiblioteket sine nettsider.  

## Lever pressemelding

På denne siden kan du levere pressemelding til UiB sine pressemeldingssider om nye doktorgrader (lenke) og data til kalenderoppføring over disputas og prøveforelesning på uib.no.  

For å kunne gjennomføre leveringen trenger du følgende informasjon:  

-   En pressemelding på norsk som er godkjent av ditt fakultet  
-   En kort og fengende tittel som fanger det sentrale ved avhandlingen 
-   Kort tekst om personalia  
-   Din kontaktinformasjon (e-post og eventuelt telefonnummer)  
-   Et pressebilde av deg som kan tilgjengeliggjøres på uib.no, med informasjon om hvem som har opphavsrett til bilde (fotograf)  
-   Tid og sted for disputas.  
-   Tid og sted for prøveforelesning (ikke obligatorisk) 

Kommunikasjonsavdelingen kan kontaktes dersom noe er feil eller du trenger å få lagt til informasjon i ettertid.  

## Bestill eksemplarer til komiteen

Her kan du valgfritt bestille trykkekopier av din avhandling til bedømmelseskomiteen. Dette kommer i tillegg til de versjonene du bestiller til disputas. Trykkekopien blir merket med informasjon om at dette er den versjonen som sendes til bedømmelse. Data blir gjenbrukt når du senere leverer avhandlingen til trykking og digital tilgjengeliggjøring.   

Noen institutt ved UiB sender ut digital fil til bedømmelseskomiteen, og bestilling av trykt kopi vil da ikke være aktuelt. Hør med ditt institutt hva som er rutinene hos dere.   

## Mal for avhandling

Vi har utarbeidet en mal som viser anbefalt format og innholdselementer for avhandlingen. Malen er å finne på Avhandlingsportalen. Malen er enkel og med få regler og uten makroer eller andre automatiske funksjoner. Det forutsettes ikke at kandidatene har tilgang til spesiell programvare eller ferdigheter i avansert tekstbehandling.  

-   De aller fleste doktoravhandlinger trykkes i bokformat 17 x 24 cm, men skrives i sidestørrelse A4. For å kompensere for dette er skriftstørrelsen i brødtekst satt til 13pt og linjeavstand satt til 1,5 i malen. Det er for å sikre at teksten har god lesbarhet når dokumentet senere krympes fra A4 til bokformat.  
-   Hvis du av faglige grunner ønsker å trykke avhandlingen i A4-format, er det mulighet for det. Ta da kontakt med trykkeriet for videre oppfølging. 
-   Hvis artikler legges i avhandlingen med utgivers formatering kan dette føre til at teksten i artikkelen blir vanskelig å lese i den trykte utgaven.  
-   Kapitler, innholdsfortegnelse, forord o.l. bør starte på høyre side i boken, det vil si en side med oddetall. 
-   Omslag, tittelark og kolofonside styres av egne maler og genereres når du legger inn bestilling i skjemaet. [Her er eksempel på omslag, tittelark og kolofonside](http://uib.skipnes.no/wp-content/uploads/2018/08/omslag-og-tk_2018.pdf). 

Les korrektur for skrivefeil og se over bilder, figurer og lignende før du bestiller. Minst mulig endringer betyr ofte kortere leveringstid. 

### Errataliste

Kandidaten kan etter innlevering søke fakultetet om å rette formelle feil i avhandlingen. Til søknaden skal det legges ved en liste (errataliste) som viser de rettinger kandidaten ønsker å foreta i avhandlingen. Verken den rettede versjonen av avhandlingen eller erratalisten skal oversendes bedømmelseskomiteen.  

Frist for søknad om retting av formelle feil er én uke etter at kandidaten har mottatt innstillingen. Erratalisten legges som innstikk i avhandlingen som er tilgjengelig under disputasen.  

Det er laget en tekstmal for erratalisten. Fakultet, institutt eller kandidaten vil ofte selv ta seg av utskrift, men trykkeriet kan levere opptrykk av errata. Ta kontakt med trykkeriet dersom dette er ønskelig.  

## Bildekvalitet, grafikk og figurer

Det er viktig at bilder som brukes i avhandlingen har høy nok kvalitet for trykking. Ved bestilling hos trykkeriet foretas en automatisert sjekk av bildekvalitet, og bilder med for lav kvalitet blir avvist.   

Det er likevel viktig å ha et bevisst forhold til kvalitet før du skal trykke avhandlingen. Under følger derfor noen råd:   

-   Teknisk kvalitet på bilder måles best i ppi (pixels per inch). Bruker du et bilderedigeringsprogram vil du kunne se ppi for bildene du ønsker å bruke. Hvis du limer inn et bilde i avhandlingen din, som ofte skrives i et A4-format, bør bildet være minst 200 ppi.  
-   Her er noen eksempler på ulike bildestørrelser og det antallet piksler som er tilfredsstillende i A4-format:  
-   Hvis du for eksempel vil fylle hele siden (tekstområdet), blir bildet 16 x 24 cm (w x h). Det må da inneholde 1260 x 1890 piksler (b x h).  
-   Skal du bruke et mindre bilde, f.eks. 5 x 4 cm (b x h), er det nok med 315 x 395 piksler.   
-   Avhandlinger skaleres ned fra A4 til bokformat (17 x 24 cm) ved trykking. Et bilde som har 200 ppi i A4, vil få rundt 250 ppi i bokformat. 250 ppi regnes som kurant kvalitet for trykking, men også bilder med lavere kvalitet kan fungere greit. De nedre grensene for antall piksler er fleksible, med andre ord.  

### Noen tips om bildebruk

Unngå å skalere opp små bilder, det reduserer kvaliteten. Bruker du jpg-bilder, bør de være lagret med høy kvalitet, det vil si med lite komprimering. Filstørrelsen sier nødvendigvis ikke så mye om bildekvaliteten siden ulike filformater kan ha ulik grad av komprimering. Selv om et bilde har nok piksler, kan det likevel se dårlig ut. Dette kan skje ved at det er for mye komprimert. Det kan også være at et lite bilde har blitt skalert opp og fått lagt til flere piksler. Bildet kan da bli grumsete.  

### Grafikk og figurer

Vi anbefaler å lage .png av vektorgrafikk, fordi strektykkelser, fargekoding og transparente farger kan føre til skuffende trykkresultat. Vi anbefaler også at disse blir laget i et størrelsesforhold 1:1 og med minst 200ppi i oppløsning, som nevnt over. Punkter og streker (f.eks. figurer) bør ikke ha mindre tykkelse enn 0,5 pt.  

Ta kontakt med support hos trykkeriet hvis noe er uklart.  

## Opphavsrettigheter og publisering

Det kan ikke legges begrensinger på offentliggjøring av en avhandling. Den må være offentlig tilgjengelig to uker før disputas, minimum i trykt utgave.  

Du har som forfatter selv opphavsrett til din doktorgradsavhandling. Du velger selv om du vil gjøre den digitalt tilgjengelig eller ikke.   

Selv om du eier rettigheter til selve avhandlingen, kan det ligge begrensninger på retten til bruk av materiale som er skapt av andre, eller deler av avhandlingen som er publisert.  

### Bruk av artikler i trykt utgave

Dersom du skal trykke publiserte artikler som en del av avhandlingen må du innhente tillatelse fra utgiver til dette. De aller fleste utgivere har gitt tillatelse til gjenbruk artikler i egne (trykte) publikasjoner i publiseringskontrakten, eller informasjon om dette finnes på utgiver/tidsskriftets nettsider. Hvis du følger lenke fra artikkelsiden hos utgiver til rettsklarering (RightsLink) kan du søke om tillatelse til bruk i egen avhandling der.  

Det er likevel slik at det ikke kan legges restriksjoner på offentliggjøring av doktorgradsavhandlinger. De versjonene av artikler som legges i avhandlingen må derfor kunne gjenbrukes med tillatelse fra utgiver.  

### Bruk av artikler i digital utgave

Egne betingelser gjelder for aksepterte/ publiserte artikler som gjøres tilgjengelig i digital utgave av doktorgradsavhandlingen.   

Følgende gjelder for artikler i digital utgave:  

- **Manuskript/preprint**: Du/dine medforfattere eier rettigheter. Hvis artikkelen er under publisering/ publisert gjelder utgivers betingelser. De fleste utgivere tillater at preprint-artikler tilgjengeliggjøres i åpent arkiv, men innenfor noen fagfelt aksepterer ikke utgiver at artikkelen er tilgjengelig før publisering.

  **Hva du kan gjøre**: Du velger om du vil legge ut preprint eller ikke sammen med dine medforfattere. Universitetsbiblioteket sjekker betingelser for bruk av innsendte artikler i Sherpa/RoMEO-databasen (du kan også sjekke selv). Du kan også legge på en embargoperiode som sikrer at artikkelen ikke blir lagt ut før etter publisering.  
- **Akseptert versjon (postprint)**: Dette er den siste fagfellevurderte versjonen som du sender til utgiver før publisering (før utgivers formatering). Utgiver eier vanligvis rettigheter og utgivers betingelser gjelder. De fleste utgivere tillater at aksepterte versjoner tilgjengeliggjøres i åpent arkiv, som oftest etter en embargoperiode på 6-24 måneder etter publisering.  

  **Hva du kan gjøre**: Du velger om du vil legge ut artikkelen eller ikke. Universitetsbiblioteket sjekker betingelser for bruk av artikkelen i Sherpa/RoMEO-databasen (du kan også sjekke selv).  

- **Publisert versjon**: Den versjonen som utgiver publiserer online eller i et nummer (utgivers PDF). Utgiver eier vanligvis rettigheter og utgivers betingelser gjelder. De aller fleste utgivere tillater IKKE at den publiserte versjonen tilgjengeliggjøres i åpent arkiv.   

  **Hva du kan gjøre**: Du velger om du vil legge ut artikkelen eller ikke. Universitetsbiblioteket sjekker betingelser for bruk av artikkelen i Sherpa/RoMEO-databasen (du kan også sjekke selv). Unntaket er Open Access artikler. Disse kan tilgjengeliggjøres fritt hvis de har en åpen lisens (Creative Commons).   

### Opphavsrett til bilder, grafer og figurer

Hvis du skal gjenbruke andre sine bilder, grafer og figurer i din avhandling må du spørre kunstner/fotograf/forfatter om tillatelse til å gjøre dette. Det er ulovlig å gjenbruke andre sitt materiale uten å spørre om tillatelse. 

**I publisert materiale**: Du må kontakte enten forfatter eller utgiver for tillatelse dersom bilde, figur eller graf er en del av forfatters publiserte artikkel eller bok, avhengig av hvem som eier rettighetene. På utgivers nettsider vil det ofte finnes en lenke til en tjeneste for å rettghetsklarere denne typen materiale (RightsLink).   

**Tilknytning til tekst**: Det er tillat å bruke fotografiske verk og billedkunst i tilknytning til tekst i en vitenskapelig fremstilling uten å spørre om tillatelse. Dette gjelder i den tykte versjonen. Bilder som brukes som illustrasjon må rettighetsklareres på vanlig måte. 

**Utvidede rettigheter**: BONO-avtalen gir utvidede rettigheter til bruk av fotografiske verk og billedkunst i avhandlingen.  BONO har inngått avtale med en rekke nasjonale og internasjonale kunstnere, og verk fra disse kunstnerne kan brukes uten å spørre om tillatelse. Dette gjelder også bruk som illustrasjon. 

Søk i BONO sin kunstdatabase.  

**Åpne lisenser**: Hvis bilder, grafer og figurer er tilgengelig med en åpen lisens kan du bruke disse uten å spørre om tillatelse.  

**Sitere bilder**: Det er viktig å referere korrekt ved gjenbruk av bilder, grafer og figurer.  Sett inn hvem som er skaper, rettighetshaver og hvilke tillatelser til gjenbruk som gjelder. 

Les mer Opphavsrett på Universitetsbiblioteket sine nettsider.
