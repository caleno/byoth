## Hvis du har problemer

Spørsmål om skjemaet, BORA, artikler og lisens kan sendes til bora@uib.no.

### Trykktekniske spørsmål

For trykktekniske spørsmål, for eksempel problem med PDF-validering eller omslag, ta kontakt med trykkeriet på 12341234 eller xyz@abc.no.

