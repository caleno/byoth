import { Component, Inject, LOCALE_ID, OnInit, OnDestroy, ViewContainerRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as marked from 'marked';

interface MenuItem {
    target: string,
    label: string,
    active: boolean
}

function isOnScreen(element: Element) {
    if (!element) {
        return false;
    }

    const rect = element.getBoundingClientRect();

    return (rect.top < window.innerHeight && rect.top > 0) ||
        (rect.height > window.innerHeight && rect.top <= 0 && rect.bottom >= window.innerHeight)
    ;
}

const markActiveHeader = (rootElement: Element, menuItems: MenuItem[]) => () => {
    let foundActive = false;
    menuItems.forEach(item => {
        const header = rootElement.querySelectorAll(item.target);
        if (!foundActive && isOnScreen(header[0])) {
            item.active = true;
            foundActive = true;
        } else {
            item.active = false;
        }
    })
}

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.component.html',
    styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit, OnDestroy {
    @ViewChild('contents', { read: ViewContainerRef }) contents: ViewContainerRef;

    private contentElement: Element;
    private scrollListener;

    items: MenuItem[] = [];

    constructor(private http: HttpClient,
                @Inject(LOCALE_ID) private locale) { }

    ngOnInit() {
        const file = 'assets/home-' + this.locale + '.md';
        this.http.get(file, { responseType: 'text' })
        .subscribe(
            doc => this.setContents(marked(doc))
        );
    }

    ngOnDestroy() {
        window.removeEventListener('scroll', this.scrollListener);
    }

    private setContents(value: string) {
        this.contentElement = this.contents.element.nativeElement;

        const inputDoc = document.createElement('div');
        inputDoc.innerHTML = value;

        const outputDoc = document.createElement('div');

        let section: Element;
        let sectionNumber = 0;

        for (let i = 0; i < inputDoc.children.length; ++i) {
            const child = inputDoc.children[i];

            if (child.tagName.toLowerCase() === 'h2') {
                section = document.createElement('section');
                section.id = 'section-' + (++sectionNumber);
                outputDoc.appendChild(section);
                this.items.push({
                    target: '#' + section.id,
                    label: child.textContent,
                    active: isOnScreen(child)
                });
            }

            if (section) {
                section.appendChild(child.cloneNode(true));
            } else {
                outputDoc.appendChild(child.cloneNode(true));
            }
        }

        this.contentElement.appendChild(outputDoc);

        this.scrollListener = markActiveHeader(this.contentElement, this.items)
        window.addEventListener('scroll', this.scrollListener);
    }

    scrollTo(target: string) {
        const targetElement: Element = this.contentElement.querySelector(target);
        window.scrollBy({ left: 0, top: targetElement.getBoundingClientRect().top - 150, behavior: 'smooth'});
    }
}
