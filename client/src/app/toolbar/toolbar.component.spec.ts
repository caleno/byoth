import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MaterialModule } from 'app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_DIALOG_DATA } from '@angular/material';

import { of, Observable, BehaviorSubject } from 'rxjs';

import { ThesisContainerService } from 'services/thesis-container.service';
import { MessageService } from 'services/message.service';
import { AuthService } from 'services/auth.service';
import { ToolbarComponent, ToolbarHelpDialogComponent, LOCATION_TOKEN } from './toolbar.component';

@NgModule({
    declarations: [ ToolbarHelpDialogComponent ],
    entryComponents: [ ToolbarHelpDialogComponent ]
})
class TestModule {};

describe('ToolbarComponent', () => {
    let component: ToolbarComponent;
    let fixture: ComponentFixture<ToolbarComponent>;
    const locale = 'en';

    class MockContainer {
        save() { return of(null); }
        delete() { return of(null); }
    }

    class MockAuth {
        userSubject = new BehaviorSubject<boolean>(false);
        currentUser() { return this.userSubject.asObservable(); }
        login(arg) { return null; }
        logout(arg) { return null; }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ RouterTestingModule, HttpClientTestingModule, MaterialModule, NoopAnimationsModule, TestModule ],
            declarations: [ ToolbarComponent ],
            providers: [
                { provide: ThesisContainerService, useClass: MockContainer },
                { provide: AuthService, useClass: MockAuth },
                { provide: LOCATION_TOKEN, useValue: {pathname: 'foo'} },
                { provide: MAT_DIALOG_DATA, useValue: ''},
                { provide: LOCALE_ID, useValue: locale},
                MessageService
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ToolbarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should send info message upon successful save', fakeAsync(() => {
        spyOn(component['message'], 'inform').and.callFake(() => {});
        component.save();
        tick();
        expect(component['message'].inform).toHaveBeenCalled();
    }));

    it('should send info message upon successful delete', fakeAsync(() => {
        spyOn(component['message'], 'inform').and.callFake(() => {});
        spyOn(component['router'], 'navigate').and.callFake(() => {});
        component.delete();
        tick();
        expect(component['message'].inform).toHaveBeenCalled();
    }));

    it('should log user out when logout() is called', () => {
        spyOn(component['auth'], 'logout').and.callThrough();
        component.logout();
        expect(component['auth'].logout).toHaveBeenCalled();
    });

    it('should log user in when login() is called', () => {
        spyOn(component['auth'], 'login').and.callThrough();
        component.login();
        expect(component['auth'].login).toHaveBeenCalled();
    });

    describe('toggleLanguage', () => {
        it('should change language to English if language is not English', () => {
            spyOn(component['location'], 'path').and.callFake(() => '/foo');
            component.langIsEnglish = false;
            component.toggleLanguage();
            expect(component['nativeLocation'].pathname).toEqual('/en/foo');
        });

        it('should change language to Norwegian if language is English', () => {
            spyOn(component['location'], 'path').and.callFake(() => '/foo');
            component.langIsEnglish = true;
            component.toggleLanguage();
            expect(component['nativeLocation'].pathname).toEqual('/foo');
        });
    });

    describe('openHelp', () => {
        it('should display a dialog box with the help message', () => {
            spyOn(component['dialog'], 'open').and.callThrough();
            component.openHelp();
            expect(component['dialog'].open).toHaveBeenCalled()}
        );
    });

    describe('ngOnInit', () => {
        it('should fetch help message from remote based on locale', fakeAsync(() => {
            spyOn(component['http'], 'get').and.callFake(() => {
                return of('foo')
            });
            expect(component.helpMessage).toEqual(undefined);
            component.ngOnInit();
            tick();
            expect(component.helpMessage).toContain('foo');
            expect((<any>component['http'].get).calls.argsFor(0)[0]).toContain('help-' + locale + '.md');
        }));

        it('should set default help message if remote request fails', fakeAsync(() => {
            spyOn(component['http'], 'get').and.callFake(() => {
                return Observable.create(observer => observer.error('error message'))
            });
            expect(component.helpMessage).toEqual(undefined);
            component.ngOnInit();
            tick();
            expect(component.helpMessage).toContain('Could not load help message');
        }));
    });
});
