import { Component, OnInit, LOCALE_ID, Inject, InjectionToken, isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import * as marked from 'marked';

import { User, AuthService } from 'services/auth.service';
import { ThesisContainerService } from 'services/thesis-container.service';
import { MessageService } from 'services/message.service';
import { Observable } from 'rxjs';

export const LOCATION_TOKEN = new InjectionToken<any>('Window location object');

@Component({
    selector: 'app-toolbar-help-dialog',
    template: '<div [innerHtml]="innerHtml"></div>'
})
export class ToolbarHelpDialogComponent {
    constructor(private dialogRef: MatDialogRef<ToolbarHelpDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public innerHtml: string) { }

    onNoClick(): void {
        this.dialogRef.close();
    }
}

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
    user$: Observable<User> = null;
    devmode = false;
    langIsEnglish = false;
    helpMessage: string;

    constructor(private location: Location,
                private container: ThesisContainerService,
                private message: MessageService,
                private auth: AuthService,
                private http: HttpClient,
                private router: Router,
                private dialog: MatDialog,
                @Inject(LOCALE_ID) private locale,
                @Inject(LOCATION_TOKEN) private nativeLocation: any) {
        this.user$ = this.auth.currentUser();
        this.devmode = isDevMode();

        this.langIsEnglish = locale === 'en';
    }

    ngOnInit() {
        const file = 'assets/help-' + this.locale + '.md';
        this.http.get(file, { responseType: 'text' })
        .subscribe(
            doc => this.helpMessage = marked(doc as string),
            err => this.helpMessage = '<p>Could not load help message for this language</p>',
        );
    }

    save() {
        this.container.save()
        .subscribe(() => this.message.inform({ en: 'Saved all data', nb: 'Lagret alle data' }));
    }

    delete() {
        this.container.delete()
        .subscribe(() => {
            this.message.inform({en: 'Deleted all data', nb: 'Slettet alle data' });
            this.router.navigate(['/']);
        });
    }

    login(): void {
        this.auth.login(this.locale);
    }

    logout(): void {
        this.auth.logout(this.locale);
    }

    toggleLanguage(): void {
        let path = this.location.path();

        if (!this.langIsEnglish) {
            path = '/en' + path;
        }

        this.nativeLocation.pathname = path;
    }

    openHelp(): void {
        this.dialog.open(ToolbarHelpDialogComponent, { width: '450px', data: this.helpMessage });
    }
}
