export const base64url = {
    encode: (str: string) => {
        return btoa(str).replace(/\+/g, '-').replace(/\//g, '_').replace(/\=+$/m, '');
    },
    decode: (b64str: string) => {
        return atob(b64str.replace(/\-/g, '+').replace(/_/g, '/').padEnd(b64str.length + (4 - b64str.length % 4) % 4, '='));
    }
}
