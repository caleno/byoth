import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, UrlSegment } from '@angular/router';
import { map } from 'rxjs/operators';


@Component({
    selector: 'app-error-page',
    template: `<h1>Error: {{statusCode}}</h1><p>Path: {{path}}</p><p>{{message}}</p>`,
    styleUrls: ['./error-page.component.css']
})
export class ErrorPageComponent implements OnInit {
    statusCode = null;
    message = null;
    path: UrlSegment[];

    constructor(private route: ActivatedRoute) { }

    ngOnInit() {
        this.route.data
        .pipe(map((data: any) => {
            this.statusCode = data['statusCode'];
            this.message = data['message'];
        }))
        .subscribe();

        this.route.url
        .pipe(map(path => this.path = path))
        .subscribe();
    }
}
