export interface LocalizedString {
    [langCode: string]: string
}
