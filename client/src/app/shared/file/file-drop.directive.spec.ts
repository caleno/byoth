import { of } from 'rxjs';
import { FileDropDirective } from './file-drop.directive';

describe('FileDropDirective', () => {
    let mockMessage;
    let mockService;
    let mockEvent;
    let directive: FileDropDirective;

    beforeEach(() => {
        mockMessage = {
            inform: (msg) => { },
            warn: (msg) => { },
            error: (msg) => {}
        };

        mockService = {
            postFile: (file) => of({})
        };

        mockEvent = {
            preventDefault: () => {},
            stopPropagation: () => {},
            dataTransfer: {}
        };

        directive = new FileDropDirective(mockMessage as any);
        expect(directive).toBeTruthy();
        directive.accepts = [ 'foo' ];
        directive.service = mockService as any;
        directive.ngOnInit();
    });

    function testDragOverColor(event, dragOverBg, dragOverColor) {
        expect(directive.background).toEqual(FileDropDirective.defaultBg);
        expect(directive.color).toEqual(FileDropDirective.defaultColor);

        directive.onDragOver(mockEvent);

        expect(directive.background).toEqual(dragOverBg);
        expect(directive.color).toEqual(dragOverColor);

        directive.onDragLeave(mockEvent);

        expect(directive.background).toEqual(FileDropDirective.defaultBg);
        expect(directive.color).toEqual(FileDropDirective.defaultColor);
    }

    it('should change to error colors on dragOver if files have wrong file type', () => {
        mockEvent.dataTransfer.items = [ { kind: 'file', type: 'bar' } ];

        testDragOverColor(mockEvent, FileDropDirective.errorBg, FileDropDirective.errorColor);

        mockEvent.dataTransfer.items = undefined;
        mockEvent.dataTransfer.files = [ { type: 'bar' } ];

        testDragOverColor(mockEvent, FileDropDirective.errorBg, FileDropDirective.errorColor);
    });

    it('should change to accept colors on dragOver if files have acceptable file type', () => {
        mockEvent.dataTransfer.items = [ { kind: 'file', type: 'foo' } ];

        testDragOverColor(mockEvent, FileDropDirective.acceptBg, FileDropDirective.acceptColor);

        mockEvent.dataTransfer.items = undefined;
        mockEvent.dataTransfer.files = [ { type: 'foo' } ];

        testDragOverColor(mockEvent, FileDropDirective.acceptBg, FileDropDirective.acceptColor);
    });

    it('should only upload dropped files of the correct mimetype', () => {
        const serviceSpy = spyOn(directive.service, 'postFile').and.callThrough();

        mockEvent.dataTransfer.files = [ { type: 'foo' }, { type: 'bar' }];

        directive.onDrop(mockEvent);
        expect(serviceSpy).toHaveBeenCalledTimes(1);
        serviceSpy.calls.reset();

        mockEvent.dataTransfer.files = [ { type: 'foo' }, { type: 'foo' } ]

        directive.onDrop(mockEvent);
        expect(serviceSpy).toHaveBeenCalledTimes(2);
    });
});
