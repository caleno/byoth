import { BehaviorSubject, concat } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { MessageService } from 'services/message.service';
import { FileService } from 'services/file.service';

export class FileUploader {
    uploadCount$ = new BehaviorSubject<number>(0);

    constructor(private accepts: string[],
                private service: FileService,
                private message: MessageService) { }

    upload(files: FileList) {
        const acceptedFiles: File[] = [];
        for (let i = 0; i < files.length; ++i) {
            const file = files[i];
            if (this.accept(file.type)) {
                acceptedFiles.push(file);
            }
        }

        this.uploadCount$.next(acceptedFiles.length);

        concat(...acceptedFiles.map(file => this.service.postFile(file)))
        .pipe(
            tap(() => this.uploadCount$.next(this.uploadCount$.value - 1)),
            finalize(() => {
                this.uploadCount$.next(0);
                this.message.inform({en: 'Files uploaded', nb: 'Filer lastet opp'})
            })
        )
        .subscribe();
    }

    accept(fileType: string) {
        return this.accepts.includes(fileType);
    }
}
