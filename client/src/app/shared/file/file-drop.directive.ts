import { Directive, HostListener, HostBinding, Input, OnInit } from '@angular/core';

import { MessageService } from 'services/message.service';
import { FileService } from 'services/file.service';

import { FileUploader } from './file-uploader';

@Directive({
    selector: '[appFileDrop]'
})
export class FileDropDirective implements OnInit {
    static defaultBg = '#f5f5f5';
    static defaultColor = 'black';

    static errorBg = '#e55343';
    static errorColor = 'white';

    static acceptBg = '#d5d5d5';
    static acceptColor = 'black';

    @HostBinding('style.background') background = FileDropDirective.defaultBg;
    @HostBinding('style.color') color = FileDropDirective.defaultColor;

    @Input()
    service: FileService;

    @Input()
    accepts: string[];

    private uploader: FileUploader;

    constructor(private message: MessageService) { }

    ngOnInit() {
        this.uploader = new FileUploader(this.accepts, this.service, this.message);
    }

    @HostListener('dragover', ['$event']) onDragOver(event) {
        event.preventDefault();
        event.stopPropagation();

        if (event.dataTransfer.items) {
            const items = <DataTransferItemList>event.dataTransfer.items;
            for (let i = 0; i < items.length; ++i) {
                if (items[i].kind === 'file') {
                    if (!this.uploader.accept(items[i].type)) {
                        this.background = FileDropDirective.errorBg;
                        this.color = FileDropDirective.errorColor;
                    } else {
                        this.background = FileDropDirective.acceptBg;
                        this.color = FileDropDirective.acceptColor;
                    }
                }
            }
        } else {
            const files = <FileList>event.dataTransfer.files;
            for (let i = 0; i < files.length; i++) {
                if (!this.uploader.accept(files[i].type)) {
                    this.background = FileDropDirective.errorBg;
                    this.color = FileDropDirective.errorColor;
                } else {
                    this.background = FileDropDirective.acceptBg;
                    this.color = FileDropDirective.acceptColor;
                }
            }
        }
    }

    @HostListener('dragleave', ['$event']) onDragLeave(event) {
        event.preventDefault();
        event.stopPropagation();

        this.background = FileDropDirective.defaultBg;
        this.color = FileDropDirective.defaultColor;
    }

    @HostListener('drop', ['$event']) onDrop(event) {
        event.preventDefault();
        event.stopPropagation();

        this.background = FileDropDirective.defaultBg;
        this.color = FileDropDirective.defaultColor;

        this.uploader.upload(<FileList>event.dataTransfer.files);
    }

    get upload$() {
        return this.uploader.uploadCount$;
    }
}
