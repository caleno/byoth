import { of } from 'rxjs';

import { FileSelectDirective } from './file-select.directive';

describe('FileSelectDirective', () => {
    let mockMessage;
    let mockService;

    beforeEach(() => {
        mockMessage = {
            inform: (msg) => { },
            warn: (msg) => { },
            error: (msg) => {}
        };

        mockService = {
            postFile: (file) => of({})
        };
    })

    it('should create an instance', () => {
        const directive = new FileSelectDirective(mockMessage as any, {} as any);
        expect(directive).toBeTruthy();
    });

    it('should only accept files of the correct mimetype', () => {
        const directive = new FileSelectDirective(mockMessage as any, {} as any);
        directive.accepts = [ 'foo' ];
        directive.service = mockService as any;
        const serviceSpy = spyOn(directive.service, 'postFile').and.callThrough();
        directive.ngOnInit();

        directive.onChanges([ { type: 'foo' }, { type: 'bar' } ]);
        expect(serviceSpy).toHaveBeenCalledTimes(1);
        serviceSpy.calls.reset();

        directive.onChanges([ { type: 'foo' }, { type: 'foo' } ]);
        expect(serviceSpy).toHaveBeenCalledTimes(2);
    });
});
