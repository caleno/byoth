import { Directive, HostListener, Input, OnInit, ElementRef } from '@angular/core';

import { MessageService } from 'services/message.service';
import { FileService } from 'services/file.service';

import { FileUploader } from './file-uploader';

@Directive({
    selector: '[appFileSelect]'
})
export class FileSelectDirective implements OnInit {
    @Input()
    service: FileService;

    @Input()
    accepts: string[];

    private uploader: FileUploader;

    constructor(private message: MessageService, private el: ElementRef) { }

    ngOnInit() {
        this.uploader = new FileUploader(this.accepts, this.service, this.message);
    }

    @HostListener('click', ['$event']) onClick() {
        this.el.nativeElement.value = '';
    }

    @HostListener('change', ['$event.target.files']) onChanges(files) {
        this.uploader.upload(files);
    }

    get upload$() {
        return this.uploader.uploadCount$;
    }
}
