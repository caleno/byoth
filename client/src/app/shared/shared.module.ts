import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MaterialModule } from '../material.module';

import { ErrorPageComponent } from './error/error-page.component';

import { FormStepperComponent } from './form/form-stepper.component';
import { FormStepWrapperComponent } from './form/form-step-wrapper.component';
import { FormStepperToolbarHostDirective} from './form/form-stepper-toolbar-host.directive';

import { ArticleModule } from './form/article/article.module';
import { ArticlesComponent } from './form/articles/articles.component';
import { CandidateComponent } from './form/candidate/candidate.component';
import { CoverComponent } from './form/cover/cover.component';
import { EventComponent } from './form/event/event.component';
import { EventDateComponent } from './form/event/event-date/event-date.component';
import { EventLocationComponent } from './form/event/event-location/event-location.component';
import { FilesComponent } from './form/files/files.component';
import { ThesisLicenseComponent, LicenseHelpDialogComponent } from './form/thesis-license/thesis-license.component';
import { MetadataComponent } from './form/metadata/metadata.component';
import { ThesisComponent } from './form/thesis/thesis.component';
import { ThesisAbstractComponent } from './form/thesis/thesis-abstract/thesis-abstract.component';

import { ConfirmPrintModule } from './form/confirm-print/confirm-print.module';
import { ConfirmDepositionModule } from './form/confirm-deposition/confirm-deposition.module';
import { FileModule } from './file/file.module';
import { OrderModule } from './form/order/order.module';

@NgModule({
    imports: [
        CommonModule, MaterialModule, ReactiveFormsModule, DragDropModule,
        ArticleModule, OrderModule, FileModule, ConfirmPrintModule,
        ConfirmDepositionModule
    ],
    declarations: [
        ArticlesComponent,
        CandidateComponent,
        CoverComponent,
        ThesisLicenseComponent,
        ErrorPageComponent,
        EventComponent,
        EventDateComponent,
        EventLocationComponent,
        FilesComponent,
        FormStepperComponent,
        FormStepWrapperComponent,
        FormStepperToolbarHostDirective,
        LicenseHelpDialogComponent,
        MetadataComponent,
        ThesisComponent,
        ThesisAbstractComponent
    ],
    exports: [
        ArticlesComponent,
        ArticleModule,
        CoverComponent,
        CandidateComponent,
        ThesisLicenseComponent,
        ErrorPageComponent,
        EventComponent,
        EventDateComponent,
        EventLocationComponent,
        FilesComponent,
        FormStepperComponent,
        MetadataComponent,
        ThesisComponent,
    ],
    providers: [ FormBuilder ],
    entryComponents: [ LicenseHelpDialogComponent ]
})
export class SharedModule { }
