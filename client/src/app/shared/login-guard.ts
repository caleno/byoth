import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { CanActivate } from '@angular/router';
import { of } from 'rxjs';
import { mergeMap} from 'rxjs/operators';

import { AuthService } from 'services/auth.service';

@Injectable()
export class LoginGuard implements CanActivate {
    constructor(private auth: AuthService, @Inject(LOCALE_ID) private locale) { }

    canActivate(next) {
        return this.auth.currentUser()
        .pipe(
            mergeMap(user => {
                if (user) {
                    return of(true);
                } else {
                    this.auth.login(this.locale, next.url.join('/'));
                }
            })
        );
    }
}
