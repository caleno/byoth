import { pipe } from 'rxjs';
import { delay, tap, take, retryWhen } from 'rxjs/operators';

/** Pipe requests to this if they can potentially have 423 errors. */
export const retryOnLock = (times = 3, delayTime = 1000) =>
    pipe(
        retryWhen(errors =>
            errors.pipe(
                delay(delayTime),
                tap(error => {
                    if (error.status !== 423) {
                        throw error;
                    }
                }),
                take(times)
            )
        )
    );
