import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { interval, combineLatest } from 'rxjs';
import { takeWhile, mergeMap, tap, finalize } from 'rxjs/operators';

import { ByothFile, FileStatus } from 'model/byoth-file';
import { OrderTypes } from 'model/order';
import { Thesis } from 'model/thesis';

import { ThesisContainerService } from 'services/thesis-container.service';
import { MessageService } from 'services/message.service';
import { BaseFormComponent } from 'shared/form/form-component';
import { FormHandler } from 'shared/form/form-handler';
import { FileDropDirective } from 'shared/file/file-drop.directive';
import { FileSelectDirective } from 'shared/file/file-select.directive';


@Component({
    selector: 'app-files',
    templateUrl: './files.component.html',
    styleUrls: ['./files.component.scss']
})
export class FilesComponent extends BaseFormComponent implements OnInit, AfterViewInit {
    files: ByothFile[] = [];
    fileOk = FileStatus.OK;
    fileWarning = FileStatus.WARNING;
    fileError = FileStatus.ERROR;
    emptyFiles = [];
    contentPdfUrl = '';
    thesis: Thesis;

    options: { orderType: OrderTypes }

    form: FormGroup;

    validating = false;
    reordering = false;

    @ViewChild(FileDropDirective) filedrop: FileDropDirective;
    @ViewChild(FileSelectDirective) fileselect: FileSelectDirective;

    constructor(private container: ThesisContainerService,
                private message: MessageService,
                private fb: FormBuilder) {
        super();

        this.form = this.fb.group({
            ok: [false, Validators.requiredTrue]
        });

        this.addFormHandler(new FormHandler(this.form, null));
    }

    ngOnInit() {
        // this is a developer error
        if (!this.options || !this.options.orderType) {
            throw Error('Files component initialized without orderType');
        }

        this.addSubscription(
            this.container.inputFiles.getItemServices()
            .subscribe(services => {
                this.files = services.map(service => service.value);

                if (!services.length || this.hasUnsubmittedFiles() || this.numErrors()) {
                    this.form.get('ok').setValue(false);
                } else {
                    this.form.get('ok').setValue(true);
                }
            })
        );

        this.container.contentPdfPreviewDownloadURL(this.options.orderType, false).subscribe(url => this.contentPdfUrl = url);

        this.addSubscription(this.container.thesis.valueChanges.subscribe(thesis => this.thesis = thesis));
    }

    ngAfterViewInit() {
        this.addSubscription(
            combineLatest([this.filedrop.upload$, this.fileselect.upload$])
            .subscribe(counts => this.emptyFiles = new Array(counts[0] + counts[1]))
        );
    }

    dropFile(event: CdkDragDrop<ByothFile>) {
        if (event.previousIndex === event.currentIndex) {
            return;
        }

        this.reordering = true;
        moveItemInArray(this.files, event.previousIndex, event.currentIndex);
        const newOrder = this.files.map(file => file['@id']);

        this.container.inputFiles.unsubmitFiles()
            .pipe(
                mergeMap(() => this.container.inputFiles.reorder(newOrder)),
                mergeMap(() => this.container.inputFiles.refresh()),
                finalize(() => this.reordering = false)
            )
            .subscribe();
    }

    get fileService() {
        return this.container.inputFiles;
    }

    deleteFile(file: ByothFile) {
        this.container.inputFiles.unsubmitFiles()
        .pipe(mergeMap(() => this.container.inputFiles.delete(file)))
        .subscribe(
            () => {
                this.message.inform({en: `File ${file.filename} deleted`, nb: `Filen ${file.filename} slettet`});
            }
        );
    }

    numPages(): number {
        return this.files
        .reduce((prev, curr) => prev + (curr.numPages || 0), 0);
    }

    numErrors(): number {
        return this.files
        .reduce((prev, curr) => prev + ((curr.status === FileStatus.ERROR) ? 1 : 0), 0);
    }

    numWarnings(): number {
        return this.files
        .reduce((prev, curr) => prev + ((curr.status === FileStatus.WARNING) ? 1 : 0), 0);
    }

    replaceFileService(file) {
        return this.container.inputFiles.replaceFileService(file['@id']);
    }

    submitFiles() {
        this.validating = true;
        this.container.inputFiles.unsubmitFiles()
        .pipe(
            mergeMap(() => this.container.inputFiles.submitFiles(this.options.orderType)),
            mergeMap(() => this.container.inputFiles.refresh()),
            mergeMap(() => this.container.thesis.refresh()), // update page count
            finalize(() => this.validating = false)
        )
        .subscribe();
    }

    activate() {
        if (!this.validating) {
            interval(1000)
            .pipe(
                mergeMap(() => this.container.inputFiles.isSubmitting()),
                tap(value => this.validating = value),
                takeWhile(value => value),
            )
            .subscribe(null, null, () => this.container.inputFiles.refresh().subscribe());
        }
    }

    deactivate() {
        if (this.hasUnsubmittedFiles() && !this.validating) {
            this.submitFiles();
            interval(1000).pipe(takeWhile(() => this.validating))
            .subscribe(
                () => this.message.inform({en: 'Validating files ...', nb: 'Validerer filer ...'}, 0),
                () => {},
                () => this.message.inform({en: 'Finished validation', nb: 'Filvalidering ferdig'}, 2000)
            );
        }
    }

    fileUrl(fileId: string) {
        return this.container.fileUrl(fileId);
    }

    hasUnsubmittedFiles() {
        return this.files.length > 0 && this.files.some(file => file.status === undefined || file.status === null);
    }

    fileCss(file: ByothFile) {
        if (!file) {
            return '';
        }

        switch (file.status) {
            case FileStatus.OK: return 'file-green';
            case FileStatus.WARNING: return 'file-yellow';
            case FileStatus.ERROR: return 'file-red';
            default: return 'file-none';
        }
    }
}
