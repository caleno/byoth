import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { MaterialModule } from 'app/material.module';
import { ThesisContainerService } from 'services/thesis-container.service';
import { NonpersistentDocumentSetService, NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { MessageService } from 'services/message.service';

import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { ByothFile, FileStatus } from 'model/byoth-file';
import { OrderTypes } from 'model/order';

import { FileDropDirective } from 'shared/file/file-drop.directive';
import { FileSelectDirective } from 'shared/file/file-select.directive';
import { FilesComponent } from './files.component';

describe('FilesComponent', () => {
    let component: FilesComponent;
    let fixture: ComponentFixture<FilesComponent>;
    let service: NonpersistentDocumentSetService<ByothFile>;

    class MockContainer {
        inputFiles = new NonpersistentDocumentSetService();
        thesis = new NonpersistentDocumentService();
        fileUrl = () => of('foo');
        contentPdfPreviewDownloadURL = () => of('bar');
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ FilesComponent, FileDropDirective, FileSelectDirective ],
            imports: [ MaterialModule, ReactiveFormsModule, NoopAnimationsModule, DragDropModule ],
            providers: [
                { provide: ThesisContainerService, useClass: MockContainer },
                MessageService ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FilesComponent);
        component = fixture.componentInstance;
        component.options = { orderType: OrderTypes.FINAL };
        service = component['container'].inputFiles as any;
        (<any>service).replaceFileService = () => { return { postFile: () => of({}) } };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should show same number of files as service provides', fakeAsync(() => {
        component['container'].inputFiles.post().subscribe();
        fixture.detectChanges();
        tick();

        const files = fixture.nativeElement.querySelectorAll('.file-item');
        expect(files.length).toEqual(1);
    }));

    it('should reorder files on drop event if order changed', fakeAsync(() => {
        component.files = [ { '@id': 'foo' }, { '@id': 'bar' } ] as any;
        const spy = spyOn(service, 'reorder').and.callThrough();
        (<any>service).unsubmitFiles = () => of(null);

        component.dropFile({ previousIndex: 0, currentIndex: 1} as any);
        tick();

        expect(spy).toHaveBeenCalledTimes(1);
    }));

    it('should not reorder files on drop event if nothing changed', fakeAsync(() => {
        component.files = [ { '@id': 'foo' }, { '@id': 'bar' } ] as any;
        const spy = spyOn(service, 'reorder').and.callThrough();
        component.dropFile({ previousIndex: 0, currentIndex: 0} as any)
        tick();

        expect(spy).toHaveBeenCalledTimes(0);
    }));

    it('should send info message when file is deleted', fakeAsync(() => {
        (<any>service).unsubmitFiles = () => of(null);
        spyOn(component['message'], 'inform').and.callFake(() => {});
        service.post().subscribe();
        tick();
        component.deleteFile(component.files[0]);
        tick();
        expect(component['message'].inform).toHaveBeenCalled();
    }));

    it('should count total number of pages', () => {
        component.files = [{numPages: 3 }, {numPages: 4 }] as any[];
        expect(component.numPages()).toEqual(7);
    });

    it('should count total number of errors and warnings', () => {
        component.files = [{status: FileStatus.WARNING}, {status: FileStatus.ERROR}] as any[];
        expect(component.numWarnings()).toEqual(1);
        expect(component.numErrors()).toEqual(1);
    });

    it('should check if already validating upon activation and set value of .validating accordingly', fakeAsync(() => {
        let calls = 0;
        (<any>service).submitFiles = () => of(null).pipe(delay(1));
        (<any>service).unsubmitFiles = () => of(null);
        (<any>service).isSubmitting = () => of(true);
        spyOn(<any>service, 'isSubmitting').and.callFake(() => {
            if (calls++) {
                return of(false);
            }
            return of(true);
        });

        expect(component.validating).toBeFalsy();
        component.activate();
        tick(1001)
        expect(component.validating).toBeTruthy();
        tick(1000);
        expect(component.validating).toBeFalsy();
    }));

    describe('submitFiles()', () => {
        it('should set validating=true, reset file validation on server and then send validate request', fakeAsync(() => {
            // add delay so we can catch if the spinner is showing
            (<any>service).submitFiles = () => of(null).pipe(delay(1));
            (<any>service).unsubmitFiles = () => of(null);
            spyOn(<any>service, 'submitFiles').and.callThrough();
            spyOn(<any>service, 'unsubmitFiles').and.callThrough();

            expect(component.validating).toBeFalsy();
            component.submitFiles();
            expect(component.validating).toBeTruthy();

            tick(2);
            expect(component.validating).toBeFalsy();
            expect(service['unsubmitFiles']).toHaveBeenCalled();
            expect(service['submitFiles']).toHaveBeenCalled();
        }));
    });
});
