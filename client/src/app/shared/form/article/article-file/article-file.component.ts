import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

import { of } from 'rxjs';
import { mergeMap, map, distinctUntilChanged } from 'rxjs/operators';

import { FormHandler } from 'shared/form/form-handler';
import { BaseFormComponent } from 'shared/form/form-component';
import { DocumentService } from 'services/document.service';
import { ThesisContainerService } from 'services/thesis-container.service';
import { Article, getArticleFileId, DO_NOT_PUBLISH_ARTICLE } from 'model/article';
import { ByothFile } from 'model/byoth-file';

@Component({
    selector: 'app-article-file',
    templateUrl: './article-file.component.html',
    styleUrls: ['./article-file.component.css']
})
export class ArticleFileComponent extends BaseFormComponent implements OnInit {
    @Input()
    service: DocumentService<Article>;

    form: FormGroup;
    fileSourceForm: FormControl;
    inputFiles: ByothFile[];
    filename: string;

    private article: Article;

    constructor(private fb: FormBuilder,
                private container: ThesisContainerService) {
        super();

        this.form = this.fb.group({
            inFile: [null],
            hasFile: [null]
        });

        this.fileSourceForm = this.fb.control(null);

        this.form.setValidators(control => {
            const file = control.value.inFile || control.value.hasFile;

            if (!file) {
                return { file: 'missing' };
            }

            return null;
        });
    }

    ngOnInit() {
        this.addFormHandler(new FormHandler(this.form, this.service));

        this.article = this.service.value;
        this.addSubscription(this.service.valueChanges.subscribe(article => this.article = article));

        this.addSubscription(
            this.container.inputFiles.valueChanges.subscribe(files => {
                this.inputFiles = files && files.items;
            })
        );

        this.addSubscription(
            this.service.valueChanges
            .pipe(distinctUntilChanged((x, y) => x.publish === y.publish))
            .subscribe(article => {
                if (article.publish === DO_NOT_PUBLISH_ARTICLE) {
                    this.deleteFile();
                }
            })
        )

        this.addSubscription(
            this.service.valueChanges
            .pipe(
                map(article => getArticleFileId(article)),
                distinctUntilChanged(),
                mergeMap(fileId => {
                    if (fileId) {
                        return this.container.fileMetadata(fileId);
                    } else {
                        return of({} as any);
                    }
                }),
                map(file => file.filename)
            )
            .subscribe(filename => this.filename = filename)
        )
    }

    get uploadFile(): boolean {
        return this.fileSourceForm.value;
    }

    get fileService() {
        return this.container.articles.getArticleFileService(this.article['@id']);
    }

    get fileId(): string {
        return getArticleFileId(this.article);
    }

    deleteFile() {
        this.container.articles.deleteArticleFile(this.article['@id']).subscribe();

        // the default form handler doesn't handle undefined fields well, this is a bit of a hack.
        this.form.get('inFile').setValue(null, {emitEvent: false});
    }
}
