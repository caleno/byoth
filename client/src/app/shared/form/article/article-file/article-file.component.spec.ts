import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ThesisContainerService } from 'services/thesis-container.service';

import { NonpersistentDocumentSetService } from 'services/nonpersistent-document.service';

import { MaterialModule } from 'app/material.module';
import { FileModule } from 'shared/file/file.module';
import { ArticleFileComponent } from './article-file.component';

describe('ArticleFileComponent', () => {
    let component: ArticleFileComponent;
    let fixture: ComponentFixture<ArticleFileComponent>;

    class MockContainer {
        inputFiles = new NonpersistentDocumentSetService();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, MaterialModule, FileModule ],
            declarations: [ ArticleFileComponent ],
            providers: [
                { provide: ThesisContainerService, useClass: MockContainer }
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArticleFileComponent);
        component = fixture.componentInstance;
        component.service = new NonpersistentDocumentSetService();

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
