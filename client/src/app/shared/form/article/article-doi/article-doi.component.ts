import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, ValidationErrors, AbstractControl } from '@angular/forms';
import * as doiRegex from 'doi-regex';

import { FormHandler } from 'shared/form/form-handler';
import { BaseFormComponent } from 'shared/form/form-component';
import { DocumentService } from 'services/document.service';
import { Article } from 'model/article';

import { CrossrefService } from 'services/crossref.service';

function validateDoi(ctrl: AbstractControl): ValidationErrors | null {
    if (ctrl.value && !ctrl.value.match(doiRegex())) {
        return { invalidDoi: true };
    }

    return null;
}

/** Component for fetching an article reference based on its DOI */
@Component({
    selector: 'app-article-doi',
    templateUrl: './article-doi.component.html',
    styleUrls: ['./article-doi.component.css']
})
export class ArticleDoiComponent extends BaseFormComponent implements OnInit {
    /** Read only, need the ID to get proper sub-property id's in new article */
    @Input()
    service: DocumentService<Article>;

    /** */
    @Output()
    gotReference: EventEmitter<Article> = new EventEmitter();

    fetchingDoi = false;
    doiSuccess = false;
    doiError = false;

    form: FormGroup;

    constructor(private crossref: CrossrefService, private fb: FormBuilder) {
        super();
        this.form = this.fb.group({ doi: [null, validateDoi]});
    }

    ngOnInit() {
        this.addFormHandler(new FormHandler(this.form, this.service));
    }

    fetchReference(): void {
        const doi = this.form.get('doi').value;

        this.fetchingDoi = true;
        this.doiSuccess = false;
        this.doiError = false;

        this.crossref.getPublication(doi, this.service.value['@id'])
        .subscribe(
            article => {
                this.fetchingDoi = false
                this.gotReference.emit(article);
                this.doiSuccess = true;
            },
            err => {
                this.fetchingDoi = false
                this.doiError = true;
            }
        );
    }
}
