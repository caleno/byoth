import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { of } from 'rxjs';

import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { CrossrefService } from 'services/crossref.service';
import { ArticleDoiComponent } from './article-doi.component';

describe('ArticleDoiComponent', () => {
    let component: ArticleDoiComponent;
    let fixture: ComponentFixture<ArticleDoiComponent>;

    class MockCrossRef {
        getPublication() { return of({}) }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, MaterialModule, NoopAnimationsModule ],
            declarations: [ ArticleDoiComponent ],
            providers: [ { provide: CrossrefService, useClass: MockCrossRef } ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArticleDoiComponent);
        component = fixture.componentInstance;
        component.service = new NonpersistentDocumentService() as any;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
