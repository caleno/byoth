import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { ArticleReferenceComponent } from './article-reference.component';

describe('ArticleReferenceComponent', () => {
    let component: ArticleReferenceComponent;
    let fixture: ComponentFixture<ArticleReferenceComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, MaterialModule, NoopAnimationsModule ],
            declarations: [ ArticleReferenceComponent ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArticleReferenceComponent);
        component = fixture.componentInstance;
        component.service = new NonpersistentDocumentService as any;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
