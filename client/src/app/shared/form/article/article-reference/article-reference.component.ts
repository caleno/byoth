import { Component, OnInit, ViewChild, Input, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { FormHandler } from 'shared/form/form-handler';
import { DocumentService } from 'services/document.service';
import { Article, isUnpublished } from 'model/article';
import { ArticleAuthorsComponent } from '../article-authors/article-authors.component';
import { ArticleContainerComponent } from '../article-container/article-container.component';

import { BaseFormComponent } from '../../form-component';

/** Component to display and edit an article reference */
@Component({
    selector: 'app-article-reference',
    templateUrl: './article-reference.component.html',
    styleUrls: ['./article-reference.component.css']
})
export class ArticleReferenceComponent extends BaseFormComponent implements OnInit, AfterViewInit {
    @Input()
    service: DocumentService<Article>;

    @ViewChild(ArticleAuthorsComponent)
    authorsComponent: ArticleAuthorsComponent;

    @ViewChild(ArticleContainerComponent)
    containerComponent: ArticleContainerComponent;

    form: FormGroup;

    constructor(private fb: FormBuilder) {
        super();

        this.form = this.fb.group({
            title: [null, Validators.required],
            abstract: [null],
            publicationYear: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(4)]],
            volume: [null],
            issue: [null],
            pageStart: [null],
            pageEnd: [null],
            number: [null]
        });
    }

    ngOnInit() {
        this.addFormHandler(new FormHandler(this.form, this.service));
    }

    ngAfterViewInit() {
        this.children = [ this.authorsComponent, this.containerComponent ];
    }

    get isUnpublished(): boolean {
        return isUnpublished(this.service.value);
    }

    setArticle(article: Article) {
        this.service.setValue(article);
    }
}
