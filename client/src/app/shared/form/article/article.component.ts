import { Component, OnInit, AfterViewInit, Input, Output, ViewChild, EventEmitter, DoCheck } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MatExpansionPanel } from '@angular/material/expansion';

import { MessageService } from 'services/message.service';
import { ThesisContainerService } from 'services/thesis-container.service';
import { DocumentService } from 'services/document.service';

import { BaseFormComponent } from 'shared/form/form-component';
import { ArticleReferenceComponent } from './article-reference/article-reference.component';
import { ArticleLicenseComponent } from './article-license/article-license.component';
import { ArticleFileComponent } from './article-file/article-file.component';
import { ArticleTypeComponent } from './article-type/article-type.component';

import { Article, addArticleAuthor, DO_NOT_PUBLISH_ARTICLE} from 'model/article';

/**
 * Component for Article form
 *
 * Needs to update form both from user input and results from crossref.
 * Since the article model has some complexity, we generate a new form from scratch
 * when new data is received from Crossref.
 */
@Component({
    selector: 'app-article',
    templateUrl: './article.component.html',
    styleUrls: ['./article.component.scss']
})
export class ArticleComponent extends BaseFormComponent implements OnInit, AfterViewInit, DoCheck {
    @Output() articleDeleted = new EventEmitter<Article>();

    @ViewChild(ArticleTypeComponent)
    typeComponent: ArticleTypeComponent;

    @ViewChild(ArticleReferenceComponent)
    referenceComponent: ArticleReferenceComponent;

    @ViewChild(ArticleLicenseComponent)
    licenseComponent: ArticleLicenseComponent;

    @ViewChild(ArticleFileComponent)
    fileComponent: ArticleFileComponent;

    @ViewChild(MatExpansionPanel)
    expansionPanel: MatExpansionPanel;

    @Input() service: DocumentService<Article>;

    article: Article;

    isOpen = false;

    invalid = false;

    constructor(private container: ThesisContainerService,
                private message: MessageService) {
        super();
    }

    ngOnInit(): void {
        this.article = this.service.value;
        this.addSubscription(this.service.valueChanges.subscribe(article => this.article = article));

        if (!this.article.authors || this.article.authors.length === 0) {
            this.container.candidate.get()
            .subscribe(candidate => {
                const article = this.service.value;
                addArticleAuthor(article, candidate.givenName + ' ' + candidate.familyName);
                this.service.setValue(article);
            });
        }
    }

    ngAfterViewInit() {
        this.children = [ this.typeComponent, this.referenceComponent, this.licenseComponent, this.fileComponent ];
        setTimeout(() => this.invalid = this.formControl.invalid, 0);
    }

    ngDoCheck() {
        this.invalid = this.formControl.invalid;
    }

    setArticle(article: Article) {
        this.service.setValue(article);
    }

    get canBeUploaded(): boolean {
        return this.article.publish !== DO_NOT_PUBLISH_ARTICLE;
    }

    open(): void {
        this.isOpen = true;
    }

    close(): void {
        this.isOpen = false;
    }

    save(): Observable<any> {
        return this.service.save()
        .pipe(tap(() => this.close()));
    }

    saveLocal() {
        this.save()
        .subscribe(couldSave => {
            if (couldSave) {
                this.message.inform({ en: `Saved '${this.article.title}'`, nb: `Lagret '${this.article.title}'` });
            } else {
                this.message.warn({ en: 'Invalid data', nb: 'Ugyldige verdier' });
            }
        });
    }

    delete(): void {
        this.expansionPanel.close();
        this.articleDeleted.emit(this.article);
    }
}

