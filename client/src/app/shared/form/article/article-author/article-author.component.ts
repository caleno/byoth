import { Component, OnInit, Input } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import { BaseFormComponent } from 'shared/form/form-component';
import { Author } from 'model/article';

/** Component to edit an article author's name */
@Component({
    selector: 'app-article-author',
    templateUrl: './article-author.component.html',
    styleUrls: ['./article-author.component.css']
})
export class ArticleAuthorComponent extends BaseFormComponent implements OnInit {
    @Input()
    subject: BehaviorSubject<Author>;

    form: FormControl;

    constructor() {
        super();
        this.form = new FormControl('', Validators.required);
    }

    ngOnInit() {
        this.form.setValue(this.subject.value.name);
        this.addSubscription(
            this.subject
            .pipe(distinctUntilChanged())
            .subscribe(author => {
                this.form.setValue(author.name);
            })
        )

        this.addSubscription(
            this.form.valueChanges
            .pipe(distinctUntilChanged())
            .subscribe(name => {
                const author = this.subject.value;
                author.name = name;
                this.subject.next(author);
            })
        )
    }

    hasErrors() {
        return this.form.invalid;
    }
}
