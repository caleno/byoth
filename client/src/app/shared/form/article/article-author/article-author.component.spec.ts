import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BehaviorSubject } from 'rxjs';

import { MaterialModule } from 'app/material.module';

import { ArticleAuthorComponent } from './article-author.component';

describe('ArticleAuthorComponent', () => {
    let component: ArticleAuthorComponent;
    let fixture: ComponentFixture<ArticleAuthorComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, MaterialModule, NoopAnimationsModule ],
            declarations: [ ArticleAuthorComponent ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArticleAuthorComponent);
        component = fixture.componentInstance;
        component.subject = new BehaviorSubject({name: 'foo'} as any);
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
