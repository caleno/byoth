import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { ArticleContainerComponent } from './article-container.component';

describe('ArticleContainerComponent', () => {
    let component: ArticleContainerComponent;
    let fixture: ComponentFixture<ArticleContainerComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, MaterialModule, NoopAnimationsModule ],
            declarations: [ ArticleContainerComponent ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArticleContainerComponent);
        component = fixture.componentInstance;
        component.service = new NonpersistentDocumentService() as any;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
