import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { distinctUntilChanged } from 'rxjs/operators';

import { DocumentService } from 'services/document.service';
import { Article, setArticleBook, setArticleSeries, isJournalArticle, isAnthologyChapter, isProceedingsArticle } from 'model/article';
import { BaseFormComponent } from 'shared/form/form-component';

/** Represents an article container, such as a Journal or Book */
@Component({
    selector: 'app-article-container',
    templateUrl: './article-container.component.html',
    styleUrls: ['./article-container.component.css']
})
export class ArticleContainerComponent extends BaseFormComponent implements OnInit {
    @Input()
    service: DocumentService<Article>;

    form: FormControl;

    ngOnInit() {
        this.form = new FormControl('');
        this.addSubscription(
            this.service.valueChanges
            .pipe(distinctUntilChanged((a, b) => (a.book === b.book && a.series === b.series)))
            .subscribe(article => {
                if (isAnthologyChapter(article) && article.book) {
                    this.form.setValue(article.book.title);
                } else if ((isJournalArticle(article) ||  isProceedingsArticle(article)) && article.series) {
                    this.form.setValue(article.series.title);
                }
            })
        );

        this.addSubscription(
            this.form.valueChanges
            .subscribe(value => {
                const article = this.service.value;
                if (this.inBook) {
                    setArticleBook(article, this.form.value);
                    article.series = null;
                } else if (this.inSeries) {
                    setArticleSeries(article, this.form.value);
                    article.book = null;
                }
                this.service.setValue(article);
            })
        );
    }

    getType() {
        if (this.inBook) {
            return 'Book';
        } else if (this.inJournal) {
            return 'Journal';
        } else if (this.inProceedings) {
            return 'Proceedings';
        }
    }

    get inBook(): boolean {
        return isAnthologyChapter(this.service.value);
    }

    get inJournal(): boolean {
        return isJournalArticle(this.service.value);
    }

    get inProceedings(): boolean {
        return isProceedingsArticle(this.service.value)
    }

    get inSeries(): boolean {
        return this.inJournal || this.inProceedings;
    }
}
