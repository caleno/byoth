import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Article,
         MANUSCRIPT, JOURNAL_ARTICLE, PROCEEDINGS_ARTICLE, BOOK_CHAPTER, UNPUBLISHED_ARTICLE,
         PREPRINT, POSTPRINT, PUBLISHERS_VERSION} from 'model/article';

import { DocumentService } from 'services/document.service';
import { FormHandler } from 'shared/form/form-handler';
import { BaseFormComponent } from 'shared/form/form-component';

@Component({
    selector: 'app-article-type',
    templateUrl: './article-type.component.html',
    styleUrls: ['./article-type.component.css']
})
export class ArticleTypeComponent extends BaseFormComponent implements OnInit {
    form: FormGroup;

    unpublished = UNPUBLISHED_ARTICLE;
    journalArticle = JOURNAL_ARTICLE;
    proceedingsArticle = PROCEEDINGS_ARTICLE;
    bookChapter = BOOK_CHAPTER;

    manuscript = MANUSCRIPT;
    preprint = PREPRINT;
    postprint = POSTPRINT;
    publishersVersion = PUBLISHERS_VERSION;

    isPublished = true;

    @Input()
    service: DocumentService<Article>;

    constructor(private fb: FormBuilder) {
        super();
        this.form = this.fb.group({
            articleType: [null, Validators.required],
            version: [null, Validators.required]
        });
    }

    ngOnInit() {
        this.addFormHandler(new FormHandler(this.form, this.service));

        this.addSubscription(
            this.form.get('articleType').valueChanges
            .subscribe(type => {
                const versionCtrl = this.form.get('version');
                if (type === UNPUBLISHED_ARTICLE) {
                    versionCtrl.setValue(MANUSCRIPT)
                    this.isPublished = false;
                } else {
                    this.isPublished = true;

                    if (versionCtrl.value === MANUSCRIPT) {
                        versionCtrl.setValue(PREPRINT);
                    }
                }
            })
        );
    }
}
