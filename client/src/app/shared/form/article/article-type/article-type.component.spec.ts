import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { ArticleTypeComponent } from './article-type.component';

describe('ArticleTypeComponent', () => {
    let component: ArticleTypeComponent;
    let fixture: ComponentFixture<ArticleTypeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, MaterialModule, NoopAnimationsModule ],
            declarations: [ ArticleTypeComponent ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArticleTypeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
