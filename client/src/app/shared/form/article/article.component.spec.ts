import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';

import { ModelService } from 'services/model.service';
import { MessageService } from 'services/message.service';
import { ThesisContainerService } from 'services/thesis-container.service';
import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';

import { ArticleComponent } from './article.component';

describe('ArticleComponent', () => {
    let component: ArticleComponent;
    let fixture: ComponentFixture<ArticleComponent>;

    class MockContainer {
        candidate = new NonpersistentDocumentService();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, NoopAnimationsModule, MaterialModule ],
            declarations: [ ArticleComponent ],
            providers: [
                { provide: ThesisContainerService, useClass: MockContainer },
                ModelService, MessageService ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArticleComponent);
        component = fixture.componentInstance;
        component.service = new NonpersistentDocumentService() as any;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
