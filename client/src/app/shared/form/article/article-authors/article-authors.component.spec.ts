import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'app/material.module';

import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { ArticleAuthorsComponent } from './article-authors.component';

describe('ArticleAuthorsComponent', () => {
    let component: ArticleAuthorsComponent;
    let fixture: ComponentFixture<ArticleAuthorsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ MaterialModule ],
            declarations: [ ArticleAuthorsComponent ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArticleAuthorsComponent);
        component = fixture.componentInstance;
        component.service = new NonpersistentDocumentService() as any;
        component.service.setValue({authors: []} as any)
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
