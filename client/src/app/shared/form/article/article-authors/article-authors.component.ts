import { Component, OnInit, Input, ViewChildren, QueryList, AfterViewInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { DocumentService } from 'services/document.service';
import { BaseFormComponent } from 'shared/form/form-component';
import { Article, Author, addArticleAuthor, deleteArticleAuthor } from 'model/article';

import { CrossrefService } from 'services/crossref.service';

import { ArticleAuthorComponent } from '../article-author/article-author.component';

/** Component to handle a list of article authors */
@Component({
    selector: 'app-article-authors',
    templateUrl: './article-authors.component.html',
    styleUrls: ['./article-authors.component.css']
})
export class ArticleAuthorsComponent extends BaseFormComponent implements OnInit, AfterViewInit {
    @Input()
    service: DocumentService<Article>;

    authorSubjects: BehaviorSubject<Author>[] = [];

    maxNumberAuthors = CrossrefService.MAX_AUTHORS;

    @ViewChildren(ArticleAuthorComponent)
    articleAuthorComponents: QueryList<ArticleAuthorComponent>;

    constructor() {
        super();
    }

    ngOnInit() {
        this.authorSubjects = this.service.value.authors.map(author => new BehaviorSubject<Author>(author));

        this.addSubscription(
            this.service.valueChanges
            .subscribe(article => {
                this.authorSubjects = article.authors.map(author => new BehaviorSubject<Author>(author));
            })
        )
    }

    ngAfterViewInit() {
        this.articleAuthorComponents.changes
        .subscribe(() => {
            this.children = this.articleAuthorComponents.toArray();
        });
    }

    addAuthor(name = ''): void {
        const article = this.service.value;
        addArticleAuthor(article, name);
        this.service.setValue(article);
    }

    removeAuthor(author: BehaviorSubject<Author>): void {
        const article = this.service.value;
        deleteArticleAuthor(article, author.value);
        this.service.setValue(article);
    }
}
