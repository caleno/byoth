import { Component, OnInit, Input} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { FormHandler } from 'shared/form/form-handler';
import { BaseFormComponent } from 'shared/form/form-component';
import { DocumentService } from 'services/document.service';
import { Article, isUnpublished,
    PUBLISH_ARTICLE_IF_POSSIBLE, PUBLISH_MANUSCRIPT_WITH_EMBARGO,
    PUBLISH_MANUSCRIPT_WITHOUT_EMBARGO, DO_NOT_PUBLISH_ARTICLE } from 'model/article';

@Component({
    selector: 'app-article-license',
    templateUrl: './article-license.component.html',
    styleUrls: ['./article-license.component.css']
})
export class ArticleLicenseComponent extends BaseFormComponent implements OnInit {
    @Input()
    service: DocumentService<Article>;

    form: FormGroup;

    ifPossible = PUBLISH_ARTICLE_IF_POSSIBLE;
    doNotPublish = DO_NOT_PUBLISH_ARTICLE;
    withEmbargo = PUBLISH_MANUSCRIPT_WITH_EMBARGO;
    withoutEmbargo = PUBLISH_MANUSCRIPT_WITHOUT_EMBARGO;

    constructor(private fb: FormBuilder) {
        super();

        this.form = this.fb.group({
            publish: [null, Validators.required],
            embargo: [0 ]
        });
    }

    ngOnInit() {
        this.addFormHandler(new FormHandler(this.form, this.service));
    }

    get isPublished() {
        return !isUnpublished(this.service.value);
    }

    get canHaveEmbargo() {
        return this.form.get('publish').value === PUBLISH_MANUSCRIPT_WITH_EMBARGO;
    }

    hasError(): boolean {
        return this.form.invalid && this.form.touched;
    }
}
