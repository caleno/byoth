import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { ArticleLicenseComponent } from './article-license.component';

describe('ArticleLicenseComponent', () => {
    let component: ArticleLicenseComponent;
    let fixture: ComponentFixture<ArticleLicenseComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, MaterialModule, NoopAnimationsModule ],
            declarations: [ ArticleLicenseComponent ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArticleLicenseComponent);
        component = fixture.componentInstance;
        component.service = new NonpersistentDocumentService() as any;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
