import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';

import { ArticleComponent } from './article.component';
import { ArticleLicenseComponent } from './article-license/article-license.component';
import { ArticleAuthorComponent } from './article-author/article-author.component';
import { ArticleAuthorsComponent } from './article-authors/article-authors.component';
import { ArticleDoiComponent } from './article-doi/article-doi.component';
import { ArticleReferenceComponent } from './article-reference/article-reference.component';
import { ArticleContainerComponent } from './article-container/article-container.component';
import { ArticleFileComponent } from './article-file/article-file.component';

import { FileModule } from 'shared/file/file.module';
import { ArticleTypeComponent } from './article-type/article-type.component';

@NgModule({
    imports: [
        CommonModule, MaterialModule, ReactiveFormsModule, FileModule
    ],
    declarations: [
        ArticleComponent,
        ArticleLicenseComponent,
        ArticleAuthorComponent,
        ArticleAuthorsComponent,
        ArticleDoiComponent,
        ArticleReferenceComponent,
        ArticleContainerComponent,
        ArticleFileComponent,
        ArticleTypeComponent
    ],
    exports: [
        ArticleComponent
    ]
})
export class ArticleModule { }
