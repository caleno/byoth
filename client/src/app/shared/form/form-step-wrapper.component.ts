import { Component, Input, OnDestroy, AfterViewInit, AfterViewChecked, OnChanges,
         ViewChild, ComponentRef, ViewContainerRef,
         ComponentFactoryResolver, ChangeDetectorRef } from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { Observable, BehaviorSubject } from 'rxjs';

import { BaseFormComponent } from './form-component';
import { FormStepperToolbarHostDirective } from './form-stepper-toolbar-host.directive';

/**
 * Sub-component that wraps an individual BaseFormComponent in a FormStepperComponent.
 */
@Component({
    selector: 'app-form-step-wrapper',
    template: `<div class="container"><div #target></div></div>`
})
export class FormStepWrapperComponent implements OnChanges, OnDestroy, AfterViewInit, AfterViewChecked {
    @ViewChild('target', { read: ViewContainerRef }) target;
    @Input() type;
    @Input() options;
    @Input() bottomToolbarHost: FormStepperToolbarHostDirective;

    public componentRef: ComponentRef<BaseFormComponent>;
    private isViewInitialized = false;

    private control$: BehaviorSubject<AbstractControl> = new BehaviorSubject<AbstractControl>(null);

    constructor(private componentFactoryResolver: ComponentFactoryResolver,
                private cdRef: ChangeDetectorRef) {}

    updateComponent() {
        if (!this.isViewInitialized) {
            return;
        }
        if (this.componentRef) {
            this.componentRef.destroy();
        }

        const factory = this.componentFactoryResolver.resolveComponentFactory(this.type);
        this.componentRef = this.target.createComponent(factory);
        this.componentRef.instance.options = this.options;
        this.componentRef.instance.toolbarHostRef = this.bottomToolbarHost.viewContainerRef;

        this.control$.next(this.componentRef.instance.formControl);

        this.cdRef.detectChanges();
    }

    ngOnChanges() {
        this.updateComponent();
    }

    ngAfterViewInit() {
        this.isViewInitialized = true;
        this.updateComponent();
    }

    // Some forms take several cycles to build. Is there a more efficient way to do this?
    ngAfterViewChecked() {
        this.control$.next(this.componentRef.instance.formControl);
    }

    ngOnDestroy() {
        if (this.componentRef) {
            this.componentRef.destroy();
        }
    }

    get formControl(): Observable<AbstractControl> {
        return this.control$;
    }
}
