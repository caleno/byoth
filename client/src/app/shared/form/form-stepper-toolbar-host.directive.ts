import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[appFormStepperToolbarHost]'
})
export class FormStepperToolbarHostDirective {
    constructor(public viewContainerRef: ViewContainerRef) {}
}
