import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MaterialModule } from 'app/material.module';
import { ConfirmationPrintToolbarComponent } from './confirm-print-toolbar/confirm-print-toolbar.component';
import { ConfirmPrintComponent } from './confirm-print.component';

@NgModule({
    declarations: [
        ConfirmationPrintToolbarComponent,
        ConfirmPrintComponent
    ],
    imports: [
        CommonModule,
        MaterialModule
    ],
    entryComponents: [
        ConfirmPrintComponent,
        ConfirmationPrintToolbarComponent
    ],
    exports: [ ConfirmPrintComponent ]
})
export class ConfirmPrintModule { }
