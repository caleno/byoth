import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { ThesisContainerService } from 'services/thesis-container.service';
import { SubmissionStatusService } from 'services/submission-status.service';
import { MessageService } from 'services/message.service';
import { MaterialModule } from 'app/material.module';
import { ConfirmationPrintToolbarComponent } from './confirm-print-toolbar.component';

describe('ConfirmPrintToolbarComponent', () => {
    let component: ConfirmationPrintToolbarComponent;
    let fixture: ComponentFixture<ConfirmationPrintToolbarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ConfirmationPrintToolbarComponent ],
            imports: [ HttpClientTestingModule, MaterialModule ],
            providers: [ ThesisContainerService, MessageService, SubmissionStatusService ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfirmationPrintToolbarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
