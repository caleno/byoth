import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { mergeMap, tap, finalize } from 'rxjs/operators';

import { ThesisContainerService } from 'services/thesis-container.service';
import { SubmissionStatusService } from 'services/submission-status.service';
import { MessageService } from 'services/message.service';
import { DocumentService } from 'services/document.service';
import { Order, OrderTypes } from 'model/order';

/**
 * Toolbar Component for order confirmation page, handles order submission.
 * Will be created dynamically, hence setOrderType() should be called at initialization by creating component.
 */
@Component({
    selector: 'app-toolbar',
    templateUrl: './confirm-print-toolbar.component.html',
    styleUrls: ['./confirm-print-toolbar.component.css']
})
export class ConfirmationPrintToolbarComponent implements OnInit {
    valid = false;
    private orderType: OrderTypes;
    private orderService: DocumentService<Order>;

    submitting = false;

    constructor(private container: ThesisContainerService,
                private message: MessageService,
                private submissionStatus: SubmissionStatusService) { }

    ngOnInit() { }

    setOrderType(type: OrderTypes) {
        this.orderType = type;
        if (type === OrderTypes.EVALUATION) {
            this.orderService = this.container.evaluationOrder;
        } else if (type === OrderTypes.FINAL) {
            this.orderService = this.container.finalOrder;
        }
    }

    submitOrder() {
        // we are picky here becuase this is potentially a serious developer error
        if (!this.orderType || !this.orderService) {
            throw Error('ConfirmationToolbarComponent is misconfigured, specify order type');
        }

        this.submitting = true;

        this.container.save()
        .pipe(
            mergeMap(couldSave => {
                if (couldSave) {
                    return this.container.submitOrder(this.orderType)
                    .pipe(
                        tap(order => this.orderService.setValue(order)),
                        tap(() => this.submissionStatus.refresh())
                    );
                } else {
                    this.message.warn({ en: 'Could not save to submit', nb: 'Kunne ikke lagre for å sende inn' });
                    return of({});
                }
            }),
            finalize(() => this.submitting = false)
        ).subscribe(
            () => this.message.inform({en: 'Order submitted', nb: 'Bestilling sendt'})
        );
    }
}
