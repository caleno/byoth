import { Component, OnInit, ComponentFactory, ComponentRef, ComponentFactoryResolver, isDevMode } from '@angular/core';

import { ThesisContainer } from 'model/thesis-container';
import { Order, OrderTypes } from 'model/order';

import { ThesisContainerService, ValidationReport } from 'services/thesis-container.service';
import { DocumentService } from 'services/document.service';
import { BaseFormComponent } from 'shared/form/form-component';
import { ConfirmationPrintToolbarComponent } from './confirm-print-toolbar/confirm-print-toolbar.component';

@Component({
  selector: 'app-confirm-print',
  templateUrl: './confirm-print.component.html',
  styleUrls: ['./confirm-print.component.scss']
})
export class ConfirmPrintComponent extends BaseFormComponent implements OnInit {
    validationReport: ValidationReport;
    document: ThesisContainer;
    devMode: boolean;
    order: Order;
    orderService: DocumentService<Order>;
    options: { orderType: OrderTypes };
    isFinal: boolean;

    toolbarFactory: ComponentFactory<ConfirmationPrintToolbarComponent>;
    toolbar: ComponentRef<ConfirmationPrintToolbarComponent>;

    constructor(private container: ThesisContainerService,
                private componentFactoryResolver: ComponentFactoryResolver) {
        super();
        this.devMode = isDevMode();
        this.toolbarFactory = this.componentFactoryResolver.resolveComponentFactory(ConfirmationPrintToolbarComponent);
    }

    ngOnInit() {
        if (!this.options.orderType) {
            throw Error('Can not create ConfirmationComponent without orderType, behavior ill-defined');
        }

        this.isFinal = this.options.orderType && this.options.orderType === OrderTypes.FINAL;
    }

    activate() {
        this.container.validateOrder(this.options.orderType)
        .subscribe(res => {
            this.validationReport = res;
            this.document = res.document;

            if (this.options.orderType === OrderTypes.FINAL) {
                this.order = this.document.finalOrder as Order;
            } else if (this.options.orderType === OrderTypes.EVALUATION) {
                this.order = this.document.evaluationOrder as Order;
            }

            this.toolbar = this.toolbarHostRef.createComponent(this.toolbarFactory);
            this.toolbar.instance.valid = res.valid;
            this.toolbar.instance.setOrderType(this.options.orderType);
        });
    }

    deactivate() {
        this.toolbarHostRef.clear();
        this.toolbar = undefined;
    }

    private _getField(document: any, path: string[]) {
        if (path.length === 1) {
            return document[path[0]];
        } else {
            return this._getField(document[path[0]], path.slice(1));
        }
    }

    getField(fieldPath: string) {
        const path = fieldPath.split('.');
        const value = this._getField(this.validationReport.document, path);
        let error;
        if (this.validationReport.errors) {
            error = this.validationReport.errors.find(e => {
                if (e.dataPath === ('.' + fieldPath)) {
                    return true;
                }

                if (e.params && (<any>e.params).missingProperty &&
                    e.dataPath + '.' + (<any>e.params).missingProperty === '.' + fieldPath) {
                    return true;
                }

                return false;
            });
        }
        return { value: value, error: error };
    }
}
