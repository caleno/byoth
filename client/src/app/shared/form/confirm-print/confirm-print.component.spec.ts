import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { ThesisContainerService } from 'services/thesis-container.service';
import { MessageService } from 'services/message.service';
import { ConfirmPrintComponent } from './confirm-print.component';
import { ConfirmPrintModule } from './confirm-print.module';

import { OrderTypes } from 'model/order';

describe('ConfirmPrintComponent', () => {
    let component: ConfirmPrintComponent;
    let fixture: ComponentFixture<ConfirmPrintComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ConfirmPrintModule, HttpClientTestingModule ],
            providers: [ ThesisContainerService, MessageService ],
            schemas: [NO_ERRORS_SCHEMA]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfirmPrintComponent);
        component = fixture.componentInstance;
        component.options = { orderType: OrderTypes.FINAL }
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
