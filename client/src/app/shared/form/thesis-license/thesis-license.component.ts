import { Component, OnInit, Inject, LOCALE_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { tap } from 'rxjs/operators';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import * as marked from 'marked';

import { ThesisContainerService } from 'services/thesis-container.service';
import { BaseFormComponent } from 'shared/form/form-component';

import { ThesisTypes, PublishThesis } from 'model/thesis';

import { FormHandler } from 'shared/form/form-handler';

@Component({
    selector: 'app-toolbar-help-dialog',
    template: '<div [innerHtml]="innerHtml"></div>'
})
export class LicenseHelpDialogComponent {
    constructor(private dialogRef: MatDialogRef<LicenseHelpDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public innerHtml: string) { }

    onNoClick(): void {
        this.dialogRef.close();
    }
}

@Component({
    selector: 'app-thesis-license',
    templateUrl: './thesis-license.component.html',
    styleUrls: ['./thesis-license.component.css']
})
export class ThesisLicenseComponent extends BaseFormComponent implements OnInit {
    isMonograph = false;
    form: FormGroup;
    helpMessage: string;
    options: { inPrintForm: boolean }

    inPrintForm = false;

    // to give template access to these values
    dontPublish = PublishThesis.DONT;
    withEmbargo = PublishThesis.WITH_EMBARGO;
    withoutEmbargo = PublishThesis.WITHOUT_EMBARGO;

    constructor(private fb: FormBuilder,
                private container: ThesisContainerService,
                private dialog: MatDialog,
                private http: HttpClient,
                @Inject(LOCALE_ID) private locale) {
        super();

        this.form = this.fb.group({
            publish: [null, Validators.required],
            license: new FormControl({value: null, disabled: false}),
            embargo: new FormControl({value: null, disabled: false})
        });

        this.addFormHandler(new FormHandler(this.form, this.container.thesis));
    }

    ngOnInit() {
        this.inPrintForm = this.options && this.options.inPrintForm;

        this.container.thesis.get().subscribe(
            thesis => {
                if (thesis) {
                    this.setControllerStatus(thesis.publish);
                }
            }
        );

        this.addSubscription(
            this.container.thesis.valueChanges.subscribe(
                thesis => {
                    if (thesis) {
                        if (thesis.thesisType === ThesisTypes.MONOGRAPH) {
                            this.isMonograph = true;
                        } else {
                            this.isMonograph = false;
                        }

                        this.setControllerStatus(thesis.publish);
                    }
                }
            )
        );

        const publishSubscription = this.form.get('publish')
        .valueChanges
        .pipe(
            tap(publish => {
                this.setControllerStatus(publish);
            })
        )
        .subscribe();

        this.addSubscription(publishSubscription);

        const file = 'assets/help-license-' + this.locale + '.md';
        this.http.get(file, { responseType: 'text' })
        .subscribe(
            doc => this.helpMessage = marked(doc as string),
            err => this.helpMessage = '<p>Could not load help message for this language</p>',
        );
    }

    hasError(value: 'publish' | 'license' | 'embargo') {
        return !!this.form && this.form.get(value) && this.form.get(value).invalid && this.form.touched;
    }

    /** Depending on the publication decision, different controller will be enabled or disabled */
    setControllerStatus(publish: string) {
        const license = this.form.get('license');
        const embargo = this.form.get('embargo');

        if (publish === PublishThesis.WITH_EMBARGO) {
            embargo.enable({onlySelf: true});
            embargo.setValidators([Validators.required]);
        } else if (publish) {
            embargo.disable({onlySelf: true});
            embargo.setValue(null);
            embargo.setValidators([]);
        }

        if (publish !== PublishThesis.DONT) {
            license.enable({onlySelf: true});
            license.setValidators([Validators.required])
        } else if (publish) {
            license.disable({onlySelf: true});
            license.setValue(null);
            license.setValidators([]);
        }
    }

    openHelp(): void {
        this.dialog.open(LicenseHelpDialogComponent, { width: '450px', data: this.helpMessage });
    }
}
