import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'app/material.module';

import { ThesisContainerService } from 'services/thesis-container.service';
import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';

import { ThesisLicenseComponent } from './thesis-license.component';

describe('ThesisLicenseComponent', () => {
    let component: ThesisLicenseComponent;
    let fixture: ComponentFixture<ThesisLicenseComponent>;

    class MockContainer {
        thesis = new NonpersistentDocumentService();
        articles = new NonpersistentDocumentService();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ThesisLicenseComponent ],
            imports: [ RouterTestingModule, HttpClientTestingModule, MaterialModule, ReactiveFormsModule, NoopAnimationsModule ],
            providers: [ { provide: ThesisContainerService, useClass: MockContainer } ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ThesisLicenseComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
