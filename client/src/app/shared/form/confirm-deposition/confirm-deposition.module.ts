import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'app/material.module';

import { ConfirmDepositionComponent } from './confirm-deposition.component';
import { ConfirmDepositionToolbarComponent } from './confirm-deposition-toolbar/confirm-deposition-toolbar.component';

@NgModule({
    declarations: [
        ConfirmDepositionComponent,
        ConfirmDepositionToolbarComponent
    ],
    imports: [
        CommonModule, MaterialModule
    ],
    entryComponents: [
        ConfirmDepositionToolbarComponent, ConfirmDepositionComponent
    ],
    exports: [
        ConfirmDepositionComponent
    ]
})
export class ConfirmDepositionModule { }
