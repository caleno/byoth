import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { ThesisContainerService } from 'services/thesis-container.service';
import { ConfirmDepositionToolbarComponent } from './confirm-deposition-toolbar.component';

describe('ConfirmDepositionToolbarComponent', () => {
    let component: ConfirmDepositionToolbarComponent;
    let fixture: ComponentFixture<ConfirmDepositionToolbarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ HttpClientTestingModule, RouterTestingModule ],
            declarations: [ ConfirmDepositionToolbarComponent ],
            providers: [ ThesisContainerService ],
            schemas: [ NO_ERRORS_SCHEMA ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfirmDepositionToolbarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
