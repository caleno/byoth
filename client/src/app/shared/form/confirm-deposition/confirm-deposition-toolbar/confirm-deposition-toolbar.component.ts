import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ThesisContainerService } from 'services/thesis-container.service';

@Component({
    selector: 'app-confirm-deposition-toolbar',
    templateUrl: './confirm-deposition-toolbar.component.html',
    styleUrls: ['./confirm-deposition-toolbar.component.css']
})
export class ConfirmDepositionToolbarComponent implements OnInit {
    saving = false;

    constructor(private container: ThesisContainerService,
                private router: Router) { }

    ngOnInit() { }

    save() {
        this.saving = true;
        this.container.save().subscribe(
            () => this.router.navigate(['/']),
            null,
            () => this.saving = false
        );
    }

}
