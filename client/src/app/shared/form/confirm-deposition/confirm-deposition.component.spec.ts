import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { ThesisContainerService } from 'services/thesis-container.service';
import { ConfirmDepositionComponent } from './confirm-deposition.component';
import { ConfirmDepositionModule } from './confirm-deposition.module';

describe('ConfirmDepositionComponent', () => {
    let component: ConfirmDepositionComponent;
    let fixture: ComponentFixture<ConfirmDepositionComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ HttpClientTestingModule, ConfirmDepositionModule ],
            schemas: [ NO_ERRORS_SCHEMA ],
            providers: [ ThesisContainerService ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConfirmDepositionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
