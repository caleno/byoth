import { Component, OnInit, ComponentFactoryResolver, ComponentFactory, ComponentRef } from '@angular/core';

import { BaseFormComponent } from '../form-component';
import { ThesisContainerService } from 'services/thesis-container.service';
import { ConfirmDepositionToolbarComponent } from './confirm-deposition-toolbar/confirm-deposition-toolbar.component';

import { Thesis } from 'model/thesis';
import { Article } from 'model/article';

@Component({
    selector: 'app-confirm-deposition',
    templateUrl: './confirm-deposition.component.html',
    styleUrls: ['./confirm-deposition.component.scss']
})
export class ConfirmDepositionComponent extends BaseFormComponent implements OnInit {
    toolbarFactory: ComponentFactory<ConfirmDepositionToolbarComponent>;
    toolbar: ComponentRef<ConfirmDepositionToolbarComponent>;
    thesis: Thesis;
    articles: Article[] = [];

    constructor(private container: ThesisContainerService,
                private componentFactoryResolver: ComponentFactoryResolver) {
        super();
        this.toolbarFactory = this.componentFactoryResolver.resolveComponentFactory(ConfirmDepositionToolbarComponent);
    }

    ngOnInit() { }

    activate() {
        this.container.thesis.get().subscribe(thesis => this.thesis = thesis);
        this.container.articles.refresh().subscribe(articleSet => this.articles = articleSet.items);

        this.toolbar = this.toolbarHostRef.createComponent(this.toolbarFactory);
    }

    deactivate() {
        this.toolbarHostRef.clear();
        this.toolbar = undefined;
    }
}
