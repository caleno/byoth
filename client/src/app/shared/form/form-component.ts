import { OnDestroy, ViewContainerRef } from '@angular/core';
import { FormArray, AbstractControl } from '@angular/forms';
import { Subscription } from 'rxjs';

import { FormHandler } from './form-handler';

export interface FormComponent {
    formControl: AbstractControl;
}

export class BaseFormComponent implements FormComponent, OnDestroy {
    formHandlers: FormHandler[] = [];
    children: FormComponent[] = [];
    options: any;
    private subscription: Subscription;

    toolbarHostRef: ViewContainerRef;

    addFormHandler(formHandler: FormHandler) {
        this.formHandlers.push(formHandler);
    }

    addSubscription(subscription: Subscription) {
        if (!this.subscription) {
            this.subscription = subscription;
        } else {
            this.subscription.add(subscription);
        }
    }

    ngOnDestroy() {
        this.formHandlers.forEach(fh => fh.destroy());
        this.formHandlers = [];

        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    // Since all components in a stepper are initialized at the same time, ngOnInit() and its siblings are sometimes not finegrained enough.

    /** If anything needs to be done when the component is activated, override this */
    activate() { }

    /** If anything needs to be done when the component is deactivated, override this*/
    deactivate() { }

    /** Add a child component */
    addChild(child: FormComponent) {
        this.children.push(child);
    }

    /** Remove a child component */
    removeChild(childToRemove: FormComponent) {
        const index = this.children.findIndex(child => child === childToRemove);
        this.children.splice(index, 1);
    }

    get formControl(): AbstractControl {
        const control: FormArray = new FormArray(this.formHandlers.map(formHandler => formHandler.getForm()));

        if (this.children) {
            this.children.filter(child => !!child).forEach(child => control.push(child.formControl));
        }

        return control;
    }
}
