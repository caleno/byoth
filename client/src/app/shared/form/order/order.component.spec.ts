import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from 'app/material.module';
import { ThesisContainerService } from 'services/thesis-container.service';
import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { HomeInstitutionService } from 'services/home-institution.service';

import { of } from 'rxjs';

import { OrderComponent } from './order.component';
import { OrderToolbarComponent } from './order-toolbar/order-toolbar.component';

import { OrderTypes } from 'model/order';

@NgModule({
    declarations: [ OrderToolbarComponent ],
    entryComponents: [ OrderToolbarComponent ]
})
class TestModule {};

describe('OrderComponent', () => {
    let component: OrderComponent;
    let fixture: ComponentFixture<OrderComponent>;

    class MockContainer {
        thesis = new NonpersistentDocumentService();
        finalOrder = new NonpersistentDocumentService();
        candidate = new NonpersistentDocumentService();
        department = new NonpersistentDocumentService();
        getPrice() { return of({ offer: { price: '1 NOK' } }); };
    }

    class MockInsitution {
        getPlace() { return of({address: {city: 'foo'}})}
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ OrderComponent ],
            imports: [ MaterialModule, ReactiveFormsModule, NoopAnimationsModule, TestModule ],
            providers: [
                { provide: ThesisContainerService, useClass: MockContainer },
                { provide: HomeInstitutionService, useClass: MockInsitution }
            ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(OrderComponent);
        component = fixture.componentInstance;
        component.options = { orderType: OrderTypes.FINAL };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
