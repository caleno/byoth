import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';
import { OrderComponent } from './order.component';
import { OrderToolbarComponent } from './order-toolbar/order-toolbar.component';
import { OrderAddressComponent } from './order-address/order-address.component';

@NgModule({
    declarations: [
        OrderComponent,
        OrderToolbarComponent,
        OrderAddressComponent,
    ],
    imports: [
        CommonModule, ReactiveFormsModule, MaterialModule
    ],
    exports: [
        OrderComponent,
    ],
    entryComponents: [ OrderToolbarComponent ],
})
export class OrderModule { }
