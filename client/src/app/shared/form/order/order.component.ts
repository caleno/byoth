import {
    Component,
    OnInit,
    AfterViewInit,
    ComponentFactoryResolver,
    ComponentRef,
    ComponentFactory,
    ViewChildren,
    QueryList } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable, forkJoin, of, Subject } from 'rxjs';
import { mergeMap, map, tap, debounceTime, switchMap, delay } from 'rxjs/operators';
import { MatCheckboxChange } from '@angular/material'

import { DocumentService } from 'services/document.service';
import { ThesisContainerService } from 'services/thesis-container.service';
import { HomeInstitutionService } from 'services/home-institution.service';
import { BaseFormComponent } from 'shared/form/form-component';
import { FormHandler } from 'shared/form/form-handler';
import { OrderToolbarComponent } from './order-toolbar/order-toolbar.component';
import { OrderAddressComponent } from './order-address/order-address.component';

import {
    Order,
    OrderTypes,
    initOrderOffer,
    initOrderDelivery,
    initOrderCustomer,
    MANDATORY_COPIES,
    addOrderProofDeliveryAddress } from 'model/order';

// need some special treatment for nested forms and also to deduct and add the mandatory copies
class OrderFormHandler extends FormHandler {
    patchForm(order: Order) {
        super.patchForm(order);
    }

    patchDocument(value: any) {
        const document = this.service.value as Order;

        Object.assign(document.offer, value.offer);
        Object.assign(document.customer, value.customer);

        document.delivery.description = value.delivery.description;
        document.isTest = value.isTest;

        this.service.setValue(document);
    }
}

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.scss']
})
export class OrderComponent extends BaseFormComponent implements OnInit, AfterViewInit {
    @ViewChildren(OrderAddressComponent)
    addressComponents: QueryList<OrderAddressComponent>;

    form: FormGroup;
    gettingPrice = false;
    toolbarFactory: ComponentFactory<OrderToolbarComponent>;
    toolbar: ComponentRef<OrderToolbarComponent>;
    service: DocumentService<Order>;
    mandatoryCopies = MANDATORY_COPIES;

    options: { orderType: OrderTypes };
    isFinal: boolean;
    minCopies = 1;

    // Post to next() to generate a new price. A value should be submitted to allow switchMap to know when something has actually changed.
    priceRequests = new Subject<{quantity?: number, printProofs?: boolean, printColor?: boolean} | string>();

    constructor(private fb: FormBuilder,
                private container: ThesisContainerService,
                private institution: HomeInstitutionService,
                private componentFactoryResolver: ComponentFactoryResolver) {
        super();

        this.toolbarFactory = this.componentFactoryResolver.resolveComponentFactory(OrderToolbarComponent);
    }

    ngOnInit() {
        if (!this.options.orderType) {
            throw Error('Order component initialized without order type');
        }

        this.isFinal = this.options.orderType === OrderTypes.FINAL;

        if (!this.isFinal) {
            this.service = this.container.evaluationOrder;
        } else {
            this.service = this.container.finalOrder;
        }

        this.minCopies = this.isFinal ? MANDATORY_COPIES + 1 : 1;

        this.form = this.fb.group({
            offer: this.fb.group({
                quantity: [null, Validators.min(this.minCopies)],
                numPages: new FormControl({value: 0, disabled: true}),
                printProofs: [false],
                printColor: [false]
            }),
            delivery: this.fb.group({
                description: [null]
            }),
            customer: this.fb.group({
                name: [null, Validators.required],
                telephone: [null, Validators.required],
                email: [null, Validators.email]
            })
        });

        this.addFormHandler(new OrderFormHandler(this.form, this.service));

        this.prefillOrder().subscribe(null);

        // Listen for price request events.
        // To avoid race conditions the order observable will not be updated here.
        // This is ok because the only value that changes is price, and price isn't part of the form.
        // The calculated price is saved with the order on the server.
        this.addSubscription(
            this.priceRequests
            .pipe(
                tap(() => {
                    if (this.toolbar) {
                        this.toolbar.instance.gettingPrice = true;
                    }
                }),
                delay(10), // make sure changes to form have propagated to order document
                switchMap(() => this.container.postPriceRequest(this.service.value, this.options.orderType))
            )
            .subscribe(orderWithPrice => {
                const order = this.service.value;
                order.offer.price = orderWithPrice.offer.price;
                this.service.setValue(order);

                if (this.toolbar) {
                    this.toolbar.instance.price = order.offer.price;
                    this.toolbar.instance.gettingPrice = false;
                }
            })
        );

        // listen for changes to quantity field, to update price
        this.addSubscription(
            this.form.get('offer').get('quantity').valueChanges
            .pipe(debounceTime(300)) // can potentially generate very many requests here if we don't filter a bit
            .subscribe(quantity => this.priceRequests.next({ quantity: quantity }))
        );

        this.addSubscription(
            this.form.get('offer').get('printProofs').valueChanges
            .subscribe(printProofs => this.priceRequests.next({ printProofs: printProofs }))
        );

        this.addSubscription(
            this.form.get('offer').get('printColor').valueChanges
            .subscribe(printColor => this.priceRequests.next({ printColor: printColor }))
        );
    }

    ngAfterViewInit() {
        this.addSubscription(
            this.addressComponents.changes
            .subscribe(list => {
                this.children = list.toArray();
            })
        );
    }

    prefillOrder(): Observable<Order> {
        return forkJoin([this.service.get(), this.container.candidate.get()])
        .pipe(
            // init order object if necessary
            map(([order, candidate]) => {
                if (!order.offer) {
                    initOrderOffer(order);
                    order.offer.quantity = this.minCopies;
                }

                if (!order.delivery || !order.delivery.address) {
                    initOrderDelivery(order);
                }

                if (!order.customer) {
                    initOrderCustomer(order);
                    order.customer.email = candidate.email;
                    order.customer.name = candidate.givenName + ' ' + candidate.familyName;
                }

                return order;
            }),
            // init address based on thesis department
            mergeMap(order => {
                const address = order.delivery.address;

                // skip if address already has value or if this is for the evaluation committee
                if (!this.isFinal || address && (address.city || address.name || address.postalCode || address.streetAddress)) {
                    return of(order);
                }

                return this.container.department.get()
                .pipe(
                    mergeMap(department => {
                        if (department.departmentId) {
                            return this.institution.getPlace(department.departmentId);
                        } else if (department.facultyId) {
                            return this.institution.getPlace(department.facultyId);
                        } else {
                            return of(null);
                        }
                    }),
                    map(place => {
                        if (place) {
                            Object.assign(order.delivery.address, place.address);
                        }
                        return order;
                    })
                );
            }),
            tap(order => this.service.setValue(order)),
        );
    }

    toggleProofAddress(event: MatCheckboxChange) {
        if (event.checked === true) {
            this.addProofAddress();
        } else {
            this.deleteProofAddress();
        }
    }

    private addProofAddress() {
        const order = this.service.value;
        addOrderProofDeliveryAddress(order);
        this.service.setValue(order);
    }

    private deleteProofAddress() {
        const order = this.service.value;
        order.delivery.proofAddress = undefined;
        this.service.setValue(order);
    }

    get wantSeparateProofAddress(): Observable<boolean> {
        return this.service.valueChanges
            .pipe(map(order => !!order && !!order.delivery && !!order.delivery.proofAddress));
    }

    activate() {
        this.toolbar = this.toolbarHostRef.createComponent(this.toolbarFactory);
        this.priceRequests.next('activate');
    }

    deactivate() {
        this.toolbarHostRef.clear();
        this.toolbar = undefined;
    }
}
