import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BaseFormComponent } from 'shared/form/form-component';
import { FormHandler } from 'shared/form/form-handler';
import { DocumentService } from 'services/document.service';
import { Order, initOrderDelivery } from 'model/order';

export enum AddressType {
    PRIMARY = 'address',
    PROOF = 'proofAddress'
};

class OrderAddressFormHandler extends FormHandler {
    constructor(form: FormGroup, service: DocumentService<Order>, private type: AddressType) {
        super(form, service, false);

        // we need explicit this binding, otherwise the type will be undefined
        this.init.call(this, form, service);
    }

    patchDocument(value: any) {
        const document = this.service.value as Order;

        if (!document.delivery) {
            initOrderDelivery(document);
        }

        Object.assign(document.delivery[this.type], value);

        this.service.setValue(document);
    }

    patchForm(order: Order) {
        if (order && order.delivery) {
            super.patchForm(order.delivery[this.type] as any);
        }
    }
}

@Component({
    selector: 'app-order-address',
    templateUrl: './order-address.component.html',
    styleUrls: ['./order-address.component.css']
})
export class OrderAddressComponent extends BaseFormComponent implements OnInit {
    @Input()
    service: DocumentService<Order>;

    @Input()
    type: AddressType;

    form: FormGroup;

    constructor(private fb: FormBuilder) {
        super();
        this.form = this.fb.group({
            name: [null],
            streetAddress: [null, Validators.required],
            postalCode: [null, Validators.pattern(/\d\d\d\d/)],
            city: [null, Validators.required]
        });
    }

    ngOnInit() {
        this.addFormHandler(new OrderAddressFormHandler(this.form, this.service, this.type));
    }
}
