import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { MaterialModule } from 'app/material.module';
import { OrderAddressComponent } from './order-address.component';

describe('OrderAddressComponent', () => {
    let component: OrderAddressComponent;
    let fixture: ComponentFixture<OrderAddressComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ MaterialModule, ReactiveFormsModule, NoopAnimationsModule ],
            declarations: [ OrderAddressComponent ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(OrderAddressComponent);
        component = fixture.componentInstance;
        component.service = new NonpersistentDocumentService();
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
