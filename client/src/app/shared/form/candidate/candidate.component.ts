import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { OrderTypes } from 'model/order';

import { ThesisContainerService } from 'services/thesis-container.service';
import { BaseFormComponent } from 'shared/form/form-component';
import { FormHandler } from 'shared/form/form-handler';

@Component({
    selector: 'app-candidate',
    templateUrl: './candidate.component.html',
    styleUrls: ['./candidate.component.css']
})
export class CandidateComponent extends BaseFormComponent implements OnInit {
    @Input()
    orderType: OrderTypes;

    isFinal: boolean;

    form: FormGroup;

    constructor(
        private container: ThesisContainerService,
        private fb: FormBuilder) {
        super();
        this.form = this.fb.group({
            givenName: ['', Validators.required ],
            familyName: ['', Validators.required ],
            email: ['', [ Validators.required, Validators.email ] ],
            orcid: ['', Validators.pattern(/^(https?:\/\/orcid.org\/)?(\d\d\d\d\-){3}\d\d\d(\d|X)$/)]
        });

        this.addFormHandler(new FormHandler(this.form, this.container.candidate));
    }

    ngOnInit() {
        this.isFinal = this.orderType === OrderTypes.FINAL;
    }
}
