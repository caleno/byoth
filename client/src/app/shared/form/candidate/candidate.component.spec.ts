import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';

import { ThesisContainerService } from 'services/thesis-container.service';
import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { HomeInstitutionService } from 'services/home-institution.service';
import { MessageService } from 'services/message.service';

import { CandidateComponent } from './candidate.component';

describe('CandidateComponent', () => {
    let component: CandidateComponent;
    let fixture: ComponentFixture<CandidateComponent>;

    class MockContainer {
        candidate = new NonpersistentDocumentService();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ CandidateComponent ],
            imports: [ HttpClientTestingModule, RouterTestingModule, NoopAnimationsModule, ReactiveFormsModule, MaterialModule ],
            providers: [ { provide: ThesisContainerService, useClass: MockContainer }, HomeInstitutionService, MessageService ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CandidateComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
