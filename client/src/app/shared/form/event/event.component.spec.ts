import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { EventComponent } from './event.component';

describe('EventComponent', () => {
    let component: EventComponent;
    let fixture: ComponentFixture<EventComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ EventComponent ],
            imports: [ ReactiveFormsModule ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EventComponent);
        component = fixture.componentInstance;
        component.service = new NonpersistentDocumentService();
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
