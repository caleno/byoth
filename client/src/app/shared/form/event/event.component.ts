import { Component, OnInit, Input, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { BaseFormComponent } from 'shared/form/form-component';
import { FormHandler } from 'shared/form/form-handler';

import { EventLocationComponent } from './event-location/event-location.component';
import { EventDateComponent } from './event-date/event-date.component';

import { DocumentService } from 'services/document.service';
import { ByothEvent } from 'model/event';

@Component({
    selector: 'app-event',
    templateUrl: './event.component.html',
    styleUrls: ['./event.component.css']
})
export class EventComponent extends BaseFormComponent implements OnInit, AfterViewInit {
    form: FormGroup;

    @Input()
    service: DocumentService<ByothEvent>;

    @Input()
    hasTitle = false;

    @Input()
    hasDate = true;

    @Input()
    hasLocation = true;

    @Input()
    showTime = true;

    @ViewChild(EventLocationComponent)
    private eventLocation: EventLocationComponent;

    @ViewChild(EventDateComponent)
    private eventDate: EventDateComponent;

    constructor(private fb: FormBuilder) {
        super();
    }

    ngOnInit() {
        if (!this.service) {
            throw Error('EventComponent created without service');
        }

        if (this.hasTitle) {
            this.form = this.fb.group({
                title: [null, Validators.required]
            });
            this.addFormHandler(new FormHandler(this.form, this.service));
        }
    }

    ngAfterViewInit() {
        if (this.hasLocation) {
            this.addChild(this.eventLocation);
        }

        if (this.hasDate) {
            this.addChild(this.eventDate);
        }
    }
}
