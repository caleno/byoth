import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { tap } from 'rxjs/operators';

import { BaseFormComponent } from 'shared/form/form-component';
import { FormHandler } from 'shared/form/form-handler';

import { ByothEvent, addEventLocation } from 'model/event';
import { HomeInstitutionService, NamedEntity } from 'services/home-institution.service';
import { DocumentService } from 'services/document.service';

class EventLocationFormHandler extends FormHandler {
    patchDocument(value: any) {
        const event = this.service.value as ByothEvent;
        if (!event.location) {
            addEventLocation(event, '');
        }
        event.location.name = value.location.name;
        this.service.setValue(event);
    }
}

@Component({
    selector: 'app-event-location',
    templateUrl: './event-location.component.html',
    styleUrls: ['./event-location.component.css']
})
export class EventLocationComponent extends BaseFormComponent implements OnInit {
    @Input()
    service: DocumentService<ByothEvent>;

    form: FormGroup;
    dropdownForm: FormGroup;

    areas: NamedEntity[];
    buildings: NamedEntity[];
    rooms: NamedEntity[];

    constructor(private roomService: HomeInstitutionService,
                private fb: FormBuilder) {
        super();

        this.form = this.fb.group({
            location: this.fb.group({
                name: [null, Validators.required],
            })
        });

        this.dropdownForm = this.fb.group({
            area: [''],
            building: [''],
            room: [''],
        });
    }

    ngOnInit() {
        this.addFormHandler(new EventLocationFormHandler(this.form, this.service));

        const locationForm = this.form.get('location').get('name');

        this.roomService.getAreas()
        .pipe(tap(res => this.areas = res))
        .subscribe(() => {});

        this.dropdownForm.get('area').valueChanges
        .subscribe(area => {
            this.roomService.getBuildings(area.id)
            .pipe(tap(res => this.buildings = res), tap(() => this.rooms = []))
            .subscribe(() => locationForm.setValue(null));
        });

        this.dropdownForm.get('building').valueChanges
        .subscribe(building => {
            this.roomService.getRooms(building.id)
            .pipe(tap(res => this.rooms = res))
            .subscribe(() => {
                if (!this.rooms || !this.rooms.length) {
                    locationForm.setValue(building.name);
                } else {
                    locationForm.setValue(null);
                }
            });
        });

        this.dropdownForm.get('room').valueChanges
        .subscribe(room => {
            locationForm.setValue(this.dropdownForm.get('building').value.name + ', ' + room.name);
        });
    }
}
