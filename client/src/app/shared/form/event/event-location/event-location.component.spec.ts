import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../../../material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { of } from 'rxjs';

import { HomeInstitutionService } from 'services/home-institution.service';

import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';

import { EventLocationComponent } from './event-location.component';

describe('EventLocationComponent', () => {
    let component: EventLocationComponent;
    let fixture: ComponentFixture<EventLocationComponent>;

    const areas = [{id: 'fooArea', name: 'foo'}];
    const buildings = [{id: 'fooBuilding', name: 'bar'}];
    const rooms = [{id: 'fooRoom', name: 'baz'}];


    class MockHomeInstitutionService {
        getAreas() {
            return of(areas);
        }

        getBuildings(id: string) {
            return of(buildings);
        }

        getRooms(id: string) {
            return of(rooms);
        }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ EventLocationComponent ],
            imports: [ ReactiveFormsModule, MaterialModule, NoopAnimationsModule ],
            providers: [
                { provide: HomeInstitutionService, useClass: MockHomeInstitutionService }
            ]
        })
        .compileComponents();
    }));

    describe('with empty location', () => {
        beforeEach(fakeAsync(() => {
            fixture = TestBed.createComponent(EventLocationComponent);
            component = fixture.componentInstance;
            component.service = new NonpersistentDocumentService();
            component.service.setValue({ '@id': 'foo' } as any);

            fixture.detectChanges();
            tick();
        }));

        it('should create', () => {
            expect(component).toBeTruthy();
        });

        it('should initialize with list of areas', () => {
            expect(component.areas).toEqual(areas);
        });

        it('should request list of buildings when an area has been selected', () => {
            expect(component.buildings).toBeFalsy();
            component.dropdownForm.get('area').setValue(areas[0]);
            expect(component.buildings).toEqual(buildings);
        });

        it('should request list of rooms when a building has been selected', () => {
            expect(component.rooms).toBeFalsy();
            component.dropdownForm.get('building').setValue(buildings[0]);
            expect(component.rooms).toEqual(rooms);
        });

        it('should set location name when a room is selected', () => {
            expect(component.service.value.location).toBeFalsy();
            component.dropdownForm.get('building').setValue(buildings[0])
            component.dropdownForm.get('room').setValue(rooms[0]);
            expect(component.form.get('location').get('name').value).toEqual(buildings[0].name + ', ' + rooms[0].name);
            expect(component.service.value.location).toBeTruthy();
            expect(component.service.value.location.name).toEqual(buildings[0].name + ', ' + rooms[0].name);
        });

        it('should set event location name when location input value is changed', () => {
            expect(component.service.value.location).toBeFalsy();
            component.form.get('location').get('name').setValue('foobar');
            expect(component.service.value.location).toBeTruthy();
            expect(component.service.value.location.name).toEqual('foobar');
        });
    });

    describe('with location from before', () => {
        const locationName = 'foobar';

        beforeEach(() => {
            fixture = TestBed.createComponent(EventLocationComponent);
            component = fixture.componentInstance;
            component.service = new NonpersistentDocumentService();
            component.service.setValue({ '@id': 'foo', location: {'@id': 'foo#location', name: locationName } } as any);

            fixture.detectChanges();
        });

        it('should create', () => {
            expect(component).toBeTruthy();
        });

        it('should display location name', () => {
            const value = fixture.debugElement.nativeElement.querySelector('input[formControlName="name"]').value;
            expect(value).toEqual(locationName);
        });

        it('should overwrite location name upon room selection', () => {
            component.dropdownForm.get('building').setValue(buildings[0])
            component.dropdownForm.get('room').setValue(rooms[0]);
            expect(component.form.get('location').get('name').value).toEqual(buildings[0].name + ', ' + rooms[0].name);
            expect(component.service.value.location.name).toEqual(buildings[0].name + ', ' + rooms[0].name);
        });
    });
});
