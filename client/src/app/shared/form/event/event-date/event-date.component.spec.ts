import { fakeAsync, async, ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { ByothEvent, createModelDocument, getModel } from 'model/index';
import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { EventDateComponent } from './event-date.component';

describe('EventDateComponent', () => {
    let component: EventDateComponent;
    let fixture: ComponentFixture<EventDateComponent>;
    let compiled: any;
    const model = getModel('');

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ EventDateComponent ],
            imports: [ ReactiveFormsModule, MaterialModule, NoopAnimationsModule ],
        })
        .compileComponents();
    }));

    // These two function help us deal with the fact that tests can be run in a non UTC environment,
    // and thus dates displayed with different times than the input value.
    // Use the actual date to take daylight saving into account.
    function UTCToLocal(date: Date): string {
        const offset = new Date(date.toDateString()).getTimezoneOffset() * 60000;
        return new Date(date.getTime() + offset).toISOString();
    }

    function LocalToUTC(date: Date): string {
        const offset = new Date(date.toDateString()).getTimezoneOffset() * 60000;
        return new Date(date.getTime() - offset).toISOString();
    }

    describe('When initialized with empty event', () => {
        beforeEach(fakeAsync(() => {
            fixture = TestBed.createComponent(EventDateComponent);
            component = fixture.componentInstance;
            component.service = new NonpersistentDocumentService();
            fixture.detectChanges();
            compiled = fixture.debugElement.nativeElement;
            tick();
        }));

        it('should create', () => {
            expect(component).toBeTruthy();
        });

        it('should display empty date', () => {
            const value = compiled.querySelector('input[formControlName="date"]').value;
            expect(value).toEqual('');
        });

        it('should have disabled and empty start and end times', () => {
            expect(component.form.get('startTime').value).toBeNull();
            expect(component.form.get('startTime').disabled).toBe(true);
            expect(component.form.get('endTime').value).toBeNull();
            expect(component.form.get('endTime').disabled).toBe(true);
        });

        it('should update event date and undisable start and end times, when date is set', fakeAsync(() => {
            const date = new Date();
            component.form.get('date').setValue(date);
            tick();
            expect(component.service.value.start).toEqual(date.toISOString());

            expect(component.form.get('startTime').value).toBeNull();
            expect(component.form.get('startTime').disabled).toBe(false);
            expect(component.form.get('endTime').value).toBeNull();
            expect(component.form.get('endTime').disabled).toBe(false);
        }));

        it('should store start time when set', () => {
            component.form.get('date').setValue(new Date());
            component.form.get('startTime').setValue('17:53');

            expect(LocalToUTC(new Date(component.service.value.start))).toMatch('17:53:00');
        });

        it('should store end time when set', fakeAsync(() => {
            component.form.get('date').setValue(new Date());
            component.form.get('endTime').setValue('17:53');
            tick();
            expect(LocalToUTC(new Date(component.service.value.end))).toMatch('17:53:00');
        }));

        it('should transform HHMM string to HH:MM', () => {
            component.form.get('date').setValue(new Date());
            component.form.get('startTime').setValue('1912');
            expect(LocalToUTC(new Date(component.service.value.start))).toMatch('19:12:00');
            expect(component.form.get('startTime').invalid).toBeFalsy();
        });

        it('should choke on invalid time', () => {
            component.form.get('date').setValue(new Date());
            component.form.get('startTime').setValue('2520');
            expect(component.form.get('startTime').invalid).toBeTruthy();
        });
    });

    describe('When initialized with event data', () => {
        beforeEach(fakeAsync(() => {
            fixture = TestBed.createComponent(EventDateComponent);
            component = fixture.componentInstance;
            component.service = new NonpersistentDocumentService();

            const event: ByothEvent = <ByothEvent>createModelDocument(model.defense, 'foo', {});
            event.start = UTCToLocal(new Date('2000-01-01T12:00Z'));
            event.end = UTCToLocal(new Date('2000-01-01T15:00Z'));
            event.location = { name: '', '@id': 'foo' };
            component.service.setValue(event);

            fixture.detectChanges();
            compiled = fixture.debugElement.nativeElement;
            tick();
        }));

        it('should create', () => {
            expect(component).toBeTruthy();
        });

        it('should display correct date', () => {
            const value = compiled.querySelector('input[formControlName="date"]').value;
            expect(new Date(value).toDateString()).toEqual(new Date(component.service.value.start).toDateString());
        });

        it('should display correct start time', () => {
            const value = compiled.querySelector('input[formControlName="startTime"]').value;
            expect(value).toEqual(new Date(component.service.value.start).toTimeString().substr(0, 5));
        });

        it('should update start date when form changes', () => {
            const date = new Date();
            component.form.get('date').setValue(date);
            expect(new Date(component.service.value.start).toDateString()).toEqual(date.toDateString());
        });

        it('should update start time when form changes', () => {
            component.form.get('startTime').setValue('17:00');
            expect(new Date(component.service.value.start).toTimeString()).toMatch('17:00:00');
            expect(new Date(component.service.value.end).toTimeString()).toMatch('15:00:00');
        });

        it('should update end time when form changes', () => {
            component.form.get('endTime').setValue('17:00');
            expect(new Date(component.service.value.end).toTimeString()).toMatch('17:00:00');
            expect(new Date(component.service.value.start).toTimeString()).toMatch('12:00:00');
        });
    });
});
