import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup, AbstractControl } from '@angular/forms';
import { map, filter } from 'rxjs/operators';

import { BaseFormComponent } from 'shared/form/form-component';
import { FormHandler } from 'shared/form/form-handler';
import { DocumentService } from 'services/document.service';

import { ByothEvent } from 'model/event';

const timeRegex = /([0-1]\d|2[0-3]):?[0-5]\d/;

class EventTimeFormHandler extends FormHandler {
    patchDocument(value: any) {
        const event = this.service.value as ByothEvent;
        const date = new Date(value.date);
        const start = value.startTime;
        const end = value.endTime;

        if (date && date.valueOf() > 0) {
            if (start && start.match(timeRegex)) {
                const time = start.split(':');
                date.setHours(time[0], time[1], 0, 0);
            }

            event.start = date.toISOString();

            if (end && end.match(timeRegex)) {
                const time = end.split(':');
                date.setHours(time[0], time[1], 0, 0);
            }

            event.end = date.toISOString();

            this.service.setValue(event);
        }
    }
}

@Component({
    selector: 'app-event-date',
    templateUrl: './event-date.component.html',
    styleUrls: ['./event-date.component.css']
})
export class EventDateComponent extends BaseFormComponent implements OnInit {
    @Input()
    service: DocumentService<ByothEvent>;

    @Input() showTime = true;

    form: FormGroup;

    constructor(private fb: FormBuilder) {
        super();
    }

    ngOnInit() {
        this.service.get().subscribe(event => {
            this.initForm(event);
        });
    }

    initForm(event: ByothEvent) {
        let date = null;
        let startTime = null;
        let endTime = null;

        if (event.start) {
            date = new Date(event.start);
            startTime = date.toTimeString().substr(0, 5);
        }

        if (event.end) {
            endTime = new Date(event.end).toTimeString().substr(0, 5);
        }

        const group: any = {};
        group.date = [date, Validators.required ];
        if (this.showTime) {
            group.startTime = [ startTime, [ Validators.required, Validators.pattern(timeRegex) ] ];
            group.endTime = [ endTime, [ Validators.required, Validators.pattern(timeRegex) ] ];
        } else {
            // set a time in the middle of the day to avoid timezone confusion
            group.startTime = [ startTime || '12:00' ];
            group.endTime = [ endTime  || '12:00' ];
        }

        this.form = this.fb.group(group);
        this.addFormHandler(new EventTimeFormHandler(this.form, this.service));

        const start = this.form.get('startTime');
        const end = this.form.get('endTime');

        if (!date) {
            start.disable();
            end.disable();
        }

        const dateListener = this.form.get('date').valueChanges
        .subscribe(value => {
            if (value) {
                start.enable();
                end.enable();
            } else {
                start.disable();
                end.disable();
            }
        });
        this.addSubscription(dateListener);

        this.addTimeListener(start, 'start');
        this.addTimeListener(end, 'end');
    }

    save() {
        return this.service.save().pipe(map(res => true));
    }

    addTimeListener(control: AbstractControl, associatedDate: string) {
        const subscription = control.valueChanges
        .pipe(
            filter(value => !!value),
            filter(value => !!value.match(timeRegex)),
            map(value => {
                if (value.match(/\d\d\d\d/)) {
                    value = value.slice(0, 2) + ':' + value.slice(2);
                    control.setValue(value)
                }
                return value;
            })
        )
        .subscribe();

        this.addSubscription(subscription);
    }

}
