import { CanDeactivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import { ThesisContainerService } from 'services/thesis-container.service';
import { MessageService } from 'services/message.service';
import { FormComponent } from './form-component';

@Injectable()
export class SaveFormGuard implements CanDeactivate<FormComponent> {
    constructor(private container: ThesisContainerService, private message: MessageService) { }

    canDeactivate(component: FormComponent) {
        return this.container.id.get()
        .pipe(
            mergeMap(id => {
                // If the id is null, we can't save anything (probably because the container has
                // been deleted), and we can bypass the SaveFormGuard.
                if (!id) {
                    return of(true);
                }
                return this.container.save()
            }),
            map(couldSave => {
                if (!couldSave) {
                    this.message.warn({ en: 'Could not save', nb: 'Kunne ikke lagre' });
                }
                return !!couldSave;
            }),
            catchError(err => {
                return of(false);
            })
        );
    }
}
