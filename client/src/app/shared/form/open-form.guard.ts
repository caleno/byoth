import { Router, CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';
import { of, Subject } from 'rxjs';
import { map, mergeMap, catchError, tap } from 'rxjs/operators';

import { ThesisContainerService } from 'services/thesis-container.service';
import { AuthService } from 'services/auth.service';

@Injectable()
export class OpenFormGuard implements CanActivate {
    private loading$ = new Subject<boolean>();
    loading = false;

    constructor(private router: Router,
                private auth: AuthService,
                private container: ThesisContainerService) {
        this.loading$.next(false);
        this.loading$.subscribe(value => this.loading = value);
    }

    canActivate() {
        this.loading$.next(true);
        return this.auth.currentUser()
        .pipe(
            mergeMap(user => {
                if (user) {
                    return this.container.id.get().pipe(
                        mergeMap(id => {
                            if (!id) {
                                return this.container.create().pipe(map(() => true));
                            }

                            return of(true);
                        })
                    );
                }

                return of(false);
            }),
            tap(() => this.loading$.next(false)),
            catchError(err => of(false)),
            tap(canActivate => {
                if (!canActivate) {
                    this.router.navigate(['/']);
                }
            })
        );
    }
}
