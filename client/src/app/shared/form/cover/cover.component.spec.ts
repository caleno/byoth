import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';
import { OrderTypes } from 'model/order';

import { MaterialModule } from 'app/material.module';
import { CoverComponent } from './cover.component';
import { ThesisContainerService } from 'services/thesis-container.service';
import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { MessageService } from 'services/message.service';

describe('CoverComponent', () => {
    let component: CoverComponent;
    let fixture: ComponentFixture<CoverComponent>;

    class MockContainer {
        finalOrder = new NonpersistentDocumentService();
        thesis = new NonpersistentDocumentService();
        defense = new NonpersistentDocumentService();
        candidate = new NonpersistentDocumentService();
        coverPdfPreviewDownloadURL = () => of('');
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ MaterialModule, HttpClientTestingModule ],
            declarations: [ CoverComponent ],
            providers: [ { provide: ThesisContainerService, useClass: MockContainer }, MessageService, FormBuilder ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CoverComponent);
        component = fixture.componentInstance;
        component.options = { orderType: OrderTypes.FINAL };
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
