import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { BaseFormComponent } from 'shared/form/form-component';
import { FormHandler } from 'shared/form/form-handler';

import { DocumentService } from 'services/document.service';
import { ThesisContainerService } from 'services/thesis-container.service';

import { Order, OrderTypes } from 'model/order';

import { merge } from 'rxjs';

@Component({
    selector: 'app-cover',
    templateUrl: './cover.component.html',
    styleUrls: ['./cover.component.scss']
})
export class CoverComponent extends BaseFormComponent implements OnInit {
    form: FormGroup;
    thumbnails: string[] = [];
    options: { orderType: OrderTypes };
    downloadUrl: string;

    private service: DocumentService<Order>;

    constructor(public container: ThesisContainerService,
                private fb: FormBuilder) {
        super();
        this.form = this.fb.group({
            coverPreviewAccepted: [null, Validators.requiredTrue]
        });
    }

    ngOnInit() {
        if (!this.options || !this.options.orderType) {
            throw Error('Cover component initialized without orderType');
        }

        if (this.options.orderType === OrderTypes.EVALUATION) {
            this.service = this.container.evaluationOrder;
        } else if (this.options.orderType === OrderTypes.FINAL) {
            this.service = this.container.finalOrder;
        }

        this.addFormHandler(new FormHandler(this.form, this.service));

        this.container.coverPdfPreviewDownloadURL(this.options.orderType, false)
        .subscribe(url => this.downloadUrl = url);

        // these three properties are the ones that affect the cover,
        // so changing them will cause the spinner to be displayed while thumbnails are regenerated
        // (for simplicity they are also generated when there are no changes, see activate())
        this.addSubscription(
            merge(
                this.container.thesis.valueChanges,
                this.container.defense.valueChanges,
                this.container.candidate.valueChanges
            ).subscribe(() => this.thumbnails = [])
        );
    }

    activate() {
        if (this.service) {
            this.thumbnails = [];
            this.container.coverThumbnails(this.options.orderType, true)
            .subscribe(thumbnails => this.thumbnails = thumbnails);
        }
    }
}
