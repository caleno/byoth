import { Component, OnInit, ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import { combineLatest } from 'rxjs';

import { BaseFormComponent } from 'shared/form/form-component';
import { CandidateComponent } from 'shared/form/candidate/candidate.component';
import { ThesisComponent } from 'shared/form/thesis/thesis.component';
import { EventComponent } from 'shared/form/event/event.component';

import { ThesisContainerService } from 'services/thesis-container.service';

import { OrderTypes } from 'model/order';
import { ByothEvent } from 'model/event';

@Component({
    selector: 'app-metadata',
    templateUrl: './metadata.component.html',
    styleUrls: ['./metadata.component.css']
})
export class MetadataComponent extends BaseFormComponent implements OnInit, AfterViewInit {
    options: { orderType: OrderTypes };
    hasDefense: boolean;
    isFinal: boolean;

    defense: ByothEvent;

    @ViewChildren(ThesisComponent)
    thesisComponent: QueryList<ThesisComponent>;

    @ViewChildren(CandidateComponent)
    candidateComponent: QueryList<CandidateComponent>;

    @ViewChildren(EventComponent)
    defenseComponent: QueryList<EventComponent>;

    constructor(public container: ThesisContainerService) {
        super();
    }

    ngOnInit() {
        this.hasDefense = this.isFinal = this.options && this.options.orderType === OrderTypes.FINAL;

        this.container.defense.get()
        .subscribe(defense => this.defense = defense);
    }

    ngAfterViewInit() {
        this.children = [ this.thesisComponent.first, this.defenseComponent.first, this.candidateComponent.first ];

        const subscription = combineLatest(this.thesisComponent.changes, this.defenseComponent.changes, this.candidateComponent.changes)
        .subscribe(() => {
            this.children = [ this.thesisComponent.first, this.defenseComponent.first, this.candidateComponent.first ];
        });

        this.addSubscription(subscription);
    }
}
