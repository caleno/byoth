import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { MaterialModule } from 'app/material.module';
import { ThesisContainerService } from 'services/thesis-container.service';
import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { MessageService } from 'services/message.service';
import { MetadataComponent } from './metadata.component';

describe('MetadataComponent', () => {
    let component: MetadataComponent;
    let fixture: ComponentFixture<MetadataComponent>;

    class MockContainer {
        defense = new NonpersistentDocumentService();
        thesis = new NonpersistentDocumentService();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ MaterialModule ],
            declarations: [ MetadataComponent ],
            providers: [ { provide: ThesisContainerService, useClass: MockContainer }, MessageService, FormBuilder ],
            schemas: [ NO_ERRORS_SCHEMA ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MetadataComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
