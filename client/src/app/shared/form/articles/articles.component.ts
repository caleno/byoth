import { Component, OnInit, ViewChildren, AfterViewInit, QueryList } from '@angular/core';
import { first } from 'rxjs/operators';

import { Article } from 'model/index';

import { ThesisContainerService } from 'services/thesis-container.service';
import { DocumentService } from 'services/document.service';
import { BaseFormComponent } from 'shared/form/form-component';
import { ArticleComponent } from 'shared/form/article/article.component';

@Component({
    selector: 'app-articles',
    templateUrl: './articles.component.html',
    styleUrls: ['./articles.component.css']
})
export class ArticlesComponent extends BaseFormComponent implements OnInit, AfterViewInit {
    services: DocumentService<Article>[];

    @ViewChildren(ArticleComponent)
    articleComponents: QueryList<ArticleComponent>;

    constructor(private container: ThesisContainerService) {
        super();
    }

    ngOnInit() {
        this.addSubscription(
            this.container.articles.getItemServices()
            .subscribe(services => {
                this.services = services;
            })
        );

        // Depening on context, we may or may not have initialized this service
        this.container.inputFiles.get().subscribe();
    }

    ngAfterViewInit() {
        this.children = this.articleComponents.toArray();

        this.addSubscription(
            this.articleComponents.changes
            .subscribe(changes => {
                this.children = this.articleComponents.toArray();
            })
        );
    }

    addArticle() {
        this.container.articles.post()
        .subscribe(article => {
            this.articleComponents.changes
            .pipe(first())
            .subscribe(changes => {
                setTimeout(() => this.articleComponents.last.open());
            });
        });
    }

    delete(articleService: DocumentService<Article>) {
        this.container.articles.delete(articleService.value)
        .subscribe(() => { });
    }
}
