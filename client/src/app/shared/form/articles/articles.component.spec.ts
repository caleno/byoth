import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';
import { ArticleModule } from '../article/article.module';

import { ThesisContainerService} from 'services/thesis-container.service';
import { CrossrefService } from 'services/crossref.service';
import { ModelService } from 'services/model.service';
import { NonpersistentDocumentSetService, NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { AuthService } from 'services/auth.service';
import { MessageService } from 'services/message.service';

import { ArticlesComponent } from './articles.component';

import { Article } from 'model/article';
import { Candidate } from 'model/candidate';
import { ByothFile } from 'model/byoth-file';

describe('ArticlesComponent', () => {
    let component: ArticlesComponent;
    let fixture: ComponentFixture<ArticlesComponent>;

    class MockContainer {
        articles = new NonpersistentDocumentSetService<Article>({items: []} as any);
        inputFiles = new NonpersistentDocumentSetService<ByothFile>({items: []} as any);
        candidate =  new NonpersistentDocumentService<Candidate>({} as any);
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, HttpClientTestingModule, RouterTestingModule, NoopAnimationsModule,
                       MaterialModule, ArticleModule ],
            declarations: [ ArticlesComponent ],
            providers: [
                { provide: ThesisContainerService, useClass: MockContainer },
                AuthService, MessageService, CrossrefService, ModelService
            ],
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ArticlesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should show one pane per article', fakeAsync(() => {
        component.addArticle();
        fixture.detectChanges();
        tick();
        let articles = fixture.nativeElement.querySelectorAll('app-article');
        expect(articles.length).toBe(1);

        component.addArticle();
        fixture.detectChanges();
        tick();
        articles = fixture.nativeElement.querySelectorAll('app-article');
        expect(articles.length).toBe(2);

        component.delete(component.services[0]);
        fixture.detectChanges();
        tick();
        articles = fixture.nativeElement.querySelectorAll('app-article');
        expect(articles.length).toBe(1);
    }));
});
