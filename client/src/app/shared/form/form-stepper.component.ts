import { Component, Input, AfterViewInit, OnInit,
         ViewChild, ViewChildren, QueryList,
         ChangeDetectorRef, Inject, LOCALE_ID } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { MatHorizontalStepper } from '@angular/material';

import { MessageService } from 'services/message.service';
import { LocalizedString } from '../localized-string';
import { FormStepWrapperComponent } from './form-step-wrapper.component';
import { FormStepperToolbarHostDirective } from './form-stepper-toolbar-host.directive';

/**
 * Component that builds a mat-stepper with BaseFormComponent forms for each step.
 */
@Component({
    selector: 'app-form-stepper',
    templateUrl: './form-stepper.component.html',
    styleUrls: [ './form-stepper.component.scss' ]
})
export class FormStepperComponent implements AfterViewInit, OnInit {
    /** Steps in the formstepper */
    @Input()
    steps: { label: LocalizedString, type: any, options?: any }[];

    @Input()
    saveFunction: () => Observable<void>;

    @ViewChildren(FormStepWrapperComponent)
    children: QueryList<FormStepWrapperComponent>;

    @ViewChild(FormStepperToolbarHostDirective) bottomToolbarHost;

    @ViewChild(MatHorizontalStepper) matStepper;

    hasError: boolean[] = [];

    controls: Observable<AbstractControl>[] = [];

    info$: Observable<string>;

    constructor(protected cdRef: ChangeDetectorRef,
                private message: MessageService,
                private route: ActivatedRoute,
                @Inject(LOCALE_ID) public locale) { }

    ngOnInit() {
        window.scroll(0, 0);
    }

    ngAfterViewInit() {
        this.controls = this.children.toArray().map(child => child.formControl);

        this.hasError = new Array(this.controls.length);
        this.hasError.fill(false);

        this.info$ = this.message.info$;

        this.cdRef.detectChanges();

        this.route.fragment.pipe(first()).subscribe(fragment => {
            if (fragment && fragment.match(/\d+/)) {
                const selectedIndex = parseInt(fragment, 10) - 1;
                this.matStepper.selectedIndex = selectedIndex;
                this.hasError.fill(false);
                this.matStepper.steps.forEach(step => { step.interacted = false; });
                this.cdRef.detectChanges();
            }
        });
    }

    // mark all lower index forms as touched (display error icon if they are skipped and faulty)
    private markPreviousSteps(selectedIndex) {
        if (selectedIndex > 0) {
            for (let i = 0; i < selectedIndex; ++i) {
                this.controls[i].pipe(first()).subscribe(control => {
                    control.markAsTouched();
                    this.hasError[i] = control.invalid;
                });
            }
        }
    }

    /**
     * Handle mat-stepper selection event.
     *
     * Performs four actions:
     *   - Persist all changed values/save.
     *   - Deactivate previous component and activate selected component.
     *   - Mark all steps with lower index as touched and set error status in hasError array
     *   - Scroll to top
     */
    onSelection($event) {
        this.saveFunction()
        .subscribe(() => {
            // activate/deactivate
            if ($event.selectedIndex >= 0) {
                const children = this.children.toArray()

                // deactivate first, to avoid conflicts
                if ($event.previouslySelectedIndex >= 0) {
                    children[$event.previouslySelectedIndex].componentRef.instance.deactivate();
                }

                children[$event.selectedIndex].componentRef.instance.activate();

                window.location.hash = ($event.selectedIndex + 1).toString();

                // the below also works, but overkill? this.router should be Router from @angular/router
                // this.router.navigate(this.router.routerState.snapshot.root.url, { fragment: ($event.selectedIndex + 1).toString() });
            }

            this.markPreviousSteps($event.selectedIndex);

            window.scroll(0, 0);
        });
    }
}
