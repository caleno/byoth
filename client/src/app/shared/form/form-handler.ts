import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CompactedDocument } from 'model/index';
import { DocumentService } from 'services/document.service';

/**
 * Form handler that maps a DocumentService to a reactive form FormGroup.
 *
 * Intended to be used by a BaseFormComponent.
 * The service parameter is optional,
 * and if left out one can have dummy forms to allow validation of non-document entities.
 */
export class FormHandler {
    /** Subscriptions to changes that need to be destroyed when handler is destroyed */
    subscription: Subscription;
    protected form: FormGroup;
    protected service: DocumentService<CompactedDocument>;

    /**
     * Create a new FormHandler
     *
     * @param form The associated form group
     * @param service An optional service
     * @param init If true the form-handler will start listening for changes immediately,
     *      if false the method init() must be called manually
     */
    constructor(form: FormGroup, service?: DocumentService<CompactedDocument>, init = true) {
        if (init) {
            this.init(form, service);
        }
    }

    init(form, service) {
        this.form = form;
        this.service = service;

        this.subscription = this.form.valueChanges
            .subscribe(value => this.patchDocument(value));

        if (this.service) {
            this.service.get()
            .subscribe(doc => this.patchForm(doc));

            this.subscription.add(this.service.valueChanges
                .subscribe(doc => this.patchForm(doc)));
        }
    }

    /** To be called when FormHandler is destroyed */
    destroy() {
        this.subscription.unsubscribe();
    }

    /**
     * Update form based on document.
     *
     * Does not emit a change event to form to avoid infinite loops.
     * Can be specialized for more elaborate cases.
     *
     * Automatically called if the service value changes.
     *
     * @param document The new value.
     */
    patchForm(document: CompactedDocument) {
        if (document) {
            this.form.patchValue(document, {emitEvent: false});
        }
    }

    /**
     * Update document based on form value by a simple `Object.assign()`.
     *
     * This will need to be specialized in nested forms
     * (it will for example overwrite '@type' and '@id' in subnodes).
     *
     * Automatically called if the form changes.
     *
     * @param formValue The new value
     */
    patchDocument(formValue: any) {
        if (this.service) {
            const doc = this.service.value;
            Object.assign(doc, formValue);

            this.service.setValue(doc);
        }
    }

    /** Is the form valid */
    hasErrors(): boolean {
        return !!this.form.invalid;
    }

    /** Mark form as touched */
    touch(): void {
        this.form.markAsTouched();
    }

    /** Access form */
    getForm(): FormGroup {
        return this.form;
    }
}
