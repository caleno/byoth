import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'app/material.module';

import { ThesisContainerService } from 'services/thesis-container.service';
import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { ISBNService } from 'services/isbn.service';
import { AuthService } from 'services/auth.service';

import { ThesisComponent } from './thesis.component';

describe('ThesisComponent', () => {
    let component: ThesisComponent;
    let fixture: ComponentFixture<ThesisComponent>;

    class MockContainer {
        thesis = new NonpersistentDocumentService();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, HttpClientTestingModule, RouterTestingModule, MaterialModule, NoopAnimationsModule ],
            declarations: [ ThesisComponent ],
            providers: [ ISBNService, { provide: ThesisContainerService, useClass: MockContainer }, AuthService ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ThesisComponent);
        component = fixture.componentInstance;

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
