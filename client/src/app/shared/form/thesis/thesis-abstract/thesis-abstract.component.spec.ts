import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';

import { MaterialModule } from 'app/material.module';
import { ThesisAbstractComponent } from './thesis-abstract.component';

describe('ThesisAbstractComponent', () => {
    let component: ThesisAbstractComponent;
    let fixture: ComponentFixture<ThesisAbstractComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ MaterialModule, ReactiveFormsModule, NoopAnimationsModule ],
            declarations: [ ThesisAbstractComponent ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ThesisAbstractComponent);
        component = fixture.componentInstance;
        component.service = new NonpersistentDocumentService({ abstract: { en: 'abstract ' } } as any);
        component.lang = 'en';
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
