import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BaseFormComponent } from 'shared/form/form-component';
import { FormHandler } from 'shared/form/form-handler';
import { DocumentService } from 'services/document.service';

import { Thesis } from 'model/thesis';

class ThesisAbstractFormHandler extends FormHandler {
    constructor(protected form, protected service, private lang: string) {
        super(form, service);
    }

    patchForm(document: Thesis) {
        if (document && document.abstract) {
            this.form.get('abstract').setValue(document.abstract[this.lang], { emitEvent: false });
        }
    }

    patchDocument(formValue: any) {
        if (this.service) {
            const doc = this.service.value;
            doc.abstract[this.lang] = formValue.abstract;

            this.service.setValue(doc);
        }
    }
}

@Component({
    selector: 'app-thesis-abstract',
    templateUrl: './thesis-abstract.component.html',
    styleUrls: ['./thesis-abstract.component.css']
})
export class ThesisAbstractComponent extends BaseFormComponent implements OnInit {
    form: FormGroup;

    @Input()
    service: DocumentService<Thesis>;

    @Input()
    lang: string;

    constructor(private fb: FormBuilder) {
        super();

        this.form = this.fb.group({
            abstract: ['', Validators.required]
        });
    }

    ngOnInit() {
        this.addFormHandler(new ThesisAbstractFormHandler(this.form, this.service, this.lang));
        // don't understand why this is necessary
        this.form.setValue({abstract: this.service.value.abstract[this.lang]});
    }
}
