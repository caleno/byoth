import { Component, OnInit, ViewChildren, QueryList, AfterViewInit, isDevMode, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { ThesisTypes, DegreeTypes, PrintFormats } from 'model/thesis';
import { ISO_LANGUAGES, LanguageDescription } from 'model/languages';
import { OrderTypes } from 'model/order';

import { ThesisContainerService } from 'services/thesis-container.service';
import { BaseFormComponent } from 'shared/form/form-component';
import { FormHandler } from 'shared/form/form-handler';

import { ThesisAbstractComponent } from './thesis-abstract/thesis-abstract.component';

@Component({
    selector: 'app-thesis',
    templateUrl: './thesis.component.html',
    styleUrls: ['./thesis.component.scss'],
})
export class ThesisComponent extends BaseFormComponent implements OnInit, AfterViewInit {
    @Input()
    orderType: OrderTypes;

    hasISBN: boolean;
    isFinal: boolean;

    @ViewChildren(ThesisAbstractComponent)
    abstractComponents: QueryList<ThesisAbstractComponent>;

    form: FormGroup;
    otherLanguages: LanguageDescription[]; // Languages other than nn, nb and en.
    newAbstractLanguage: string;

    // to be used in form
    collectionThesis = ThesisTypes.COLLECTION;
    monographThesis = ThesisTypes.MONOGRAPH;
    a4 = PrintFormats.A4;
    b17x24 = PrintFormats.B17x24;
    phd = DegreeTypes.PHD;
    drPhilos = DegreeTypes.DR_PHILOS;

    devmode = false;

    constructor(private container: ThesisContainerService,
                private fb: FormBuilder) {
        super();

        this.otherLanguages = ISO_LANGUAGES.filter(lang => lang.code !== 'nb' && lang.code !== 'nn' && lang.code !== 'en');

        this.devmode = isDevMode();
    }

    ngOnInit() {
        this.hasISBN = this.isFinal = this.orderType === OrderTypes.FINAL;

        this.form = this.fb.group({
            title: ['', Validators.required],
            subtitle: [''],
            language: [null, Validators.required],
            thesisType: ['', Validators.required],
            degreeType: ['', Validators.required],
            printFormat: ['', Validators.required],
        });

        if (this.hasISBN) {
            // this is not a form element per se in the template, but required for error reporting
            this.form.addControl('isbn', this.fb.control('', Validators.required));
        }

        this.addFormHandler(new FormHandler(this.form, this.container.thesis));

        this.container.thesis.get().subscribe(thesis => {
            if (!thesis.abstract) {
                thesis.abstract = {};
            }

            if (!thesis.abstract.en) {
                thesis.abstract.en = '';
            }

            this.container.thesis.setValue(thesis);
        });
    }

    ngAfterViewInit() {
        this.addSubscription(
            this.abstractComponents.changes.subscribe(() => {
                this.children = this.abstractComponents.toArray();
            })
        );
    }

    get abstractLanguages(): string[] {
        const thesis = this.container.thesis.value;
        if (thesis && thesis.abstract) {
            return Object.keys(this.container.thesis.value.abstract);
        } else {
            return [];
        }
    }

    get unusedOtherAbstractLanguages(): LanguageDescription[] {
        return this.otherLanguages.filter(lang => !this.abstractLanguages.includes(lang.code));
    }

    getLangDescription(langCode): string {
        return ISO_LANGUAGES.find(lang => lang.code === langCode).nativeName;
    }

    hasAbstractLanguage(langCode): boolean {
        return this.abstractLanguages.some(lang => lang === langCode);
    }

    addAbstract() {
        const thesis = this.container.thesis.value;
        thesis.abstract[this.newAbstractLanguage] = '';
        this.container.thesis.setValue(thesis);

        this.newAbstractLanguage = null;
    }

    removeAbstract(langCode) {
        const thesis = this.container.thesis.value;
        delete thesis.abstract[langCode];
        this.container.thesis.setValue(thesis);
    }

    // needed for layout
    get abstractCount(): number {
        return this.abstractLanguages.length;
    }

    assignISBN() {
        this.container.assignISBN().subscribe();
    }

    unassignISBN() {
        this.container.unassignISBN().subscribe();
    }

    get isbn(): string {
        return this.container.thesis && this.container.thesis.value && this.container.thesis.value.isbn;
    }

    deactivate() {
        this.form.get('isbn').markAsTouched();
    }
}

