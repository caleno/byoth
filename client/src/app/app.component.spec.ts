import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { of } from 'rxjs';

import { AppComponent } from './app.component';
import { ToolbarComponent, LOCATION_TOKEN } from './toolbar/toolbar.component';
import { WelcomeComponent } from './welcome/welcome.component';

import { AuthService } from 'services/auth.service';
import { MessageService } from 'services/message.service';
import { ThesisContainerService } from 'services/thesis-container.service';
import { OpenFormGuard } from 'shared/form/open-form.guard';
import { SharedModule } from 'shared/shared.module';
import { MaterialModule } from './material.module';

describe('AppComponent', () => {
    class MockContainer {
        save() { return of(null); }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent, ToolbarComponent, WelcomeComponent
            ],
            imports: [
                RouterTestingModule, HttpClientTestingModule, SharedModule, MaterialModule
            ],
            providers: [
                AuthService, MessageService, OpenFormGuard,
                { provide: ThesisContainerService, useClass: MockContainer },
                { provide: LOCATION_TOKEN, useValue: {} }
            ]
        }).compileComponents();
    }));

    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
});
