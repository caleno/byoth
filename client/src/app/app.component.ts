import { Component, Inject, LOCALE_ID, Renderer2 } from '@angular/core';
import { Meta } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { Observable } from 'rxjs';

import { User, AuthService } from 'services/auth.service';
import { OpenFormGuard } from 'shared/form/open-form.guard';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent {
    title = 'PhD Portal';
    activeComponent: any;
    user$: Observable<User>;

    constructor(private meta: Meta,
                public openForm: OpenFormGuard,
                auth: AuthService,
                @Inject(LOCALE_ID) locale,
                renderer: Renderer2,
                @Inject(DOCUMENT) doc: Document) {
        this.user$ = auth.currentUser();

        renderer.setAttribute(doc.documentElement, 'lang', locale);

        this.meta.addTag({ name: 'description', content: 'Bring your own thesis' });
        this.meta.addTag({ name: 'author', content: 'University of Bergen Library' });
        this.meta.addTag({ name: 'keywords', content: 'PhD Thesis, University of Bergen'});
    }
}
