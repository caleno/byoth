import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from './material.module';

import { SharedModule } from './shared/shared.module';
import { ErrorPageComponent } from './shared/error/error-page.component';

import { ArticlesComponent } from 'shared/form/articles/articles.component';
import { CoverComponent } from 'shared/form/cover/cover.component';
import { FilesComponent } from 'shared/form/files/files.component';
import { ThesisLicenseComponent } from 'shared/form/thesis-license/thesis-license.component';
import { OrderComponent } from 'shared/form/order/order.component';
import { MetadataComponent } from 'shared/form/metadata/metadata.component';

import { PrintFinalComponent } from './print-final/print-final.component';
import { PrintCommitteeComponent } from './print-committee/print-committee.component';

import { PressReleaseModule } from './press-release/press-release.module';
import { PressReleaseFormComponent } from './press-release/press-release-form.component';

import { LoginGuard } from './shared/login-guard';
import { OpenFormGuard } from './shared/form/open-form.guard';
import { SaveFormGuard } from './shared/form/save-form.guard';

import { AppComponent } from './app.component';

import { ThesisContainerService } from './services/thesis-container.service';
import { AuthService } from './services/auth.service';
import { ErrorInterceptor } from './services/error.interceptor';
import { HomeInstitutionService } from './services/home-institution.service';
import { CrossrefService } from './services/crossref.service';
import { ISBNService } from './services/isbn.service';
import { ModelService } from './services/model.service';
import { MessageService } from './services/message.service';
import { SubmissionStatusService } from './services/submission-status.service';

import { WelcomeComponent } from './welcome/welcome.component';
import { ToolbarComponent, ToolbarHelpDialogComponent, LOCATION_TOKEN } from './toolbar/toolbar.component';
import { AfterSubmissionComponent } from './after-submission/after-submission.component';
import { DepositionComponent } from './deposition/deposition.component';

const ROUTES: Routes = [
    {
        path: '',
        component: WelcomeComponent,
    },
    {
        path: 'thesis/final',
        component: PrintFinalComponent,
        canActivate: [ LoginGuard, OpenFormGuard ],
        canDeactivate: [ SaveFormGuard ]
    },
    {
        path: 'press-release',
        component: PressReleaseFormComponent,
        canActivate: [ LoginGuard, OpenFormGuard ],
        canDeactivate: [ SaveFormGuard ]
    },
    {
        path: 'thesis/committee',
        component: PrintCommitteeComponent,
        canActivate: [ LoginGuard, OpenFormGuard ],
        canDeactivate: [ SaveFormGuard ]
    },
    {
        path: 'deposit',
        component: DepositionComponent,
        canActivate: [ LoginGuard, OpenFormGuard ],
        canDeactivate: [ SaveFormGuard ]
    },
    {
        path: '**',
        component: ErrorPageComponent,
        data: { statusCode: 404, message: 'Page not found'}
    }
];

@NgModule({
    declarations: [
        AppComponent,
        WelcomeComponent,
        ToolbarComponent,
        ToolbarHelpDialogComponent,
        PrintCommitteeComponent,
        PrintFinalComponent,
        AfterSubmissionComponent,
        DepositionComponent,
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpClientModule,
        RouterModule.forRoot(ROUTES),
        MaterialModule,
        PressReleaseModule,
        SharedModule
    ],
    providers: [
        ThesisContainerService,
        AuthService,
        HomeInstitutionService,
        CrossrefService,
        ISBNService,
        ModelService,
        MessageService,
        SubmissionStatusService,
        LoginGuard,
        OpenFormGuard,
        SaveFormGuard,
        { provide: LOCALE_ID, useValue: 'nb' },
        { provide: LOCATION_TOKEN, useValue: window.location },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
    ],
    bootstrap: [AppComponent],
    entryComponents: [
        ArticlesComponent,
        CoverComponent,
        FilesComponent,
        ThesisLicenseComponent,
        MetadataComponent,
        OrderComponent,
        ToolbarHelpDialogComponent
    ]
})
export class AppModule { }
