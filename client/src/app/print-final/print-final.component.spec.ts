import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { PrintFinalComponent } from './print-final.component';

import { ThesisContainerService } from 'services/thesis-container.service';
import { MessageService } from 'services/message.service';
import { SubmissionStatusService } from 'services/submission-status.service';

import { MaterialModule } from 'app/material.module';

describe('PrintFinalComponent', () => {
    let component: PrintFinalComponent;
    let fixture: ComponentFixture<PrintFinalComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                PrintFinalComponent,
            ],
            imports: [
                HttpClientTestingModule,
                MaterialModule,
            ],
            providers: [ ThesisContainerService, MessageService, SubmissionStatusService ],
            schemas: [NO_ERRORS_SCHEMA]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PrintFinalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
