import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ThesisContainerService } from 'services/thesis-container.service';
import { MessageService } from 'services/message.service';
import { SubmissionStatusService } from 'services/submission-status.service';

import { OrderTypes } from 'model/order';

import { CoverComponent} from 'shared/form/cover/cover.component';
import { FilesComponent } from 'shared/form/files/files.component';
import { ThesisLicenseComponent } from 'shared/form/thesis-license/thesis-license.component';
import { MetadataComponent } from 'shared/form/metadata/metadata.component';
import { OrderComponent } from 'shared/form/order/order.component';
import { ConfirmPrintComponent } from 'shared/form/confirm-print/confirm-print.component';

@Component({
    selector: 'app-print-final',
    templateUrl: './print-final.component.html',
})
export class PrintFinalComponent implements OnInit {
    isMonograph = false;

    thesisSubscription: Subscription;
    orderType: OrderTypes = OrderTypes.FINAL;

    saveFunction: () => Observable<void>;

    steps = [
        { label: { en: 'Metadata', nb: 'Metadata' }, type: MetadataComponent, options: { orderType: OrderTypes.FINAL } },
        { label: { en: 'Cover', nb: 'Omslag' }, type: CoverComponent, options: { orderType: OrderTypes.FINAL } },
        { label: { en: 'Files', nb: 'Filer' }, type: FilesComponent, options: { orderType: OrderTypes.FINAL } },
        { label: { en: 'BORA', nb: 'BORA' }, type: ThesisLicenseComponent, options: { inPrintForm: true } },
        { label: { en: 'Order', nb: 'Bestilling' }, type: OrderComponent, options: { orderType: OrderTypes.FINAL } },
        { label: { en: 'Review', nb: 'Se gjennom'}, type: ConfirmPrintComponent, options: { orderType: OrderTypes.FINAL } }
    ];

    constructor(private container: ThesisContainerService,
                private message: MessageService,
                private submissionStatus: SubmissionStatusService) {
        this.saveFunction = () =>
            this.container.save()
                .pipe(tap(() => this.message.inform({en: 'Saved all data', nb: 'Lagret alle data'})))

        this.submissionStatus.refresh();
    }

    ngOnInit() { }

    get hasSubmittedAlready$() {
        return this.submissionStatus.finalOrderIsSubmitted$.asObservable();
    }
}
