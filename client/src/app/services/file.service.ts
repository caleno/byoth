import { Observable } from 'rxjs';
import { ByothFile } from 'model/byoth-file';

export interface FileService {
    postFile(file: File): Observable<ByothFile>
}
