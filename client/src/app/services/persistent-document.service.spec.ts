import { of, forkJoin, Observable } from 'rxjs';

import { PersistentDocumentService } from './persistent-document.service';
import { Candidate } from 'model/candidate';

import { ContainerIdService } from './container-id.service';

class MockHttp {
    get(arg) { return of({} as any); }
    put(arg1, arg2) { return of({} as any); }
    post(arg) { return of({} as any); }
    delete(arg) { return  of({} as any); }
    head(arg) { return of({} as any); }
}

describe('PersistentDocumentService', () => {
    let service: PersistentDocumentService<Candidate>;
    let mockHttp: MockHttp;
    let idService: ContainerIdService;

    beforeEach(() => {
        mockHttp = new MockHttp();
        idService = new ContainerIdService(mockHttp as any);
        idService.set('foobar');
        service = new PersistentDocumentService(idService, mockHttp as any, 'candidate');
    });

    it('should return a valid document and update cache', done => {
        spyOn(service, 'setValue').and.callThrough();

        mockHttp.get = () => { return of({familyName: 'bar'}); };

        service.get()
        .subscribe(doc => {
            expect(doc.familyName).toEqual('bar');
            expect(service.setValue).toHaveBeenCalled();
            done();
        });
    });

    it('should update document if changed', () => {
        const value = { familyName: 'bar' };
        const newValue = { familyName: 'baz' };

        const putSpy = spyOn(mockHttp, 'put').and.returnValue(of(newValue));
        spyOn(service, 'setValue').and.callThrough();

        mockHttp.get = () => { return of(value); };

        return service.get().toPromise()
        .then(doc => {
            expect(doc.familyName).toEqual(value.familyName);
            service.setValue(newValue as any);
            return service.save().toPromise();
        })
        .then(savedDoc => {
            expect(putSpy.calls.mostRecent().args[0]).toMatch(/\/api\/container\/\w+\/candidate/)
            expect(putSpy.calls.mostRecent().args[1]).toEqual(newValue);
            expect(service.setValue).toHaveBeenCalledWith(newValue);
            expect(savedDoc.familyName).toEqual(newValue.familyName);
        });
    });

    it('should not update document if it did not change', () => {
        const value = { familyName: 'bar' };

        spyOn(mockHttp, 'put').and.callThrough();
        spyOn(service, 'setValue').and.callThrough();

        mockHttp.get = () => { return of(value); };

        return service.get().toPromise()
        .then(doc => {
            expect(doc).not.toBeNull('first saved document should not be null');
            expect(doc.familyName).toEqual(value.familyName);
            service.setValue(value as any);
            expect(service.setValue).toHaveBeenCalledTimes(2);
            return service.save().toPromise();
        })
        .then(savedDoc => {
            expect(savedDoc).not.toBeNull('second saved document should not be null');
            expect(mockHttp.put).toHaveBeenCalledTimes(0);
            expect(service.setValue).toHaveBeenCalledTimes(2);
            expect(savedDoc.familyName).toEqual(value.familyName);
        });
    });

    it('should get value from cache on repeated get calls', () => {
        spyOn(service, 'setValue').and.callThrough();
        spyOn(idService, 'get').and.callThrough();

        mockHttp.get = () => { return of({familyName: 'bar'}); };

        return service.get().toPromise()
        .then(() => service.get().toPromise())
        .then(doc => {
            expect(doc.familyName).toEqual('bar');
            expect(service.setValue).toHaveBeenCalled();
            expect(idService.get).toHaveBeenCalledTimes(1);
        });
    });

    it('should only get from remote once if two identical request fired at once', () => {
        spyOn(idService, 'get').and.callThrough();
        spyOn(service, 'setValue').and.callThrough();

        mockHttp.get = () => { return of({familyName: 'bar'}); };

        return forkJoin([service.get(), service.get()]).toPromise()
        .then(data => {
            expect(data[0].familyName).toEqual('bar');
            expect(data[1].familyName).toEqual('bar');
            expect(service.setValue).toHaveBeenCalledTimes(1);
            expect(idService.get).toHaveBeenCalledTimes(1);
        });
    });

    it('should do nothing if attempting to save an uninitialized service', () => {
        spyOn(mockHttp, 'put').and.callThrough();

        return service.save().toPromise()
        .then(doc => {
            expect(doc).toBeNull();
            expect(mockHttp.put).not.toHaveBeenCalled();
        });
    });

    it('should retry GET request if server returns lock error', () => {
        let called = false;

        spyOn(mockHttp, 'get').and.callFake(() =>
            Observable.create(observer => {
                if (called) {
                    observer.next({ familyName: 'bar' });
                }
                called = true;
                observer.error({status: 423});
            })
        );

        return service.get().toPromise()
        .then(response => {
            expect(response.familyName).toEqual('bar');
            expect(called).toEqual(true);
        });
    });

    it('should not retry GET request if server returns error that is not lock error', () => {
        let called = false;

        spyOn(mockHttp, 'get').and.callFake(() =>
            Observable.create(observer => {
                if (called) {
                    observer.next({ familyName: 'bar' });
                }
                called = true;
                observer.error({status: 400});
            })
        );

        return service.get().toPromise()
        // should not get a response here ...
        .then(response => expect(false).toEqual(true))
        // but an error
        .catch(err => {
            expect(err.status).toEqual(400);
        });
    });
});
