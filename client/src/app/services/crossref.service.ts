import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { createModelDocument, } from 'model/index';
import { Article, addArticleAuthor, setArticleSeries, setArticleBook,
         JOURNAL_ARTICLE, BOOK_CHAPTER, PROCEEDINGS_ARTICLE } from 'model/article';
import { ModelService } from './model.service';
import * as doiRegex from 'doi-regex';

interface CrossRefResponse {
    status: string,
    message: any
}

@Injectable()
export class CrossrefService {
    public static MAX_AUTHORS = 10;
    // static types = ['journal-article', 'proceedings-article', 'book-section']

    constructor(private http: HttpClient, private model: ModelService) { }

    // Handles 10.123/.., doi:10.123/.. and http://dx.doi.org/10.123/..
    private stripToDoi(doi: string): string {
        if (!doiRegex().test(doi)) {
            return '';
        }

        return doi.match(doiRegex())[0];
    }

    getPublication(urlOrDoi: string, articleId: string): Observable<Article> {
        const doi = this.stripToDoi(urlOrDoi);

        if (!doi) {
            return throwError(Error('Invalid DOI'));
        }

        return forkJoin([
            this.model.get()
                .pipe(map(model => createModelDocument(model.article, articleId))),
            this.http.get('http://api.crossref.org/works/' + doi)
                .pipe(map(res => <CrossRefResponse>res)),
        ]).pipe(map(data => {
            const article: Article = data[0];
            const res: CrossRefResponse = data[1];

            if (res.status !== 'ok') {
                throw new Error(res.message);
            }

            const ref = res.message;

            if (ref.type) {
                switch (ref.type) {
                    case 'book-chapter':
                        article.articleType = BOOK_CHAPTER;
                        break;
                    case 'journal-article':
                        article.articleType = JOURNAL_ARTICLE;
                        break;
                    case 'proceedings-article':
                        article.articleType = PROCEEDINGS_ARTICLE;
                        break;
                    default:
                        throw Error(`Work type '${ref.type}' not supported`);
                }
            } else {
                throw Error(`Can't read reference without type`);
            }

            article.doi = ref.DOI;
            article.crossrefData = JSON.stringify(ref);

            if (ref.title) {
                article.title = ref.title[0];
            }

            if (ref.issue) {
                article.issue = ref.issue;
            }

            if (ref.volume) {
                article.volume = ref.volume;
            }

            if (ref.abstract) {
                article.abstract = ref.abstract;
            }

            if (ref['container-title']) {
                if (ref.ISBN) {
                    setArticleBook(article, ref['container-title'][0], ref.ISBN);
                }

                if (ref.ISSN) {
                    setArticleSeries(article, ref['container-title'][0], ref.ISSN);
                }

                if (!ref.ISSN && !ref.ISBN) {
                    setArticleSeries(article, ref['container-title'][0]);
                }
            }

            if (ref.author) {
                let authors = ref.author;

                if (ref.author.length > CrossrefService.MAX_AUTHORS) {
                    authors = authors.slice(0, CrossrefService.MAX_AUTHORS);
                }

                authors.forEach(author => {
                    if (author.given && author.family) {
                        addArticleAuthor(article, author.given + ' ' + author.family);
                    } else if (author.name) {
                        addArticleAuthor(article, author.name);
                    } else {
                        addArticleAuthor(article, 'N/A');
                    }
                });
            }

            if (ref.page) {
                if (ref.page.match(/^\S+-\S+$/)) {
                    const pages = ref.page.split('-');
                    article.pageStart = pages[0];
                    article.pageEnd = pages[1];
                } else {
                    article.pageStart = ref.page;
                }
            }

            if (ref.issued) {
                if (ref.issued['date-parts']) {
                    try {
                        // const dateString = ref.issued['date-parts'][0]
                        //     .map(part => part.toString().padStart(2, '0')).join('-');
                        article.publicationYear = ref.issued['date-parts'][0][0].toString();
                    } catch (e) {
                        /* istanbul ignore next */
                        console.error(e); // this only happens in Safari, at the time of writing.
                    }
                }
            }

            return article;
        }));
    }
}
