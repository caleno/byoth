import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';

import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'

import { MessageService } from './message.service';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private message: MessageService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler) {
        return next.handle(request).pipe(
            catchError((error: HttpErrorResponse) => {
                switch (error.status) {
                    case 401:
                        this.message.error({
                            en: 'Unauthorized request, try logging in again.',
                            nb: 'Autorisering mangler, prøv å logge inn igjen.'
                        })
                        break;
                    case 404:
                        this.message.error({
                            en: 'Server error. Resource missing.',
                            nb: 'Server-feil. Ressursen finnes ikke.'
                        });
                        break;
                    default:
                        this.message.error({
                            en: `Server error (${error.status}). Try again later. ` +
                                'If the problem does not disappear contact support.',
                            nb: `Server-feil. (${error.status}). Prøv igjen seinere. ` +
                                'Hvis problemet ikke forsvinner, ta kontakt med teknisk support.'
                        })
                }

                return throwError(error);
            })
        );
    }
}
