import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { mergeMap, map, tap, delay } from 'rxjs/operators';

import { CompactedDocument } from 'model/ld-model';

import { retryOnLock } from 'shared/retry-on-lock';

import { DocumentService } from './document.service';
import { NonpersistentDocumentService } from './nonpersistent-document.service';
import { ContainerIdService } from './container-id.service';

/** For a document that is a subnode to a thesis container */
export class PersistentDocumentService<T extends CompactedDocument> extends NonpersistentDocumentService<T> implements DocumentService<T> {
    // last value from server
    protected document: T;

    // keep track of a request is already in progress
    private getting = false;

    constructor(
        protected idService: ContainerIdService,
        protected http: HttpClient,
        protected slug: string) {
        super()
        this.value$ = new BehaviorSubject<T>(null);
    }

    /** Where to get/put document */
    protected path(id: string) {
        return '/api/container/' + id + '/' + this.slug;
    }

    get(): Observable<T> {
        if (this.value) {
            return of(this.value);
        } else {
            return this.refresh();
        }
    }

    refresh(): Observable<T> {
        if (this.getting) {
            // we get lock errors if several requests are fired closely on non-existent document
            return of(null).pipe(delay(200), mergeMap(() => this.get()));
        }

        this.getting = true;
        return this.idService.get()
        .pipe(
            mergeMap(id => this.http.get(this.path(id)).pipe(retryOnLock())),
            map(res => <T>res),
            tap(doc => {
                this.document = JSON.parse(JSON.stringify(doc));
                this.setValue(doc);
                this.getting = false;
            })
        );
    }

    protected put(document: T): Observable<T> {
        if (JSON.stringify(this.document) === JSON.stringify(document)) {
            return of(this.document);
        }

        return this.idService.get()
        .pipe(
            mergeMap(id => this.http.put(this.path(id), document).pipe(retryOnLock())),
            map(doc => <T>doc),
            tap(() => this.document = JSON.parse(JSON.stringify(document))),
            tap(() => this.setValue(document)),
        );
    }

    save(): Observable<T> {
        if (this.value) {
            return this.put(this.value);
        } else {
            return of(null);
        }
    }
}
