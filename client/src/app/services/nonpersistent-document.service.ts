import * as uuid from 'uuid/v4';
import { Observable, BehaviorSubject, of, forkJoin } from 'rxjs';
import { mergeMap, map, tap } from 'rxjs/operators';

import { CompactedDocument } from 'model/ld-model';
import { LdpSet } from 'model/ldp-set';

import { DocumentService, DocumentSetService } from './document.service';

/**
 * Does everything a DocumentService is supposed to do, except that save() does nothing.
 * Can be used for mocking in tests or to extend from by replacing the
 */
export class NonpersistentDocumentService<T extends CompactedDocument> implements DocumentService<T> {
    protected value$ = new BehaviorSubject<T>({} as any);

    constructor(value?: T) {
        if (value) {
            this.setValue(value);
        }
    }

    get value() {
        return JSON.parse(JSON.stringify(this.value$.value));
    }

    get valueChanges(): Observable<T> {
        return this.value$.asObservable();
    }

    setValue(value: T) {
        const clone = JSON.stringify(value);
        if (JSON.stringify(this.value) !== clone) {
            this.value$.next(JSON.parse(clone));
        }
    }

    get() {
        return of(this.value);
    }

    save() {
        return this.get();
    }

    refresh() {
        return this.get();
    }

    reset() {
        this.setValue(null);
    }

    rebroadcast(): void {
        this.value$.next(this.value);
    }
}

/**
 * Does everything a DocumentSetService is supposed to do except that save(), post() and delet() do not persist the changes.
 * Mainly intended for mocking in tests.
 */
export class NonpersistentDocumentSetService<T extends CompactedDocument>
    extends NonpersistentDocumentService<LdpSet>
    implements DocumentSetService<T> {
    protected itemServices$: BehaviorSubject<DocumentService<T>[]> = null

    protected initItemServices() {
        if (!this.itemServices$) {
            this.itemServices$ = new BehaviorSubject([]);
        }

        return this.getArray().pipe(
            map(items => items.map(item => new NonpersistentDocumentService<T>(item))),
            tap(services => this.itemServices$.next(services))
        );
    }

    getItemServices() {
        if (!this.itemServices$) {
            return this.initItemServices()
            .pipe(mergeMap(() => this.itemServices$.asObservable()));
        }
        return this.itemServices$;
    }

    getArray() {
        return this.get().pipe(map(res => res && <T[]>res.items || []));
    }

    private setArray(items: T[]) {
        const set = this.value;
        set.items = items;
        this.setValue(set);
    }

    post() {
        const set = this.value;
        const newItem = { '@id': uuid() } as T;

        if (set.items) {
            set.items.push(newItem);
        } else {
            set.items = [ newItem ];
        }

        this.setValue(set);

        return this.initItemServices()
        .pipe(mergeMap(() => of(newItem)));
    }

    delete(document: T) {
        const set = this.value;
        set.items = set.items.filter(item => item['@id'] !== document['@id']);
        this.setValue(set);
        return this.initItemServices()
        .pipe(mergeMap(() => of(null)));
    }

    save(): Observable<LdpSet> {
        if (!this.itemServices$) {
            return of(this.value);
        }

        return this.itemServices$.pipe(
            mergeMap(services => forkJoin(services.map(service => service.save()))),
            tap(data => this.setArray(data)),
            mergeMap(() => this.get())
        );
    }

   reset() {
        this.setValue(null);
        this.itemServices$.next([]);
    }

    reorder(documentIds: string[]): Observable<string[]> {
        this.setArray(documentIds.map(id => { return <T>{ '@id': id }; }));
        return this.initItemServices().pipe(mergeMap(() => of(documentIds)));
    }
}
