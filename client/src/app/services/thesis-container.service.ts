import { HttpClient} from '@angular/common/http';
import { CanActivate } from '@angular/router';
import { Injectable } from '@angular/core';

import { Observable, forkJoin } from 'rxjs';
import { mergeMap, map, tap } from 'rxjs/operators';
import { ErrorObject } from 'ajv';

import { CompactedDocument } from 'model/ld-model';
import { TrialLecture } from 'model/trial-lecture';
import { Defense } from 'model/defense';
import { Department } from 'model/department';
import { ByothFile } from 'model/byoth-file';
import { Thesis } from 'model/thesis';
import { Candidate } from 'model/candidate';
import { ThesisContainer } from 'model/thesis-container';
import { Order, OrderTypes } from 'model/order';

import { DocumentService } from './document.service';

import { ContainerIdService } from './container-id.service';
import { PersistentDocumentService } from './persistent-document.service';
import { FileSetService } from './file-set.service';
import { ArticleSetService } from './article-set.service';

import { base64url } from 'shared/base64url';
import { retryOnLock } from 'shared/retry-on-lock';

export interface ValidationReport {
    valid: boolean;
    errors: ErrorObject[];
    document: ThesisContainer;
}

function orderType2Slug(type: OrderTypes) {
    return type === OrderTypes.FINAL ? 'final' : 'evaluation';
}

/**
 * Service for a thesis container object.
 *
 * Will give the thesis container that belongs to the currently logged in user.
 *
 * Also gives access to all sub-containers.
 * Saving or deleting on this class will also save or delete all subcontainers (if they have changed),
 * and reset any observables these expose.
 */
@Injectable()
export class ThesisContainerService implements CanActivate {
    id: ContainerIdService;

    /** Service for the container's candidate */
    candidate: DocumentService<Candidate>;

    /** Service for the container's defense */
    defense: DocumentService<Defense>;

    /** Service for the container's department */
    department: DocumentService<Department>;

    /** Service for the container's thesis */
    thesis: DocumentService<Thesis>;

    /** Service for the container's trial lecture */
    trialLecture: DocumentService<TrialLecture>;

    /** Service for the container's final order */
    finalOrder: DocumentService<Order>;

    /** Service for the container's evaluation order */
    evaluationOrder: DocumentService<Order>;

    /** Service for the container's input files */
    inputFiles: FileSetService;

    /** Service for the container's articles */
    articles: ArticleSetService;

    private properties: DocumentService<CompactedDocument>[];

    constructor(private http: HttpClient) {
        this.id = new ContainerIdService(http);

        this.candidate = new PersistentDocumentService<Candidate>(this.id, this.http, 'candidate');
        this.thesis = new PersistentDocumentService<Thesis>(this.id, this.http, 'thesis');
        this.department = new PersistentDocumentService<Department>(this.id, this.http, 'department');
        this.defense = new PersistentDocumentService<Defense>(this.id, this.http, 'defense');
        this.trialLecture = new PersistentDocumentService<TrialLecture>(this.id, this.http, 'trialLecture');
        this.finalOrder = new PersistentDocumentService<Order>(this.id, this.http, 'finalOrder');
        this.evaluationOrder = new PersistentDocumentService<Order>(this.id, this.http, 'evaluationOrder');

        this.articles = new ArticleSetService(this.id, this.http, 'articles');
        this.inputFiles = new FileSetService(this.id, this.http, 'inputFiles');

        this.properties = [
            this.candidate,
            this.thesis,
            this.department,
            this.defense,
            this.evaluationOrder,
            this.finalOrder,
            this.trialLecture,
            this.articles,
            this.inputFiles
        ];
    }

    canActivate() {
        return this.id.get()
        .pipe(map(id => !!id));
    }

    save(): Observable<any> {
        return forkJoin(this.properties.map(property => property.save()));
    }

    create(): Observable<any> {
        return this.http.post('/api/user/container', null)
        .pipe(
            map(res => (<any>res).containerId),
            tap(containerId => this.id.set(containerId))
        );
    }

    get() {
        return this.id.get()
        .pipe(
            mergeMap(id => {
                if (!id) {
                    throw Error('No thesis to get');
                }
                return this.http.get('/api/container/' + id);
            }),
            map(res => <ThesisContainer>res)
        );
    }

    put(container: ThesisContainer): Observable<any> {
        return this.id.get()
        .pipe(
            mergeMap(id => {
                if (!id) {
                    throw Error('No thesis to update');
                }
                return this.http.put('/api/container/' + id, container);
            })
        );
    }

    delete(): Observable<any> {
        return this.id.get()
        .pipe(
            mergeMap(id => {
                if (!id) {
                    throw Error('No thesis to delete')
                }
                return this.http.delete('/api/container/' + id);
            }),
            tap(() => this.id.set(null)),
            tap(() => this.properties.forEach(prop => prop.reset()))
        );
    }

    hasContainer(): Observable<boolean> {
        return this.http.head('/api/user/container', {observe: 'response', responseType: 'arraybuffer'})
        .pipe(
            map(res => {
                if (res.status === 200) {
                    return true;
                } else if (res.status === 204) {
                    return false;
                } else {
                    throw new Error('Error accessing HEAD /api/user/container')
                }
            })
        );
    }

    postPriceRequest(order: Order, type: OrderTypes): Observable<Order> {
        return this.id.get()
        .pipe(
            mergeMap(id =>
                this.http.post('/api/container/' + id + '/' + orderType2Slug(type) + '/price-request', order)
                .pipe(retryOnLock())
            ),
            map(updatedOrder => <Order>updatedOrder)
        );
    }

    validateOrder(type: OrderTypes): Observable<ValidationReport> {
        return this.id.get()
        .pipe(
            mergeMap(id =>
                this.http.post('/api/container/' + id + '/' + orderType2Slug(type) + '/validate', {})
            ),
            map(result => <any>result)
        );
    }

    submitOrder(type: OrderTypes): Observable<Order> {
        return this.id.get()
        .pipe(
            mergeMap(id =>
                this.http.post('/api/container/' + id + '/' + orderType2Slug(type) + '/submit', {}, { params: { isTest: 'true'} })
                .pipe(retryOnLock())
            ),
            map(order => <Order>order)
        );
    }

    cancelOrder(type: OrderTypes): Observable<Order> {
        return this.id.get()
        .pipe(
            mergeMap(id =>
                this.http.delete('/api/container/' + id + '/' + orderType2Slug(type) + '/submit')
                .pipe(retryOnLock())
            ),
            map(order => <Order>order)
        );
    }

    orderIsSubmitted(type: OrderTypes): Observable<boolean> {
        return this.id.get()
        .pipe(
            mergeMap(id => this.http.head(
                '/api/container/' + id + '/' + orderType2Slug(type) + '/submit',
                { observe: 'response', responseType: 'arraybuffer'})
            ),
            map(res => {
                if (res.status === 200) {
                    return true;
                } else if (res.status === 204) {
                    return false;
                }
            })
        );
    }

    assignISBN(): Observable<void> {
        return this.id.get()
        .pipe(
            mergeMap(id =>
                this.http.post('/api/container/' + id + '/thesis/assign-isbn', {})
                .pipe(retryOnLock())
            ),
            map(thesisWithIsbn => {
                const thesis = this.thesis.value;
                thesis.isbn = (<Thesis>thesisWithIsbn).isbn;
                this.thesis.setValue(thesis);
            })
        );
    }

    unassignISBN(): Observable<void> {
        return this.id.get()
        .pipe(
            mergeMap(id =>
                this.http.post('/api/container/' + id + '/thesis/unassign-isbn', {})
                .pipe(retryOnLock())
            ),
            map(thesisWithoutIsbn => {
                const thesis = this.thesis.value;
                thesis.isbn = null;
                this.thesis.setValue(thesis);
            })
        );
    }

    /* Returns pdf as Uint8Array */
    coverPdfPreview(type: OrderTypes, regenerate = true): Observable<Uint8Array> {
        return this.coverPdfPreviewDownloadURL(type, regenerate)
        .pipe(
            mergeMap(url => this.http.get(url, { responseType: 'arraybuffer'})),
            map(buffer => new Uint8Array(buffer))
        );
    }

    coverPdfPreviewDownloadURL(type: OrderTypes, regenerate = true): Observable<string> {
        return this.id.get()
        .pipe(map(id => '/api/container/'  + id + '/' + orderType2Slug(type) +
            '/cover-preview/pdf?regenerate=' + regenerate.toString()));
    }

    /* return jpg as base64 strings */
    coverThumbnails(type: OrderTypes, regenerate = true): Observable<string[]> {
        return this.id.get()
        .pipe(
            mergeMap(id =>
                this.http.get('/api/container/'  + id + '/' + orderType2Slug(type) +
                    '/cover-preview/png?regenerate=' + regenerate.toString())
            ),
            map(res => <string[]>res)
        );
    }

    contentPdfPreviewDownloadURL(type: OrderTypes, regenerate = true): Observable<string> {
        return this.id.get()
        .pipe(map(id => '/api/container/'  + id + '/' + orderType2Slug(type) +
            '/content-preview/pdf?regenerate=' + regenerate.toString()));
    }

    fileUrl(fileId: string): Observable<string> {
        return this.id.get()
        .pipe(map(id => '/api/container/' + id + '/files/' + base64url.encode(fileId)));
    }

    fileMetadata(fileId: string): Observable<ByothFile> {
        return this.id.get()
        .pipe(
            mergeMap(id => this.http.get('/api/container/' + id + '/files/' + base64url.encode(fileId) + '/metadata')),
            map(res => <ByothFile>res)
        );
    }
}
