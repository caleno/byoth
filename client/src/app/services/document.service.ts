import { Observable } from 'rxjs';

import { CompactedDocument } from 'model/ld-model';
import { LdpSet } from 'model/ldp-set';

/** A service that can be used to access, update and persist a document */
export interface DocumentService<T extends CompactedDocument> {
    /** Observable for the document */
    valueChanges: Observable<T>;

    /** Get an instant value of the document */
    value: T;

    /** Set the value of the document */
    setValue(document: T): void;

    /** Get the document once */
    get(): Observable<T>;

    /** Persist the document */
    save(): Observable<T>;

    /** Update the value to the latest from the store */
    refresh(): Observable<T>;

    /** Reset to null document */
    reset();

    /**
     * Broadcast the current value again
     */
    rebroadcast();
}

/** A service that can be used to access, add to and delete from a set of documents */
export interface DocumentSetService<T extends CompactedDocument> extends DocumentService<LdpSet> {
    /** Get DocumentServices for all the items of the set */
    getItemServices(): Observable<DocumentService<T>[]>;

    /** Create a new item in the set */
    post(): Observable<T>;

    /** Delete an item from the set (based on the document id) */
    delete(document: T): Observable<any>;

    /** Persist all items of the set */
    save(): Observable<any>;

    /** Reset all values */
    reset();
}

export interface OrderedDocumentSetService<T extends CompactedDocument> extends DocumentSetService<T> {
    /** Post new order of document ids */
    reorder(ids: string[]);
}
