import { Observable } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';

import { Article } from 'model/article';
import { ByothFile } from 'model/byoth-file';

import { PersistentDocumentSetService } from './persistent-document-set.service';
import { FileService } from './file.service';
import { retryOnLock } from 'shared/retry-on-lock';

import { base64url } from 'shared/base64url';

export class ArticleSetService extends PersistentDocumentSetService<Article> {
    private articleApiRoute(containerId: string, articleId: string) {
        return this.documentSetApiRoute(containerId) + '/' + base64url.encode(articleId);
    }

    private postArticleFile(articleId: string, file: File): Observable<ByothFile> {
        const formData = new FormData();
        formData.append('file', file);
        return this.idService.get()
        .pipe(
            mergeMap(id => this.save().pipe(map(() => id))),
            mergeMap(id =>
                <Observable<ByothFile>> this.http.post(this.articleApiRoute(id, articleId) + '/file', formData)
                .pipe(retryOnLock())
            ),
            mergeMap(doc => this.refreshItemService(articleId).pipe(map(() => doc)))
        );
    }

    getArticleFileService(articleId: string): FileService {
        return {
            postFile: (file: File) => this.postArticleFile(articleId, file),
        };
    }

    deleteArticleFile(articleId: string): Observable<any> {
        return this.idService.get()
        .pipe(
            mergeMap(id => this.save().pipe(map(() => id))),
            mergeMap(id => this.http.delete(this.articleApiRoute(id, articleId) + '/file')),
            mergeMap(() => this.refreshItemService(articleId))
        );
    }
}
