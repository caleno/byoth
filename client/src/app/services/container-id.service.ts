import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { base64url } from 'shared/base64url';

/** Get the current container ID based on a the user session. */
export class ContainerIdService {
    private id: string;

    constructor(private http: HttpClient) {}

    /** Returns a URL-encoded ID */
    get(): Observable<string> {
        if (this.id) {
            return of(base64url.encode(this.id));
        }

        return this.http.get('/api/user', {observe: 'response', responseType: 'json'})
        .pipe(
            map(res => {
                if (res.status === 200) {
                    this.id = res.body['containerId'];
                    if (this.id) {
                        return base64url.encode(this.id);
                    } else {
                        return null;
                    }
                } else {
                    throw new Error('Error accessing GET /api/user');
                }
            })
        );
    }

    /** Set ID value (input should not be URL-encoded) */
    set(id: string) {
        this.id = id;
    }
}
