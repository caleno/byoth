import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of, forkJoin } from 'rxjs';
import { mergeMap, map, tap, first } from 'rxjs/operators';

import { CompactedDocument } from 'model/ld-model';
import { LdpSet } from 'model/ldp-set';

import { retryOnLock } from 'shared/retry-on-lock';
import { base64url } from 'shared/base64url';

import { DocumentService, DocumentSetService } from './document.service';
import { ContainerIdService } from './container-id.service';
import { PersistentDocumentService } from './persistent-document.service';

/** For a document in a set */
class DocumentSetItemService<T extends CompactedDocument> extends PersistentDocumentService<T> {
    constructor(idService: ContainerIdService,
                http: HttpClient,
                document: T,
                containerSlug: string) {
        super(idService, http, containerSlug);
        this.setValue(document);
        this.document = document;
    }

    protected path(id: string) {
        return super.path(id) + '/' + base64url.encode(this.document['@id']);
    }
}

/** For a document that is a subnode to a thesis container and has an array of subnodes itself */
export class PersistentDocumentSetService<T extends CompactedDocument>
    extends PersistentDocumentService<LdpSet> implements DocumentSetService<T> {
    protected itemServices$: BehaviorSubject<DocumentService<T>[]> = null;

    constructor(idService: ContainerIdService,
                http: HttpClient,
                protected containerSlug: string) {
        super(idService, http, containerSlug);
    }

    protected documentSetApiRoute(baseContainerId: string) {
        return '/api/container/' + baseContainerId + '/' + this.containerSlug;
    }

    getArray(): Observable<T[]> {
        return this.get()
        .pipe(map(res => res && <T[]>res.items || []));
    }

    protected initItemServices(): Observable<DocumentService<T>[]> {
        if (!this.itemServices$) {
            this.itemServices$ = new BehaviorSubject<DocumentService<T>[]>([]);
        }

        return this.getArray()
        .pipe(
            map(data => data.map(item =>
                new DocumentSetItemService<T>(this.idService, this.http, item, this.containerSlug))
            ),
            tap(services => this.itemServices$.next(services))
        );
    }

    getItemService(documentId: string): Observable<DocumentService<T>> {
        return this.itemServices$.pipe(
            first(),
            map(services => services.find(service => service.value['@id'] === documentId))
        );
    }

    refresh() {
        return super.refresh().pipe(
            mergeMap((set) => this.initItemServices().pipe(map(() => set)))
        );
    }

    protected refreshItemService(documentId: string): Observable<T> {
        return this.getItemService(documentId).pipe(
            mergeMap(service => {
                if (service) {
                    return service.refresh();
                }
            })
        );
    }

    getItemServices(): Observable<DocumentService<T>[]> {
        if (!this.itemServices$) {
            return this.initItemServices()
            .pipe(mergeMap(() => this.itemServices$.asObservable()));
        }

        return this.itemServices$.asObservable();
    }

    /**
     * Reorder items in set.
     *
     * Needs to be followed by a refresh() to update item services.
     */
    reorder(documentIds: string[]): Observable<any> {
        return this.idService.get()
        .pipe(
            mergeMap(id =>
                <Observable<string[]>> this.http.put(this.documentSetApiRoute(id) + '/order', documentIds)
                .pipe(retryOnLock())
            )
        );
    }

    post(): Observable<T> {
        return this.idService.get()
        .pipe(
            mergeMap(id =>
                <Observable<T>> this.http.post(this.documentSetApiRoute(id), null)
                .pipe(retryOnLock())),
            mergeMap(doc => this.refresh().pipe(map(() => doc)))
        );
    }

    delete(document: T): Observable<any> {
        return this.idService.get()
        .pipe(
            mergeMap(id => this.http.delete(this.documentSetApiRoute(id) + '/' + base64url.encode(document['@id']))),
            mergeMap(() => this.refresh())
        );
    }

    save(): Observable<LdpSet> {
        if (!this.itemServices$) {
            return of(this.value);
        }

        return this.itemServices$.pipe(
            first(),
            mergeMap(services => {
                if (services.length > 0) {
                    return forkJoin(services.map(service => service.save()))
                    .pipe(mergeMap(() => this.get()));
                }
                return of(this.value);
            }),
        );
    }

    reset() {
        this.setValue(null);
        if (this.itemServices$) {
            this.itemServices$.value.forEach(item => item.reset());
            this.itemServices$.next([]);
        }
    }
}

