import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { BehaviorSubject} from 'rxjs';
import { MatSnackBar } from '@angular/material';

import { LocalizedString } from 'shared/localized-string';

export enum MessageType { ERROR, WARNING, INFO };

@Injectable()
export class MessageService {
    info$: BehaviorSubject<string> = new BehaviorSubject<string>('');
    warning$: BehaviorSubject<string> = new BehaviorSubject<string>('');
    error$: BehaviorSubject<string> = new BehaviorSubject<string>('');

    private messagePrefix: string[] = [];
    private closeString: string;

    static messageClass(type: MessageType) {
        switch (type) {
            case MessageType.WARNING:
                return 'warning';
            case MessageType.ERROR:
                return 'error';
            case MessageType.INFO:
            default:
                return 'info';
        }
    }

    constructor(private snackbar: MatSnackBar, @Inject(LOCALE_ID) private locale: string) {
        if (locale === 'nb') {
            this.messagePrefix[MessageType.ERROR] = 'Feil: ';
            this.messagePrefix[MessageType.WARNING] = 'Advarsel: ';
            this.messagePrefix[MessageType.INFO] = ''
            this.closeString = 'LUKK';
        } else {
            this.messagePrefix[MessageType.ERROR] = 'Error: ';
            this.messagePrefix[MessageType.WARNING] = 'Warning: ';
            this.messagePrefix[MessageType.INFO] = ''
            this.closeString = 'CLOSE';
        }
    }

    private publish(message: string, type: MessageType, duration: number) {
        this.snackbar.open(this.messagePrefix[type] + message, this.closeString,
            {
                panelClass: MessageService.messageClass(type),
                duration: duration
            }
        );
    }

    private localize(message: LocalizedString) {
        return message[this.locale] || message['en'];
    }

    /**
     * Publish an information message with a duration.
     * If the duration is 0, it will be visible until a new message is publish.
     */
    inform(message: LocalizedString, duration = 5000) {
        const localizedMessage = this.localize(message);

        this.info$.next(localizedMessage);

        if (duration) {
            setTimeout(() => this.info$.next(''), duration);
        }

        console.log(message);
    }

    warn(message: LocalizedString, duration = 5000) {
        const localizedMessage = this.localize(message);
        this.publish(localizedMessage, MessageType.WARNING, duration);
        this.warning$.next(localizedMessage);
        console.warn(message);
    }

    error(message: LocalizedString, duration = 5000) {
        const localizedMessage = this.localize(message);
        this.publish(localizedMessage, MessageType.ERROR, duration);
        this.error$.next(localizedMessage);
        console.error(message);
    }
}
