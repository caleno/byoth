import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { ThesisContainerService } from './thesis-container.service';
import { base64url } from 'shared/base64url';
import { OrderTypes } from 'model/order';

class MockHttp {
    get(arg) { return of({} as any); }
    put(arg1, arg2) { return of({} as any); }
    post(arg) { return of({} as any); }
    delete(arg) { return  of({} as any); }
    head(arg) { return of({} as any); }
}

describe('ThesisContainerService', () => {
    let service: ThesisContainerService;
    let mockHttp: MockHttp;

    beforeEach(() => {
        mockHttp = new MockHttp();
        service = new ThesisContainerService(mockHttp as any);
    });

    it('should return container id and set id property when creating container', done => {
        spyOn(mockHttp, 'post').and.returnValue(of({containerId: 'foo'}));
        service.create()
        .pipe(
            map(id => expect(id).toEqual('foo')),
            mergeMap(() => service.id.get()))
        .subscribe(id => {
            expect(id).toEqual(base64url.encode('foo'));
            done();
        });
    });

    it('should get container for correct id', done => {
        const response = 'foo' as any;
        service.id.set('bar');
        const spy = spyOn(mockHttp, 'get').and.returnValue(of(response));

        service.get()
        .subscribe(res => {
            expect(spy).toHaveBeenCalledWith('/api/container/' + base64url.encode('bar'));
            expect(res).toEqual(response);
            done();
        });
    });

    it('should get error if no id available when requesting container', done => {
        const spy = spyOn(mockHttp, 'get');
        spyOn(service.id, 'get').and.returnValue(of(null));

        service.get()
        .subscribe(() => {}, err => {
            expect(err.message).toMatch('No thesis');
            expect(spy).not.toHaveBeenCalled();
            done();
        });
    });

    it('should deny canActivate if no id', done => {
        spyOn(mockHttp, 'get').and.returnValue(of({status: 200, body: {}}));

        service.canActivate()
        .subscribe(res => {
            expect(res).toBe(false);
            done();
        });
    });

    it('should allow canActivate if id', done => {
        spyOn(mockHttp, 'get').and.returnValue(of({status: 200, body: {containerId: 'foo'}}));

        service.canActivate()
        .subscribe(res => {
            expect(res).toBe(true);
            done();
        });
    });

    it('should be able to update container if id available', done => {
        const response = 'foo';
        service.id.set('bar');
        const spy = spyOn(mockHttp, 'put').and.returnValue(of(response));

        service.put({} as any)
        .subscribe(res => {
            expect(spy).toHaveBeenCalledWith('/api/container/' + base64url.encode('bar'), {});
            expect(res).toEqual(response);
            done();
        });
    });

    it('should not be able to update container without id', done => {
        const spy = spyOn(mockHttp, 'put');
        spyOn(service.id, 'get').and.returnValue(of(null));

        service.put({} as any)
        .subscribe(() => {}, err => {
            expect(err.message).toMatch('No thesis');
            expect(spy).not.toHaveBeenCalled();
            done();
        });
    });

    it('should be able to delete container if id available and reset properties', done => {
        const response = 'foo';
        service.id.set('bar');
        const spy = spyOn(mockHttp, 'delete').and.returnValue(of(response));
        spyOn(service.articles, 'reset').and.callThrough();
        spyOn(service.candidate, 'reset').and.callThrough();

        service.delete()
        .subscribe(res => {
            expect(spy).toHaveBeenCalledWith('/api/container/' + base64url.encode('bar'));
            expect(res).toEqual(response);
            expect(service.articles.reset).toHaveBeenCalled();
            expect(service.candidate.reset).toHaveBeenCalled();
            done();
        });
    });

    it('should not be able to delete container without id', done => {
        const spy = spyOn(mockHttp, 'delete');
        spyOn(service.id, 'get').and.returnValue(of(null));

        service.delete()
        .subscribe(() => {}, err => {
            expect(err.message).toMatch('No thesis');
            expect(spy).not.toHaveBeenCalled();
            done();
        });
    });

    it('should have container if HEAD /api/container has status 200', done => {
        spyOn(mockHttp, 'head').and.returnValue(of({status: 200}));

        service.hasContainer()
        .subscribe(res => {
            expect(res).toBe(true);
            done();
        });
    });

    it('should not have container if HEAD /api/container has status 204', done => {
        spyOn(mockHttp, 'head').and.returnValue(of({status: 204}));

        service.hasContainer()
        .subscribe(res => {
            expect(res).toBe(false);
            done();
        });
    });

    it('should get error if HEAD /api/container returns other status than 200 or 204', done => {
        spyOn(mockHttp, 'head').and.returnValue(of({status: 400}));

        service.hasContainer()
        .subscribe(() => {}, err => {
            expect(err.message).toMatch('Error accessing HEAD');
            done();
        });
    });

    it('should save properties when save() is called', done => {
        spyOn(service.articles, 'save').and.callThrough();
        spyOn(service.candidate, 'save').and.callThrough();
        service.save()
        .subscribe(() => {
            expect(service.articles.save).toHaveBeenCalled();
            expect(service.candidate.save).toHaveBeenCalled();
            done();
        });
    });

    // The following five tests don't really make much sense, but allow us to traverse a few more lines
    it('should post final order price request using postPriceRequest()', done => {
        service.id.set('foo');
        const spy = spyOn(mockHttp, 'post').and.returnValue(of({}));
        service.postPriceRequest('foo' as any, OrderTypes.FINAL)
        .subscribe(order => {
            expect(spy.calls.argsFor(0)[0]).toMatch(/final\/price-request$/);
            expect(spy.calls.argsFor(0)[1]).toEqual('foo');
            done();
        });
    });

    it('should submit final order using submitOrder()', done => {
        service.id.set('foo');
        const spy = spyOn(mockHttp, 'post').and.returnValue(of({}));
        service.submitOrder(OrderTypes.FINAL)
        .subscribe(order => {
            expect(spy.calls.argsFor(0)[0]).toMatch(/final\/submit$/);
            done();
        });
    });

    it('should cancel final order using cancelOrder', done => {
        service.id.set('foo');
        const spy = spyOn(mockHttp, 'delete').and.returnValue(of({}));
        service.cancelOrder(OrderTypes.FINAL)
        .subscribe(order => {
            expect(spy.calls.argsFor(0)[0]).toMatch(/final\/submit$/);
            done();
        });
    });

    it('should post evaluation order price request using postPriceRequest()', done => {
        service.id.set('foo');
        const spy = spyOn(mockHttp, 'post').and.returnValue(of({}));
        service.postPriceRequest('foo' as any, OrderTypes.EVALUATION)
        .subscribe(order => {
            expect(spy.calls.argsFor(0)[0]).toMatch(/evaluation\/price-request$/);
            expect(spy.calls.argsFor(0)[1]).toEqual('foo');
            done();
        });
    });

    it('should submit evaluation order using submitOrder()', done => {
        service.id.set('foo');
        const spy = spyOn(mockHttp, 'post').and.returnValue(of({}));
        service.submitOrder(OrderTypes.EVALUATION)
        .subscribe(order => {
            expect(spy.calls.argsFor(0)[0]).toMatch(/evaluation\/submit$/);
            done();
        });
    });

    it('should cancel evaluation order using cancelOrder', done => {
        service.id.set('foo');
        const spy = spyOn(mockHttp, 'delete').and.returnValue(of({}));
        service.cancelOrder(OrderTypes.EVALUATION)
        .subscribe(order => {
            expect(spy.calls.argsFor(0)[0]).toMatch(/evaluation\/submit$/);
            done();
        });
    });

    it('should assign ISBN using assignISBN()', done => {
        service.id.set('foo');
        const spy = spyOn(mockHttp, 'post').and.returnValue(of({}));
        service.thesis = { value: {} as any, setValue: () => {}} as any;
        service.assignISBN()
        .subscribe(order => {
            expect(spy.calls.argsFor(0)[0]).toMatch(/thesis\/assign-isbn$/);
            done();
        });
    });

    it('should unassign ISBN using unassignISBN()', done => {
        service.id.set('foo');
        const spy = spyOn(mockHttp, 'post').and.returnValue(of({}));
        service.thesis = { value: {} as any, setValue: () => {}} as any;
        service.unassignISBN()
        .subscribe(order => {
            expect(spy.calls.argsFor(0)[0]).toMatch(/thesis\/unassign-isbn$/);
            done();
        });
    });

    describe('ContainerIdService', () => {
        it('should return container id from api', done => {
            spyOn(mockHttp, 'get').and.returnValue(of({status: 200, body: {containerId: 'foo'}}));
            service.id.get()
            .subscribe(id => {
                expect(id).toEqual(base64url.encode('foo'));
                done();
            });
        });

        it('should return null if api return no ID', done => {
            spyOn(mockHttp, 'get').and.returnValue(of({status: 200, body: {}}));
            service.id.get()
            .subscribe(id => {
                expect(id).toBeNull();
                done();
            });
        });

        it('should fail if API fails', done => {
            spyOn(mockHttp, 'get').and.returnValue(of({status: 500, body: {}}));
            service.id.get()
            .pipe(catchError(err => of(err)))
            .subscribe(err => {
                expect(err.message).toMatch('Error accessing');
                done();
            });
        });
    });
});
