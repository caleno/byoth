import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ISBNService {
    constructor(private http: HttpClient) { }

    getNewISBN(): Observable<string> {
        return this.http.get('/api/new-isbn')
        .pipe(map(res => res['isbn']));
    }
}
