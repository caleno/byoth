import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ThesisContainerService } from './thesis-container.service';
import { OrderTypes } from 'model/order';

@Injectable()
export class SubmissionStatusService {
    finalOrderIsSubmitted$ = new BehaviorSubject<boolean>(null);
    evaluationOrderIsSubmitted$ = new BehaviorSubject<boolean>(null);

    constructor(private container: ThesisContainerService) {
        this.refresh();
    }

    refresh() {
        this.container.orderIsSubmitted(OrderTypes.FINAL)
            .subscribe(value => this.finalOrderIsSubmitted$.next(value));
        this.container.orderIsSubmitted(OrderTypes.EVALUATION)
            .subscribe(value => this.evaluationOrderIsSubmitted$.next(value));
    }
}
