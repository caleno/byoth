import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, publishReplay, refCount, take } from 'rxjs/operators';

import { AddressProperties } from 'model/order';

export interface Place {
    id: string;
    name: {'@language': string, '@value': string}[];
    address: AddressProperties,
}

export type Faculty = Place;

export type Department = Place;

export interface NamedEntity {
    id: string;
    name: string;
}

@Injectable()
export class HomeInstitutionService {
    faculties$: Observable<Faculty[]>;
    departments$: Observable<Department[]>;
    areas$: Observable<NamedEntity[]>;

    constructor(private http: HttpClient) {
        this.faculties$ = this.http.get('/api/faculty')
        .pipe(
            map(res => <Faculty[]>res),
            publishReplay(1),
            refCount(),
            take(1)
        );

        this.departments$ = this.http.get('/api/departments')
        .pipe(
            map(res => <Department[]>res),
            publishReplay(1),
            refCount(),
            take(1)
        );

        this.areas$ = this.http.get('/api/areas')
        .pipe(
            map(res => <NamedEntity[]>res),
            publishReplay(1),
            refCount(),
            take(1)
        );
    }

    getFaculties(): Observable<Faculty[]> {
        return this.faculties$;
    }

    getDepartments(facultyId): Observable<Department[]> {
        return this.http.get('/api/faculty/' + facultyId + '/departments')
        .pipe(map(res => <Department[]>res));
    }

    getPlace(placeId): Observable<Place> {
        return this.http.get('/api/place/' + placeId)
        .pipe(map(res => <Place>res));
    }

    getAreas(): Observable<NamedEntity[]> {
        return this.areas$;
    }

    getBuildings(areaId: string): Observable<NamedEntity[]> {
        return this.http.get('/api/buildings/' + areaId)
        .pipe(map(res => <NamedEntity[]>res));
    }

    getRooms(buildingId: string): Observable<NamedEntity[]> {
        return this.http.get('/api/rooms/' + buildingId)
        .pipe(map(res => <NamedEntity[]>res));
    }
}
