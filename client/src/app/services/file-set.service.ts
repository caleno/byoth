import { Observable } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';

import { ByothFile } from 'model/byoth-file';

import { base64url } from 'shared/base64url';

import { PersistentDocumentSetService } from './persistent-document-set.service';
import { FileService } from './file.service';
import { retryOnLock } from 'shared/retry-on-lock';

import { OrderTypes } from 'model/order';

export class FileSetService extends PersistentDocumentSetService<ByothFile> implements FileService {
    postFile(file: File): Observable<ByothFile> {
        const formData = new FormData();
        formData.append('file', file);
        return this.idService.get()
        .pipe(
            mergeMap(id =>
                <Observable<ByothFile>> this.http.post(this.documentSetApiRoute(id), formData)
                .pipe(retryOnLock())
            ),
            mergeMap(doc => this.refresh().pipe(map(() => doc)))
        );
    }

    isSubmitting(): Observable<boolean> {
        return this.idService.get()
        .pipe(
            mergeMap(id => this.http.head(this.documentSetApiRoute(id) + '/submitting', { observe: 'response'})),
            map(response => {
                const { status } = response;
                if (status === 200) {
                    return true;
                } else if (status === 204) {
                    return false;
                } else {
                    throw Error('Invalid response');
                }
            })
        );
    }

    /**
     * Submit all files for upload and validation.
     *
     * The refresh() method needs to be called after this to update item services.
     */
    submitFiles(type: OrderTypes): Observable<any> {
        const typeString = type === OrderTypes.FINAL ? 'final' : 'evaluation';
        return this.idService.get()
        .pipe(
            mergeMap(id => this.http.post(this.documentSetApiRoute(id) + '/submit', null, { params: { type: typeString }})),
        );
    }

    /**
     * Remove all validation data from files, to indicate that they need to be resent.
     *
     * The refresh() method needs to be called after this to update item services,
     */
    unsubmitFiles(): Observable<any> {
        return this.idService.get()
        .pipe(
            mergeMap(id => this.http.post(this.documentSetApiRoute(id) + '/unsubmit', null)),
        );
    }

    /**
     * Returns a file service that can be used to replace a file.
     */
    replaceFileService(fileId: string): FileService {
        return {
            postFile: file => {
                const formData = new FormData();
                formData.append('file', file);
                return this.idService.get()
                .pipe(
                    mergeMap(id =>
                        <Observable<ByothFile>> this.http
                            .post(this.documentSetApiRoute(id) + '/' + base64url.encode(fileId) + '/replace', formData)
                            .pipe(retryOnLock())
                    ),
                    mergeMap(doc => this.refresh().pipe(map(() => doc)))
                );
            }
        }
    }
}
