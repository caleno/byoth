import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, publishReplay, refCount, take } from 'rxjs/operators';


export interface User {
    name: string,
    email: string,
    containerId?: string
}

@Injectable()
export class AuthService {
    private user$: Observable<User> = null;

    constructor(private http: HttpClient) {
        this.user$ = this.http.get('/auth', {observe: 'response', responseType: 'json'})
        .pipe(
            map(res => {
                if (res.status === 200) {
                    return <User>res.body;
                }

                return null;
            }),
            publishReplay(1, 100), // cache values for 100 ms to avoid unnecessary multiple requests
            refCount(),
            take(1)
        );
    }

    currentUser(): Observable<User> {
        return this.user$;
    }

    login(lang: string, targetRoute?: string): void {
        const origin = window.location.origin;
        let path = '/login?lang=' + lang
        if (targetRoute) {
            path += '&target=' + targetRoute;
        }
        window.location.assign(origin + path);
    }

    logout(lang: string): void {
        const origin = window.location.origin;
        let path = '/logout';
        if (lang) {
            path += '?lang=' + lang;
        }
        window.location.assign(origin + path);
    }
}
