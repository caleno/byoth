import { of } from 'rxjs';
import { first, map, mergeMap, tap } from 'rxjs/operators';

import { PersistentDocumentSetService } from './persistent-document-set.service';
import { Article } from 'model/article';

import { ContainerIdService } from './container-id.service';

class MockHttp {
    get(arg) { return of({} as any); }
    put(arg1, arg2) { return of({} as any); }
    post(arg) { return of({} as any); }
    delete(arg) { return  of({} as any); }
    head(arg) { return of({} as any); }
}

describe('PersistentDocumentSetService', () => {
    let service: PersistentDocumentSetService<Article>;
    let mockHttp: MockHttp;

    beforeEach(() => {
        mockHttp = new MockHttp();
        const idService = new ContainerIdService(mockHttp as any);
        idService.set('foobar');
        service = new PersistentDocumentSetService(idService, mockHttp as any, 'foo');
    });

    it('should return a valid document and update cache', (done) => {
        spyOn(service, 'setValue').and.callThrough();

        mockHttp.get = () => { return of({items: []}); };

        service.get()
        .subscribe(doc => {
            expect(doc.items.length).toEqual(0);
            expect(service.setValue).toHaveBeenCalled();
            done();
        });
    });

    it('should return 0 item services if there are 0 items', (done) => {
        mockHttp.get = () => { return of({items: []}); };

        service.getItemServices().pipe(first())
        .subscribe(services => {
            expect(services.length).toEqual(0);
            done();
        });
    });

    it('should return 2 item services if there are 2 items', (done) => {
        spyOn(service, 'setValue').and.callThrough();

        spyOn(mockHttp, 'get').and.returnValue(of({items: [{}, {}]}));

        service.getItemServices().pipe(first())
        .subscribe(services => {
            expect(services.length).toEqual(2);
            expect(service.setValue).toHaveBeenCalled();
            done();
        });
    });

    // most logic will happen server-side here, just making sure that properties are updated based on assumed changes to server data
    it('should add 1 item when post() is called', (done) => {
        spyOn(mockHttp, 'get').and.returnValues(of({ items: [{}] }), of({ items: [{}, {}] }));

        service.post()
        .pipe(
            mergeMap(() => service.get()),
            map(doc => expect(doc.items.length).toEqual(1)),
            mergeMap(() => service.getItemServices().pipe(first())),
            map(services => expect(services.length).toEqual(1)),
            mergeMap(() => service.post()),
            mergeMap(() => service.get()),
            map(doc => expect(doc.items.length).toEqual(2)),
            mergeMap(() => service.getItemServices().pipe(first())),
            map(services => expect(services.length).toEqual(2)),
        ).subscribe(done);
    });

    // most logic will happen server-side here, just making sure that properties are updated based on assumed changes to server data
    it('should delete 1 item when delete() is called', (done) => {
        spyOn(mockHttp, 'get').and.returnValues(
            of({ items: [{'@id': 'foo'}, { '@id': 'bar' }] }),
            of({ items: [{'@id': 'bar'}] }));

        service.delete({'@id': 'baz'} as any)
        .pipe(
            mergeMap(() => service.get()),
            map(doc => expect(doc.items.length).toEqual(2)),
            mergeMap(() => service.getItemServices().pipe(first())),
            map(services => expect(services.length).toEqual(2)),
            mergeMap(() => service.delete({'@id': 'foo'} as any)),
            mergeMap(() => service.get()),
            map(doc => expect(doc.items.length).toEqual(1)),
            mergeMap(() => service.getItemServices().pipe(first())),
            map(services => expect(services.length).toEqual(1)),
        ).subscribe(done);
    });

    it('should do nothing when save() is called on empty set', (done) => {
        spyOn(mockHttp, 'get').and.returnValue(of({items: [] }));
        spyOn(mockHttp, 'put').and.callThrough();
        spyOn(service, 'get').and.callThrough();

        // need to call getItemServices to test on initialized object
        service.getItemServices().pipe(first(), mergeMap(() => service.save()))
        .subscribe(() => {
            expect(service.get).toHaveBeenCalledTimes(2);
            expect(mockHttp.put).not.toHaveBeenCalled();
            done();
        });
    });

    it('should call save on all item services when set save() is called and persist changed documents', (done) => {
        let spies;
        const newItem = {foo: 'bar'};

        spyOn(mockHttp, 'get').and.returnValue(of({items: [{}, {}] }));
        const putSpy = spyOn(mockHttp, 'put').and.callThrough();

        service.getItemServices()
        .pipe(
            first(),
            tap(itemServices => itemServices[0].setValue(newItem as any)),
            map(itemServices => spies = itemServices.map(item => spyOn(item, 'save').and.callThrough())),
            map(() => expect(spies.length).toEqual(2)),
            mergeMap(() => service.save()),
            map(() => spies.forEach(spy => expect(spy).toHaveBeenCalled())),
            map(() => expect(putSpy.calls.argsFor(0)[1]).toEqual(newItem))
        ).subscribe(done);
    });

    it('should save all changes to items when save() is called', (done) => {
        let spies;

        spyOn(mockHttp, 'get').and.returnValue(of({items: [{}, {}] }));

        service.getItemServices()
        .pipe(
            first(),
            map(itemServices => spies = itemServices.map(item => spyOn(item, 'save').and.callThrough())),
            map(() => expect(spies.length).toEqual(2)),
            mergeMap(() => service.save()),
            map(() => spies.forEach(spy => expect(spy).toHaveBeenCalled()))
        ).subscribe(done);
    });

    it('should reset all itemServices when reset() is called', (done) => {
        let spies;

        spyOn(mockHttp, 'get').and.returnValue(of({items: [{}, {}] }));

        service.getItemServices()
        .pipe(
            first(),
            map(itemServices => spies = itemServices.map(item => spyOn(item, 'reset').and.callThrough())),
            map(() => expect(spies.length).toEqual(2)),
            map(() => service.reset()),
            map(() => spies.forEach(spy => expect(spy).toHaveBeenCalled()))
        ).subscribe(done);
    });

    it('should send new item order to remote when reorder() is called', (done) => {
        spyOn(mockHttp, 'get').and.returnValue(of({items: [{}, {}] }));
        const putSpy = spyOn(mockHttp, 'put').and.callThrough();
        const newOrder = ['foo', 'bar'];

        service.reorder(newOrder)
        .subscribe(() => {
            expect(putSpy).toHaveBeenCalled();
            expect(putSpy.calls.argsFor(0)[1]).toEqual(newOrder);
            done();
        });
    });
});
