import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { getModel } from 'model/index';
import { CrossrefService } from './crossref.service';
import { JOURNAL_ARTICLE, PROCEEDINGS_ARTICLE, BOOK_CHAPTER } from 'model/article';

const validDOI = '10.1234/123.456';

// these are actual responses from CrossRef
const journalArticle = {
    'status': 'ok',
    'message-type': 'work',
    'message-version': '1.0.0',
    'message': {
        'indexed': {'date-parts': [[2018, 5, 2]], 'date-time': '2018-05-02T18:50:04Z', 'timestamp': 1525287004574},
        'reference-count': 31,
        'publisher': 'Elsevier BV',
        'issue': '3',
        'license': [
            {
                'URL': 'http:\/\/www.elsevier.com\/tdm\/userlicense\/1.0\/',
                'start': {
                    'date-parts': [[2001, 1, 1]],
                    'date-time': '2001-01-01T00:00:00Z',
                    'timestamp': 978307200000
                },
                'delay-in-days': 0,
                'content-version': 'tdm'
            }
        ],
        'content-domain': {'domain': [], 'crossmark-restriction': false},
        'short-container-title': ['Journal of Molecular Biology'],
        'published-print': {'date-parts': [[2001, 1]]},
        'DOI': '10.1006\/jmbi.2000.4282',
        'type': 'journal-article',
        'created': {'date-parts': [[2002, 9, 18]], 'date-time': '2002-09-18T19:24:54Z', 'timestamp': 1032377094000},
        'page': '377-388',
        'source': 'Crossref',
        'is-referenced-by-count': 16,
        'title': ['Specific interaction between anticodon nuclease and the tRNALys wobble base11Edited by D. Draper'],
        'prefix': '10.1006',
        'volume': '305',
        'author': [
            {'given': 'Yue', 'family': 'Jiang', 'sequence': 'first', 'affiliation': []},
            {'given': 'Roberto', 'family': 'Meidler', 'sequence': 'additional', 'affiliation': []},
            {'given': 'Michal', 'family': 'Amitsur', 'sequence': 'additional', 'affiliation': []},
            {'given': 'Gabriel', 'family': 'Kaufmann', 'sequence': 'additional', 'affiliation': []}
        ],
        'member': '78',
        'container-title': ['Journal of Molecular Biology'],
        'original-title': [],
        'language': 'en',
        'link': [
            {
                'URL': 'http:\/\/api.elsevier.com\/content\/article\/PII:S0022283600942827?httpAccept=text\/xml',
                'content-type': 'text\/xml',
                'content-version': 'vor',
                'intended-application': 'text-mining'
            },
            {
                'URL': 'http:\/\/api.elsevier.com\/content\/article\/PII:S0022283600942827?httpAccept=text\/plain',
                'content-type': 'text\/plain',
                'content-version': 'vor',
                'intended-application': 'text-mining'
            }
        ],
        'deposited': {'date-parts': [[2017, 6, 14]], 'date-time': '2017-06-14T17:09:33Z', 'timestamp': 1497460173000},
        'score': 1.0,
        'subtitle': [],
        'short-title': [],
        'issued': {'date-parts': [[2001, 1]]},
        'references-count': 31,
        'journal-issue': {'published-print': {'date-parts': [[2001, 1]]}, 'issue': '3'},
        'alternative-id': ['S0022283600942827'],
        'URL': 'http:\/\/dx.doi.org\/10.1006\/jmbi.2000.4282',
        'relation': {},
        'ISSN': ['0022-2836'],
        'issn-type': [{'value': '0022-2836', 'type': 'print'}]
    }
};

const conferenceArticle = {
    'status': 'ok',
    'message-type': 'work',
    'message-version': '1.0.0',
    'message': {
        'indexed': {'date-parts': [[2018, 5, 3]], 'date-time': '2018-05-03T18:13:46Z', 'timestamp': 1525371226908},
        'publisher-location': 'New York, New York, USA',
        'reference-count': 0,
        'publisher': 'ACM Press',
        'isbn-type': [{'value': '0897918533', 'type': 'print'}],
        'content-domain': {'domain': [], 'crossmark-restriction':     false},
        'short-container-title': [],
        'published-print': {'date-parts': [[1997]]},
        'DOI': '10.1145\/263699.263703',
        'type': 'proceedings-article',
        'created': {'date-parts': [[2003, 5, 5]],
        'date-time': '2003-05-05T12:19:38Z', 'timestamp': 1052137178000},
        'source': 'Crossref',
        'is-referenced-by-count': 33,
        'title': ['Fast and accurate flow-insensitive points-to analysis'],
        'prefix': '10.1145',
        'author': [
            {'given': 'Marc', 'family': 'Shapiro', 'sequence': 'first', 'affiliation': []},
            {'given': 'Susan', 'family': 'Horwitz', 'sequence': 'additional', 'affiliation': []}
        ],
        'member': '320',
        'event': {'name': 'the 24th ACM SIGPLAN-SIGACT symposium',
        'location': 'Paris, France',
        'sponsor': [
            "L'Ecole des Mines de Paris",
            'SIGACT, ACM Special Interest Group on Algorithms and Computation Theory',
            'SIGPLAN, ACM Special Interest Group on Programming Languages'
        ],
        'acronym': "POPL '97",
        'number': '24',
        'start': {'date-parts': [[1997, 1, 15]]}, 'end': {'date-parts': [[1997, 1, 17]]}},
        'container-title': ["Proceedings of the 24th ACM SIGPLAN-SIGACT symposium on Principles of programming languages  - POPL '97"],
        'original-title': [],
        'link': [
            {
                'URL': 'http:\/\/dl.acm.org\/ft_gateway.cfm?id=263703&amp;ftid=39213&amp;dwn=1',
                'content-type': 'unspecified',
                'content-version': 'vor',
                'intended-application': 'similarity-checking'
            }
        ],
        'deposited': {'date-parts': [[2016, 12, 12]], 'date-time': '2016-12-12T15:43:05Z', 'timestamp': 1481557385000},
        'score': 1.0,
        'subtitle': [],
        'short-title': [],
        'issued': {'date-parts': [[1997]]},
        'ISBN': ['0897918533'],
        'references-count': 0,
        'URL': 'http:\/\/dx.doi.org\/10.1145\/263699.263703',
        'relation': {}
    }
};

const bookChapter = {
    'status': 'ok',
    'message-type': 'work',
    'message-version': '1.0.0',
    'message': {
        'indexed': {'date-parts': [[2018, 5, 8]], 'date-time': '2018-05-08T01:33:14Z', 'timestamp': 1525743194329},
        'publisher-location': 'Cham',
        'reference-count': 31,
        'publisher': 'Springer International Publishing',
        'isbn-type': [
            {'value': '9783319653785', 'type': 'print'},
            {'value': '9783319653792', 'type': 'electronic'}
         ],
        'license': [
            {
                'URL': 'http:\/\/www.springer.com\/tdm',
                'start': {
                    'date-parts': [[2017, 10, 4]],
                    'date-time': '2017-10-04T00:00:00Z',
                    'timestamp': 1507075200000
                },
                'delay-in-days': 0,
                'content-version': 'unspecified'
            }
        ],
        'content-domain': {'domain': [], 'crossmark-restriction': false},
        'short-container-title': [],
        'published-print': {'date-parts': [[2018]]},
        'DOI': '10.1007\/978-3-319-65379-2_3',
        'type': 'book-chapter',
        'created': {'date-parts': [[2017, 10, 3]], 'date-time': '2017-10-03T00:43:41Z', 'timestamp': 1506991421000},
        'page': '27-42',
        'source': 'Crossref',
        'is-referenced-by-count': 0,
        'title': ['Apps as Companions: How Quantified Self Apps Become Our Audience and Our Companions'],
        'prefix': '10.1007',
        'author': [
            {
                'given': 'Jill Walker',
                'family': 'Rettberg',
                'sequence': 'first',
                'affiliation': []
            }
        ],
        'member': '297',
        'published-online': {'date-parts': [[2017, 10, 4]]},
        'reference': [
            {
                'issue': 'January',
                'key': '3_CR1',
                'doi-asserted-by': 'publisher',
                'first-page': '1',
                'DOI': '10.1177\/2055207616689509',
                'volume': '3',
                'author': 'Btihaj Ajana',
                'year': '2017',
                'unstructured': 'Ajana, Btihaj. 2017. [...]',
                'journal-title': 'Digital Health'
            }
            // truncated, not relevant
        ],
        'container-title': ['Self-Tracking'],
        'original-title': [],
        'link': [
            {
                'URL': 'http:\/\/link.springer.com\/content\/pdf\/10.1007\/978-3-319-65379-2_3',
                'content-type': 'unspecified',
                'content-version': 'vor',
                'intended-application': 'similarity-checking'
            }
        ],
        'deposited': {'date-parts': [[2017, 10, 3]], 'date-time': '2017-10-03T00:44:23Z', 'timestamp': 1506991463000},
        'score': 1.0,
        'subtitle': [],
        'short-title': [],
        'issued': {'date-parts': [[2017, 10, 4]]},
        'ISBN': ['9783319653785', '9783319653792'],
        'references-count': 31,
        'URL': 'http:\/\/dx.doi.org\/10.1007\/978-3-319-65379-2_3',
        'relation': {'cites': []}
    }
};

describe('CrossRefService', () => {
    let service: CrossrefService;
    let response = { status: 'ok', message: {}}

    const mockHttpService = {
        get: () => of(response)
    }

    const mockModelService = {
        get: () => of(getModel('article'))
    }

    beforeEach(() => {
        service = new CrossrefService(mockHttpService as any, mockModelService as any);
    });

    it('should throw on invalid DOI', done => {
        service.getPublication('1', '')
        .pipe(catchError(err => of(err)))
        .subscribe(err => {
            expect(err).toBeTruthy();
            expect(err.message).toMatch('Invalid DOI');
            done();
        }, done);
    });

    it('should throw on non-existent DOI', done => {
        response = { status: 'error', message: 'Mock error' };
        service.getPublication(validDOI, '')
        .pipe(catchError(err => of(err)))
        .subscribe(err => {
            expect(err).toBeTruthy();
            expect(err.message).toMatch('Mock error');
            done();
        }, done);
    });

    it('should throw on DOI that refers to non-article', done => {
        response = { status: 'ok', message: {type: 'book' } };
        service.getPublication(validDOI, '')
        .pipe(catchError(err => of(err)))
        .subscribe(err => {
            expect(err).toBeTruthy();
            expect(err.message).toMatch('\'book\' not supported');
            done();
        }, done);
    });

    it('should throw on crossref response without type', done => {
        response = { status: 'ok', message: { } };
        service.getPublication(validDOI, '')
        .pipe(catchError(err => of(err)))
        .subscribe(err => {
            expect(err).toBeTruthy();
            expect(err.message).toMatch('without type');
            done();
        }, done);
    });

     it('should generate almost empty article based on minimal crossref response', done => {
        const minimalArticle = {
            'status': 'ok',
            'message': {
                'type': 'journal-article'
            }
        }
        response = minimalArticle;
        service.getPublication(validDOI, '')
        .subscribe(article => {
            expect(article.title).toBeFalsy();
            expect(article.articleType).toEqual(JOURNAL_ARTICLE);
            expect(article.series).toBeFalsy();
            expect(article.book).toBeFalsy();
            done();
        }, done);
    });

    it('should generate correct article based on simple mock response', done => {
        const simpleArticle = {
            'status': 'ok',
            'message': {
                'type': 'journal-article',
                'container-title': ['foo'],
                'title': ['Foo Bar Baz'],
                'abstract': 'foobar',
                'author': [
                    {'given': 'Foo', 'family': 'Bar'},
                    {'given': 'Baz', 'family': 'Bar'},
                ],
                'issued': { 'date-parts': [[2000, 1, 1]] },
                'page': '123',
                'volume': '1',
                'issue': '2'
            }
        }
        response = simpleArticle;
        service.getPublication(validDOI, '')
        .subscribe(article => {
            expect(article.title).toEqual(simpleArticle.message.title[0]);
            expect(article.articleType).toEqual(JOURNAL_ARTICLE);
            expect(article.series).toBeTruthy();
            expect(article.series.title).toEqual(simpleArticle.message['container-title'][0]);
            expect(article.series.issn).toBeFalsy();
            expect(article.book).toBeFalsy();
            expect(article.abstract).toEqual(simpleArticle.message.abstract);
            expect(article.authors[0].name).toEqual('Foo Bar');
            expect(article.authors[1].name).toEqual('Baz Bar');
            expect(article.publicationYear).toMatch('2000');
            expect(article.pageStart).toEqual(simpleArticle.message.page);
            expect(article.pageEnd).toBeFalsy();
            expect(article.volume).toEqual(simpleArticle.message.volume);
            expect(article.issue).toEqual(simpleArticle.message.issue);
            done();
        }, done);
    });

    it('should generate article based on actual journal article from crossref', done => {
        response = journalArticle;
        service.getPublication(validDOI, '')
        .subscribe(article => {
            expect(article.title).toEqual(journalArticle.message.title[0]);
            expect(article.articleType).toEqual(JOURNAL_ARTICLE);
            expect(article.series).toBeTruthy();
            expect(article.book).toBeFalsy();
            expect(article.series.title).toEqual(journalArticle.message['container-title'][0]);
            expect(article.series.issn).toEqual(journalArticle.message.ISSN);
            done();
        }, done);
    });

    it('should generate article based on actual book chapter from crossref', done => {
        response = bookChapter;
        service.getPublication(validDOI, '')
        .subscribe(article => {
            expect(article.title).toEqual(bookChapter.message.title[0]);
            expect(article.articleType).toEqual(BOOK_CHAPTER);
            expect(article.book).toBeTruthy();
            expect(article.series).toBeFalsy();
            expect(article.book.title).toEqual(bookChapter.message['container-title'][0]);
            expect(article.book.isbn).toEqual(bookChapter.message.ISBN)
            done();
        }, done);
    });

    it('should generate article based on actual proceedings article from crossref', done => {
        response = conferenceArticle;
        service.getPublication(validDOI, '')
        .subscribe(article => {
            expect(article.title).toEqual(conferenceArticle.message.title[0]);
            expect(article.articleType).toEqual(PROCEEDINGS_ARTICLE);
            done();
        }, done);
    });

    // TODO: Add more tests for special cases, try to cover variability in possible inputs.
    // Make sure we store all relevant metadata from CrossRef for the different types
});
