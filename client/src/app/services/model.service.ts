import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators'

import { ByothModel } from 'model/index';

@Injectable()
export class ModelService {
    private model: ByothModel;

    constructor(private http: HttpClient) {}

    get(): Observable<ByothModel> {
        if (!this.model) {
            return this.http.get('/api/model')
            .pipe(
                map(res => <ByothModel>res),
                tap(res => this.model = res)
            );
        }

        return of(this.model);
    }
}
