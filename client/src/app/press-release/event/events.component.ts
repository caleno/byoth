import { Component, ViewChildren, QueryList, AfterViewInit } from '@angular/core';

import { ThesisContainerService } from 'services/thesis-container.service';
import { BaseFormComponent } from 'shared/form/form-component';
import { EventComponent } from 'shared/form/event/event.component';

@Component({
    selector: 'app-events',
    templateUrl: './events.component.html',
})
export class EventsComponent extends BaseFormComponent implements AfterViewInit {
    @ViewChildren(EventComponent)
    eventComponents: QueryList<EventComponent>;

    constructor(public container: ThesisContainerService) {
        super();
    }

    ngAfterViewInit() {
        this.children = this.eventComponents.toArray();
        this.eventComponents.changes
        .subscribe(changes => {
            this.children = this.eventComponents.toArray();
        });
    }
}
