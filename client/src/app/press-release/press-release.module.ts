import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { MaterialModule } from '../material.module';
import { SharedModule } from '../shared/shared.module';

import { PressReleaseFormComponent } from './press-release-form.component';
import { CandidateComponent } from './candidate/candidate.component';
import { EventsComponent } from './event/events.component';
import { DepartmentComponent } from './candidate/department/department.component';
import { PersonalInformationComponent } from './candidate/personal-information/personal-information.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        RouterModule,
        MaterialModule
    ],
    declarations: [
        EventsComponent,
        CandidateComponent,
        PressReleaseFormComponent,
        DepartmentComponent,
        PersonalInformationComponent
    ],
    exports: [ PressReleaseFormComponent ],
    entryComponents: [ CandidateComponent, EventsComponent ]
})
export class PressReleaseModule { }
