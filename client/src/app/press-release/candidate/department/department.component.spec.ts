import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'app/material.module';

import { of } from 'rxjs';

import { HomeInstitutionService } from 'services/home-institution.service';
import { ThesisContainerService } from 'services/thesis-container.service';
import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { MessageService } from 'services/message.service';

import { DepartmentComponent } from './department.component';

describe('DepartmentComponent', () => {
    let component: DepartmentComponent;
    let fixture: ComponentFixture<DepartmentComponent>;

    class MockContainer {
        department = new NonpersistentDocumentService();
    }

    class MockRoomService {
        getFaculties() { return of(null)};
        getDepartments() { return of(null)};
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, MaterialModule, NoopAnimationsModule ],
            declarations: [ DepartmentComponent ],
            providers: [
                { provide: HomeInstitutionService, useClass: MockRoomService },
                { provide: ThesisContainerService, useClass: MockContainer },
                MessageService
            ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DepartmentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
