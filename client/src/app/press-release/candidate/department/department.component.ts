import { Component, OnInit, Inject, LOCALE_ID } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, AbstractControl } from '@angular/forms';

import { map, filter } from 'rxjs/operators';

import { FormHandler } from 'shared/form/form-handler';
import { BaseFormComponent } from 'shared/form/form-component';
import { HomeInstitutionService, Faculty as FacultyData, Department as DepartmentData } from 'services/home-institution.service';
import { ThesisContainerService } from 'services/thesis-container.service';

@Component({
    selector: 'app-department',
    templateUrl: './department.component.html',
    styleUrls: ['./department.component.css']
})
export class DepartmentComponent extends BaseFormComponent implements OnInit {
    form: FormGroup;
    dropdownForm: FormGroup;

    facultyControl: AbstractControl;
    departmentControl: AbstractControl;

    faculties: FacultyData[];
    facultyDepartments: DepartmentData[];

    constructor(
            private homeInstitutionService: HomeInstitutionService,
            private container: ThesisContainerService,
            private fb: FormBuilder,
            @Inject(LOCALE_ID) private locale: string) {
        super();

        this.form = this.fb.group({
            departmentName: [null],
            facultyName: [null],
            facultyId: [''],
            departmentId: new FormControl({value: '', disabled: true})
        });

        this.facultyControl = this.form.get('facultyId');
        this.departmentControl = this.form.get('departmentId');

        this.addFormHandler(new FormHandler(this.form, this.container.department));
    }

    ngOnInit() {
        this.container.department.get()
        .subscribe(department => {
            this.setFaculty(department.facultyId);
        });

        this.addSubscription(
            this.facultyControl.valueChanges
            .pipe(filter(id => !!id))
            .subscribe(id => {
                this.form.patchValue({facultyName: this.faculties.find(fac => fac.id === id).name})
                this.departmentControl.setValue(null);
                this.setFaculty(id);
            })
         );

        this.addSubscription(
            this.departmentControl.valueChanges
            .pipe(filter(id => !!id))
            .subscribe(id => {
                if (this.facultyDepartments) {
                    this.form.patchValue({departmentName: this.facultyDepartments.find(dep => dep.id === id).name });
                }
            })
        );

        // Get lists of faculties
        this.homeInstitutionService.getFaculties()
        .pipe(map(f => this.faculties = f))
        .subscribe(() => { });
    }

    setFaculty(facultyId) {
        this.homeInstitutionService.getDepartments(facultyId)
        .subscribe(departments => {
            this.facultyDepartments = departments;

            if (departments && departments.length) {
                this.departmentControl.enable();
            } else {
                this.departmentControl.disable();
            }
        });
    }

    getFacultyName(faculty: FacultyData): string {
        if (!faculty) {
            return '';
        }

        if (faculty.name[this.locale]) {
            return faculty.name[this.locale];
        }

        // if we can't find the name in the correct language, use anything
        const validLang = Object.keys(faculty.name).find(lang => !!faculty.name[lang]);

        return faculty.name[validLang];
    }


    getDepartmentName(department: DepartmentData): string {
        if (!department) {
            return '';
        }

        if (department.name[this.locale]) {
            return department.name[this.locale];
        }

        // if we can't find the name in the correct language, use anything
        const validLang = Object.keys(department.name).find(lang => !!department.name[lang]);

        return department.name[validLang];
    }
}
