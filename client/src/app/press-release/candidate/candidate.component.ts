import { Component, OnInit, ViewChild } from '@angular/core';

import { BaseFormComponent } from 'shared/form/form-component';

import { DepartmentComponent } from './department/department.component';
import { PersonalInformationComponent } from './personal-information/personal-information.component'

@Component({
    selector: 'app-candidate',
    templateUrl: './candidate.component.html',
    styleUrls: ['./candidate.component.css']
})
export class CandidateComponent extends BaseFormComponent implements OnInit  {
    @ViewChild(DepartmentComponent)
    departmentComponent: DepartmentComponent;

    @ViewChild(PersonalInformationComponent)
    personalInformationComponent: PersonalInformationComponent;

    ngOnInit() {
        this.addChild(this.departmentComponent);
        this.addChild(this.personalInformationComponent);
    }
}
