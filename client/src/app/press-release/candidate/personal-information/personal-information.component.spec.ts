import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'app/material.module';

import { ThesisContainerService } from 'services/thesis-container.service';
import { NonpersistentDocumentService } from 'services/nonpersistent-document.service';
import { MessageService } from 'services/message.service';

import { PersonalInformationComponent } from './personal-information.component';

describe('PersonalInformationComponent', () => {
    let component: PersonalInformationComponent;
    let fixture: ComponentFixture<PersonalInformationComponent>;

    class MockContainer {
        candidate = new NonpersistentDocumentService();
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ ReactiveFormsModule, MaterialModule, NoopAnimationsModule ],
            declarations: [ PersonalInformationComponent ],
            providers: [ { provide: ThesisContainerService, useClass: MockContainer }, MessageService ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PersonalInformationComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
