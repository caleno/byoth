import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { ThesisContainerService } from 'services/thesis-container.service';
import { FormHandler } from 'shared/form/form-handler';
import { BaseFormComponent } from 'shared/form/form-component';

@Component({
    selector: 'app-personal-information',
    templateUrl: './personal-information.component.html',
    styleUrls: ['./personal-information.component.css']
})
export class PersonalInformationComponent extends BaseFormComponent {
    form: FormGroup;

    constructor(
        private container: ThesisContainerService,
        private fb: FormBuilder) {
        super();

        this.form = this.fb.group({
            givenName: ['', Validators.required],
            familyName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            orcid: ['', Validators.pattern(/^(https?:\/\/orcid.org\/)?(\d\d\d\d\-){3}\d\d\d(\d|X)$/)],
            birthDate: ['', Validators.required],
            gender: ['']
        });

        this.addFormHandler(new FormHandler(this.form, this.container.candidate));
    }
}
