import { NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidateComponent } from './candidate.component';

describe('CandidateComponent', () => {
    let component: CandidateComponent;
    let fixture: ComponentFixture<CandidateComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ CandidateComponent ],
            imports: [ ],
            providers: [ ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CandidateComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
