import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { MessageService } from 'services/message.service';
import { ThesisContainerService } from 'services/thesis-container.service';

import { CandidateComponent } from './candidate/candidate.component';
import { EventsComponent } from './event/events.component';

@Component({
    selector: 'app-press-release-form',
    templateUrl: './press-release-form.component.html',
})
export class PressReleaseFormComponent implements OnInit {
    saveFunction: () => Observable<void>;

    steps = [
        { label: { en: 'Candidate', nb: 'Personalia' }, type: CandidateComponent },
        { label: { en: 'Defense', nb: 'Disputas' }, type: EventsComponent }
    ];

    constructor(private container: ThesisContainerService, private message: MessageService) {
        this.saveFunction = () =>
            this.container.save()
                .pipe(tap(() => this.message.inform({en: 'Saved all data', nb: 'Lagret alle data'})));
    }

    ngOnInit() { }
}
