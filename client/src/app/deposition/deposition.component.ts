import { Component, OnInit } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ThesisContainerService } from 'services/thesis-container.service';
import { MessageService } from 'services/message.service';

import { ThesisLicenseComponent } from 'shared/form/thesis-license/thesis-license.component';
import { ArticlesComponent } from 'shared/form/articles/articles.component';
import { ConfirmDepositionComponent } from 'shared/form/confirm-deposition/confirm-deposition.component';

@Component({
    selector: 'app-deposition',
    templateUrl: './deposition.component.html',
    styleUrls: ['./deposition.component.css']
})
export class DepositionComponent implements OnInit {
    thesisSubscription: Subscription;

    saveFunction: () => Observable<void>;

    steps = [
        { label: { en: 'Thesis', nb: 'Avhandlingen' }, type: ThesisLicenseComponent },
        { label: { en: 'Articles', nb: 'Artikler' }, type: ArticlesComponent },
        { label: { en: 'Review', nb: 'Se gjennom' }, type: ConfirmDepositionComponent }
    ];

    constructor(private container: ThesisContainerService,
                private message: MessageService) {
        this.saveFunction = () =>
            this.container.save()
                .pipe(tap(() => this.message.inform({en: 'Saved all data', nb: 'Lagret alle data'})))
    }

    ngOnInit() { }
}
