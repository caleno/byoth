import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MaterialModule } from 'app/material.module';

import { PrintCommitteeComponent } from './print-committee.component';

import { AuthService } from 'services/auth.service';
import { ThesisContainerService } from 'services/thesis-container.service';
import { SubmissionStatusService } from 'services/submission-status.service';
import { MessageService } from 'services/message.service';

describe('PrintCommitteeComponent', () => {
    let component: PrintCommitteeComponent;
    let fixture: ComponentFixture<PrintCommitteeComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ PrintCommitteeComponent ],
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                MaterialModule,
            ],
            providers: [ ThesisContainerService, AuthService, MessageService, SubmissionStatusService ],
            schemas: [NO_ERRORS_SCHEMA]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PrintCommitteeComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
