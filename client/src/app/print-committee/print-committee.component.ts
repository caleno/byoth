import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { ThesisContainerService } from 'services/thesis-container.service';
import { MessageService } from 'services/message.service';
import { SubmissionStatusService } from 'services/submission-status.service';
import { OrderTypes } from 'model/order';

import { CoverComponent} from 'shared/form/cover/cover.component';
import { FilesComponent } from 'shared/form/files/files.component';
import { MetadataComponent } from 'shared/form/metadata/metadata.component';
import { OrderComponent } from 'shared/form/order/order.component';
import { ConfirmPrintComponent } from 'shared/form/confirm-print/confirm-print.component';

@Component({
    selector: 'app-print-committee',
    templateUrl: './print-committee.component.html',
})
export class PrintCommitteeComponent implements OnInit {
    saveFunction: () => Observable<void>;
    orderType: OrderTypes = OrderTypes.EVALUATION;

    private _steps = [
        { label: { en: 'Metadata', nb: 'Metadata' }, type: MetadataComponent, options: { orderType: OrderTypes.EVALUATION }  },
        { label: { en: 'Cover', nb: 'Omslag' }, type: CoverComponent, options: { orderType: OrderTypes.EVALUATION } },
        { label: { en: 'Files', nb: 'Filer' }, type: FilesComponent, options: { orderType: OrderTypes.EVALUATION } },
        { label: { en: 'Order', nb: 'Bestilling' }, type: OrderComponent, options: { orderType: OrderTypes.EVALUATION } },
        { label: { en: 'Review', nb: 'Se gjennom' }, type: ConfirmPrintComponent, options: { orderType: OrderTypes.EVALUATION } }
    ];

    constructor(private container: ThesisContainerService,
                private message: MessageService,
                private submissionStatus: SubmissionStatusService) {
        this.saveFunction = () =>
            this.container.save()
                .pipe(tap(() => this.message.inform({en: 'Saved all data', nb: 'Lagret alle data'})));
    }

    ngOnInit() { }

    get steps() {
        return this._steps;
    }

    get hasSubmittedAlready$() {
        return this.submissionStatus.evaluationOrderIsSubmitted$.asObservable();
    }
}
