import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { MaterialModule } from 'app/material.module';

import { SubmissionStatusService } from 'services/submission-status.service';
import { AfterSubmissionComponent } from './after-submission.component';
import { ThesisContainerService } from 'services/thesis-container.service';

describe('AfterSubmissionComponent', () => {
    let component: AfterSubmissionComponent;
    let fixture: ComponentFixture<AfterSubmissionComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AfterSubmissionComponent ],
            imports: [ HttpClientTestingModule, MaterialModule ],
            providers: [ SubmissionStatusService, ThesisContainerService ],
            schemas: [ NO_ERRORS_SCHEMA ]
        })
        .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AfterSubmissionComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
