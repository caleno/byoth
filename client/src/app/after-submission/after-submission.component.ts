import { Component, OnInit, Input } from '@angular/core';

import { ThesisContainerService } from 'services/thesis-container.service';
import { SubmissionStatusService } from 'services/submission-status.service';

import { OrderTypes } from 'model/order';
import { Thesis, ThesisTypes, PublishThesis } from 'model/thesis';

@Component({
    selector: 'app-after-submission',
    templateUrl: './after-submission.component.html',
    styleUrls: ['./after-submission.component.css']
})
export class AfterSubmissionComponent implements OnInit {
    @Input() orderType: OrderTypes;

    isMonograph = false;
    publishThesis = false;
    isFinal = false;
    thesis: Thesis;

    constructor(private container: ThesisContainerService,
                private submissionStatus: SubmissionStatusService) { }

    ngOnInit() {
        window.location.hash = '';
        this.isFinal = this.orderType === OrderTypes.FINAL;
        this.container.thesis.get().subscribe(
            thesis => {
                this.thesis = thesis;
                this.isMonograph = this.thesis.thesisType === ThesisTypes.MONOGRAPH;
                this.publishThesis = this.thesis.publish === PublishThesis.WITH_EMBARGO ||
                    this.thesis.publish === PublishThesis.WITHOUT_EMBARGO;
            }
        )
    }

    cancelOrder() {
        this.container.cancelOrder(this.orderType)
        .subscribe(() => this.submissionStatus.refresh());
    }
}
