import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultEmptyString, defaultNull, defaultZero } from './ld-model';

import { prefixes } from './prefixes';

const TYPE = 'pcdm:File';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    filename: { '@id': 'ebucore:filename' },
    mimetype: { '@id': 'ebucore:hasMimeType' },
    status: { '@id': 'byoth:hasFileStatus', '@type': '@id' },
    errors: { '@id': 'byoth:hasError', '@container': '@set' },
    warnings: { '@id': 'byoth:hasWarning', '@container': '@set' },
    numPages: { '@id': 'bibo:numPages' },
    numColorPages: { '@id': 'byoth:numColorPages' },
    size: { '@id': 'premis:hasSize', '@type': 'xsd:long' },
    messageDigest: { '@id': 'premis:hasMessageDigest', '@type': '@id' }, // The message digest is a URN
    hasCorrection: { '@id': 'byoth:hasCorrection', '@type': '@id' },
    isCorrectionFor: { '@id': 'byoth:isCorrectionFor', '@type': '@id' },
    hasValidationReport: { '@id': 'byoth:hasValidationReport', '@type': '@id' },
    isValidationReportFor: { '@id': 'byoth:isValidationReportFor', '@type': '@id' }
};

export const PCDMFILE_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@type': TYPE,
    '@explicit': true,
    filename: defaultEmptyString,
    mimetype: defaultNull,
    status: defaultNull,
    errors: defaultNull,
    warnings: defaultNull,
    numPages: defaultZero,
    numColorPages: defaultZero,
    size: defaultNull,
    messageDigest: defaultNull,
    hasCorrection: defaultNull,
    isCorrectionFor: defaultNull,
    hasValidationReport: defaultNull,
    isValidationReportFor: defaultNull,
}

export enum FileStatus {
    OK = 'byoth:fileOk',
    WARNING = 'byoth:fileWarning',
    ERROR = 'byoth:fileError',
};

export interface ByothFileProperties {
    size: string, // xsd:long is string, not number
    mimetype: string,
    messageDigest: string,
    filename?: string,
    status?: FileStatus,
    errors?: string[],
    warnings?: string[],
    numPages?: number,
    numColorPages?: number,
    hasCorrection?: string,
    isCorrectionFor?: string,
    hasValidationReport?: string,
    isValidationReportFor?: string
}

// we don't call it simply File because the name is taken
export interface ByothFile extends CompactedDocumentHeader, ByothFileProperties { }

