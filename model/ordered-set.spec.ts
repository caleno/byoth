import { expect } from 'chai';
import { getOrderedSetArray, setOrderedSetArray, deleteOrderedSetMember, appendOrderedSet, OrderedSet } from './ordered-set';

describe('OrderedSet', () => {
    let simpleSet: OrderedSet;
    let largeSet: OrderedSet;

    beforeEach(() => {
        simpleSet = {
            '@id': 'bar',
            '@type': 'ldp:container',
        }

        largeSet = {
            '@id': 'bar',
            '@type': 'ldp:container',
            proxies: [
                { '@id': 'p1', '@type': 'ore:Proxy', proxyFor: 'i1', prev: null, next: 'p2' },
                { '@id': 'p2', '@type': 'ore:Proxy', proxyFor: 'i2', prev: 'p1', next: 'p3' },
                { '@id': 'p3', '@type': 'ore:Proxy', proxyFor: 'i3', prev: 'p2', next: null },
            ],
            first: 'p1',
            last: 'p3',
        };
    });


    it('should allow creating empty set from empty array', () => {
        const set = simpleSet;
        setOrderedSetArray(set, []);

        expect(set.proxies.length).to.equal(0);
        expect(set.first).to.equal(null);
        expect(set.last).to.equal(null);
    });

    it('should allow creating single element set from single element array', () => {
        const set = simpleSet;
        setOrderedSetArray(set, [ 'i1' ]);

        expect(set.proxies.length).to.equal(1);
        expect(set.first).to.equal(set.last);
        expect(set.proxies[0].proxyFor).to.equal('i1');
        expect(set.proxies[0].prev).to.equal(null);
        expect(set.proxies[0].next).to.equal(null);
    });

    it('should allow creating an ordered set with correct order from multi element array', () => {
        const targetItems = [ 'i1', 'i2', 'i3' ];
        const set = simpleSet;

        setOrderedSetArray(set, targetItems);
        expect(set.proxies.length).to.equal(3);
        expect(set.first).not.to.equal(set.last);

        set.proxies.forEach(proxy => {
            expect(proxy['@id']).to.match(/^bar#proxy-\S+$/);
        });

        const firstProxy = set.proxies.find(proxy => !proxy.prev)
        expect(set.first).to.equal(firstProxy['@id']);
        expect(firstProxy.proxyFor).to.equal('i1');

        const lastProxy = set.proxies.find(proxy => !proxy.next);
        expect(set.last).to.equal(lastProxy['@id']);
        expect(lastProxy.proxyFor).to.equal('i3');

        const middleProxy = set.proxies.find(proxy => !!proxy.next && !!proxy.prev);
        expect(middleProxy.proxyFor).to.equal('i2');

        expect(middleProxy.prev).to.equal(firstProxy['@id']);
        expect(middleProxy.next).to.equal(lastProxy['@id']);
        expect(firstProxy.next).to.equal(middleProxy['@id']);
        expect(lastProxy.prev).to.equal(middleProxy['@id']);
    });

    it('should allow extracting members in order they were set', () => {
        const set = simpleSet;
        setOrderedSetArray(set, [ 'i1', 'i2', 'i3' ]);
        expect(getOrderedSetArray(set)).to.deep.equal(['i1', 'i2', 'i3']);
    });

    it('should preserve set order after deleting member', () => {
        let set = JSON.parse(JSON.stringify(largeSet));
        deleteOrderedSetMember(set, 'i1');
        expect(getOrderedSetArray(set)).to.deep.equal(['i2', 'i3']);

        set = JSON.parse(JSON.stringify(largeSet));
        deleteOrderedSetMember(set, 'i2');
        expect(getOrderedSetArray(set)).to.deep.equal(['i1', 'i3']);

        set = JSON.parse(JSON.stringify(largeSet));
        deleteOrderedSetMember(set, 'i3');
        expect(getOrderedSetArray(set)).to.deep.equal(['i1', 'i2']);
    });

    it('should not allow deleting non-existing member', () => {
        const set = simpleSet;
        expect(() => deleteOrderedSetMember(set, 'foo')).to.throw(RangeError);
    });

    it('should preserve set order after appending member', () => {
        const array = getOrderedSetArray(largeSet);
        const set = largeSet;

        appendOrderedSet(set, 'i4');
        array.push('i4')
        expect(getOrderedSetArray(set)).to.deep.equal(array);
    });

    it('should allow appending member to empty set', () => {
        const set = simpleSet;

        appendOrderedSet(set, 'i1');

        expect(getOrderedSetArray(set)).to.deep.equal([ 'i1' ]);
    });

    it('should not allow appending a member a second time', () => {
        const set = simpleSet;

        appendOrderedSet(set, 'i1');
        expect(() => appendOrderedSet(set, 'i1')).to.throw(RangeError);
    });
});
