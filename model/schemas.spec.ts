import { expect } from 'chai';
import * as Ajv from 'ajv';
import { ValidateFunction } from 'ajv';

import { MANDATORY_COPIES } from './order';
import { FileStatus } from './byoth-file';
import { ThesisTypes, PrintFormats, DegreeTypes, PublishThesis } from './thesis';

import { FINAL_ORDER_SCHEMA, EVALUATION_ORDER_SCHEMA } from './schemas';

const ajv = new Ajv({allErrors: true});

describe('Validation schemas', () => {
    let thesis;
    let candidate;
    let defense;
    let inputFiles;
    let order;
    let container;
    let validate: ValidateFunction;

    beforeEach(() => {
        thesis = {
            title: 'foo bar',
            subtitle: '123',
            isbn: '123',
            publish: PublishThesis.WITH_EMBARGO,
            license: 'http://license.com',
            embargo: 6,
            thesisType: ThesisTypes.COLLECTION,
            degreeType: DegreeTypes.PHD,
            printFormat: PrintFormats.B17x24,
            abstract: {
                en: 'an abstract'
            }
        };
        candidate = { familyName: 'Foo', givenName: 'Bar' };
        defense = { start: '1999-01-01Z10:00' };
        inputFiles = { items: [{ errors: [], status: FileStatus.OK }]};
        order = {
            offer: { quantity: MANDATORY_COPIES + 1 },
            coverPreviewAccepted: true,
            delivery: {
                address: {
                    name: 'Foo Bar',
                    postalCode: '1234',
                    city: 'Baz',
                    streetAddress: 'Foo-street 1'
                }
            },
            customer: {
                name: 'Foo Bar',
                telephone: '1234',
                email: 'foo@bar.com'
            }
        };
    });

    function setupOrder() {
        container =  {} as any;
        container.thesis = thesis;
        container.candidate = candidate;
        container.defense = defense;
        container.inputFiles = inputFiles;
        container.finalOrder = order;
        container.evaluationOrder = order;
    }

    describe('FINAL_ORDER_SCHEMA', () => {
        beforeEach(() => {
            validate = ajv.compile(FINAL_ORDER_SCHEMA);
        });

        it('should validate complete order', () => {
            setupOrder();
            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should reject an order without thesis title', () => {
            thesis.title = undefined;
            setupOrder();

            const valid = validate(container);

            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.thesis');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'title');
        });

        it('should reject an order with empty string thesis title', () => {
            thesis.title = '';
            setupOrder();

            const valid = validate(container);

            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.thesis.title');
            expect(validate.errors[0]).to.have.property('params').that.has.property('limit');
        });

         it('should reject an order with empty english abstract', () => {
            thesis.abstract.en = '';
            setupOrder();

            let valid = validate(container);

            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.thesis.abstract.en');
            expect(validate.errors[0]).to.have.property('params').that.has.property('limit');

            thesis.abstract = { nb: 'norsk abstract' };
            setupOrder();

            valid = validate(container);

            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.thesis.abstract');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'en');
        });

        it('should reject an order without ISBN', () => {
            thesis.isbn = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.thesis');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'isbn');
        });

        it('should reject an order without publication decision', () => {
            thesis.publish = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.thesis');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'publish');
        });

        it('should reject an order with positive publication decision but without license', () => {
            thesis.publish = PublishThesis.WITHOUT_EMBARGO;
            thesis.license = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(2);
        });

        it('should accept an order with positive non-embargoed publication decision and license', () => {
            thesis.publish = PublishThesis.WITHOUT_EMBARGO;
            thesis.embargo = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should accept an order with positive embargoed publication decision and license', () => {
            thesis.publish = PublishThesis.WITH_EMBARGO;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should reject an order with an embargoed license without specified embargo', () => {
            thesis.publish = PublishThesis.WITH_EMBARGO;
            thesis.embargo = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(2);
        });

        it('should reject an order without candidate family name', () => {
            candidate.familyName = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.candidate');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'familyName');
        });

        it('should reject an order with empty string candidate family name', () => {
            candidate.familyName = '';
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.candidate.familyName');
            expect(validate.errors[0]).to.have.property('params').that.has.property('limit');
        });

        it('should reject an order without candidate given name', () => {
            candidate.givenName = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.candidate');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'givenName');
        });

        it('should reject an order with empty string candidate given name', () => {
            candidate.givenName = '';
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.candidate.givenName');
            expect(validate.errors[0]).to.have.property('params').that.has.property('limit');
        });

        it('should reject an order without defense date', () => {
            defense.start = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.defense');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'start');
        });

        it('should reject an order without inputFiles', () => {
            inputFiles = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'inputFiles');
        });

        it('should reject an order without files in inputFiles', () => {
            inputFiles.items = [];
            setupOrder();

            let valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.inputFiles.items');
            expect(validate.errors[0]).to.have.property('params').that.has.property('limit', 1);

            inputFiles.items = undefined;
            valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.inputFiles');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'items');
        });

        it('should reject an order with files that are not validated', () => {
            inputFiles.items = [{ errors: [], status: FileStatus.ERROR }];
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.inputFiles.items[0].status');
            expect(validate.errors[0]).to.have.property('params').that.has.property('allowedValues');
        });

        it('should reject an order without accepted cover preview', () => {
            order.coverPreviewAccepted = false;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.finalOrder.coverPreviewAccepted');
            expect(validate.errors[0]).to.have.property('params').that.has.property('allowedValues');
        });

        it('should reject an order with less than MANDATORY_COPIES + 1 copies', () => {
            order.offer.quantity = MANDATORY_COPIES - 1;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.finalOrder.offer.quantity');
            expect(validate.errors[0]).to.have.property('params').that.has.property('limit', MANDATORY_COPIES + 1);
        });

        it('should reject an order without customer information', () => {
            order.customer.name = '';
            order.customer.telephone = '';
            order.customer.email = '';
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(3);
        });

        it('should reject an order without delivery information', () => {
            order.delivery.address.name = '';
            order.delivery.address.postalCode = '';
            order.delivery.address.streetAddress = '';
            order.delivery.address.city = '';
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(4);
        });

        it('should reject an order with incomplete proof address', () => {
            order.delivery.proofAddress = { name: 'foo' };

            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(3);
        });
    });

    describe('EVALUATION_ORDER_SCHEMA', () => {
        beforeEach(() => {
            validate = ajv.compile(EVALUATION_ORDER_SCHEMA);
        });

        it('should validate complete order', () => {
            setupOrder();
            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should accept an order without ISBN', () => {
            thesis.isbn = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should accept an order without publicationd decision', () => {
            thesis.publish = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should accept an order without license agreement', () => {
            thesis.license = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should reject an order without thesis title', () => {
            thesis.title = undefined;
            setupOrder();

            const valid = validate(container);

            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.thesis');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'title');
        });

        it('should reject an order without candidate family name', () => {
            candidate.familyName = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.candidate');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'familyName');
        });

        it('should reject an order without candidate given name', () => {
            candidate.givenName = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.candidate');
            expect(validate.errors[0]).to.have.property('params').that.has.property('missingProperty', 'givenName');
        });

        it('should accept an order without defense date', () => {
            defense.start = undefined;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should reject an order without files', () => {
            inputFiles.items = [];
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.inputFiles.items');
            expect(validate.errors[0]).to.have.property('params').that.has.property('limit', 1);
        });

        it('should reject an order with files that are not validated', () => {
            inputFiles.items = [{ errors: [], status: FileStatus.ERROR }];
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.inputFiles.items[0].status');
            expect(validate.errors[0]).to.have.property('params').that.has.property('allowedValues');
        });

        it('should reject an order without accepted cover preview', () => {
            order.coverPreviewAccepted = false;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
            expect(validate.errors).to.have.length(1);
            expect(validate.errors[0]).to.have.property('dataPath', '.evaluationOrder.coverPreviewAccepted');
            expect(validate.errors[0]).to.have.property('params').that.has.property('allowedValues');
        });

        it('should accept an order with less than MANDATORY_COPIES + 1 copies', () => {
            order.offer.quantity = MANDATORY_COPIES - 1;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should accept an order with 1 copies', () => {
            order.offer.quantity = 1;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should accept an order with printProofs false', () => {
            order.offer.printProofs = false;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(true);
        });

        it('should reject an order with printProofs true', () => {
            order.offer.printProofs = true;
            setupOrder();

            const valid = validate(container);
            expect(valid).to.equal(false);
        });
    });
});
