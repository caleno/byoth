import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultEmptyString, defaultNull } from './ld-model';

import { prefixes } from './prefixes';

const TYPE = 'byoth:Person';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    owns: { '@id': 'byoth:ownsObject', '@type': '@id' },
    name: { '@id': 'schema:name' },
    email: { '@id': 'schema:email' }
}

export const USER_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@type': TYPE,
    '@explicit': true,
    name: defaultEmptyString,
    email: defaultEmptyString,
    owns: defaultNull
};

export interface UserProperties {
    name: string,
    email: string,
    owns?: string
}

export interface User extends CompactedDocumentHeader, UserProperties {}
