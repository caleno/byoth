import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultNull, JsonLdIdProperty } from './ld-model';

import { Candidate } from './candidate';
import { Department } from './department';
import { Defense } from './defense'
import { LdpSet } from './ldp-set';
import { Order } from './order';
import { Thesis } from './thesis';
import { TrialLecture } from './trial-lecture';

import { prefixes } from './prefixes';

const TYPE = 'byoth:ThesisContainer';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    thesis: { '@id': 'byoth:hasThesis' },
    candidate: { '@id': 'byoth:hasCandidate' },
    creator: { '@id': 'dct:creator', '@type': '@id' },
    created: { '@id': 'fedora:created', '@type': 'xsd:dateTime' },
    modified: { '@id': 'fedora:lastModified', '@type': 'xsd:dateTime' },
    department: { '@id': 'byoth:degreeAwardedAt' },
    defense: { '@id': 'byoth:hasDefense' },
    trialLecture: { '@id': 'byoth:hasTrialLecture' },
    inputFiles: { '@id': 'byoth:hasInputFileCollection' },
    articles: { '@id': 'byoth:hasArticles' },
    finalOrder: { '@id': 'byoth:hasFinalOrder' },
    evaluationOrder: { '@id': 'byoth:hasEvaluationOrder'},
    finalContentPreview: { '@id': 'byoth:hasFinalContentPreviewFile' },
    evaluationContentPreview: { '@id': 'byoth:hasEvalutionContentPreviewFile' },
    archiveFile: { '@id': 'byoth:hasArchiveFile' },
    finalCoverPreview: { '@id': 'bytoh:hasFinalCoverPreview' },
    evaluationCoverPreview: { '@id': 'bytoh:hasEvaluationCoverPreview' },
    approvalDate: { '@id': 'byoth:hasApprovalDate', '@type': 'xsd:dateTime' },
    submissionDate: { '@id': 'byoth:hasSubmissionDate', '@type': 'xsd:dateTime' },
};

export const THESIS_CONTAINER_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@type': TYPE,
    '@explicit': true,
    creator: defaultNull,
    created: defaultNull,
    modified: defaultNull,
    approvalDate: defaultNull,
    submissionDate: defaultNull,
    thesis: defaultNull,
    candidate: defaultNull,
    department: defaultNull,
    defense: defaultNull,
    trialLecture: defaultNull,
    articles: defaultNull,
    finalOrder: defaultNull,
    evalutionOrder: defaultNull,
    archiveFile: defaultNull,
    inputFiles: defaultNull,
    finalCoverPreview: defaultNull,
    evaluationCoverPreview: defaultNull,
    finalContentPreview: defaultNull,
    evaluationsContentPreview: defaultNull,
};

export interface ThesisContainerProperties {
    thesis: Thesis | JsonLdIdProperty,
    candidate: Candidate | JsonLdIdProperty,
    creator: string,
    created: string,
    modified: string,
    approvalDate: string,
    submissionDate: string,
    department: Department | JsonLdIdProperty,
    defense: Defense | JsonLdIdProperty,
    trialLecture: TrialLecture | JsonLdIdProperty,
    articles: LdpSet | JsonLdIdProperty,
    finalOrder: Order | JsonLdIdProperty,
    evaluationOrder: Order | JsonLdIdProperty,
    inputFile: LdpSet | JsonLdIdProperty,
    archiveFile: JsonLdIdProperty,
    finalCoverPreview: JsonLdIdProperty,
    evaluationCoverPreview: JsonLdIdProperty,
    finalContentPreview: JsonLdIdProperty,
    evaluationContentPreview: JsonLdIdProperty,
}

export interface ThesisContainer
    extends CompactedDocumentHeader, ThesisContainerProperties { }
