import { JsonLdContext } from './ld-model';

/**
 * Model definitions for ordered sets
 *
 * Based on the PCMD ordering extension.
 * Provides fragments of context and frame to allow it being integrated into other models.
 * Can also be used standalone with the non-fragment versions.
 */

import { prefixes } from './prefixes';

// Short ids are used for proxies
import * as shortid from 'shortid';

const TYPE = 'pcdm:Collection';

/** JSON LD Context fragment for an ordered set, that can be inserted into other contexts */
export const ORDEREDSET_CONTEXT_FRAGMENT: JsonLdContext = {
    first: { '@id': 'iana:first', '@type': '@id' },
    last: { '@id': 'iana:last', '@type': '@id' },
    next: { '@id': 'iana:next', '@type': '@id' },
    prev: { '@id': 'iana:prev', '@type': '@id' },
    proxyFor: { '@id': 'ore:proxyFor', '@type': '@id' },
    proxyIn: { '@id': 'ore:proxyIn', '@type': '@id' },
    proxies: { '@reverse': 'proxyIn', '@container': '@set' }
}

/** JSON LD Context for an ordered set */
export const ORDEREDSET_CONTEXT: JsonLdContext = {
    ...prefixes,
    ...ORDEREDSET_CONTEXT_FRAGMENT,
}

/** JSON LD frame fragment that can be inserted into other frames */
export const ORDEREDSET_FRAME_FRAGMENT = {
    '@type': TYPE,
    first: { '@embed': '@never'},
    last: { '@embed': '@never'},
    proxies: [{
        '@type': 'ore:Proxy',
        proxyFor: { '@embed': '@never' },
        next: { '@embed': '@never' },
        prev: { '@embed': '@never' },
        '@embed': '@always',
        '@explicit': true
    }],
    '@explicit': true
}

/** JSON LD frame for an ordered set */
export const ORDEREDSET_FRAME = {
    '@context': ORDEREDSET_CONTEXT,
    ...ORDEREDSET_FRAME_FRAGMENT
}

/** ORE Proxy*/
export interface OreProxy {
    '@type': string,
    '@id': string,
    proxyFor: string,
    next?: string,
    prev?: string
}

/** Ordered set */
export interface OrderedSet {
    '@type': string,
    '@id': string,
    first?: string,
    last?: string,
    proxies?: OreProxy[],
}

function proxyFor(setId, member): OreProxy {
    return {
        '@type': 'ore:Proxy',
        '@id': setId + (setId.includes('#') ? '-' : '#' ) + 'proxy-' + shortid.generate(),
        proxyFor: member,
        next: null,
        prev: null,
    };
}

function normalizeSet(set: OrderedSet) {
    if (!set.proxies) {
        set.proxies = [];
        set.first = null;
        set.last = null;
    }
}

/** Create an empty ordered set */
export function createEmptyOrderedSet(containerId, setName): OrderedSet {
    return {
        '@id': containerId + '#' + setName,
        '@type': TYPE,
        first: null,
        last: null,
        proxies: []
    }
}

/** Get the members of an ordered set as an ordered array of id's */
export function getOrderedSetArray(set: OrderedSet): string[] {
    normalizeSet(set);

    let current = set.proxies.find(proxy => proxy['@id'] === set.first);
    const result = [];

    while (current) {
        result.push(current.proxyFor);
        current = set.proxies.find(proxy => proxy['@id'] === current.next);
    }

    return result;
}

/** Set the members of a set, in a given order. Any previous values will be over-written */
export function setOrderedSetArray(set: OrderedSet, members: string[]) {
    set.proxies = [];
    set.first = null;
    set.last = null;
    members.forEach((member, index) => {
        const proxy = proxyFor(set['@id'], member);

        if (index === 0) {
            set.first = proxy['@id'];
        }

        if (index === members.length - 1) {
            set.last = proxy['@id'];
        }

        if (index > 0) {
            const prevProxy = set.proxies[index - 1];
            proxy.prev = prevProxy['@id'];
            prevProxy.next = proxy['@id'];
        }

        set.proxies.push(proxy);
    });

}

/** Delete a member from set */
export function deleteOrderedSetMember(set: OrderedSet, memberToDelete: string) {
    normalizeSet(set);

    const proxyToDeleteIndex = set.proxies.findIndex(proxy => proxy.proxyFor === memberToDelete);

    if (proxyToDeleteIndex === -1) {
        throw RangeError(`Member '${memberToDelete}' is not a member of set`);
    }

    const proxyToDelete = set.proxies[proxyToDeleteIndex];

    if (proxyToDelete.prev) {
        const prevProxy = set.proxies.find(proxy => proxy['@id'] === proxyToDelete.prev);
        prevProxy.next = proxyToDelete.next;

        if (!proxyToDelete.next) {
            set.last = proxyToDelete.prev;
        }
    } else {
        set.first = proxyToDelete.next;
    }

    if (proxyToDelete.next) {
        const nextProxy = set.proxies.find(proxy => proxy['@id'] === proxyToDelete.next);

        nextProxy.prev = proxyToDelete.prev;

        if (!proxyToDelete.prev) {
            set.first = proxyToDelete.next;
        }
    } else {
        set.last = proxyToDelete.prev;
    }

    set.proxies.splice(proxyToDeleteIndex, 1);
}

/** Append a member to set, if it already is in members, only a proxy will be added */
export function appendOrderedSet(set: OrderedSet, memberToAdd: string) {
    normalizeSet(set);

    if (set.proxies.find(proxy => proxy.proxyFor === memberToAdd)) {
        throw RangeError(`Member '${memberToAdd}' is already in set`);
    }

    const lastProxy = set.proxies.find(proxy => proxy['@id'] === set.last);
    const newProxy = proxyFor(set['@id'], memberToAdd);

    set.proxies.push(newProxy);

    if (lastProxy) {
        lastProxy.next = newProxy['@id'];
        newProxy.prev = lastProxy['@id'];
    } else {
        set.first = newProxy['@id'];
    }

    set.last = newProxy['@id'];
}

/** Replace a member by another */
export function replaceOrderedSetItem(set: OrderedSet, oldMember: string, newMember: string) {
    normalizeSet(set);

    const oldMemberIndex = set.proxies.findIndex(proxy => proxy.proxyFor === oldMember);

    if (oldMemberIndex < 0) {
        throw RangeError(`Can not replace member '${oldMember}' in ordered set. No such member.`);
    }

    set.proxies[oldMemberIndex].proxyFor = newMember;
}

