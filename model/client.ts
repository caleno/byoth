import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultEmptyString } from './ld-model';

import { prefixes } from './prefixes';

const TYPE = 'foaf:Agent';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    name: { '@id': 'schema:name' },
}

export const CLIENT_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@type': TYPE,
    '@explicit': true,
    name: defaultEmptyString,

};

export interface ClientProperties {
    name: string,
}

export interface Client extends CompactedDocumentHeader, ClientProperties {}
