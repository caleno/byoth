import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultNull } from './ld-model';

import { prefixes } from './prefixes';

const TYPE = 'byoth:Institution';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    facultyName: { '@id': 'byoth:facultyName', '@container': '@language' },
    departmentName: { '@id': 'byoth:departmentName', '@container': '@language' },
    facultyId: { '@id': 'byoth:facultyId' },
    departmentId: { '@id': 'byoth:departmentId' }
}

export const DEPARTMENT_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@type': TYPE,
    '@explicit': true,
    facultyName: '', // don't know why framing fails with default null / ""
    departmentName: '',
    facultyId: defaultNull,
    departmentId: defaultNull
}

export interface DepartmentProperties {
    departmentName: {
        [property: string]: string
    }[],
    facultyName: {
        [property: string]: string
    }[],
    facultyId: string,
    departmentId: string,
}

export interface Department extends CompactedDocumentHeader, DepartmentProperties {}
