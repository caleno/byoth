import { PublishThesis, PrintFormats, ThesisTypes, DegreeTypes } from './thesis';
import { FileStatus } from './byoth-file';
import { MANDATORY_COPIES } from './order';

/**
 * JSON Schemas that define what properties are necessary for a certain action *
 */

/*********************************/
/*** Reusable schema fragments ***/
/*********************************/

/** Customer as in the Order model */
const CUSTOMER_SUB_SCHEMA = {
    description: 'Customer information',
    type: 'object',
    properties: {
        name: {
            type: 'string',
            minLength: 1
        },
        telephone: {
            type: 'string',
            minLength: 1
        },
        email: {
            type: 'string',
            minLength: 1
        }
    },
    required: ['name', 'telephone', 'email']
};

/** Address schema in Order deliveries */
const ADDRESS_SUB_SCHEMA = {
    description: 'Delivery address',
    type: 'object',
    properties: {
        name: {
            type: 'string',
            minLength: 1
        },
        postalCode: {
            type: 'string',
            minLength: 1,
        },
        streetAddress: {
            type: 'string',
            minLength: 1
        },
        city: {
            type: 'string',
            minLength: 1
        }
    },
    required: ['name', 'postalCode', 'streetAddress', 'city']
};

/** Delivery as in the Order model */
const DELIVERY_SUB_SCHEMA = {
    description: 'Delivery information',
    type: 'object',
    properties: {
        address: ADDRESS_SUB_SCHEMA,
        proofAddress: ADDRESS_SUB_SCHEMA
    },
    required: ['address']
};

/** Candidate as in the Candidate model */
const CANDIDATE_SUB_SCHEMA = {
    description: 'Thesis candidate',
    type: 'object',
    properties: {
        givenName: {
            description: 'Given name',
            type: 'string',
            minLength: 1
        },
        familyName: {
            description: 'Family name',
            type: 'string',
            minLength: 1
        },
    },
    required: ['familyName', 'givenName']
};

/** Schema for list of files */
const FILES_SUB_SCHEMA = {
    description: 'Files',
    type: 'object',
    properties: {
        items: {
            description: 'List of files',
            type: 'array',
            minItems: 1,
            items: {
                description: 'File metadata',
                type: 'object',
                properties: {
                    status: {
                        enum: [ FileStatus.OK, FileStatus.WARNING ]
                    }
                },
                required: ['status']
            }
        }
    },
    required: ['items']
};


/**********************/
/*** Actual schemas ***/
/**********************/

/** Schema for final submission of thesis for printing and archiving */
export const FINAL_ORDER_SCHEMA = {
    $schema: 'http://json-schema.org/draft-07/schema#',
    $id: 'http://purl.org/byoth/final-order.schema.json',
    title: 'Final Order',
    description: 'A BYOTh thesis container for final order',
    type: 'object',
    required: ['thesis', 'candidate', 'defense', 'inputFiles', 'finalOrder'],
    properties: {
        thesis: {
            description: 'Thesis metadata',
            type: 'object',
            properties: {
                title: {
                    description: 'Thesis title',
                    type: 'string',
                    minLength: 3
                },
                subtitle: {
                    description: 'Thesis subtitle',
                    type: 'string',
                },
                abstract: {
                    description: 'Abstract',
                    type: 'object',
                    properties: {
                        en: {
                            type: 'string',
                            description: 'English abstract',
                            minLength: 1
                        }
                    },
                    required: ['en']
                },
                isbn: {
                    description: 'ISBN',
                    type: 'string'
                },
                publish: {
                    description: 'Decision to publish or not',
                    enum: [ PublishThesis.DONT, PublishThesis.WITH_EMBARGO, PublishThesis.WITHOUT_EMBARGO ]
                },
                license: {
                    description: 'Publication license',
                    type: 'string'
                },
                embargo: {
                    description: 'Embargo months',
                    type: 'number'
                },
                thesisType: {
                    description: 'Thesis type',
                    enum: [ ThesisTypes.COLLECTION, ThesisTypes.MONOGRAPH ]
                },
                degreeType: {
                    description: 'Degree type',
                    enum: [ DegreeTypes.PHD, DegreeTypes.DR_PHILOS ]
                },
                printFormat: {
                    description: 'Format to print thesis in',
                    enum: [ PrintFormats.A4, PrintFormats.B17x24 ]
                }
            },
            allOf: [
                {
                    if: {
                        properties: {
                            publish: {
                                enum: [ PublishThesis.WITH_EMBARGO, PublishThesis.WITHOUT_EMBARGO ]
                            }
                        }
                    },
                    then: {
                        required: [ 'license' ]
                    }
                },
                {
                    if: {
                        properties: {
                            publish: {
                                enum: [ PublishThesis.WITH_EMBARGO ]
                            }
                        }
                    },
                    then: {
                        required: [ 'embargo' ]
                    }
                }
            ],
            required: [ 'title', 'isbn', 'publish', 'printFormat', 'thesisType', 'degreeType', 'abstract' ]
        },
        candidate: CANDIDATE_SUB_SCHEMA,
        defense: {
            description: 'Thesis defense',
            type: 'object',
            properties: {
                date: {
                    description: 'Defense date',
                    type: 'string',
                    minLength: 1,
                }
            },
            required: ['start']
        },
        inputFiles: FILES_SUB_SCHEMA,
        finalOrder: {
            description: 'Order specification for printing',
            type: 'object',
            properties: {
                coverPreviewAccepted: {
                    description: 'Has customer accepted the thesis cover preview',
                    enum: [ true ]
                },
                offer: {
                    properties: {
                        quantity: {
                            description: 'Quantity ordered',
                            minimum: MANDATORY_COPIES + 1
                        }
                    }
                },
                delivery: DELIVERY_SUB_SCHEMA,
                customer: CUSTOMER_SUB_SCHEMA
            },
            required: ['coverPreviewAccepted', 'offer', 'delivery', 'customer']
        }
    }
}

/** Schema for submission of thesis for printing evaluation copies */
export const EVALUATION_ORDER_SCHEMA = {
    $schema: 'http://json-schema.org/draft-07/schema#',
    $id: 'http://purl.org/byoth/committee-order.schema.json',
    title: 'Committee Order',
    description: 'A BYOTh thesis container for order of committee copies',
    type: 'object',
    required: ['thesis', 'candidate', 'inputFiles', 'evaluationOrder'],
    properties: {
        thesis: {
            description: 'Thesis metadata',
            type: 'object',
            properties: {
                title: {
                    description: 'Thesis title',
                    type: 'string',
                },
                subtitle: {
                    description: 'Thesis subtitle',
                    type: 'string',
                },
                degreeType: {
                    description: 'Degree type',
                    enum: [ DegreeTypes.PHD, DegreeTypes.DR_PHILOS ]
                },
                printFormat: {
                    description: 'Format to print thesis in',
                    enum: [ PrintFormats.A4, PrintFormats.B17x24 ]
                }
            },
            required: [ 'title', 'printFormat' ]
        },
        candidate: CANDIDATE_SUB_SCHEMA,
        inputFiles: FILES_SUB_SCHEMA,
        evaluationOrder: {
            description: 'Order specification for printing',
            type: 'object',
            properties: {
                coverPreviewAccepted: {
                    description: 'Has customer accepted the thesis cover preview',
                    enum: [ true ]
                },
                offer: {
                    properties: {
                        quantity: {
                            description: 'Quantity ordered',
                            minimum: 1
                        },
                        printProofs: {
                            description: 'Order printed proofs',
                            type: 'boolean',
                            enum: [ false, undefined, null ]
                        }
                    }
                },
                customer: CUSTOMER_SUB_SCHEMA,
                delivery: DELIVERY_SUB_SCHEMA
            }
        }
    }
}
