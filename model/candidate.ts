import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultEmptyString, defaultNull } from './ld-model';

import { prefixes } from './prefixes';

const TYPE = 'byoth:Person';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    givenName: { '@id': 'schema:givenName' },
    familyName: { '@id': 'schema:familyName' },
    email: { '@id': 'schema:email' },
    birthDate: { '@id': 'dbo:birthDate', '@type': 'xsd:date' },
    orcid: { '@id': 'dbo:orcidId' },
    gender: { '@id': 'schema:gender' }
}

export const CANDIDATE_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@type': TYPE,
    '@explicit': true,
    givenName: defaultEmptyString,
    familyName: defaultEmptyString,
    email: defaultEmptyString,
    birthDate: defaultNull,
    orcid: defaultNull,
    gender: defaultNull,
};

export interface CandidateProperties {
    givenName: string,
    familyName: string,
    email: string,
    brithDate: string,
    orcid: string,
    gender: string
}

export interface Candidate extends CompactedDocumentHeader, CandidateProperties { }
