import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultNull } from './ld-model';

import { prefixes } from './prefixes';

export const MANDATORY_COPIES = 8;

// need these to be non-zero
export enum OrderTypes {
    FINAL = 1,
    EVALUATION = 2
}

const CONTEXT: JsonLdContext = {
    ...prefixes,
    orderNumber: { '@id': 'schema:orderNumber' },
    confirmationNumber: { '@id': 'schema:confirmationNumber' },
    offer: { '@id': 'schema:acceptedOffer' },
    customer: { '@id': 'schema:customer' },
    name: { '@id': 'schema:name'},
    price: { '@id': 'schema:price', },
    currency: { '@id': 'schema:priceCurrency' },
    quantity: { '@id': 'schema:eligibleQuanitity' },
    printProofs: { '@id': 'byoth:printProofs' },
    printColor: { '@id': 'byoth:printColor' },
    agent: { '@id': 'schema:agent' },
    date: { '@id': 'schema:orderDate'},
    delivery: { '@id': 'schema:orderDelivery' },
    address: { '@id': 'schema:deliveryAddress' },
    proofAddress: { '@id': 'byoth:proofDeliveryAddress' },
    postalCode: { '@id': 'schema:postalCode' },
    streetAddress: { '@id': 'schema:streetAddress' },
    city: { '@id': 'schema:locality' },
    telephone: { '@id': 'schema:telephone' },
    email: { '@id': 'schema:email' },
    isTest: { '@id': 'byoth:isTest' },
    status: { '@id': 'schema:orderStatus' },
    haveReadProofs: { '@id': 'byoth:haveReadProofs' },
    description: { '@id': 'schema:description' },
    coverPreviewAccepted: { '@id': 'byoth:isCoverPreviewAccepted' },
};

export const ORDER_FRAME: JsonLdFrame = {
    '@type': 'byoth:Order',
    '@context': CONTEXT,
    '@explicit': true,
    offer: {
        '@type': 'schema:Offer',
        '@explicit': true,
        quantity: { '@default': 1 },
        price: defaultNull,
        currency: { '@default': 'NOK' },
        printProofs: { '@default': false },
        printColor: { '@default': false }
    },
    customer: {
        '@type': 'byoth:Person',
        '@explicit': true,
        name: defaultNull,
        telephone: defaultNull,
        email: defaultNull,
    },
    orderNumber: defaultNull,
    confirmationNumber: defaultNull,
    agent: defaultNull,
    delivery: {
        '@type': 'schema:ParcelDelivery',
        '@explicit': true,
        description: defaultNull,
        address: {
            '@type': 'schema:PostalAddress',
            '@explicit': true,
            name: defaultNull,
            postalCode: defaultNull,
            streetAddress: defaultNull,
            city: defaultNull,
        },
        proofAddress: {
            '@type': 'schema:PostalAddress',
            '@explicit': true,
            name: defaultNull,
            postalCode: defaultNull,
            streetAddress: defaultNull,
            city: defaultNull,
        }
    },
    date: defaultNull,
    isTest: { '@default': false },
    haveReadProofs: { '@default': false },
    coverPreviewAccepted: { '@default': false },
    status: {
        '@type': 'schema:OrderStatus',
        description: defaultNull,
    }
}

export interface Customer {
    '@type': string,
    '@id': string,
    name?: string,
    telephone?: string,
    email?: string
}

export interface AddressProperties {
    postalCode?: string,
    streetAddress?: string,
    city?: string,
    name?: string
}

export interface Address extends AddressProperties {
    '@type': string,
    '@id': string,
}


export interface Delivery {
    '@type': string,
    '@id': string,
    description?: string,
    address?: Address,
    proofAddress?: Address,
}

export interface Offer {
    '@type': string,
    '@id': string,
    quantity?: number,
    price?: string,
    currency?: string,
    numPages?: number,
    printColor?: boolean,
    printProofs?: boolean
}

export interface OrderStatus {
    '@type': string,
    '@id': string,
    description: string,
}

export type Specification = Offer;

export interface OrderProperties {
    orderNumber?: string,
    confirmationNumber?: string,
    customer?: Customer,
    offer?: Offer,
    agent?: string,
    date?: string,
    delivery?: Delivery,
    isTest?: boolean,
    haveReadProofs?: boolean,
    coverPreviewAccepted?: boolean,
    status?: OrderStatus,
    printProofs?: boolean
}

export interface Order extends CompactedDocumentHeader, OrderProperties { }

export function setOrderStatus(order: Order, status: string | null) {
    order.status = {
        '@id': order['@id'] + '#status',
        '@type': ORDER_FRAME.status['@type'],
        description: status
    }
}

export function initOrderOffer(order: Order) {
    order.offer = {
        '@id': order['@id'] + '#offer',
        '@type': 'schema:Offer',
        quantity: 1
    };
}

export function initOrderDelivery(order: Order) {
    order.delivery = {
        '@id': order['@id'] + '#delivery',
        '@type': 'schema:ParcelDelivery',
        address: {
            '@id': order['@id'] + '#delivery-address',
            '@type': 'schema:PostalAddress'
        }
    };
}

export function addOrderProofDeliveryAddress(order: Order) {
    order.delivery.proofAddress = {
        '@id': order['@id'] + '#delivery-proof-address',
        '@type': 'schema:PostalAddress'
    };
}

export function initOrderCustomer(order: Order) {
    order.customer = {
        '@id': order['@id'] + '#customer',
        '@type': 'byoth:Person',
    }
}
