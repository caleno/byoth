import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultNull } from './ld-model';

import { prefixes } from './prefixes';

const TYPE = 'ldp:Container';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    items: { '@id': 'ldp:contains', '@container': '@set' }
};

export const LDPSET_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@type': TYPE,
    '@explicit': true,
    items: defaultNull
}

export interface LdpSetProperties {
    items: any[]
}

export interface LdpSet extends CompactedDocumentHeader, LdpSetProperties { }


