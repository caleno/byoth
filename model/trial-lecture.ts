import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultEmptyString } from './ld-model';

import { ByothEventProperties, SCHEMA_EVENT_FRAME_FRAGMENT, SCHEMA_EVENT_CONTEXT_FRAGMENT } from './event';

import { prefixes } from './prefixes';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    ...SCHEMA_EVENT_CONTEXT_FRAGMENT,
    title: { '@id': 'dct:title' }
};

export const TRIAL_LECTURE_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    ...SCHEMA_EVENT_FRAME_FRAGMENT,
    title: defaultEmptyString
};


export interface TrialLectureProperties extends ByothEventProperties {
    title: string
}

export interface TrialLecture extends CompactedDocumentHeader, TrialLectureProperties { }
