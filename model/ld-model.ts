/** An element or 'row' in a JSON-LD context object. */
interface JsonLdContextElement {
    '@id'?: string,
    '@type'?: string,
    '@container'?: string,
    '@reverse'?: string
}

/** The context section of a JSON-LD document or frame. */
export interface JsonLdContext {
    [property: string]: string | JsonLdContextElement
}

/** A JSON-LD frame */
export interface JsonLdFrame {
    '@context': JsonLdContext,
    '@explicit': boolean,
    '@type': string, /// Expected to be a compact IRI
    [property: string]: any
}

export interface JsonLdIdProperty {
    '@id': string
}

/** The elements that all compacted JSON-LD document should have. */
export interface CompactedDocumentHeader {
    '@context': any,
    '@type': string,
    '@id': string,
}

export interface DocumentProperties {
    [property: string]: any
}

/** A compacted JSON-LD document */
export interface CompactedDocument extends CompactedDocumentHeader, DocumentProperties {}

/** */
export interface DocumentModel {
    expandedType: string,
    frame: JsonLdFrame,
}

/** Short-hand to be used when defining frames. */
export const defaultNull = { '@default': null };
/** Short-hand to be used when defining frames. */
export const defaultEmptyString = { '@default': '' };
/** Short-hand to be used when defining frames. */
export const defaultZero = { '@default': 0 };

/** Create a model document from a model, id and properties. */
export function createModelDocument(model: DocumentModel, id?: string, properties?: DocumentProperties): CompactedDocument {
    const doc: CompactedDocument = {
        '@context': model.frame['@context'],
        '@type': model.frame['@type'],
        '@id': id || null
    };

    if (properties) {
        return Object.assign(doc, properties);
    } else {
        return doc;
    }
}

/**
 * Expand a compact IRI to its full form, e.g.
 *     ex:foo -> http://example.org/foo
 * The prefix must be defined in the provided context.
 */
export function expandCompactIRI(compactIRI: string, context: JsonLdContext) {
    const components = compactIRI.split(/:/);
    const prefix = components[0];
    const suffix = components[1];

    if (!context[prefix]) {
        throw Error(`Prefix '${prefix}' not defined`);
    }

    return context[prefix] + suffix;
}

export function createModel(frame: JsonLdFrame, baseUrl: string): DocumentModel {
    const model = {
        expandedType: expandCompactIRI(frame['@type'], frame['@context']),
        frame: JSON.parse(JSON.stringify(frame)),
    };

    model.frame['@context']['@base'] = baseUrl;

    return model;
}
