import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame } from './ld-model';

import { ByothEventProperties, SCHEMA_EVENT_FRAME_FRAGMENT, SCHEMA_EVENT_CONTEXT_FRAGMENT } from './event';

import { prefixes } from './prefixes';

import shortid from 'shortid';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    ...SCHEMA_EVENT_CONTEXT_FRAGMENT,
    opponents: { '@id': 'byoth:hasOpponent', '@container': '@set' },
};

export const DEFENSE_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    ...SCHEMA_EVENT_FRAME_FRAGMENT,
    opponents: { '@type': 'byoth:Person' },
};

export interface DefenseOpponent {
    '@id': string,
    '@type': string,
    name: string
}

export interface DefenseProperties extends ByothEventProperties {
    opponents: DefenseOpponent[]
}

export interface Defense extends CompactedDocumentHeader, DefenseProperties { }

export function addDefenseOpponent(defense: Defense, name: string) {
    if (!defense.opponents) {
        defense.opponents = [];
    }

    defense.opponents.push({
        '@id': defense['@id'] + '#opponent-' + shortid.generate(),
        '@type': 'byoth:Person',
        name: name
    });
}
