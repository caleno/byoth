import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultEmptyString, defaultNull } from './ld-model';

import { prefixes } from './prefixes';

export const SCHEMA_EVENT_CONTEXT_FRAGMENT: JsonLdContext = {
    start: { '@id': 'schema:startDate', '@type': 'xsd:dateTime' },
    end: { '@id': 'schema:endDate', '@type': 'xsd:dateTime'},
    location: { '@id': 'schema:location' },
    name: { '@id': 'schema:name' },
    title: { '@id': 'dct:title' }
};

export const SCHEMA_EVENT_FRAME_FRAGMENT = {
    '@explicit': true,
    '@type': 'byoth:Event',
    start: defaultNull,
    end: defaultNull,
    location: {
        name: defaultEmptyString
    },
    title: defaultNull,
}

export const SCHEMA_EVENT_FRAME: JsonLdFrame = {
    '@context': {
        ...prefixes,
        ...SCHEMA_EVENT_CONTEXT_FRAGMENT
    },
    ...SCHEMA_EVENT_FRAME_FRAGMENT
};

export interface ByothEventProperties {
    start?: string,
    end?: string,
    location?: { '@id': string, name: string },
    title?: string
}

export interface ByothEvent extends CompactedDocumentHeader, ByothEventProperties { }

export function addEventLocation(event: ByothEvent, name: string) {
    event.location = {
        '@id': event['@id'] + '#location',
        name: name
    }
}

