import { ARTICLE_FRAME } from './article';
import { AUTHORIZATION_FRAME } from './authorization';
import { CANDIDATE_FRAME } from './candidate';
import { CLIENT_FRAME } from './client';
import { DEFENSE_FRAME } from './defense';
import { DEPARTMENT_FRAME } from './department';
import { LDPSET_FRAME } from './ldp-set';
import { ORDER_FRAME } from './order';
import { ORDEREDSET_FRAME } from './ordered-set';
import { PCDMFILE_FRAME } from './byoth-file';
import { THESIS_CONTAINER_FRAME } from './thesis-container';
import { THESIS_FRAME } from './thesis';
import { TRIAL_LECTURE_FRAME } from './trial-lecture';
import { USER_FRAME } from './user';
import { DocumentModel, createModel } from './ld-model';

export { JsonLdFrame, createModel, CompactedDocument, createModelDocument, DocumentModel } from './ld-model';
export { Article, Author } from './article';
export { Authorization } from './authorization';
export { Candidate } from './candidate';
export { Client } from './client';
export { Defense, addDefenseOpponent, DefenseOpponent } from './defense';
export { Department } from './department';
export { LdpSet } from './ldp-set';
export { OrderedSet, getOrderedSetArray, setOrderedSetArray, appendOrderedSet, deleteOrderedSetMember } from './ordered-set';
export { ByothFile, FileStatus } from './byoth-file';
export { addEventLocation, ByothEvent } from './event'
export { ThesisContainer } from './thesis-container';
export { Thesis } from './thesis';
export { TrialLecture } from './trial-lecture';
export { User } from './user';

/** All application models */
export interface ByothModel {
    article: DocumentModel,
    articles: DocumentModel,
    candidate: DocumentModel,
    defense: DocumentModel,
    department: DocumentModel,
    ldpSet: DocumentModel,
    evaluationOrder: DocumentModel,
    file: DocumentModel,
    files: DocumentModel,
    finalOrder: DocumentModel,
    inputFiles: DocumentModel,
    orderedSet: DocumentModel
    thesisContainer: DocumentModel,
    thesis: DocumentModel,
    trialLecture: DocumentModel,
}

export interface ByothInternalModel {
    authorization: DocumentModel,
    client: DocumentModel,
    user: DocumentModel,
}

/**
 * Get all models for the application with the correct `'@base'` in context and frame.
 */
export function getModel(baseUrl: string): ByothModel {
    const ldpSet = createModel(LDPSET_FRAME, baseUrl);
    const orderedSet = createModel(ORDEREDSET_FRAME, baseUrl);
    const order = createModel(ORDER_FRAME, baseUrl);

    return {
        article: createModel(ARTICLE_FRAME, baseUrl),
        articles: ldpSet,
        candidate: createModel(CANDIDATE_FRAME, baseUrl),
        defense: createModel(DEFENSE_FRAME, baseUrl),
        department: createModel(DEPARTMENT_FRAME, baseUrl),
        ldpSet: ldpSet,
        evaluationOrder: order,
        file: createModel(PCDMFILE_FRAME, baseUrl),
        files: ldpSet,
        finalOrder: order,
        inputFiles: orderedSet,
        orderedSet: orderedSet,
        thesisContainer: createModel(THESIS_CONTAINER_FRAME, baseUrl),
        thesis: createModel(THESIS_FRAME, baseUrl),
        trialLecture: createModel(TRIAL_LECTURE_FRAME, baseUrl),
    }
}

export function getInternalModel(baseUrl: string): ByothInternalModel {
    return {
        authorization: createModel(AUTHORIZATION_FRAME, baseUrl),
        client: createModel(CLIENT_FRAME, baseUrl),
        user: createModel(USER_FRAME, baseUrl),
    }
}
