import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultNull, defaultEmptyString, defaultZero } from './ld-model';
import { prefixes } from './prefixes';

const TYPE = 'byoth:Thesis';

const SERIES_TYPE = 'bibo:Series';

/** Thesis types */
export enum ThesisTypes {
    /** Collection thesis, i.e. article-based */
    COLLECTION = 'byoth:collectionThesis',
    /** Monograph thesis */
    MONOGRAPH = 'byoth:monographThesis',
}

/** Degree types */
export enum DegreeTypes {
    /** Regular PhD degree */
    PHD = 'byoth:phD',
    /** Dr Philos degree*/
    DR_PHILOS = 'byoth:drPhilos'
}

/** Print formats */
export enum PrintFormats {
    /** A4 paper */
    A4 = 'byoth:a4',
    /** 17x24 cm*/
    B17x24 = 'byoth:17x24'
}

/** Publication decisions */
export enum PublishThesis {
    /** Do not publish thesis */
    DONT = 'byoth:doNotPublish',
    /** Publish thesis with embargo */
    WITH_EMBARGO = 'byoth:publishWithEmbargo',
    /** Publish thesis without embargo */
    WITHOUT_EMBARGO = 'byoth:publishWithoutEmbargo'
}

const CONTEXT: JsonLdContext = {
    ...prefixes,
    thesisType: { '@id': 'byoth:hasType', '@type': '@id' },
    creator: { '@id': 'dct:creator', '@type': '@id' },
    title: { '@id': 'dct:title' },
    subtitle: { '@id': 'bibo:subtitle' },
    abstract: { '@id': 'dct:abstract', '@container': '@language' },
    language: { '@id': 'dct:language' },
    numPages: { '@id': 'bibo:numPages' },
    numColorPages: { '@id': 'byoth:numColorPages' },
    isbn: { '@id': 'bibo:isbn' },
    issn: { '@id': 'bibo:issn' },
    degreeType: { '@id': 'byoth:degreeType', '@type': '@id' },
    printFormat: { '@id': 'byoth:printFormat', '@type': '@id' },
    license: { '@id': 'dct:license', '@type': '@id' },
    series: { '@id': 'dct:isPartOf' },
    embargo: { '@id': 'byoth:numEmbargoMonths' },
    publish: { '@id': 'byoth:hasPublicationDecision', '@type': '@id' },
};

/** Frame to create Thesis from rdf */
export const THESIS_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@explicit': true,
    '@type': TYPE,
    thesisType: defaultNull,
    creator: defaultNull,
    title: defaultEmptyString,
    subtitle: defaultEmptyString,
    abstract: '',
    language: defaultNull,
    numPages: defaultZero,
    numColorPages: defaultZero,
    isbn: defaultNull,
    degreeType: { '@default': DegreeTypes.PHD },
    printFormat: { '@default': PrintFormats.B17x24 },
    license: defaultNull,
    series: {
        '@type': SERIES_TYPE,
        title: defaultEmptyString,
        issn: defaultEmptyString
    },
    embargo: defaultNull,
    publish: defaultNull,
};

/** Properties of a thesis (minus JSON-LD specifics for type, id, context */
export interface ThesisProperties  {
    thesisType: ThesisTypes,
    creator: string,
    title: string,
    subtitle: string,
    abstract: {
        [langCode: string]: string
    },
    language: string,
    numPages: number,
    numColorPages: number,
    isbn: string,
    degreeType: DegreeTypes,
    printFormat: PrintFormats,
    license: string,
    series?: {
        '@type': string,
        title: string,
        issn: string
    },
    embargo?: number,
    publish?: string
}

/** A JSON-LD document describing a thesis */
export interface Thesis extends CompactedDocumentHeader, ThesisProperties { }

