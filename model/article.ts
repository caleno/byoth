import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultEmptyString, defaultNull, defaultZero } from './ld-model';

import { prefixes } from './prefixes';
import { OrderedSet, appendOrderedSet, getOrderedSetArray,
         setOrderedSetArray, deleteOrderedSetMember, createEmptyOrderedSet,
         ORDEREDSET_CONTEXT_FRAGMENT, ORDEREDSET_FRAME_FRAGMENT } from './ordered-set';

import * as shortid from 'shortid';

const TYPE = 'byoth:AcademicArticle';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    ...ORDEREDSET_CONTEXT_FRAGMENT,
    articleType: { '@id': 'byoth:hasType', '@type': '@id' },
    version: { '@id': 'oaire:version', '@type': '@id' },
    doi: { '@id': 'bibo:doi' },
    title: { '@id': 'dct:title' },
    authors: { '@id': 'dct:creator', '@container': '@set' },
    authorOrder: { '@id': 'byoth:hasAuthorOrder'},
    name: { '@id': 'schema:name' },
    abstract: { '@id': 'dct:abstract' },
    publicationYear: { '@id': 'dct:issued' },
    series: { '@id': 'byoth:publishedInSeries', '@type': '@id' },
    book: { '@id': 'byoth:publishedInBook', '@type': '@id' },
    issn: { '@id': 'bibo:issn', '@container': '@set' },
    isbn: { '@id': 'bibo:isbn', '@container': '@set' },
    volume: { '@id': 'bibo:volume' },
    issue: { '@id': 'bibo:issue' },
    pageStart: { '@id': 'bibo:pageStart' },
    pageEnd: { '@id': 'bibo:pageEnd' },
    number: { '@id': 'bibo:number' },
    inFile: { '@id': 'byoth:inFile', '@type': '@id' },
    hasFile: { '@id': 'pcdm:hasFile', '@type': '@id' },
    publish: { '@id': 'byoth:hasPublicationDecision', '@type': '@id' },
    embargo: { '@id': 'byoth:numEmbargoMonths' },
    crossrefData: { '@id': 'byoth:crossrefData' }
}

export const ARTICLE_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@type': TYPE,
    '@explicit': true,
    articleType: defaultNull,
    version: defaultNull,
    doi: defaultNull,
    title: defaultEmptyString,
    authors: {
        '@type': 'byoth:Person',
        name: defaultNull,
        '@explicit': true,
        '@embed': '@always'
    },
    authorOrder: ORDEREDSET_FRAME_FRAGMENT,
    abstract: defaultEmptyString,
    publicationYear: defaultNull,
    series: {
        '@type': 'bibo:Series',
        title: defaultNull,
        issn: defaultNull,
        '@explicit': true
    },
    book: {
        '@type': 'bibo:Book',
        title: defaultNull,
        isbn: defaultNull,
        '@explicit': true
    },
    volume: defaultNull,
    issue: defaultNull,
    pageStart: defaultNull,
    pageEnd: defaultNull,
    number: defaultNull,
    inFile: defaultNull,
    hasFile: defaultNull,
    publish: defaultNull,
    embargo: defaultZero,
    crossrefData: defaultNull
}

export interface Author {
    '@id': string,
    '@type': string,
    name: string,
}

export interface Series {
    '@id': string,
    '@type': string,
    title?: string,
    issn?: string[]
}

export interface Book {
    '@id': string,
    '@type': string,
    title?: string,
    isbn?: string[]
}

export interface ArticleProperties {
    articleType?: string,
    version?: string,
    doi?: string,
    title?: string
    authors?: Author[],
    authorOrder?: OrderedSet,
    abstract?: string,
    publicationYear?: string,
    series?: Series,
    book?: Book,
    volume?: string,
    issue?: string,
    pageStart?: string,
    pageEnd?: string,
    /** Article number, if it doesn't have a page number */
    number?: string,
    /** If the article does not have its own file, but can be found in e.g. a thesis print files */
    inFile?: string,
    /** If the article has its own file that is not part of the thesis */
    hasFile?: string,
    publish?: string,
    embargo?: number,
    crossrefData?: string
}

export interface Article extends CompactedDocumentHeader, ArticleProperties { }

export const JOURNAL_ARTICLE = 'byoth:journalArticle';
export const BOOK_CHAPTER = 'byoth:bookChapter';
export const PROCEEDINGS_ARTICLE = 'byoth:proceedingsArticle';
export const UNPUBLISHED_ARTICLE = 'byoth:unpublishedArticle';

export const MANUSCRIPT = 'byoth:manuscript';
export const PREPRINT = 'byoth:preprint';
export const POSTPRINT = 'byoth:postprint';
export const PUBLISHERS_VERSION = 'byoth:publishersVersion';

export const DO_NOT_PUBLISH_ARTICLE = 'byoth:doNotPublish';
export const PUBLISH_ARTICLE_IF_POSSIBLE = 'byoth:publishIfPossible';
export const PUBLISH_MANUSCRIPT_WITHOUT_EMBARGO = 'byoth:publishWithoutEmbargo';
export const PUBLISH_MANUSCRIPT_WITH_EMBARGO = 'byoth:publishWithEmbargo';

export function addArticleAuthor(article: Article, name: string) {
    if (!article.authors) {
        article.authors = [];
    }
    if (!article.authorOrder) {
        article.authorOrder = createEmptyOrderedSet(article['@id'], 'authorOrder');
    }

    const newId = article['@id'] + '#author-' + shortid.generate();
    article.authors.push({
        '@id': newId,
        '@type': 'byoth:Person',
        name: name
    });

    appendOrderedSet(article.authorOrder, newId);
}

export function deleteArticleAuthor(article: Article, author: Author) {
    deleteOrderedSetMember(article.authorOrder, author['@id']);
    const index = article.authors.findIndex(a => a['@id'] === author['@id']);
    article.authors.splice(index, 1);
}

/** Makes sure the authors array is in the order specified by the authorOrder */
export function orderAuthorList(article: Article) {
    if (!article.authorOrder) {
        article.authorOrder = createEmptyOrderedSet(article['@id'], 'authorOrder');
    }

    const order = getOrderedSetArray(article.authorOrder);
    const newAuthorList = [];
    order.forEach(id => newAuthorList.push(article.authors.find(author => author['@id'] === id)));
    article.authors = newAuthorList;
}

export function redefineAuthorOrder(article: Article, authors: Author[]) {
    setOrderedSetArray(article.authorOrder, authors.map(author => author['@id']));
    article.authors = authors;
}

export function setArticleSeries(article: Article, title: string, issn?: string[]) {
    article.series = {
        '@type': 'bibo:Series',
        '@id': article['@id'] + '#series',
        title: title,
        issn: issn
    };
}

export function setArticleBook(article: Article, title: string, isbn?: string[]) {
    article.book = {
        '@type': 'bibo:Book',
        '@id': article['@id'] + '#book',
        title: title,
        isbn: isbn
    };
}

export function isUnpublished(article: Article): boolean {
    return article.articleType === UNPUBLISHED_ARTICLE;
}

export function isJournalArticle(article: Article): boolean {
    return article.articleType === JOURNAL_ARTICLE;
}

export function isAnthologyChapter(article: Article): boolean {
    return article.articleType === BOOK_CHAPTER;
}

export function isProceedingsArticle(article: Article): boolean {
    return article.articleType === PROCEEDINGS_ARTICLE;
}

export function getArticleFileId(article: Article): string {
    return article && article.hasFile || article.inFile || null;
}


