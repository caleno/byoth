import { CompactedDocumentHeader, JsonLdContext, JsonLdFrame, defaultNull } from './ld-model';

import { prefixes } from './prefixes';

const TYPE = 'acl:Authorization';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    accessType: { '@id': 'acl:accessToClass', '@type': '@id' },
    mode: { '@id': 'acl:mode', '@type': '@id' },
    agent: { '@id': 'acl:agent', '@container': '@set', '@type': '@id' }
}

export const AUTHORIZATION_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@type': TYPE,
    '@explicit': true,
    accessType: defaultNull,
    mode: defaultNull,
    agent: defaultNull
};

export interface AuthorizationProperties {
    accessType: string,
    mode: string,
    agent: string[]
}

export interface Authorization extends CompactedDocumentHeader, AuthorizationProperties {}
