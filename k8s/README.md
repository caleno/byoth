## Secrets

Ex.
  kubectl -n ub create secret generic byoth-config --from-file=config.json=config-k8s.json

Using manifest (secret-byoth.yaml)

Fill in the blanks and apply.

Ex.
  kubectl apply -f secrets-byoth.yaml
