const compression = require('compression');
const express = require('express');
const appRoot = require('app-root-path');

function initFrontend(app) {
    // English version
    app.use('/en/', compression(), express.static(appRoot + '/client/dist/en/'));
    app.get('/en/*', (req, res) => {
        res.sendFile(appRoot + '/client/dist/en/index.html');
    });

    // Norwegian version (bokmål)
    app.use('/nb/*', (req, res) => {
        res.redirect(req.originalUrl.split('nb')[1]);
    });
    app.use('/', compression(), express.static(appRoot + '/client/dist/'));
    app.get('/*', (req, res) => {
        res.sendFile(appRoot + '/client/dist/index.html');
    });
}

require('app-module-path').addPath(__dirname + '/server/dist');

const server = require('./server/dist/server/src/app.js');

server.createApp()
.then(app => {
    initFrontend(app);
    server.start(app);
});
