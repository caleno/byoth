# BYOTh - Bring Your Own Thesis

BYOTh is a web application for submission of PhD theses at University of Bergen.
It uses the Linked Data Platform (LDP) from Fedora Repository as storage backend,
but only minor modifications should be necessary to use another LDP.

The app consists of an Angular web form and a ExpressJS REST API that handles user
authentication and maps data to the LDP.
Redis is used to store session data, locks for transactions.

## Application

The app is divided into two separate parts: server and client, in the folders `server/` and `client/`.
Code shared between the two is in the folder `model/`.
The server is a Node/ExpressJS application written in TypeScript and the client an Angular 7 application.
The server and client have their own package setup and more fine-grained build and test scripts can be
run from those folders.
The root package provides scripts to build, start and test the whole application
and an index.js that starts both the server application and serves the Angular built files

### Data model

Each user gets their own thesis container.
Each container has the members candidate, thesis, department, defense, files and articles.
The container is modeled as a PCDM Container, and the members are PCDM object or PCDM files.

Metadata is stored as RDF in the LDP and converted to JSON-LD by the server.
The container and the members are associated with a JSON-LD frame which specifies
the format of the data provided by the API. Only values compatible with the frame
of each object will be saved.

The LDP will have the following structure

    /                               # root container
                                    # can be a subfolder, if your LDP will contain other data as well

    /container/                     # thesis containers
    /container/<id>                 # a thesis container
    /container/<id>/candidate       # a property of the container
    /container/<id>/thesis          #      ''
    ...
    /container/<id>/inputFiles      # a list of input files/the files to be printed, including order
    /container/<id>/files/<file-id> # the actual files, including auxilliary files like validation reports

    /person/<user-id>               # user-data, especially which user controls which thesis container

These should not be directly accessible to the end user, but will be visible as id's in the JSON-LD.
When setting up the application, the root container needs to be initialized manually.

### API

The above data model corresponds to the following REST API.

#### Thesis metadata and properties

    api/container/<id>                  # Thesis container: GET, POST (DELETE available in developer mode)
    api/container/<id>/candidate        # A property of the container: GET, PUT.
                                        #   PUT requests return the stored document after update

#### Read only file access

    api/container/<id>/files/<file-id>            # GET a file
    api/container/<id>/files/<file-id>/metadata   # GET File metadata

#### Input files

    api/container/<id>/inputFiles            # Order list of thesis input files
                                             #    GET list with metadata
                                             #    POST upload a new file

    api/container/<id>/inputFiles/order      # PUT a new file order


    api/container/<id>/inputFiles/submit     # POST Upload files to printshop, returns
                                             #   updated file metatada with validation result
                                             #   423 response if upload is already in progress

    api/container/<id>/inputFiles/unsubmit   # POST Clear all uploaded content from printshop
                                             #   and reset all validation information

    api/container/<id>/inputFiles/submitting # HEAD Check if file uploading is in process
                                             #   200 if it is
                                             #   204 if not

    api/container/<id>/inputFiles/<file-id>  # DELETE a file from list

    api/container/<id>/inputFiles/<file-id>/replace
                                             # POST Replaces file in list, will get a new file id
                                             #   Returns metadata for new file

    api/container/<id>/inputFiles/<file-id>/filename
                                             # PUT a new filename, body: { filename: '<filename>' }

#### Printshop

For the final order use the following.
Replace 'final' with 'evaluation' in the routes below for orders of evaluation copies of the thesis

    api/container/<id>/final/price      # GET price for current order from printshop
    api/container/<id>/final/submit     # POST to submit order
                                        # DELETE to cancel order
                                        # HEAD check if order has been submitted (200/204)
    api/container/<id>/final/cover-preview/[png|pdf]
                                        # GET preview of thesis cover from printshop

    api/container/<id>/final/content-preview/[png|pdf]
                                        # GET preview of thesis content from printshop

    api/container/<id>/final/validate   # POST Validate order, checks that all required fields are present, can not submit if this failes

User api:

    api/user/                           # GET Data about the current user
    api/user/container                  # The current user's container
                                        #   HEAD: check if it exists (200 if it exists, 204 if not)
                                        #   GET: Get ID
                                        #   POST: Create new one, returns ID

User's are authenticated by logging in to Dataporten at /auth/login.
This establishes a session that can be used for authenticated requests to the API.
The above assumes that all requests are authenticated.

#### ID encoding

Object IDs in the LDP will have the format `http://address.to.your.ldp/internal-path/phd/container/<id>`
All JSON-LD frames will have a `'@base': 'http://address.to.your.ldp/internal-path/phd/'` in their `@context`.
Which means IDs will have the form

    container/<id>
    container/<id>/candidate
    container/<id>/files/<id>
    ...

in the JSON-LD received from the API.

Since the IDs contain slashes, they can't be sent directly as part of the url
in a request to the API. In addition, since Fedora LDP creates pair-tree IDs
(and other LDPs might not), the full paths and not just the IDs are required.
Therefore full ID paths should be used in all requests, and should be sent in base64-url-encoded form:

    container/74/30/65/cd/743065cd-229b-44e2-8253-5907b3279bc9 =>
        dGhlc2lzLzc0LzMwLzY1L2NkLzc0MzA2NWNkLTIyOWItNDRlMi04MjUzLTU5MDdiMzI3OWJjOQ

I.e. to get the container with the above ID, call

    GET /api/container/dGhlc2lzLzc0LzMwLzY1L2NkLzc0MzA2NWNkLTIyOWItNDRlMi04MjUzLTU5MDdiMzI3OWJjOQ


## Install and configure BYOTh

Requires Node 8 or higher, an instance of Fedora Repository LDP, a Redis database,
and an instance of the ISBN service in this repo: https://gitlab.com/ubbdev/request-isbn.
The application also needs a Dataporten Application entry with a callback-url: `yourhost/auth/dataporten/callback`.
Set up the necessary folders in your LDP according to the Data Model description.

Install other depencies with

    npm install

Build the application with

    npm run build

Copy `config.template.json` to `config.json` and replace values where necessary.
Both the API and frontend Angular app are served via `index.js`.

    node index.js

The applicaton uses nconf to for configuration.
All config values can also be provided via the command line or system environment.
For instance, setting the port (`{byoth: {port: 3000} }` in the config-file)
can be done via environment variables (with separator '\_')

    byoth_port=3000 node index.js

or command-line arguments (with separator '-')

    node index.js --byoth-port 3000

For a list of accepted options see `config.template.json`.

For production instances

    npm run build:prod

and then

    NODE_ENV=prod node index.js

In addition, the following should be run after startup and preferrably as a daily cron-job

    npm run reindex

This caches data from the SEBRA API and builds a small Redis index based on data in Fedora,
to speed up queries over all theses.

## Tests

Server and client are tested separately. To run all tests, type

    npm test

This will include integration tests, that require a running test instances of all service,
and a configuration in `config.test.json` that matches.
The test configuration that ships with the repo is compatible with the
docker-compose file in `server/integration-tests`.

To run server and client tests separately, type

    npm run test:server
    npm run test:client

At the moment critical parts of the server are tested with near to full coverage, but more tests will be added.
A few critical parts of the client have unit tests, but most tests are mainly placeholders still.

## Docker setup

### Run using docker compose

The application ships with a docker-compose-file, which starts the application and a redis instance.
The setup assumes that Fedora Commons Repository and an ISBN-service are available as external services.
If not, they can be set up in a similar way as in the integrations tests,
but with persistence for all databases (except for the redis instance used by the application itself).

To run the application using docker compose, create a config-file with setup for the external services, based on
`config.template.json` and then type

    BYOTH_CONFIG_FILE=/path/to/config.json docker-compose up

This will start a BYOTh instance listening on port 3000.
In case you'd rather have your config in the environment, the compose-file can be updated accordingly,
using the nconf-mapping of config variables described above.

### Development environment using docker compose

To setup development instances of Redis, Fedora Commons and the ISBN service, run the command

    bash ./init-dev.sh

This starts non-persistent instances of the the databases and services in the background,
and initializes the ISBN service with a few numbers that can be used for development.
To shut these down, use

    docker-compose -f ./dev-docker-compose.yml down

The init script produces a partial configuration for BYOTh. Copy this into config.json and
start the application with `npm start`.

## Command line tools

BYOTh has a few command line tools for server admins in `server/src/tools/`.
The script `create-client.ts` creates a new API client and returns its ID.
This ID can then be used to create an access token using `create-jwt.ts`.
The script `modify-client-permission.ts` can be used to grant and deny a certain client rights.
Finally, the script `index-all.ts` can be used to rebuild indexes that are stored in the non-persistent redis database.
These indexes are updated dynamically when the application is run,
but the index-all script should either be called as part of startup/restart procedures, or run as a cron job.

For theses scripts to work they need access to the full configuration used by the server itself.
The shorthand `npm run reindex` illustrates how to run the script using the compiled code.

    NODE_PATH=./server/dist/ node ./server/dist/server/src/tools/index-all.js

The other scripts can be run with a similar setup.
