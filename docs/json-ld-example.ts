import * as jsonld from 'jsonld';

const n3 = `
<http://your.ldp.com/rest/phd/thesis/123-456> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/byoth#ThesisContainer> .
<http://your.ldp.com/rest/phd/thesis/123-456> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/byoth#Thesis> .
<http://your.ldp.com/rest/phd/thesis/123-456> <http://purl.org/byoth#hasThesis> <http://your.ldp.com/rest/phd/thesis/123-456> .
<http://your.ldp.com/rest/phd/thesis/123-456> <http://purl.org/dc/terms/creator> <http://your.ldp.com/rest/phd/person/user1> .`

const frame = {
    "@context": {
        "byoth": "http://purl.org/byoth#",
        "dct": "http://purl.org/dc/terms/",
        "@base": "http://your.ldp.com/rest/phd/",
        "hasThesis": {
            "@id": "byoth:hasThesis"
        }
    },
    "@type": "byoth:ThesisContainer",
    "hasThesis": { "@default": null }
};
jsonld.fromRDF(n3, { format: 'application/n-quads', useNativeTypes: true })
.then(doc => {
    console.log('unframed', JSON.stringify(doc, null, 2));
    return jsonld.frame(doc, frame);
})
.then(framedDoc => {
    console.log('framed', JSON.stringify(framedDoc, null, 2));
    return jsonld.compact(framedDoc, frame['@context']);
})
.then(compactedDoc => {
    console.log('compacted', JSON.stringify(compactedDoc, null, 2));
});

