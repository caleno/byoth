# Interacting with LDP/Fedora

BYOTh is a Node.js application that uses linked data platform (LDP) as database,
and provides an Angular client that interacts with the server using a REST interface.
The LDP used is Fedora Commons Repository (FCR), and some of the features discussed below
may or may not be specific to FCR.

The LDP provides metadata in the form of RDF, and both Node.js and Angular like consuming and producing JSON data.
Therefore the first layer between the application and the database converts the RDF to JSON-LD in reads,
and the other way around in writes.

## JSON-LD framing
By utilizing JSON-LD frames we can convert the RDF into a structured format
that is easy to work with in a javascript application.

If we for example have the following object:

~~~rdf
<http://your.ldp.com/rest/phd/thesis/123-456> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://purl.org/byoth#ThesisContainer> .
<http://your.ldp.com/rest/phd/thesis/123-456> <http://purl.org/byoth#hasThesis> <http://your.ldp.com/rest/phd/thesis/123-456> .
<http://your.ldp.com/rest/phd/thesis/123-456> <http://purl.org/dc/terms/creator> <http://your.ldp.com/rest/phd/person/user1> .
~~~

it will be converted to the following when converting to JSON-LD

~~~json
[
  {
    "@id": "http://your.ldp.com/rest/phd/thesis/123-456",
    "http://www.w3.org/2000/01/rdf-schema#type": [
      {
        "@id": "http://purl.org/byoth#ThesisContainer"
      }
    ],
    "http://purl.org/byoth#hasThesis": [
      {
        "@id": "http://your.ldp.com/rest/phd/thesis/123-456"
      }
    ],
    "http://purl.org/dc/terms/creator": [
      {
        "@id": "http://your.ldp.com/rest/phd/person/user1"
      }
    ]
  }
]
~~~

If we use the frame below to frame the document above

~~~json
{
    "@context": {
        "byoth": "http://purl.org/byoth#",
        "dct": "http://purl.org/dc/terms/",
        "@base": "http://your.ldp.com/rest/phd/",
        "hasThesis": {
            "@id": "byoth:hasThesis"
        }
    },
    "@type": "byoth:ThesisContainer",
    "hasThesis": { "@default": null }
}
~~~

we get the following document

~~~json
{
  "@context": {
    "byoth": "http://purl.org/byoth#",
    "dct": "http://purl.org/dc/terms/",
    "@base": "http://your.ldp.com/rest/phd/",
    "hasThesis": {
      "@id": "byoth:hasThesis"
    }
  },
  "@graph": [
    {
      "@id": "thesis/123-456",
      "@type": "byoth:ThesisContainer",
      "hasThesis": {
        "@id": "thesis/123-456"
      },
      "dct:creator": {
        "@id": "person/user1"
      }
    }
  ]
}
~~~

If our document only has one root node, we can simplify the output further by compactifying it

~~~json
{
  "@context": {
    "byoth": "http://purl.org/byoth#",
    "dct": "http://purl.org/dc/terms/",
    "@base": "http://your.ldp.com/rest/phd/",
    "hasThesis": {
      "@id": "byoth:hasThesis"
    }
  },
  "@id": "thesis/123-456",
  "@type": "byoth:ThesisContainer",
  "hasThesis": {
    "@id": "thesis/123-456"
  },
  "dct:creator": {
    "@id": "person/user1"
  }
}
~~~

### The flag `@explicit: true`

If we simply read from the LDP using the above we would potentially
expose internal or simply uninteresting triples to the user.
The properties `dct:creator` was for instance included in the framed document even though it wasn't in the frame.
By including the flag `@explicit: true` in our frame,
we can hide all triples not explicitly mentioned in the frame.

For example, if we frame and compactify the same document with the following frame

~~~json
{
    "@context": {
        "byoth": "http://purl.org/byoth#",
        "dct": "http://purl.org/dc/terms/",
        "@base": "http://your.ldp.com/rest/phd/",
        "hasThesis": {
            "@id": "byoth:hasThesis"
        }
    },
    "@type": "byoth:ThesisContainer",
    "hasThesis": { "@default": null },
    "@explicit": true
}
~~~

the result is

~~~json
{
  "@context": {
    "byoth": "http://purl.org/byoth#",
    "dct": "http://purl.org/dc/terms/",
    "@base": "http://your.ldp.com/rest/phd/",
    "hasThesis": {
      "@id": "byoth:hasThesis"
    }
  },
  "@id": "thesis/123-456",
  "@type": "byoth:ThesisContainer",
  "hasThesis": {
    "@id": "thesis/123-456"
  }
}
~~~

If we have a nested frame, the flag must be present at all levels of nesting.

Similarly, if a user wants to write a document that has unknown triples we can filter them away
by first framing the document before converting to RDF.

For instance, the following document

~~~json
{
  "@context": {
    "byoth": "http://purl.org/byoth#",
    "dct": "http://purl.org/dc/terms/",
    "@base": "http://your.ldp.com/rest/phd/",
    "hasThesis": {
      "@id": "byoth:hasThesis"
    }
  },
  "@id": "thesis/123-456",
  "@type": "byoth:ThesisContainer",
  "hasThesis": {
    "@id": "thesis/123-456"
  },
  "dct:creator": {
    "@id": "person/WRONG_USER"
  }
}
~~~

would be framed to (using the same frame as above):

~~~json
{
  "@context": {
    "byoth": "http://purl.org/byoth#",
    "dct": "http://purl.org/dc/terms/",
    "@base": "http://your.ldp.com/rest/phd/",
    "hasThesis": {
      "@id": "byoth:hasThesis"
    }
  },
  "@id": "thesis/123-456",
  "@type": "byoth:ThesisContainer",
  "hasThesis": {
    "@id": "thesis/123-456"
  }
}
~~~

i.e. without the changed user.
Using our explicit frames as filters we can control which fields
can be accessed and changed through a certain call.
The one field where this does not apply is the type field which can be an array.
This means that a user can add more types to a document than specified in the frame.
See below for how to at least partially protect against this.

## The LDP Client

The class `LdpCLient` (in the file `server/src/lib/ldp-cleint.ts`)
in BYOTh interacts with an LDP instance using the above framing tools.
For simplicity, each time a document (i.e. an LDP container) is updated all triples are fetched,
then deleted and the new ones inserted regardless of the overlap between new and old.
This has the benefit of not having any logic checking for changes, the drawback is that
some values can not be changed, and therefore have to be filtered somehow.

### Updates/patches
FCR does not allow the user to modify some internal values.
It will choke if a PATCH is sent modifying these triples.
Therefore the LDP Client keeps a list of reloaded prefixes that can never be written to. For FCR they are
`'http://fedora.info/definitions/v4/repository#'` and `'http://www.loc.gov/premis/rdf/v1#'`.

The function signature for an LDP update is thus

~~~typescript
async update(path: string, model: DocumentModel, doc: CompactedDocument): Promise<CompactedDocument>
~~~
The `path` argument is the path relative to root container that the client has been initialized with.
The `model` is an object that include the frame to be used, and `doc` is the value to write.

The method begins with locking the resource (using [Redlock](https://github.com/mike-marcacci/node-redlock)),
so that there will not be conflicts with other writes

~~~typescript
    let lock;

    try {
        lock = await this.redlock.lock(this.root + path, 2000);
    } catch (err) {
        throw new LockError(err.message);
    }
~~~

The rest of the method is wrapped in a try-finally block to make sure the lock is always released.

~~~typecript
    try {
        // ...
    } finally {
        await lock.unlock().catch(console.error);
    }
~~~

Inside this block we start with getting the old triples, filtering them using the frame
and removing read only triples (i.e. those with the prefixes specified above).

~~~typescript
        const oldTriples = await this.getTriples(this.root + path)
            .then(triples => Ldp.filterTriples(triples, model.frame))
            .then(triples => this.removeReadonlyTriples(triples));
~~~

We then do the same filtering with the provided document

~~~typescript
        let newTriples;

        try {
            newTriples = await Ldp.filterJsonLdToTriples(doc, model.frame)
                .then(triples => this.removeReadonlyTriples(triples));

            if (!newTriples) {
                throw new FramingError('Document not compatible with model', 422);
            }
        } catch (err) {
            if (err instanceof FramingError) {
                err.status = 422;
            }
            throw err;
        }
~~~

We then use our two sets of triples to create a patch query

~~~typescript
        const query = `DELETE {\n${oldTriples}}\nINSERT {\n${newTriples}}\nWHERE {}`;
~~~

And submit it to our LDP

~~~typescript
        try {
            await axios.patch(this.root + path, query,
                { headers: { ...Ldp.patchHeaders, authorization: this.authorization } });
        } catch (err) {
            throw convertAxiosError(err);
        }
~~~

Finally we get the updated document from the server.
Here we could probably use the triples we wrote, but to be safe we get the document that was actually stored,
and frame it before return it to the caller.

~~~typescript
        const updatedTriples = await this.getTriples(this.root + path);
        return Ldp.createLdDocument(updatedTriples, model.frame);
~~~

The functions to get from and post to the LDP are similar but simpler, and not described here.

### File handling

Posting files to the LDP is done using the Express.js-library multer.
A custom multer storage engine can be found in the same file as the LDP CLient under the name `Ldp.Storage`.
Files are fetched using their ID, i.e. if we post to `/foo/bar/file-id`, we get the file from there.
To read and write file metadata in FCR the suffix `/fcr:metadata` is appended to the path.
This is handled by the functions `LdpClient.getFile()`, `LdpCLient.postFile()`,
`LdpClient.getFileMetadata()` and `LdpClient.updateFileMetadata()`.


## Model/Ontology

The model used is described in an OWl ontology which can be found in the file `model/byoth.owl`.
It is published under http://purl.org/byoth (currently manually, trying to get a CD solution to work).
The [Portland Common Data Model](https://github.com/duraspace/pcdm/wiki) (PCDM) is used to
describe the relations between containers and describe files.
Otherwise the model mainly uses elements from [Schema.org](https://schema.org/),
[The Bibliographic Ontology](http://bibliontology.com/) and Dublin Core,
with a few others here and there.
As a principle foreign ontology entities are only explicitly mentioned in
the BYOTh ontology if they are needed for sub-classing or in domain/range statements
(becuase it is necessary in Protégé).

### Objects

The root object for each thesis is a `byoth:ThesisContainer` which subclasses `pcdm:Object`.
This class has a number of subcontainers for different aspects of a thesis,
which all are subclasses of `pcdm:Object`:

* `byoth:Person`, also subclass of `schema:Person`.
* `byoth:Thesis`, also subclass of `bibo:Thesis`.
    An object that keeps bibliographic data about the thesis, i.e. title, isbn, language, abstract, etc.
* `byoth:Event`, also subclass of `schema:Event`.
    Used to describe the PhD defense and trial lecture events.
* `byoth:Institution`, also subclass of `schema:Organization`.
    Used to store the department that awards the degree.
* `byoth:PressRelease` (could subclass `fabio:PressRelease` maybe?).
    The press release.
* `byoth:Order`, also subclass of `schema:Order`.
    Describes the order for printing the thesis.

### Relations

The application only depends on LDP paths and properties to access data.
No SPARQL endpoint was available in the FCR instance used at the time of creating the app,
which means we have to depend on paths conforming to a certain structure
and specific object properties to get the right data.
To find the PressRelease for a thesis, we can not query for members (`pcdm:hasMember`) of a given
`ThesisContainer` that have the type `PressRelease`.
Instead we look for the property `byoth:hasPressRelease` (subclass of `pcdm:hasMember`) on the container.

To simplify this querying the application enforces the following path conventions
for a container with id `foo`:

| container path  | property                | path               | type                |
|-----------------|-------------------------|--------------------|---------------------|
| `/foo`     | `byoth:hasArticleCollection` | `/foo/articles`    | `ldp:Container`     |
| `/foo`          | `byoth:hasCandidate`    | `/foo/candidate`   | `byoth:Person`      |
| `/foo`          | `byoth:hasDefense`      | `/foo/defense`     | `byoth:Event`       |
| `/foo`          | `byoth:degreeAwardedAt` | `/foo/department`  | `byoth:Instituion`  |
| `/foo`    | `byoth:hasInputFileCollection`| `/foo/inputFiles`  | `pcdm:Collection`   |
| `/foo`          | `byoth:hasOrder`        | `/foo/order`       | `byoth:Order`       |
| `/foo`          | `byoth:hasPressRelease` | `/foo/pressRelease`| `byoth:PressRelease`|
| `/foo`          | `byoth:hasThesis`       | `/foo/thesis`      | `byoth:Thesis`      |
| `/foo`          | `byoth:hasTrialLecture` | `/foo/trialLecture`| `byoth:Event`       |

All of the above object properties are subclasses of `pcdm:hasMember`.
From now on we will refer to them by their slug, i.e. _candidate_, _department_, etc.

In addition the path `/foo/files` contains the actual files that are only referenced from
`/foo/inputFiles`. This allows us to keep the files in one place and keep lists separately.
The input files are ordered according to the PCDM ordering extension.

(TODO: 1. The property `byoth:hasOrder` will eventually be split into `hasFinalOrder` and `hasCommitteeOrder` or something similar, update this doc. 2. The article and input file collections should have the same type)

If an object has object-properties itself these are not created as separate LDP containers.
Instead they are added as `#`-paths within the same container.
For example the _order_ has a sub-property `schema:Offer`, which is added at `/foo/order#Offer`.
This allows us to get the whole order in one request from the LDP.
The exceptions to this rule are articles and files,
which are relatively large objects that can be added and deleted,
these are added by posting to `/foo/articles/` and `/foo/files/`,
and will have paths `/foo/articles/<article-id>`, `/foo/files/<file-id>`.

### Model files

Each container type has an associated model definition in the directory `model/`.
The file `model/ld-model` define some general interfaces such as `JsonLdContext`, `JsonLdFrame`, etc,
which are used to construct the specific models.
It also has a few helper functions that can be used when working with models.
The model definitions are shared between the client and server,
helping the client application to understand the data it gets from the server,
and also allowing it to modify the contents of documents in a consistent manner.

The model frames include default values for all fields.
These are often `null` or an empty string,
but we can also use them to define an actual value where necessary.

#### Simple model

As an example, let's first have a look at the _candidate_, which is a simple model (`model/candidate.ts`).
The model first defines a type and context for its frame:

~~~typescript
// get prefixes shared between all models
import { prefixes } from './prefixes';

// a Candidate is a byoth:Person
const TYPE = 'byoth:Person';

const CONTEXT: JsonLdContext = {
    ...prefixes,
    givenName: { '@id': 'schema:givenName' },
    familyName: { '@id': 'schema:familyName' },
    email: { '@id': 'schema:email' },
    birthDate: { '@id': 'dbo:birthDate', '@type': 'xsd:date' },
    orcid: { '@id': 'dbo:orcidId' },
    gender: { '@id': 'schema:gender' }
}
~~~

Then creates the frame (the default values are defined in 'model/ld-model'):

~~~typescript
export const CANDIDATE_FRAME: JsonLdFrame = {
    '@context': CONTEXT,
    '@type': TYPE,
    '@explicit': true,
    givenName: defaultEmptyString,
    familyName: defaultEmptyString,
    email: defaultEmptyString,
    birthDate: defaultNull,
    orcid: defaultNull,
    gender: defaultNull,
};
~~~

Then a typescript interface is defined, which should mirror the frame.
We separate the properties from the generic JSON-LD properties (`@type`, `@id`, etc)
to allow the code to work with an interface agnostic of these when necessary.

~~~typescript
export interface CandidateProperties {
    givenName: string,
    familyName: string,
    email: string,
    brithDate: string,
    orcid: string,
    gender: string
}

export interface Candidate extends CompactedDocumentHeader, CandidateProperties { }
~~~

By construction the interface will implement the interface of `CompactedDocument`, defined in `model/ld-model.ts`,
which is data type written and read from the LDP client.

#### Nested model

Things get a bit more complicated if we have nested objects,
because every level of nesting needs to have an id.
Here is a snippet from the _order_ frame:

~~~typescript
export const ORDER_FRAME: JsonLdFrame = {
    '@type': 'byoth:Order',
    '@context': CONTEXT,
    '@explicit': true,
    offer: {
        '@type': 'schema:Offer',
        '@explicit': true,
        quantity: { '@default': MANDATORY_COPIES + 1 },
        // ...
    }
}
~~~

A snippet of the interface definition reads

~~~typescript
export interface Offer {
    '@type': string,
    '@id': string,
    quantity?: number,
    // ...
}

export interface OrderProperties {
    // ...
    offer?: Offer,
    // ...
}

export interface Order extends CompactedDocumentHeader, OrderProperties {
~~~

Here the distinction between properties and JSON-LD specifics becomes a bit muddled, since
we have to include type and id in the nested interface.
(What would be a cleaner solution?)
In addition when working with this object we need to create its subproperties explicitly with ids.
The function below generates an ID and adds an offer object to an order.

~~~typescript
export function initOrderOffer(order) {
    order.offer = {
        '@id': order['@id'] + '#offer',
        '@type': 'schema:Offer',
        quantity: MANDATORY_COPIES + 1
    };
}
~~~

Corresponding functions are available for all models and properties that have the same requirements.

#### Ordered sets

The PCDM ordering extension is used to handle ordered sets, in particular author lists and file lists.
This model has some complexity to it, so helper functions are available to get ordered arrays,
append items, delete items and change the order.
See `model/ordered-set.ts` (and its unit tests) for details.

## Thesis Container-adapter

The class `ThesisContainerAdapter` (`server/lib/thesis-container-adapter.ts`) provides
a layer above the `LdpClient` that is used by the rest of application for accessing thesis containers.
This enforces the path relations above and applies the right models to right properties, etc.

When a user logs in for the first time the method `ThesisContainerAdapter.createContainer()` is called.
This checks if the user has been logged in before, if not it creates a new person entry under
`/ldp-root/person/`. Then it creates an empty thesis container under `/ldp-root/thesis/`. These are linked by
the property `byoth:ownsObject` on the person, and `dct:creator` on the the container.
Finally a candidate and department are added to the container, since these can be initialized from login information.

When accessing other properties than the candidate and department,
the adapter first checks if they exist and create empty instances of them if not.
A user can thus interact with the adapter as if all properties are present directly after creation.
In general the thesis container is only used internally to connect its different parts. As we will see,
the REST API only exposes the properties of a thesis container.

The two properties that have additional sub-properties, _articles_ and _files_,
need to be treated separately by the adapter and have their own methods
for adding the articles and files and getting lists of these.
In particular the methods `getArticles()` and `getInputFiles()` populate the
field `items` of their container with full documents for each item,
allowing a user to get all data in one request.
The method `setInputFileOrder()` can be used to redefine the order of the input files (which matters for printing).

