# start databases
docker-compose -f ./dev-docker-compose.yml up -d || exit 1

# make sure they are active
docker-compose -f ./dev-docker-compose.yml run wait || exit 1

# Add a few ISBN numbers to the ISBN service.
# Uses the default token used by the test version of the service.
echo "Posting ISBN numbers"
response=$(curl http://localhost:9788/api/isbn -X PUT -d '[ "978-0-596-52068-7", "0-596-52068-9", "0-9752298-0-X", "960-425-059-0", "978-3-16-148410-0" ]' -H "Content-Type: application/json" -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTQxNjg0OTA4fQ.TSpwUKZ4noCjrr6D7LBoCGHhRbDFaVOwW7_tXbiYeC8') || exit  1
echo "Response $response"

echo
echo "Dev environment initialized."
echo "Here is a partial config (fill in the blanks with your own configuration):"
echo
cat <<EOF
{
    "byoth": {
        "host": "localhost",
        "port": "3000",
        "ldp": {
            "root": "http://localhost:8080/rest",
            "username": "",
            "password": "",
        },
        "dataporten": {
            ...
        },
        "sessionSecret": "...",
        "jwtSecret": "...",
        "uib": {
            ...
        },
        "isbn": {
            "host": "localhost",
            "protocol": "http",
            "port": "9788",
            "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0IiwiaWF0IjoxNTQxNjg0OTA4fQ.TSpwUKZ4noCjrr6D7LBoCGHhRbDFaVOwW7_tXbiYeC8"
        },
        "redis": {
            "host": "localhost",
            "port": "6379"
        }
    }
}
EOF
