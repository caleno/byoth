import * as express from 'express';
import { Express } from 'express';
import * as request from 'supertest';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';
import * as chai from 'chai';

import { institutionApi } from './institution';
import { Institution } from '../lib/institution';
import { _helpers } from '../middleware/authorize';

const expect = chai.expect;

describe('Institution API', () => {
    let app: Express;
    let institution: Institution;
    let sandbox: SinonSandbox;

    beforeEach(() => {
        /* istanbul ignore  next */
        institution = {
            init: () => Promise.resolve(null),
            getFaculties: () => Promise.resolve([]),
            getFaculty: groupid => Promise.resolve({} as any),
            getDepartments: facultyId => Promise.resolve([]),
            getDepartment: groupid => Promise.resolve({} as any),
            getPlace: placeid => Promise.resolve({} as any),

            getAreas: () => Promise.resolve([]),
            getBuildings: areaId => Promise.resolve([]),
            getRooms: buildingId => Promise.resolve([]),
        };

        app = express();
        app.use('/', institutionApi({ institution: institution } as any));

        sandbox = sinon.createSandbox();
    });

    afterEach(() => sandbox.restore());

    it('should return 401 on unauthenticated requests', async () => {
        await request(app).get('/').expect(401);
        await request(app).get('/faculty').expect(401);
    });

    it('GET /faculties should return list of faculties', async () => {
        sandbox.stub(_helpers, 'isAuthenticated').yields(null);
        const faculties = [{foo: 'bar'}];
        sandbox.stub(institution, 'getFaculties').resolves(faculties);

        await request(app).get('/faculty')
        .expect(200)
        .expect(res => expect(res.body).to.deep.equal(faculties));
    });

    it('GET /faculties/:id/departments should return list of departments for faculty with correct id', async () => {
        sandbox.stub(_helpers, 'isAuthenticated').yields(null);
        const departments = [{foo: 'bar'}];
        const spy = sandbox.stub(institution, 'getDepartments').resolves(departments);

        await request(app).get('/faculty/foo/departments')
        .expect(200)
        .expect(res => expect(res.body).to.deep.equal(departments));

        expect(spy.calledWith('foo'));
    });

    it('GET /place/:id should return information about requested place', async () => {
        sandbox.stub(_helpers, 'isAuthenticated').yields(null);
        const place = {foo: 'bar'};
        const spy = sandbox.stub(institution, 'getPlace').resolves(place);

        await request(app).get('/place/foo')
        .expect(200)
        .expect(res => expect(res.body).to.deep.equal(place));

        expect(spy.calledWith('foo'));
    });

    it('GET /areas should return list of areas', async () => {
        sandbox.stub(_helpers, 'isAuthenticated').yields(null);
        const areas = [{foo: 'bar'}];
        sandbox.stub(institution, 'getAreas').resolves(areas);

        await request(app).get('/areas')
        .expect(200)
        .expect(res => expect(res.body).to.deep.equal(areas));
    });

    it('GET /buildings/:areaId should return list of buildings in area', async () => {
        sandbox.stub(_helpers, 'isAuthenticated').yields(null);
        const buildings = [{foo: 'bar'}];
        const spy = sandbox.stub(institution, 'getBuildings').resolves(buildings);

        await request(app).get('/buildings/foo')
        .expect(200)
        .expect(res => expect(res.body).to.deep.equal(buildings));

        expect(spy.calledWith('foo'));
    });

    it('GET /rooms/:buildingId should return list of rooms in building', async () => {
        sandbox.stub(_helpers, 'isAuthenticated').yields(null);
        const rooms = [{foo: 'bar'}];
        const spy = sandbox.stub(institution, 'getRooms').resolves(rooms);

        await request(app).get('/rooms/foo')
        .expect(200)
        .expect(res => expect(res.body).to.deep.equal(rooms));

        expect(spy.calledWith('foo'));
    });
});
