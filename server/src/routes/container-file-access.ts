import { Router } from 'express';
import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import * as base64url from 'base64-url';

import { ensureAuthenticated, ensureContainerOwner, ensureSubContainer } from '../middleware/authorize';
import { Services } from '../services';

// Routes to read files and file metadata
export function containerFileAccessApi(services: Services): Router {
    const fileAdapter = services.fileAdapter;
    const router = express.Router();

    router.use(ensureAuthenticated(), ensureContainerOwner(services.containerAdapter));

    router.param('fileId', (req, res, next, articleId) => {
        req['fileId'] = base64url.decode(articleId);
        next();
    });

    router.use('/:fileId', ensureSubContainer('fileId'));

    // download files
    router.get('/:fileId', asyncHandler(async (req, res, next) => {
        const fileId = req['fileId'];

        const metadata = await fileAdapter.getFileMetadata(fileId);
        const stream = await fileAdapter.getFileStream(fileId);

        res.status(200).contentType(metadata.mimetype);
        stream.pipe(res);
    }));

    // download file metadata
    router.get('/:fileId/metadata', asyncHandler(async (req, res, next) => {
        const fileId = req['fileId'];

        const metadata = await fileAdapter.getFileMetadata(fileId);

        res.status(200).send(metadata);
    }));

    return router;
}
