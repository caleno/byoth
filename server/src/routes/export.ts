import { Router, Request, Response, NextFunction } from 'express';
import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import * as passport from 'passport';
import * as base64url from 'base64-url';

import { InputError, AuthorizationError, NotFoundError, DatabaseError } from '../lib/errors';
import { ThesisFileAdapter } from '../lib/thesis-file-adapter';
import { mapToMods } from '../lib/mods';
import { decodeParam } from '../middleware/decode-param';
import { ensureAuthenticated } from '../middleware/authorize';

import { Services } from '../services';

export function exportApi(services: Services): Router {
    const router = express.Router();
    const { clientAuth, containerAdapter, approvalIndex } = services;
    const fileAdapter = new ThesisFileAdapter(containerAdapter, services.ldp, services.redlock);

    router.use('/', passport.authenticate('jwt', { session: false }));

    router.use('/', ensureAuthenticated());

    router.use('/thesis', asyncHandler( async (req, res, next) => {
        if (! await clientAuth.clientHasAuthorization(req.user.id, 'readExport')) {
            next(new AuthorizationError(req));
        }

        next();
    }));

    router.get('/thesis', asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
        if (!req.query || !req.query.since) {
            throw new InputError('Must provide since query, i.e. .../thesis?since=2001-01-01', 422);
        }

        if (!req.query.since.match(/\d\d\d\d-\d\d-\d\d/)) {
            throw new InputError('Since date must have format YYYY-MM-DD', 422);
        }

        const ids = await approvalIndex.getAllAfterDate(new Date(req.query.since));

        res.json(ids.map(id => base64url.encode(id)));
    }));

    router.route('/thesis/:id/mods')
    .all(decodeParam('id'))
    .get(asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
        const mods = await mapToMods(containerAdapter, req.params.id);
        res.json(mods);
    }));

    router.route('/thesis/:id/file')
    .all(decodeParam('id'))
    .get(asyncHandler(async (req: Request, res: Response, next: NextFunction) => {
        const document = await containerAdapter.getContainer(req.params.id);

        if (!document.archiveFile) {
            throw new NotFoundError('Thesis does not have archive ready file yet');
        }

        const metadata = await fileAdapter.getFileMetadata(document.archiveFile['@id']);

        if (!metadata.messageDigest.startsWith('urn:sha1:')) {
            throw new DatabaseError('File does not have checksum');
        }

        res.set('Content-SHA1', metadata.messageDigest.substring(9));
        fileAdapter.getFile(document.archiveFile['@id'], res);
    }));

    return router;
}
