import { Router } from 'express';
import * as express from 'express';
import * as passport from 'passport';

import { userFromRequest } from '../lib/user';

export function authApi(): Router {
    const router = express.Router();

    // log in
    // if requested with /login?lang=xx&target=yy, remember this, to get correct post-login redirect below
    router.use('/login', (req, res, next) => {
        if (req.query.lang) {
            req.session.lang = req.query.lang;
        } else { // if no language, restore to default
            req.session.lang = undefined;
        }

        if (req.query.target) {
            req.session.target = req.query.target;
        } else {
            req.session.target = undefined;
        }

        next();
    });
    router.get('/login', passport.authenticate('dataporten'));

    // log out
    router.get('/logout', (req, res, next) => {
        let redirect = '/';

        if (req.query.lang) {
            redirect += req.query.lang + '/';
        }

        req.logout();
        res.redirect(redirect);
    });

    // authentication callback
    router.get('/auth/dataporten/callback', (req, res) => {
        const lang = req.session.lang;
        const target = req.session.target;

        passport.authenticate('dataporten',
            {
                successRedirect: '/' + ( lang ? lang + '/' :  '') + ( target ? target : '' ),
                failureRedirect: '/login' + (lang ? '&lang=' + lang : '') + ( target ? '&target=' + target : '' )
            }
        )(req, res);
    });

    // is user logged in
    router.get('/auth', (req, res, next) => {
        if (req.isAuthenticated()) {
            return res.status(200).send(userFromRequest.getUser(req));
        }
        res.status(204);
        res.send();
    });

    return router;
}
