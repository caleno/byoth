import { Router } from 'express';
import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import * as base64url from 'base64-url';

import { ensureAuthenticated, ensureContainerOwner } from '../middleware/authorize';
import { NotFoundError } from '../lib/errors';

import { OrderTypes } from 'model/order';

import { Services } from '../services';
import { containerInputFileApi } from './container-input-files';
import { containerFileAccessApi } from './container-file-access';
import { containerPrintshopApi } from './container-printshop';
import { containerArticleApi } from './container-articles';

/** All route ids should be base64-url encoded */
export function containerApi(services: Services): Router {
    const router = express.Router();
    const {containerAdapter, isbnHandler} = services;

    router.use('/', ensureAuthenticated());

    router.post('/', asyncHandler(async (req, res, next) => {
        const doc = await containerAdapter.createContainer(req);
        res.status(201).send(doc);
    }));

    router.param('containerId', (req, res, next, id) => {
        req['containerId'] = base64url.decode(id);
        next();
    });

    router.use('/:containerId', asyncHandler(ensureContainerOwner(containerAdapter)));

    router.get('/:containerId', asyncHandler(async (req, res, next) => {
        const data = await containerAdapter.getContainer(req['containerId']);
        res.send(data);
    }))

    // in tests and dev mode we need to be able to delete containers
    router.delete('/:containerId', asyncHandler(async (req, res, next) => {
        if (process.env.NODE_ENV !== 'production') {
            // not optimal design-wise, but also not used in production
            await isbnHandler.unassignISBN(req['containerId']);
            await containerAdapter.deleteContainer(req['containerId'], req);
            res.status(204).send();
        } else {
            /* istanbul ignore next */
            next(new NotFoundError('API path not defined', req));
        }
    }));

    // sub APIS
    router.use('/:containerId/files', containerFileAccessApi(services))
    router.use('/:containerId/inputFiles', containerInputFileApi(services));
    router.use('/:containerId/articles', containerArticleApi(services));
    router.use('/:containerId/final', containerPrintshopApi(services, OrderTypes.FINAL));
    router.use('/:containerId/evaluation', containerPrintshopApi(services, OrderTypes.EVALUATION));

    // handle ISBNS
    router.route('/:containerId/thesis/assign-isbn')
    .post(asyncHandler(async (req, res, next) => {
        const thesis = await isbnHandler.assignISBN(req['containerId']);
        res.status(200).send(thesis);
    }));

    router.route('/:containerId/thesis/unassign-isbn')
    .post(asyncHandler(async (req, res, next) => {
        const thesis = await isbnHandler.unassignISBN(req['containerId']);
        res.status(200).json(thesis);
    }));

    // properties that are "just" documents
    router.route('/:containerId/:property')
    .get(asyncHandler(async (req, res, next) => {
        const data = await containerAdapter.getProperty(req['containerId'], req.params.property, req);
        res.send(data);
    }))
    .put(asyncHandler(async (req, res, next) => {
        const result = await containerAdapter.putProperty(req['containerId'], req.params.property, req.body, req);
        res.status(200).send(result);
    }));

    return router;
}
