import { Express } from 'express';
import * as compression from 'compression';

import { NotFoundError } from '../lib/errors';

import { authApi } from './auth';
import { containerApi } from './container';
import { institutionApi } from './institution';
import { userApi } from './user';
import { exportApi } from './export';
import { approveApi } from './approve';

import { Services } from '../services';

export async function setupApiRoutes(app: Express, services: Services) {
    // API Routes
    app.use('/', compression());
    app.use('/', authApi());
    app.use('/api/container', containerApi(services));
    app.use('/api/user', userApi(services));
    app.use('/api/export', exportApi(services));
    app.use('/api/approve', approveApi(services));
    app.use('/api/', institutionApi(services));
    app.get('/api/model', (req, res, next) => {
        res.send(services.containerAdapter.model);
    });
    app.use('/api/', (req, res, next) => {
        next(new NotFoundError('API path not defined', req));
    });
}
