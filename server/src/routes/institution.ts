import { Router } from 'express';
import * as express from 'express';
import * as asyncHandler from 'express-async-handler';

import { ensureAuthenticated } from '../middleware/authorize';

import { Services } from '../services';

export function institutionApi(services: Services): Router {
    const router = express.Router();
    const institution = services.institution;

    router.use(/(|faculty|place|areas|buildings|rooms)/, ensureAuthenticated());

    router.get('/faculty', asyncHandler(async (req, res, next) => {
        const faculties = await institution.getFaculties();
        res.json(faculties);
    }));

    router.get('/faculty/:id/departments', asyncHandler(async (req, res, next) => {
        const departments = await institution.getDepartments(req.params.id);
        res.json(departments);
    }));

    router.get('/place/:id', asyncHandler(async (req, res, next) => {
        const place = await institution.getPlace(req.params.id);
        res.json(place);
    }));

    router.get('/areas', asyncHandler(async (req, res, next) => {
        const areas = await institution.getAreas();
        res.json(areas);
    }));

    router.get('/buildings/:areaId', asyncHandler(async (req, res, next) => {
        const buildings = await institution.getBuildings(req.params.areaId);
        res.json(buildings);
    }));

    router.get('/rooms/:buildingId', asyncHandler(async (req, res, next) => {
        const rooms = await institution.getRooms(req.params.buildingId);
        res.json(rooms);
    }));

    return router;
}

