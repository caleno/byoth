import { Router } from 'express';
import * as express from 'express';
import * as asyncHandler from 'express-async-handler';

import { OrderTypes } from 'model/order';

import { validateOrder } from '../lib/validate-order';

import { Services } from '../services';

export function containerPrintshopApi(services: Services, type: OrderTypes): Router {
    const { containerAdapter, printshopAdapter, fileAdapter } = services;
    const router = express.Router();

    const propString = type === OrderTypes.FINAL ? 'finalOrder' : 'evaluationOrder';

    router.use('(/cover-preview|/content-preview)', (req, res, next) => {
        let regenerate = false;
        if (req.query.regenerate && req.query.regenerate === 'true' ) {
            regenerate = true;
        }
        req.query.regenerate = regenerate;
        next();
    });

    router.get('/cover-preview/png', asyncHandler(async (req, res, next) => {
        const preview = await printshopAdapter.getCoverPreview(req['containerId'], type, req.query.regenerate);
        res.status(200).send(preview.thumbnails);
    }));

    router.get('/cover-preview/pdf', asyncHandler(async (req, res, next) => {
        const preview = await printshopAdapter.getCoverPreview(req['containerId'], type, req.query.regenerate);
        res.type('pdf');
        res.end(Buffer.from(preview.pdf, 'base64'));
    }));

    router.get('/content-preview/png', asyncHandler(async (req, res, next) => {
        const content = await printshopAdapter.getContentPreview(req['containerId'], type, req.query.regenerate);
        res.status(200).send(content.thumbnails);
    }));

    router.get('/content-preview/pdf', asyncHandler(async (req, res, next) => {
        const content = await printshopAdapter.getContentPreview(req['containerId'], type, req.query.regenerate);
        res.type('pdf');
        res.end(Buffer.from(content.pdf, 'base64'));
    }));

    router.post('/price-request', asyncHandler(async (req, res, next) => {
        await containerAdapter.putProperty(req['containerId'], propString, req.body, req);
        const result = await printshopAdapter.getPrice(req['containerId'], type);
        res.status(200).send(result);
    }));

    router.post('/validate', asyncHandler(async (req, res, next) => {
        let result;
        result = await validateOrder(containerAdapter, fileAdapter, req['containerId'], type);
        res.status(200).json(result);
    }));

    router.route('/submit')
    .all((req, res, next) => {
        let isTest = false;
        if (req.query.isTest && req.query.isTest === 'true' ) {
            isTest = true;
        }
        req.query.isTest = isTest;
        next();
    })
    .head(asyncHandler(async (req, res, next) => {
        const isSubmitted = await printshopAdapter.isSubmitted(req['containerId'], type);
        if (isSubmitted) {
            res.sendStatus(200);
        } else {
            res.sendStatus(204);
        }
    }))
    .post(asyncHandler(async (req, res, next) => {
        const order = await printshopAdapter.submitOrder(req['containerId'], type, req.query.isTest);
        res.status(200).send(order);
    }))
    .delete(asyncHandler(async (req, res, next) => {
        const order = await printshopAdapter.cancelOrder(req['containerId'], type);
        res.status(204).send(order);
    }));

    return router;
}
