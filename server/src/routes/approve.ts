import { Router } from 'express';
import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import * as passport from 'passport';


import { decodeParam } from '../middleware/decode-param';
import { ensureAuthenticated } from '../middleware/authorize';
import { AuthorizationError } from '../lib/errors';

import { Services } from '../services';

export function approveApi(services: Services): Router {
    const router = express.Router();
    const { clientAuth, containerAdapter, approvalIndex } = services;

    router.use('/',
        passport.authenticate('jwt', { session: false }),
        ensureAuthenticated(),
        asyncHandler( async (req, res, next) => {
            if (! await clientAuth.clientHasAuthorization(req.user.id, 'writeProof')) {
                next(new AuthorizationError(req));
            }

            next();
        })
    );

    router.post('/:id', decodeParam('id'), asyncHandler(async (req, res, next) => {
        const container = await containerAdapter.getContainer(req.params.id);
        const today = new Date();
        container.approvalDate = today.toISOString();

        await containerAdapter.putContainer(req.params.id, container);
        await approvalIndex.insert(container);

        res.sendStatus(200);
    }));

    router.delete('/:id', decodeParam('id'), asyncHandler(async (req, res, next) => {
        const container = await containerAdapter.getContainer(req.params.id);
        container.approvalDate = null;

        await containerAdapter.putContainer(req.params.id, container);
        await approvalIndex.delete(container);

        res.sendStatus(204);
    }));

    return router;
}
