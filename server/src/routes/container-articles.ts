import { Router } from 'express';
import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import * as base64url from 'base64-url';

import { ThesisArticleAdapter } from '../lib/thesis-article-adapter';
import { ThesisFileAdapter } from '../lib/thesis-file-adapter';

import { ensureSubContainer } from '../middleware/authorize';
import { Services } from '../services';

export function containerArticleApi(services: Services): Router {
    const router = express.Router();
    const fileAdapter = new ThesisFileAdapter(services.containerAdapter, services.ldp, services.redlock);
    const articleAdapter = new ThesisArticleAdapter(services.containerAdapter, services.ldp, fileAdapter);

    router.route('/')
    .get(asyncHandler(async (req, res, next) => {
        const data = await articleAdapter.getArticles(req['containerId'], req);
        res.send(data);
    }))
    .post(asyncHandler(async (req, res, next) => {
        const result = await articleAdapter.postArticle(req['containerId']);
        res.status(201).send(result);
    }));

    router.param('articleId', (req, res, next, articleId) => {
        req['articleId'] = base64url.decode(articleId);
        next();
    });

    router.use('/:articleId', ensureSubContainer('articleId'));

    router.route('/:articleId')
    .get(asyncHandler(async (req, res, next) => {
        const result = await articleAdapter.getArticle(req['articleId']);
        res.status(200).send(result);
    }))
    .put(asyncHandler(async (req, res, next) => {
        const result = await articleAdapter.putArticle(req['articleId'], req.body);
        res.status(200).send(result);
    }))
    .delete(asyncHandler(async (req, res, next) => {
        const result = await articleAdapter.deleteArticle(req['articleId']);
        res.status(result).send('');
    }));

    router.route('/:articleId/file')
    .post(asyncHandler(async (req, res, next) => {
        const result = await articleAdapter.uploadArticleFile(req['containerId'], req['articleId'], req);
        res.status(201).send(result);
    }))
    .delete(asyncHandler(async (req, res, next) => {
        await articleAdapter.deleteArticleFile(req['articleId']);
        res.sendStatus(204);
    }))
    .get(asyncHandler(async (req, res, next) => {
        await articleAdapter.getArticleFile(req['articleId'], res);
    }));

    return router;
}
