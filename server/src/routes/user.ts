import { Router } from 'express';
import * as express from 'express';

const asyncHandler = require('express-async-handler');

import { NotFoundError } from '../lib/errors';
import { userFromRequest } from '../lib/user';
import { ensureAuthenticated } from '../middleware/authorize';

import { Services } from '../services';

export function userApi(services: Services): Router {
    const router = express.Router();

    const containerAdapter = services.containerAdapter;

    router.use('/', ensureAuthenticated());

    router.get('/', asyncHandler(async (req, res, next) => {
        // we don't want to modify request data below
        const user = JSON.parse(JSON.stringify(<any>userFromRequest.getUser(req)));
        const uri = await containerAdapter.getUserContainerId(req);

        if (uri) {
            user.containerId = uri;
        }

        res.json(user);
    }));

    router.head('/container', asyncHandler(async (req, res, next) => {
        const containerUri = await containerAdapter.getUserContainerId(req);

        if (!containerUri) {
            res.status(204).send();
        } else {
            res.status(200).send();
        }
    }));

    router.get('/container', asyncHandler(async (req, res, next) => {
        const containerId = await containerAdapter.getUserContainerId(req);

        if (!containerId) {
            res.status(404).send(new NotFoundError('User has no thesis container', req));
        } else {
            res.status(200).type('text/plain').send(containerId);
        }
    }))

    router.post('/container', asyncHandler(async (req, res, next) => {
        const data = await containerAdapter.createContainer(req);
        res.status(201).json({containerId: data['@id']});
    }));

    return router;
};
