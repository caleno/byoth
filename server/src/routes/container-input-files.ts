import { Router } from 'express';
import * as express from 'express';
import * as asyncHandler from 'express-async-handler';
import * as base64url from 'base64-url';

import { ensureSubContainer } from '../middleware/authorize';
import { Services } from '../services';

import { RequestError } from '../lib/errors';
import { OrderTypes } from 'model/order';

// Routes to handle the list of input files, should be sub-routes to container API (need req['containerId'])
export function containerInputFileApi(services: Services): Router {
    const router = express.Router();
    const { fileAdapter, printshopAdapter } = services;

    router.route('/')
    .get(asyncHandler(async (req, res, next) => {
        const data = await fileAdapter.getInputFiles(req['containerId']);
        res.send(data);
    }))
    .post(asyncHandler(async (req, res, next) => {
        const result = await fileAdapter.postInputFileFromRequest(req['containerId'], req);
        res.status(201).send(result);
    }))

    router.put('/order', asyncHandler(async (req, res, next) => {
        const result = await fileAdapter.setInputFileOrder(req['containerId'], req.body);
        res.status(200).send(result);
    }));

    router.post('/submit', asyncHandler(async (req, res, next) => {
        let type: OrderTypes;

        if (req.query.type === 'final') {
            type = OrderTypes.FINAL;
        } else if (req.query.type === 'evaluation') {
            type = OrderTypes.EVALUATION
        } else {
            return next(new RequestError(`Parameter 'type' must have value 'final' or 'evaluation'`, 400));
        }

        const result = await printshopAdapter.uploadInputFiles(req['containerId'], type);
        res.status(200).send(result);
    }));

    router.head('/submitting', asyncHandler(async (req, res, next) => {
        const isUploading = await printshopAdapter.isUploading(req['containerId']);
        res.sendStatus(isUploading ? 200 : 204);
    }));

    router.post('/unsubmit', asyncHandler(async (req, res, next) => {
        await printshopAdapter.clearContent(req['containerId']);
        res.sendStatus(204);
    }));

    router.param('fileId', (req, res, next, fileId) => {
        req['fileId'] = base64url.decode(fileId);
        next();
    });

    router.use('/:fileId', ensureSubContainer('fileId'));

    router.delete('/:fileId', asyncHandler(async (req, res, next) => {
        const result = await fileAdapter.deleteInputFile(req['containerId'], req['fileId']);
        res.status(result).send('');
    }));

    router.post('/:fileId/replace', asyncHandler(async (req, res, next) => {
        const result = await fileAdapter.replaceInputFile(req['containerId'], req['fileId'], req);
        res.status(201).send(result);
    }));

    router.put('/:fileId/filename', asyncHandler(async (req, res, next) => {
        const result = await fileAdapter.setFileName(req['fileId'], req.body.filename);
        res.status(200).send(result);
    }));

    return router;
}
