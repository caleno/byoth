import { RedisClient } from 'redis';
import * as Redlock from 'redlock';
import { Provider } from 'nconf';

import { LdpClient } from './lib/ldp-client';
import { Institution } from './lib/institution';
import { ThesisContainerAdapter } from './lib/thesis-container-adapter';
import { ThesisFileAdapter } from './lib/thesis-file-adapter';
import { ApprovalIndex } from './lib/approval-index';
import { PrintShopAdapter } from './lib/printshop-adapter';
import { ISBNHandler } from './lib/isbn-handler';
import { ClientAuth } from './lib/client-auth';

import { UiBPrintShop } from './lib/uib-printshop';
import { DummyPrintShop } from './lib/dummy-printshop';
import { UiBInstitution } from './lib/uib-institution';

import { initRedis } from './boot/redis';
import { initRedlock } from  './boot/redlock';
import { initLdp } from './boot/ldp';

/**
 * Basic objects needed by the different API routes
 */
export interface Services {
    redis: RedisClient,
    redlock: Redlock,
    ldp: LdpClient,
    institution: Institution,
    approvalIndex: ApprovalIndex,
    containerAdapter: ThesisContainerAdapter,
    fileAdapter: ThesisFileAdapter,
    printshopAdapter: PrintShopAdapter,
    isbnHandler: ISBNHandler,
    clientAuth: ClientAuth
}

/**
 * Intialize services once and reuse between routes
 */
export async function initServices(config: Provider): Promise<Services> {
    const redis = initRedis(config);
    const redlock = initRedlock(config, redis);
    const ldp = initLdp(config, redlock);
    const institution = new UiBInstitution(config, redis);
    const approvalIndex = new ApprovalIndex(redis);
    const clientAuth = new ClientAuth(config, ldp);

    const containerAdapter = new ThesisContainerAdapter(ldp, redlock, institution);
    await containerAdapter.setupLDP();

    const fileAdapter = new ThesisFileAdapter(containerAdapter, ldp, redlock);

    let printshop;

    if (process.env.NODE_ENV === 'test') {
        printshop = new DummyPrintShop(containerAdapter);
    } else {
        printshop = new UiBPrintShop(config, containerAdapter, fileAdapter);
        await printshop.init();
    }

    const printshopAdapter = new PrintShopAdapter(printshop, containerAdapter, fileAdapter, redis);

    const isbn = new ISBNHandler(config, containerAdapter);

    return {
        redis: redis,
        redlock: redlock,
        ldp: ldp,
        institution: institution,
        approvalIndex: approvalIndex,
        containerAdapter: containerAdapter,
        printshopAdapter: printshopAdapter,
        isbnHandler: isbn,
        clientAuth: clientAuth,
        fileAdapter: fileAdapter
    };
}
