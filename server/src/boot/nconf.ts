import * as nconf from 'nconf';
import { Provider } from 'nconf';

export function initNconf(): Provider {
    let configFile = process.env.BYOTH_CONFIG_FILE;

    if (!configFile) {
        configFile = './config.json';
    }

    nconf.argv({separator: '-'})
    .env({separator: '_'})
    .file({ file: configFile });

    nconf.defaults({
        byoth: {
            host: 'localhost',
            http: {
                port: 3000
            },
            redis: {}
        }
    });

    return nconf;
}
