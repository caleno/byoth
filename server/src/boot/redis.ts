import * as redis from 'redis';
import { RedisClient } from 'redis';
import { Provider } from 'nconf';

export function initRedis(config: Provider): RedisClient {
    config.required(['byoth:redis']);
    const redisClient = redis.createClient(config.get('byoth:redis'));
    redisClient.on('error', console.error);

    return redisClient;
}
