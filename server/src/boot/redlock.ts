import { RedisClient } from 'redis';
import * as Redlock from 'redlock';
import { Provider } from 'nconf';

export function initRedlock(config: Provider, redis: RedisClient): Redlock {
    const redlock = new Redlock([redis], {retryDelay: 200, retryCount: 10});
    redlock.on('clientError', function(err) {
        console.error('Redlock: A redis error has occurred:', err);
    });

    return redlock;
}

