import { Express } from 'express';
import { Provider } from 'nconf';
import * as connectRedis from 'connect-redis';
import * as session from 'express-session';
import * as passport from 'passport';

import { LdpClient } from '../lib/ldp-client';
import { DataportenStrategy, DATAPORTEN_PROFILE_URL } from '../lib/dataporten-strategy';
import { ClientAuth } from '../lib/client-auth';

function setupDataporten(config: Provider) {
    config.required([ 'byoth:dataporten:clientID', 'byoth:dataporten:clientSecret', 'byoth:dataporten:callbackURL' ]);

    passport.use('dataporten', new DataportenStrategy(
        {
            ...config.get('byoth:dataporten'),
            authorizationURL: 'https://auth.dataporten.no/oauth/authorization',
            tokenURL: 'https://auth.dataporten.no/oauth/token',
            profileUrl: DATAPORTEN_PROFILE_URL,
            state: true,
            scope: ['userid', 'userid-feide', 'email', 'profile', 'groups'],
        },
        (accessToken, refreshToken, profile, cb) =>
            cb(null, { accessToken: accessToken, data: profile })
    ) as any);
}

export async function initAuth(config: Provider, app: Express, ldp: LdpClient) {
    config.required(['byoth:sessionSecret', 'byoth:redis', 'byoth:host']);

    const RedisStore = connectRedis(session);

    app.use(session({
        name: 'sessionId',
        store: new RedisStore(config.get('byoth:redis')),
        secret: config.get('byoth:sessionSecret'),
        saveUninitialized: true,
        resave: false,
        cookie: {
            httpOnly: true,
            maxAge: 3600 * 24 * 30 * 1000, // 30 days
            domain: config.get('byoth:host')
        }
    }));

    // store everything in the session store
    passport.serializeUser((user, cb) => {
        cb(null, user);
    });

    passport.deserializeUser((user, cb) => {
        cb(null, user);
    });

    setupDataporten(config);

    app.use(passport.initialize());
    app.use(passport.session());

    const auth = new ClientAuth(config, ldp);
    await auth.init();
}
