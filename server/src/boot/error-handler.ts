import { Express, Request, Response, NextFunction } from 'express';

export function initErrorHandler(app: Express) {
    // For the API we want JSON errors
    app.use('/api/', (err: any, req: Request, res: Response, next: NextFunction) => {
        // Tests will force a bunch of error conditions, don't want to see those
        if (process.env.NODE_ENV !== 'test') {
            console.error(err);
        }

        const output: any = {
            name: err.name || 'Error',
            message: err.message,
            status: err.status || 500,
            request: err.request,
        };

        if (req.app.get('env') === 'development') {
            output.stack = err.stack;
        }

        res.status(output.status).json({error: output});
    });

    // for general errors
    app.use(function(err: any, req: Request, res: Response, next: NextFunction) {
        console.error(err);

        // set locals, only providing error in development
        res.locals.name = err.name || 'Server error';
        res.locals.message = err.message;
        res.locals.status = err.status;
        res.locals.stack = req.app.get('env') === 'development' ? err.stack : '';

        // rename some errors outside development
        if (req.app.get('env') !== 'development' && err.code === 'ENOENT') {
            res.locals.message = 'File not found';
        }

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });


}
