import { Provider } from 'nconf';
import * as Redlock from 'redlock';
import { LdpClient } from '../lib/ldp-client';

export function initLdp(config: Provider, redlock: Redlock) {
    config.required(['byoth:ldp:root', 'byoth:ldp:username', 'byoth:ldp:password']);

    const username = config.get('byoth:ldp:username');
    const password = config.get('byoth:ldp:password');
    const ldpAuthorization = 'Basic ' + Buffer.from(username + ':' + password).toString('base64');

    return new LdpClient(redlock, { root: config.get('byoth:ldp:root'), authorization: ldpAuthorization });
}
