import { Request, Response, NextFunction } from 'express';
import * as base64url from 'base64-url';

/** Returns a middleware that base64url-decodes the request parameter with the given key */
export function decodeParam(key: string) {
    return function(req: Request, res: Response, next: NextFunction) {
        if (typeof req.params[key] === 'string') {
            req.params[key] = base64url.decode(req.params[key]);
        }

        next();
    };
}
