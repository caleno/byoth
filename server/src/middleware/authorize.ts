import { Request, Response, NextFunction } from 'express';
import { AuthorizationError } from '../lib/errors';
import { ThesisContainerAdapter } from '../lib/thesis-container-adapter';
import { userFromRequest } from '../lib/user';

// we create these to allow mocking
export const _helpers = {
    isAuthenticated: (req: Request, res: Response, next: NextFunction) => {
        if (!(<any>req).isAuthenticated()) {
            next(new AuthorizationError(req));
        } else {
            next();
        }
    },
    isOwner: async (req: Request, container: ThesisContainerAdapter, reqIdProperty: string, next: NextFunction) => {
        const owner = await container.getContainerOwner(req[reqIdProperty]);

        if (owner !== container.personId(userFromRequest.getInternalUser(req))) {
            next(new AuthorizationError(req));
        } else {
            next();
        }
    },
    isSubContainer: (req: Request, subContainerIdProperty: string, containerIdProperty: string, next: NextFunction) => {
        if (!req[subContainerIdProperty].startsWith(req[containerIdProperty])) {
            next(new AuthorizationError(req));
        } else {
            next();
        }
    }
}

/**
 * Create middleware to ensure that a request is authenticated.
 * Middleware throws AuthorizationError if request is not authenticated.
 *
 * @returns Middleware
 */
export function ensureAuthenticated() {
    return (req: Request, res: Response, next: NextFunction) => {
        _helpers.isAuthenticated(req, res, next);
    };
}

/**
 * Create async middleware to ensure that the user making the request is the owner of the requested thesis container.
 * Middleware throws AuthorizationError user is not owner.
 *
 * @param container The ThesisContainerAdapter to use.
 * @param containerIdProperty The name of the request-property containing the container id.
 * @returns Async middleware.
 */
export function ensureContainerOwner(container: ThesisContainerAdapter, containerIdProperty = 'containerId') {
    return async (req: Request, res: Response, next: NextFunction) => {
        await _helpers.isOwner(req, container, containerIdProperty, next);
    };
}

/**
 * Create middleware to ensure that a subcontainer actually is contained in its alleged parent container.
 * Middleware throws AuthorizationError if containers don't match.
 *
 * @param subContainerIdProperty The name of the request-parameter containing the sub container id.
 * @param containerIdProperty The name of the request-property containing the parent container id.
 * @returns Middleware.
 */
export function ensureSubContainer(subContainerIdProperty: string, containerIdProperty = 'containerId') {
    return (req: Request, res: Response, next: NextFunction) => {
        _helpers.isSubContainer(req, subContainerIdProperty, containerIdProperty, next);
    }
}
