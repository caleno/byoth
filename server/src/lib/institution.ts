import { AddressProperties } from 'model/order';

interface Unit {
    id: string;
    name: { [property: string]: string }; // i.e. { en: 'foo', nb: 'føø' }
}

export interface Place extends Unit {
    address: AddressProperties
};
export type Faculty = Place;
export type Department = Place;

export type Area = Unit;
export type Building = Unit;
export type Room = Unit;

/**
 * A general interface to get organizational and building units for an institution.
 *
 * Implementations can pick their own ID schemes.
 */
export interface Institution {
    /** Initialize institution, set up caches for example */
    init(): Promise<void>;

    /** Get a list of all faculties at an institution */
    getFaculties(): Promise<Faculty[]>;

    /** Get the faculty a specific group ID corresponds to */
    getFaculty(groupid: string): Promise<Faculty>;

    /** Get the departments at a faculty */
    getDepartments(facultyId): Promise<Department[]>;

    /** Get the department a specific group ID corresponds to */
    getDepartment(groupid: string): Promise<Department>;

    /** Get a given faculty, department or subdivision based on its ID */
    getPlace(placeid: string): Promise<Place>;

    /** Get all areas at an institution */
    getAreas(): Promise<Area[]>;

    /** Get all buildings in an area */
    getBuildings(areaId: string): Promise<Building[]>;

    /** Get all rooms in a building */
    getRooms(buildingId: string): Promise<Room[]>;
}
