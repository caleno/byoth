import { Request } from 'express';
import axios from 'axios';
import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Readable } from 'stream';
import { promises as jsonld } from 'jsonld';
import * as Redlock from 'redlock';
import { StorageEngine } from 'multer';
import * as multer from 'multer';

import { DatabaseError, FramingError, LockError, NotFoundError } from './errors';

import { CompactedDocument, JsonLdFrame, DocumentModel } from 'model/index';

/** Errors from Axios need to be mapped to errors that can be handled by the rest of the application */
function convertAxiosError(err) {
    if (err.response) {
        const path = err.request && err.request.method + ' ' + err.request.path + ': ';
        let message = path + err.response.statusText;
        if (err.response.data) {
            message += ' (' + err.response.data + ')';
        }
        return new DatabaseError(message, err.response.status);
    } else {
        return new DatabaseError(err.message);
    }
}

/** Some helper functions */
export namespace Ldp {
    /** Header to be used in sparql patch requests */
    export const patchHeaders = { 'Content-Type': 'application/sparql-update' }

    /**
     * Create a compacted JSON-LD from tiples document using a frame.
     *
     * @param triples RDF data to frame (n-triples).
     * @param frame JSON-LD frame to use.
     * @returns Framed and compacted JSON-LD document.
     * @throws FramingError if framing fails.
     */
    export async function createLdDocument(triples: string, frame: JsonLdFrame): Promise<CompactedDocument> {
        let doc;
        let framedDoc;
        let compactedDoc;

        // shallow copy is enough, we are only changing a top level property
        const myFrame = Object.assign({}, frame);
        myFrame['@explicit'] = true;

        try {
            doc = await jsonld.fromRDF(triples, { format: 'application/n-quads', useNativeTypes: true });
        } catch (err) {
            throw new FramingError('Input triples invalid:' + err.message);
        }

        try {
            framedDoc = await jsonld.frame(doc, myFrame);
        } catch (err) {
            throw new FramingError(err.message);
        }

        try {
            compactedDoc = await jsonld.compact(framedDoc, myFrame['@context']);
        } catch (err) {
            // if it frames it should compact, so difficult to mock an error here.
            /* istanbul ignore next */
            throw new FramingError('Compaction error: ' + err.message);
        }

        if (!compactedDoc['@id']) {
            throw new FramingError('Frame not compatible with data');
        }

        return compactedDoc;
    }

    /**
     * Filter a set of RDF triples to only contain triples compatible with a given JSON-LD frame.
     *
     * For filtering to work, the frame must have `'@explicit': true` at all levels.
     *
     * @param triples RDF data to frame (n-triples).
     * @param frame JSON-LD frame to use.
     * @returns Filtered n-triples.
     * @throws FramingError if framing fails.
     */
    export async function filterTriples(triples: string, frame: JsonLdFrame): Promise<string> {
        const framedDoc = await createLdDocument(triples, frame);

        // hide type information, can't be filtered with framing.
        framedDoc['@type'] = frame['@type'];

        return jsonld.toRDF(framedDoc, { format: 'application/n-quads' });
    }

    /**
     * Frame a JSON-LD document, and then convert to RDF triples.
     *
     * The framing step is intended to strip away unwanted properties from the input document.
     *
     * @param doc A JSON-LD document.
     * @param frame JSON-LD frame to use.
     * @returns Filtered n-triples as string.
     */
    export async function filterJsonLdToTriples(doc: any, frame: JsonLdFrame): Promise<string> {
        let framedDoc;

        try {
            framedDoc = await jsonld.frame(doc, frame);
        } catch (err) {
            throw new FramingError(err.message);
        }

        return jsonld.toRDF(framedDoc, { format: 'application/n-quads' });
    }

    export interface File {
        mimetype: string,
        originalname: string,
        stream: Readable
    }

    /**
     * A multer storage engine for LDP.
     *
     * Also provides method `handleFile` to allow storing files that are not in
     * an express request.
     */
    export class Storage implements StorageEngine {
        /**
         * Constructor
         *
         * @param fileContainerUri The URI for the LDP container that will hold the file.
         * @param authorization The authorization header value to use to connect to the LDP.
         * @param slug An optional slug for the file path.
         */
        constructor(private fileContainerUri: string, private authorization: string, private slug?: string) { }

        /** The call signature in StorageEngine, for Multer */
        _handleFile(req, file, cb) {
            this.handleFile(file)
            .then(res => cb(null, res), err => cb(err));
        }

        /* Not explicitly used, but needed to have a complete Storage engine */
        /* istanbul ignore next */
        _removeFile(req, file, cb) {
            axios({
                method: 'delete',
                url: file.path
            })
            .then(response => cb(null))
            .catch(cb);
        }

        /** Upload file */
        async handleFile(file: Ldp.File) {
            let filename = file.originalname;

            // Fedora can't handle special characters in filename,
            // map some common ones and convert the rest to '-'
            filename = filename
                .normalize('NFD') // change é to ´e, etc
                .replace(/[\u0300-\u036f]/g, '') // remove accents/diacritics
                .replace(/[øö]/gi, 'oe')
                .replace(/[æä]/gi, 'ae')
                .replace(/[å]/gi, 'aa')
                .replace(/[^a-zA-Z0-9_\.\ ]/g, '-');

            const headers: any = {
               'Content-Type': file.mimetype,
               'Content-Disposition': 'attachment; filename="' + filename + '"',
               'Authorization': this.authorization,
            };

            if (this.slug) {
                headers.slug = this.slug;
            }

            try {
                const response = await axios({
                    method: 'post',
                    url: this.fileContainerUri,
                    headers: headers,
                    data: file.stream,
                    maxContentLength: Infinity
                });

                return { path: response.data };
            } catch (err) {
                throw convertAxiosError(err);
            }
        }
    }
}

interface LdpClientOptions {
    /** The URL the LDP including the root container to use, i.e. http://localhost:1234/rest/foo/   */
    root: string;

    /** Authorization header to use when connecting to LDP */
    authorization: string;

    /** Suffix to add to file path to access its metadata, e.g. 'fcr:metadata' for Fedora */
    metadataSuffix?: string;

    /**
     * Prefixes for properties that this client should not be allowed change
     * (i.e. they will be stripped from input in SPARQL updates).
     *
     * For Fedora this value should be 'http://fedora.info/definitions/v4/repository#'.
     *
     * This feature allows you to include those properties in your models, without causing
     * errors when updating the objects.
     */
    readonlyPrefix?: string[];

    /** Suffix to post versions/snapshots of container, e.g. 'fcr:versions' for Fedora */
    versionSuffix?: string;
}

/**
 * A client for connecting to a Linked Data Platform.
 *
 * Written for Fedora 4, will probably work with only minor adjustments with other platforms.
 *
 * In some cases we could have got JSON-LD directly from server instead of getting n-triples
 * and converting them to JSON-LD, but for simplicitcy we use the n-triples approach everywhere.
 */
export class LdpClient {
    private root: string;
    private authorization: string;
    private metadataSuffix: string;
    private readonlyPrefix: string[];
    private versionSuffix: string;

    /**
     * Constructor
     *
     * @param redlock A Redlock instance to ensure concurrent writes don't interfere with each other
     * @param options LDP options, metadatasuffix and and readonlyPrefix will be set to Fedora comptatible values by default
     */
    constructor(private redlock: Redlock, options: LdpClientOptions) {
        this.root = options.root;
        this.authorization = options.authorization;

        if (!this.root.match(/\/$/)) {
            this.root += '/';
        }

        this.metadataSuffix = options.metadataSuffix || 'fcr:metadata';
        this.readonlyPrefix = options.readonlyPrefix ||
            [ 'http://fedora.info/definitions/v4/repository#', 'http://www.loc.gov/premis/rdf/v1#' ];
        this.versionSuffix = options.versionSuffix || 'fcr:versions';
    }

    /** The root url for the client */
    get rootUrl(): string {
        return this.root;
    }

    /**
     * Get RDF triples corresponding to a given full url.
     *
     * @param url The full url of the object.
     * @return n-triples as string.
     * @throws DatabaseError if failure connecting to LDP
     */
    private async getTriples(url: string): Promise<string> {
        try {
            const response = await axios({
                method: 'get',
                url: url,
                headers: {
                    accept: 'application/n-triples',
                    authorization: this.authorization,
                },
                responseType: 'text'
            });
            return response.data;
        } catch (err) {
            throw convertAxiosError(err);
        }
    }

    /**
     * Does an object exist.
     *
     * @param path The object path relative to the root url.
     * @returns True if the path corresponds to an object.
     * @throws DatabaseError if failure connecting to LDP.
     */
    async exists(path: string): Promise<boolean> {
        try {
            const response = await axios.head(this.root + path, { headers: { authorization: this.authorization }});
            return response.status === 200;
        } catch (err) {
            if (err.response) {
                if (err.response.status === 404) {
                    return false;
                } else {
                    throw convertAxiosError(err);
                }
            } else {
                throw new DatabaseError(path + ': ' + err.message, 500);
            }
        }
    }

    /**
     * Get an object as a compacted JSON-LD document framed according to a given model.
     *
     * @param path The object path relative to the root url.
     * @param model A document model containing the JSON-LD frame to use.
     * @returns A compacted JSON-LD document describing the requested object.
     * @throws DatabaseError if failure connecting to the LDP and FramingError if data incompatible with model.
     */
    async get(path: string, model: DocumentModel): Promise<CompactedDocument> {
        const triples = await this.getTriples(this.root + path);

        return Ldp.createLdDocument(triples, model.frame);
    }

    /**
     * Create an object based on a given model.
     *
     * @param path The container path relative to the root url.
     * @param model A document model containing the JSON-LD frame to use.
     * @param slug The slug for the new object (otherwise the LDP will give it an ID).
     * @returns A compacted JSON-LD document describing the created object.
     * @throws DatabaseError if failure connecting to the LDP and FramingError if data incompatible with model.
     */
    async post(path: string, model: DocumentModel, slug?: string): Promise<CompactedDocument> {
        const config: AxiosRequestConfig = {
            headers: {
                'Content-Type': 'text/turtle',
                authorization: this.authorization
            }
        };

        if (slug) {
            config.headers.slug = slug;
        }

        let response;

        try {
            response = await axios.post(this.root + path, `<> a <${model.expandedType}>`, config);
        } catch (err) {
            throw convertAxiosError(err);
        }

        const triples = await this.getTriples(response.data);
        return Ldp.createLdDocument(triples, model.frame);
    }

    /** Strip triples of readonly properties */
    private removeReadonlyTriples(triples): string {
        return triples.split('\n')
        .filter(line => !this.readonlyPrefix.some(prefix => line.match(prefix)))
        .join('\n');
    }

    /**
     * Update an object based on a model and the provided value
     *
     * Will filter out values in the document that are not part of the model.
     * (Provided that the model frame has '@explicit' flags set correctly)
     *
     * @param path The object path relative to the root url.
     * @param model The model to use.
     * @param doc A JSON-LD document describing the value to update to.
     * @returns A compacted JSON-LD document describing the updated object.
     * @throws DatabaseError if failure connecting to the LDP and FramingError if data incompatible with model.
     */
    async update(path: string, model: DocumentModel, doc: CompactedDocument): Promise<CompactedDocument> {
        let lock;

        try {
            lock = await this.redlock.lock(this.root + path, 2000);
        } catch (err) {
            throw new LockError(err.message);
        }

        try {
            const oldTriples = await this.getTriples(this.root + path)
                .then(triples => Ldp.filterTriples(triples, model.frame))
                .then(triples => this.removeReadonlyTriples(triples));

            let newTriples;

            try {
                newTriples = await Ldp.filterJsonLdToTriples(doc, model.frame)
                    .then(triples => this.removeReadonlyTriples(triples));

                if (!newTriples) {
                    throw new FramingError('Document not compatible with model', 422);
                }
            } catch (err) {
                if (err instanceof FramingError) {
                    err.status = 422;
                }
                throw err;
            }

            const query = `DELETE {\n${oldTriples}}\nINSERT {\n${newTriples}}\nWHERE {}`;

            try {
                await axios.patch(this.root + path, query,
                    { headers: { ...Ldp.patchHeaders, authorization: this.authorization } });
            } catch (err) {
                throw convertAxiosError(err);
            }

            const updatedTriples = await this.getTriples(this.root + path);
            return Ldp.createLdDocument(updatedTriples, model.frame);
        } finally {
            await lock.unlock().catch(console.error);
        }
    }

    /**
     * Delete an object
     *
     * @param path The object path relative to the root url.
     * @returns The response code of the LDP delete operation (i.e. usually 204).
     * @throws DatabaseError if failure connecting to the LDP.
     */
    async delete(path: string): Promise<number> {
        let response;

        try {
            response = await axios.delete(this.root + path, { headers: { authorization: this.authorization } });
        } catch (err) {
            throw convertAxiosError(err);
        }

        return response.status;
    }

    /**
     * For a newly created file, write empty file metadata using the correct model
     */
    private async initFileMetadata(filePath: string, fileModel: DocumentModel) {
        const fileMetadataPath = filePath + '/' + this.metadataSuffix;
        const query = `DELETE {}\nINSERT { <> a <${fileModel.expandedType}> }\nWHERE {}`;

        try {
            await axios.patch(fileMetadataPath, query, { headers: { ...Ldp.patchHeaders, authorization: this.authorization } });
        } catch (err) {
            throw convertAxiosError(err);
        }

        const triples = await this.getTriples(fileMetadataPath);

        return Ldp.createLdDocument(triples, fileModel.frame);
    }

    /**
     * Post a file based on an express Request.
     *
     * @param path The container path relative to the root url.
     * @param fileModel A model to describe file metadata.
     * @param req An Express request containing the file.
     * @param slug The slug for the file (otherwise the LDP will give it an ID).
     * @returns A compacted JSON-LD document with file metadata.
     * @throws DatabaseError if failure connecting to the LDP.
     */
    async postFileFromRequest(path: string, fileModel: DocumentModel, req: Request, slug?: string): Promise<CompactedDocument> {
        const storage = new Ldp.Storage(this.root + path, this.authorization, slug);
        const upload = multer({ storage: storage }).single('file');

        await new Promise<string>((resolve, reject) => upload(req, null, err => {
            if (err) {
                return reject(err);
            }
            resolve();
        }));

        return this.initFileMetadata(req.file.path, fileModel);
    }

    /**
     * Post a file based on a file stream.
     *
     * @param path The container path relative to the root url.
     * @param fileModel A model to describe file metadata.
     * @param file An `Ldp.File` that contains the stream, mimetype and filename.
     * @param slug The slug for the file (otherwise the LDP will give it an ID).
     * @returns A compacted JSON-LD document with file metadata.
     * @throws DatabaseError if failure connecting to the LDP.
     */
    async postFileFromStream(path: string, fileModel: DocumentModel, file: Ldp.File, slug?: string): Promise<CompactedDocument> {
        const storage = new Ldp.Storage(this.root + path, this.authorization, slug);

        const response = await storage.handleFile(file);

        return this.initFileMetadata(response.path, fileModel);
    }

    /**
     * Get file metadata.
     *
     * @param path The file path relative to the root url.
     * @param model The model to describe file metadata.
     * @returns A compacted JSON-LD document with file metadata.
     * @throws DatabaseError if failure connecting to the LDP.
     */
    async getFileMetadata(path: string, model: DocumentModel): Promise<CompactedDocument> {
        return this.get(path + '/' + this.metadataSuffix, model);
    }

    /**
     * Get a file.
     *
     * @param path The file path relative to the root url.
     * @returns a response where the data property is a stream for the requested file.
     * @throws DatabaseError if failure connecting to the LDP.
     */
    async getFile(path: string): Promise<AxiosResponse> {
        try {
            return await axios.get(this.root + path, { responseType: 'stream', headers: { authorization: this.authorization } } );
        } catch (err) {
            throw convertAxiosError(err);
        }
    }

    /**
     * Update file metadata.
     *
     * @param path The file path relative to the root url.
     * @param model The model to describe file metadata.
     * @param doc The file metadata to update with.
     * @returns A compacted JSON-LD document with file metadata.
     * @throws DatabaseError if failure connecting to the LDP.
     */
    async updateFileMetadata(path: string, model: DocumentModel, doc: CompactedDocument): Promise<CompactedDocument> {
        return this.update(path + '/' + this.metadataSuffix, model, doc);
    }

    /**
     * Store a labelled snapshot of the current state of a container (including all contained objects).
     *
     * @param path The container path relative to the root url.
     * @param label The label for the snapshot. Same label can not be used twice.
     * @returns Status returned by LDP (normally 201).
     * @throws DatabaseError if failure connecting to the LDP or label already taken.
     */
    async storeSnapshot(path: string, label: string): Promise<number> {
        try {
            return await axios.post(
                this.root + path + '/' + this.versionSuffix,
                null,
                { headers: { authorization: this.authorization, slug: label } }
            ).then(res => res.status);
        } catch (err) {
            throw convertAxiosError(err);
        }
    }

    /**
     * Delete a given snapshot
     *
     * @param path The container path relative to the root url.
     * @param label The label for the snapshot.
     * @returns Status returned by LDP (normally 204).
     * @throws DatabaseError if failure connecting to the LDP or label does not exist.
     */
    async deleteSnapshot(path: string, label: string): Promise<number> {
        try {
            return await axios.delete(
                this.root + path + '/' + this.versionSuffix + '/' + label,
                { headers: { authorization: this.authorization } }
            ).then(res => res.status);
        } catch (err) {
            if (err.response && err.response.status === 404) {
                throw new NotFoundError('Version ' + label + ' not found');
            } else {
                throw convertAxiosError(err);
            }
        }
    }

    /**
     * Get a list of stored snapshot labels
     *
     * @param path The container path relative to the root url.
     * @returns Array of labels
     * @throws DatabaseError if error from LDP.
     *     FramingError if LDP data not compatible with versions frame.
     */
    async getSnapshotLabels(path: string): Promise<string[]> {
        let triples: string;

        try {
            triples = await axios.get(
                this.root + path + '/' + this.versionSuffix,
                { headers: { authorization: this.authorization } }
            ).then(res => res.data);
        } catch (err) {
            if (err.response && err.response.status === 404) {
                return [];
            }
            throw convertAxiosError(err);
        }

        // we can use a more relaxed model definition here,
        // since we will only be exporting version labels here
        const versionsFrame = {
            '@context': {
                fedora: 'http://fedora.info/definitions/v4/repository#',
                ldp: 'http://www.w3.org/ns/ldp#',
                hasVersion: {
                    '@id': 'fedora:hasVersion',
                    '@container': '@set'
                },
                label: { '@id': 'fedora:hasVersionLabel' }
            },
            hasVersion: { '@default': [] }
        };

        return Ldp.createLdDocument(triples, versionsFrame as any)
            .then(doc => doc.hasVersion ? doc.hasVersion.map(version => version.label) : []);
    }

    /**
     * Check if a snapshot label exists
     *
     * @param path The container path relative to the root url.
     * @param label Snapshot label
     * @returns Does the snapshot exist
     * @throws DatabaseError if error from LDP.
     *     FramingError if LDP data not compatible with versions frame.
     */
    async snapshotLabelExists(path: string, label: string): Promise<boolean> {
        return this.getSnapshotLabels(path).then(labels => labels.includes(label));
    }
}
