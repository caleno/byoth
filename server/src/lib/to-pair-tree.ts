import { InputError } from './errors';

/**
 *  Convert id to pair tree.
 *
 *  If an id of form 1234abcd is converted to 3 elements of width 2, we get to 12/34/ab/1234abcd.
 *
 *  @param id id string to convert
 *  @param nElements number of elements to split into (i.e. number of slashes)
 *  @param width size of each element
 */
export function toPairTree(id: string, nElements: number, width = 2): string {
    if (id.length < nElements * width) {
        throw new InputError(`String '${id}' too short to divide into ${nElements} elements`);
    }

    let pairTree = '';
    for (let i = 0; i < nElements; ++i) {
        pairTree += id.substr(i * width, width) + '/';
    }

    return pairTree + id;
}

/**
 *  Convert id to Fedora Repository pair tree
 *
 *  12345abcde -> 12/34/5a/bc/12345abcde
 *
 *  @param id id string to convert
 *  @return pair tree string
 */
export function toFedoraPairTree(id): string {
    return toPairTree(id, 4, 2);
}
