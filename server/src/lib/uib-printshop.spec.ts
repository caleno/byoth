import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as soap from 'soap';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';

import { PrintFormats, DegreeTypes } from 'model/thesis';
import { OrderTypes } from 'model/order';

import { UiBPrintShop } from './uib-printshop';

chai.use(chaiAsPromised);
const expect = chai.expect;

const mockConfig = {
    required: () => {},
    get: () => 'foo'
}

const mockContainerAdapter = {
    getProperty: () => Promise.resolve({})
}

const mockFileAdapter = {};

describe('UiBPrintshop', () => {
    const sandbox: SinonSandbox = sinon.sandbox.create();
    let printshop: UiBPrintShop;


    beforeEach(() => {
        printshop = new UiBPrintShop(mockConfig as any, mockContainerAdapter as any, mockFileAdapter as any);
    });

    afterEach(() => sandbox.restore());

    describe('init()', () => {
        it('should fail if no connection to remote', async () => {
            sandbox.stub(soap, 'createClientAsync').rejects(Error('foo'));

            await expect(printshop.init()).to.eventually.be.rejectedWith(/foo/);
        });

        it('shoudl succeed if client creation succeeds', async () => {
            sandbox.stub(soap, 'createClientAsync').resolves({
                addHttpHeader: () => {},
                on: () => {}
            });

            return expect(printshop.init()).to.be.fulfilled;
        });
    });

    describe('getCoverPreview()', () => {
        const id = 'foo';
        let thesis, candidate, defense, response;

        beforeEach(() => {
            thesis = {
                title: 'Foo',
                isbn: '123'
            };

            candidate = {
                givenName: 'Foo',
                familyName: 'Bar',
            };

            defense = {
                start: new Date().toISOString()
            };

            response = {
                status: 'foo',
                cover_file_thumbnails: {
                    base64Binary: [ 'abc', '123' ]
                },
                cover_file_pdf: 'abc',
                covercommittee_file_pdf: '123'
            };
        })

        beforeEach(() => {
            sandbox.stub(mockContainerAdapter, 'getProperty')
                .withArgs(id, 'thesis').resolves(thesis)
                .withArgs(id, 'candidate').resolves(candidate)
                .withArgs(id, 'defense').resolves(defense);

            printshop['client'] = {
                GetCoverPreviewAsync: (payload) => Promise.resolve([{ GetCoverPreviewResult: response }])
            };
        });

        it('should send correct payload to remote', async () => {
            const spy = sandbox.spy(printshop['client'], 'GetCoverPreviewAsync');

            const result = await printshop.getCoverPreview(id, OrderTypes.FINAL);

            const payload = spy.getCall(0).args[0];
            expect(payload).to.have.property('referenceID', id);
            expect(payload).to.have.property('author_name', 'Foo Bar');
            expect(defense.start).to.include(payload.defenseDate);
            expect(result).to.have.property('status', response.status);
            expect(result).to.have.property('thumbnails').that.deep.equals(response.cover_file_thumbnails.base64Binary);
            expect(result).to.have.property('pdf', response.cover_file_pdf);
        });

        it('should convert variants and incomplete metadata to correct payload strings', async () => {
            const spy = sandbox.spy(printshop['client'], 'GetCoverPreviewAsync');

            thesis.printFormat = PrintFormats.A4;
            thesis.degreeType = DegreeTypes.PHD;
            thesis.language = 'nb';
            defense.start = null;

            await printshop.getCoverPreview(id, OrderTypes.FINAL);

            let payload = spy.getCall(0).args[0];
            spy.resetHistory();

            expect(payload).to.have.property('language', 'no');
            expect(payload).to.have.property('documentformat', 'A4');
            expect(payload).to.have.property('thesisDegree', 'Ph.d.');
            expect(payload).to.have.property('submissionYear', '');
            expect(payload).to.have.property('defenseDate', '');

            thesis.printFormat = PrintFormats.B17x24;
            thesis.degreeType = DegreeTypes.DR_PHILOS;
            thesis.language = 'nn';

            await printshop.getCoverPreview(id, OrderTypes.FINAL);

            payload = spy.getCall(0).args[0];
            spy.resetHistory();

            expect(payload).to.have.property('language', 'no');
            expect(payload).to.have.property('documentformat', '17x24');
            expect(payload).to.have.property('thesisDegree', 'Dr. Philos');

            thesis.printFormat = null;
            thesis.degreeType = null;
            thesis.language = 'en';

            await printshop.getCoverPreview(id, OrderTypes.FINAL);

            payload = spy.getCall(0).args[0];
            spy.resetHistory();

            expect(payload).to.have.property('language', 'en');
            expect(payload).to.have.property('documentformat', '');
            expect(payload).to.have.property('thesisDegree', '');
        });

        it('should return committee cover if requested', async () => {
            const result = await printshop.getCoverPreview(id, OrderTypes.EVALUATION);

            expect(result).to.have.property('pdf', response.covercommittee_file_pdf);
        });

        it('should throw error if remote returns no data', async () => {
            response.cover_file_thumbnails = null;

            await expect(printshop.getCoverPreview(id, OrderTypes.FINAL))
                .to.eventually.be.rejectedWith(/Cover not generated/);
        });
    });
});
