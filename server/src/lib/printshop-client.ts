import { Readable } from 'stream';
import { ByothFile } from 'model/byoth-file';
import { Order, OrderTypes } from 'model/order';

/** All files should be base64 encoded strings */
export interface PreviewResponse {
    pdf: string,
    thumbnails: string[],
    status: string,
    numPages?: number,
    numColorPages?: number
}

export interface OrderResponse {
    status: string,
    confirmationNumber: string,
    price: string
}

/**
 * Interface to a generic printshop, implement a version for your supplier
 */
export interface PrintShopClient {
    /**
     * Get cover preview based on Thesis metadata
     */
    getCoverPreview(containerId: string, type: OrderTypes): Promise<PreviewResponse>;

    /**
     * Get preview of complete thesis content
     */
    getContentPreview(containerId: string, type: OrderTypes): Promise<PreviewResponse>;

    /**
     * Delete all uploaded content
     */
    clearContent(containerId: string): Promise<void>;

    /**
     * Upload file.
     *
     * Will return updated file metadata based on the response from the printshop
     */
    uploadContent(containerId: string, files: Readable, metadata: ByothFile, index: number): Promise<ByothFile>;

    /**
     * Validate a sorted array of files (must be same order as upload)
     *
     * Will return an array of updated file metadata based on the response from the printshop
     */
    validateContent(containerId: string, metadata: ByothFile, index: number): Promise<ByothFile>;

    /**
     * Get the price for an order specification
     */
    getPrice(containerId: string, order: Order, type: OrderTypes): Promise<string>;

    /**
     * Submit an order
     */
    submitOrder(containerId: string, order: Order, type: OrderTypes): Promise<Order>;

    /**
     * Cancel an order
     */
    cancelOrder(containerid: string, orderId: string): Promise<string>;
}
