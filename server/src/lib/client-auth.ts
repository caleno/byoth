import * as passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt, StrategyOptions } from 'passport-jwt';
import * as jwt from 'jsonwebtoken';
import { Provider } from 'nconf';

import { LdpClient } from './ldp-client';
import { NotFoundError, AuthorizationError } from './errors';

import { getModel, getInternalModel, Authorization, Client, ByothInternalModel } from 'model/index';

export type AuthorizationType = 'readExport' | 'readProof' | 'writeProof';

export class ClientAuth {
    private internalModel: ByothInternalModel;

    /**
     * Constructor
     *
     * @param config Configuration including the jwtSecret to use (as 'byoth:jwtSecret')
     * @param ldp The LDP client to use
     */
    constructor(private config: Provider, private ldp: LdpClient) {
        config.required(['byoth:jwtSecret']);
        this.internalModel = getInternalModel(ldp.rootUrl);
    }

    /**
     * Setup the LDP to store authorization information.
     *
     * Checks if the required containers and authorization objects are present,
     * and creates them if necessary.
     *
     * @returns Empty promise.
     * @throws Errors from LDP.
     */
    private async setupLDP() {
        const internalModel = getInternalModel(this.ldp.rootUrl);
        const model = getModel(this.ldp.rootUrl);

        if (! await this.ldp.exists('authorization')) {
            await this.ldp.post('', model.ldpSet, 'authorization');
        }

        if (! await this.ldp.exists('client')) {
            await this.ldp.post('', model.ldpSet, 'client');
        }

        if (! await this.ldp.exists('authorization/readExport')) {
            const document = await this.ldp.post('authorization/', internalModel.authorization, 'readExport') as Authorization;

            document.accessType = 'byoth:accessExport';
            document.mode = 'acl:Read';
            document.agent = [];

            await this.ldp.update('authorization/readExport', internalModel.authorization, document);
        }

        if (! await this.ldp.exists('authorization/readProof')) {
            const document = await this.ldp.post('authorization/', internalModel.authorization, 'readProof') as Authorization;
            document.accessType = 'byoth:accessProof';
            document.mode = 'acl:Read';
            document.agent = [];
            await this.ldp.update('authorization/readProof', internalModel.authorization, document);
        }

        if (! await this.ldp.exists('authorization/writeProof')) {
            const document = await this.ldp.post('authorization/', internalModel.authorization, 'writeProof') as Authorization;
            document.accessType = 'byoth:accessProof';
            document.mode = 'acl:Write';
            document.agent = [];
            await this.ldp.update('authorization/writeProof', internalModel.authorization, document);
        }
    }

    /** Sets up Passport to use JwtStrategy based on this class */
    async init() {
        await this.setupLDP();

        const opts: StrategyOptions = {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: this.config.get('byoth:jwtSecret'),
        };

        passport.use(new JwtStrategy(opts, this.verify.bind(this)));
    }

    /** For Passport strategy */
    verify(jwt_payload, callback) {
        if (!jwt_payload.sub || !jwt_payload.sub.startsWith('client/')) {
            return callback(new AuthorizationError());
        }

        this.ldp.get(jwt_payload.sub, this.internalModel.client)
        .then(client => {
            if (!client) {
                return callback(new NotFoundError('Unknown client'));
            }

            return callback(null, {
                id: client['@id'],
                name: client.name
            });
        })
        .catch(err => {
            callback(err, false);
        });
    }

    /**
     * Create a new client.
     *
     * @param name The name of the new client.
     * @returns The new client
     * @throws Errors from the LDP.
     */
    async createClient(name: string): Promise<Client> {
        if (!name) {
            throw RangeError('Must specify client name');
        }

        const document = await this.ldp.post('client', this.internalModel.client).then(doc => <Client>doc);

        document.name = name;

        return this.ldp.update(document['@id'], this.internalModel.client, document).then(res => <Client>res);
    }

    /**
     * Generate a JWT for a given client
     *
     * @param clientId The client.
     * @returns The JWT.
     */
    generateToken(clientId: string): string {
        return jwt.sign({sub: clientId}, this.config.get('byoth:jwtSecret'));
    }

    /**
     * Check if client has a given authorization.
     *
     * @param clientId Client ID relative to the LDP root container.
     * @param authorization The type of authorization.
     * @returns Wether the client has the authorization.
     * @throws Errors from the LDP and NotFoundError if the client or authorization do not exist.
     */
    async clientHasAuthorization(clientId: string, authorization: AuthorizationType): Promise<boolean> {
        const authorizationId = 'authorization/' + authorization;

        if (! await this.ldp.exists(clientId) ) {
            throw new NotFoundError(`Client ${clientId} not found`);
        }

        if (! await this.ldp.exists(authorizationId) ) {
            throw new NotFoundError(`Authorization ${authorizationId} not found`);
        }

        const auth = await this.ldp.get(authorizationId, this.internalModel.authorization) as Authorization;

        return auth.agent.includes(clientId);
    }


    /**
     * Modify a client's authorizations
     *
     * @param clientID Client ID relative to the LDP root container.
     * @param authorization The authorization to change.
     * @param change The change to make, i.e. `'grant'` or `'revoke'`.
     * @returns Empty promise.
     * @throws Errors from the LDP, NotFoundError if the client or authorization do not exist
     *     and InputError if the change is not recognized.
     */
    async modifyClientPermission(
            clientId: string,
            authorization: AuthorizationType,
            change: 'grant' | 'revoke'): Promise<void> {
        const authorizationId = 'authorization/' + authorization;

        if (! await this.ldp.exists(clientId) ) {
            throw new NotFoundError(`Client ${clientId} not found`);
        }

        if (! await this.ldp.exists(authorizationId) ) {
            throw new NotFoundError(`Authorization ${authorizationId} not found`);
        }

        const auth = await this.ldp.get(authorizationId, this.internalModel.authorization);

        switch (change) {
            case 'grant':
                auth.agent.push(clientId);
                break;
            case 'revoke':
                auth.agent = auth.agent.filter(a => a.id === clientId);
                break;
            default:
                throw new RangeError('Illegal change: ' + change);
        }

        await this.ldp.update(authorizationId, this.internalModel.authorization, auth);
    }
}
