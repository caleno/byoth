import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';
import axios from 'axios';
import MockAxios from 'axios-mock-adapter';

import { ISBNHandler } from './isbn-handler';
import { DatabaseError } from './errors';

chai.use(chaiAsPromised);
const expect = chai.expect;

const mockConfig = {
    get: () => 'foo',
    required: () => {}
}

const mockContainer = {
    getProperty: async () => { return {}; },
    putProperty: async () => { return {}; }
}

describe('ISBNHandler', () => {
    let sandbox: SinonSandbox;
    let mockAxios: MockAxios;
    let isbnHandler: ISBNHandler;

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        mockAxios = new MockAxios(axios);
        isbnHandler = new ISBNHandler(mockConfig as any, mockContainer as any);
    });

    afterEach(() => sandbox.restore());

    describe('assignISBN()', () => {
        it('should get ISBN number from remote and assign it to thesis if thesis has none', async () => {
            const isbn = 'ISBN';
            mockAxios.onPost().reply(200, {isbn: isbn});
            sandbox.stub(mockContainer, 'putProperty').callsFake((id, property, data) => Promise.resolve(data));

            const result = await isbnHandler.assignISBN('foo');
            expect(result).to.have.property('isbn', isbn);
        });

        it('should do nothing if thesis already has isbn', async () => {
            const isbn = 'ISBN';
            sandbox.stub(mockContainer, 'getProperty').resolves({isbn: isbn});

            const result = await isbnHandler.assignISBN('foo');
            expect(result).to.have.property('isbn', isbn);
        });

        it('should reject with DatabaseError if failure connecting to ISBN remote', async () => {
            mockAxios.onPost().reply(500);

            await expect(isbnHandler.assignISBN('foo')).to.eventually.be.rejectedWith(DatabaseError);
        });
    });

    describe('unassignISBN()', () => {
        it('should release ISBN number back to remote and return thesis without isbn', async () => {
            const isbn = 'ISBN';
            mockAxios.onPost().reply(200);
            sandbox.stub(mockContainer, 'getProperty').resolves({isbn: isbn});
            sandbox.stub(mockContainer, 'putProperty').callsFake((id, property, data) => Promise.resolve(data));

            const result = await isbnHandler.unassignISBN('foo');
            expect(result).to.not.have.property('isbn');
        });

        it('should do nothing if thesis doesn not have isbn', async () => {
            const result = await isbnHandler.unassignISBN('foo');
            expect(result).to.not.have.property('isbn');
        });

        it('should reject with DatabaseError if failure connecting to ISBN remote', async () => {
            const isbn = 'ISBN';
            sandbox.stub(mockContainer, 'getProperty').resolves({isbn: isbn});
            mockAxios.onPost().reply(500);

            await expect(isbnHandler.unassignISBN('foo')).to.eventually.be.rejectedWith(DatabaseError);
        });
    });
});
