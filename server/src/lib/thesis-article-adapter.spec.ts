import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import * as uuid from 'uuid/v4';
import { SinonSandbox } from 'sinon';
import { ThesisArticleAdapter } from './thesis-article-adapter';

chai.use(chaiAsPromised);
const expect = chai.expect;

/* istanbul ignore next */
const mockLdp = {
    get: () => Promise.resolve({}),
    getFile: () => Promise.resolve({}),
    getFileMetadata: () => Promise.resolve({}),
    post: () => Promise.resolve({}),
    postFileFromRequest: () => Promise.resolve({}),
    postFileFromStream: () => Promise.resolve({}),
    update: () => Promise.resolve({}),
    updateFileMetadata: () => Promise.resolve({}),
    delete: () => Promise.resolve({}),
    exists: () => Promise.resolve(true),
    storeSnapshot: () => Promise.resolve({}),
    snapshotLabelExists: () => Promise.resolve(false)
}

/* istanbul ignore next */
const mockContainer = {
    getProperty: () => Promise.resolve({}),
    addProperty: () => Promise.resolve({}),
    existsProperty: () => Promise.resolve(false),
    model: { article: {} }
}

/* istanbul ignore next */
const mockFileAdapter = {
    postFileFromRequest: () => Promise.resolve({}),
    getFile: () => Promise.resolve({})
}

describe('ThesisArticleAdapter', () => {
    let sandbox: SinonSandbox;
    let adapter: ThesisArticleAdapter;

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        adapter = new ThesisArticleAdapter(<any>mockContainer, <any>mockLdp, <any>mockFileAdapter);
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('getArticle()', () => {
        it('should get an article');
    });

    describe('getArticles()', () => {
        it('should return empty array if articles container has nothing', async () => {
            sandbox.stub(mockContainer, 'getProperty')
            .resolves({ items: [ ]});

            const articles = await adapter.getArticles('bar');

            expect(articles.items).to.be.instanceof(Array);
            expect(articles.items.length).to.equal(0);
        });

        it('should return all articles in articles container as an array of articles', async () => {
            const article1 = { title: 'foo' };
            const article2 = { title: 'bar' };

            sandbox.stub(mockContainer, 'getProperty')
            .resolves({ items: [ 'a', 'b' ]});

            const ldpget = sandbox.stub(mockLdp, 'get')
            ldpget.onCall(0).resolves(article1);
            ldpget.onCall(1).resolves(article2);

            const articles = await adapter.getArticles('foo');

            expect(articles.items).to.include(article1);
            expect(articles.items).to.include(article2);
        });
    });

    describe('postArticle()', () => {
        it('should post an article', async () => {
            const spy = sandbox.spy(mockLdp, 'post');

            await adapter.postArticle('foo');

            expect(spy.calledOnce, 'should call ldp post').to.equal(true);
            expect(spy.getCall(0).args[0], 'should use correct url').to.equal('foo/articles');
            expect(spy.getCall(0).args[1], 'should use model.article').to.equal(mockContainer.model.article);
            expect(spy.getCall(0).args[2], 'should generate short id').to.match(/\w+/); // should we be more specific?
        });
    });

    describe('putArticle()', () => {
        it('should replace an article in ldp', async () => {
            const spy = sandbox.spy(mockLdp, 'update');
            const data = { title: 'bar' };

            await adapter.putArticle('foo', data as any);

            expect(spy.calledOnce, 'should call ldp update').to.equal(true);
            expect(spy.getCall(0).args[0], 'should use correct url').to.equal('foo');
            expect(spy.getCall(0).args[1], 'should use model.article').to.equal(mockContainer.model.article);
            expect(spy.getCall(0).args[2], 'should use correct input').to.equal(data);
        });
    });

    describe('deleteArticle()', () => {
        it('should delete an article in ldp', async () => {
            const articleId = uuid();
            const spy = sandbox.spy(mockLdp, 'delete');

            await adapter.deleteArticle(articleId);

            expect(spy.calledOnce, 'should call ldp delete').to.equal(true);
            expect(spy.getCall(0).args[0], 'should use correct url').to.equal(articleId);
        });

        it('should delete file in .hasFile', async () => {
            const articleId = uuid();
            const fileId = uuid();
            const spy = sandbox.spy(adapter, 'deleteArticleFile');

            sandbox.stub(adapter, 'getArticle').resolves({ '@id': articleId, hasFile: fileId });

            await adapter.deleteArticle(articleId);

            expect(spy.calledOnce, 'should delete file').to.equal(true);
        });

        it('should not delete file in .inFile', async () => {
            const articleId = uuid();
            const fileId = uuid();
            const spy = sandbox.spy(adapter, 'deleteArticleFile');

            sandbox.stub(adapter, 'getArticle').resolves({ '@id': articleId, inFile: fileId });

            await adapter.deleteArticle(articleId);

            expect(spy.calledOnce, 'should not delete file').to.equal(false);
        });
    });

    describe('uploadArticleFile()', () => {
        it('should upload a file and add it as .hasFile on the article and null out .inFile', async () => {
            const articleId = uuid();
            const fileId = uuid();

            sandbox.stub(adapter, 'getArticle').resolves({ '@id': articleId, inFile: uuid() });
            sandbox.stub(mockFileAdapter, 'postFileFromRequest').resolves({'@id': fileId});
            const putArticleSpy = sandbox.stub(adapter, 'putArticle').callsFake((id, doc) => Promise.resolve(doc));

            const file = await adapter.uploadArticleFile('foo', articleId, {} as any);
            expect(file).to.have.property('@id', fileId);

            expect(putArticleSpy.calledOnce, 'should save article with link to new file').to.equal(true);
            const article = putArticleSpy.getCall(0).args[1];
            expect(article).to.have.property('hasFile', fileId);
            expect(article).to.have.property('inFile', null);
        });

        it('should replace old file if it exists', async () => {
            const articleId = uuid();
            const oldFileId = uuid();
            const newFileId = uuid();

            sandbox.stub(adapter, 'getArticle').resolves({ '@id': articleId, hasFile: oldFileId});
            sandbox.stub(mockFileAdapter, 'postFileFromRequest').resolves({'@id': newFileId});
            const putArticleSpy = sandbox.stub(adapter, 'putArticle').callsFake((id, doc) => Promise.resolve(doc));
            const deleteArticleFileSpy = sandbox.stub(adapter, 'deleteArticleFile').resolves({});

            await adapter.uploadArticleFile('foo', articleId, {} as any);

            expect(deleteArticleFileSpy.calledOnce, 'should delete previous hasFile').to.equal(true);

            expect(putArticleSpy.calledOnce, 'should save article with link to new file').to.equal(true);
            const article = putArticleSpy.getCall(0).args[1];
            expect(article).to.have.property('hasFile', newFileId);
        });
    });

    describe('deleteArticleFile()', () => {
        it('should delete the file and set .hasFile to null in article', async () => {
            const articleId = uuid();
            const fileId = uuid();

            sandbox.stub(adapter, 'getArticle').resolves({ '@id': articleId, hasFile: fileId, version: 'foo' });
            const ldpDeleteSpy = sandbox.spy(mockLdp, 'delete');
            const putArticleSpy = sandbox.stub(adapter, 'putArticle').callsFake((id, doc) => Promise.resolve(doc));

            const article = await adapter.deleteArticleFile(articleId);

            expect(ldpDeleteSpy.calledOnce, 'should delete file').to.equal(true);
            expect(ldpDeleteSpy.getCall(0).args[0], 'should delete correct file').to.equal(fileId);
            expect(putArticleSpy.calledOnce, 'should update article').to.equal(true);
            expect(article).to.have.property('hasFile', null);
            expect(article).to.have.property('inFile', null);
        });

        it('should not delete any files if .hasFile and .inFile is empty', async () => {
            const articleId = uuid();

            sandbox.stub(adapter, 'getArticle').resolves({ '@id': articleId });
            const ldpDeleteSpy = sandbox.spy(mockLdp, 'delete');
            const putArticleSpy = sandbox.stub(adapter, 'putArticle').callsFake((id, doc) => Promise.resolve(doc));


            const article = await adapter.deleteArticleFile(articleId);

            expect(ldpDeleteSpy.callCount, 'should not delete file').to.equal(0);
            expect(putArticleSpy.calledOnce, 'should update article').to.equal(true);
            expect(putArticleSpy.calledOnce).to.equal(true);
            expect(article).to.have.property('hasFile', null);
            expect(article).to.have.property('inFile', null);
        });

        it('should keep the file in .inFile and set the property to null', async () => {
            const articleId = uuid();
            const fileId = uuid();

            sandbox.stub(adapter, 'getArticle').resolves({ '@id': articleId, inFile: fileId, version: 'foo' });
            const ldpDeleteSpy = sandbox.spy(mockLdp, 'delete');
            const putArticleSpy = sandbox.stub(adapter, 'putArticle').callsFake((id, doc) => Promise.resolve(doc));

            const article = await adapter.deleteArticleFile(articleId);

            expect(ldpDeleteSpy.callCount, 'should not delete file').to.equal(0);
            expect(putArticleSpy.calledOnce, 'should update article').to.equal(true);
            expect(article).to.have.property('hasFile', null);
            expect(article).to.have.property('inFile', null);
        });
    });

    describe('getArticleFile()', () => {
        it('should give the hasFile from LDP', async () => {
            const articleId = uuid();
            const fileId = uuid();

            sandbox.stub(adapter, 'getArticle').resolves({ '@id': articleId, hasFile: fileId });
            const getFileSpy = sandbox.spy(mockFileAdapter, 'getFile');

            await adapter.getArticleFile(articleId, {} as any);

            expect(getFileSpy.calledOnce, 'should get file').to.equal(true);
            expect(getFileSpy.getCall(0).args[0], 'should get right file').to.equal(fileId);
        });

        it('should give the inFile from LDP', async () => {
            const articleId = uuid();
            const fileId = uuid();

            sandbox.stub(adapter, 'getArticle').resolves({ '@id': articleId, inFile: fileId });
            const getFileSpy = sandbox.spy(mockFileAdapter, 'getFile');

            await adapter.getArticleFile(articleId, {} as any);

            expect(getFileSpy.calledOnce, 'should get file').to.equal(true);
            expect(getFileSpy.getCall(0).args[0], 'should get right file').to.equal(fileId);
        });

        it('should send 404 if there is no file', async () => {
            const articleId = uuid();
            const res = { sendStatus: () => { }};

            const sendStatusSpy = sandbox.spy(res, 'sendStatus');
            sandbox.stub(adapter, 'getArticle').resolves({ '@id': articleId });

            await adapter.getArticleFile(articleId, res as any);

            expect(sendStatusSpy.calledOnce, 'should send status').to.equal(true);
            expect(sendStatusSpy.getCall(0).args[0], 'should send 404 status').to.equal(404);
        });
    });
});
