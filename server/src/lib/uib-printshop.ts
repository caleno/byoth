import { Readable } from 'stream';
import { Provider } from 'nconf';
import * as soap from 'soap';

import { PrintShopClient } from './printshop-client';
import { ThesisContainerAdapter } from './thesis-container-adapter';
import { ThesisFileAdapter } from './thesis-file-adapter';
import { Thesis, PrintFormats, DegreeTypes } from 'model/thesis';
import { Candidate } from 'model/candidate';
import { ByothEvent } from 'model/event';
import { FileStatus, ByothFile } from 'model/byoth-file';
import { Order, setOrderStatus, OrderTypes } from 'model/order';

function printFormatToString(format: PrintFormats) {
    switch (format) {
        case PrintFormats.A4:     return 'A4';
        case PrintFormats.B17x24: return '17x24';
        default:                  return '';
    }
}

function degreeTypeToString(degree: DegreeTypes) {
    switch (degree) {
        case DegreeTypes.DR_PHILOS: return 'Dr. Philos';
        case DegreeTypes.PHD:       return 'Ph.d.';
        default:                    return '';
    }
}

function langCode(lang: string) {
    if (lang === 'nn' || lang === 'nb') {
        return 'no';
    } else {
        return 'en';
    }
}

async function readable2Base64(input: Readable): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        const buffers = []
        input.on('data', chunk => buffers.push(chunk));
        input.on('end', () => resolve(Buffer.concat(buffers).toString('base64')));
        input.on('error', reject);
    });
}

function base642Readable(input: string): Readable {
    const stream = new Readable();

    stream._read = () => {}
    stream.push(Buffer.from(input, 'base64'));
    stream.push(null);

    return stream;
}

/**
 * The UiB specific printshop.
 *
 * Uses a remote SOAP service.
 * The SOAP client is initialized from a WSDL url in config.
 */
export class UiBPrintShop implements PrintShopClient {
    private wsdl: string;
    private client: any;
    private payloadHeader: any;

    constructor(config: Provider, private containerAdapter: ThesisContainerAdapter, private fileAdapter: ThesisFileAdapter) {
        config.required(['byoth:uib:printshop:wsdl', 'byoth:uib:printshop:username', 'byoth:uib:printshop:password']);
        this.wsdl = config.get('byoth:uib:printshop:wsdl');

        this.payloadHeader = {
            username: config.get('byoth:uib:printshop:username'),
            password: config.get('byoth:uib:printshop:password')
        };
    }

    async init() {
        try {
            this.client = await soap.createClientAsync(
                this.wsdl,
                {
                    returnFault: true
                }
            );
        } catch (error) {
            throw Error('Can not initialize UiB Printshop client: ' + error.message);
        }

        this.client.addHttpHeader('connection', 'keep-alive');

        this.client.on('soapError', (error, eid) => {
            console.error('SOAP Error:', error);
        });

        // for debugging, creates really large logs when files are included in response
        if (process.env.NODE_ENV !== 'production') {
            this.client.on('response', body => console.log(body.substring(0, 1000)));
            this.client.on('request', body => console.log(body.substring(0, 1000)));
        }
    }

    async getCoverPreview(containerId: string, type: OrderTypes) {
        const thesis = await this.containerAdapter.getProperty(containerId, 'thesis') as Thesis;
        const candidate = await this.containerAdapter.getProperty(containerId, 'candidate') as Candidate;
        const defense = await this.containerAdapter.getProperty(containerId, 'defense') as ByothEvent;

        let defenseDate = '';
        let submissionYear = '';

        if (defense.start) {
            const date = new Date(defense.start);
            defenseDate = date.toISOString().split('T')[0];
            submissionYear = date.getFullYear().toString();
        }

        const payload = {
            ...this.payloadHeader,
            referenceID: containerId,
            language: langCode(thesis.language),
            documentformat: printFormatToString(thesis.printFormat),
            author_name: candidate.givenName + ' ' + candidate.familyName,
            title: thesis.title,
            subtitle: thesis.subtitle,
            submissionYear: submissionYear,
            thesisDegree: degreeTypeToString(thesis.degreeType),
            isbn: thesis.isbn,
            defenseDate: defenseDate
        };

        const response = await this.client.GetCoverPreviewAsync(payload)
            .then(valArray => valArray[0].GetCoverPreviewResult);

        if (!response.cover_file_thumbnails || !response.cover_file_thumbnails.base64Binary ||
            !response.cover_file_pdf || !response.covercommittee_file_pdf) {
            throw Error('Cover not generated properly');
        }

        const result = {
            status: response.status,
            thumbnails: response.cover_file_thumbnails.base64Binary,
            pdf: type === OrderTypes.FINAL ? response.cover_file_pdf : response.covercommittee_file_pdf
        }

        return result;
    }

    async getContentPreview(containerId: string, type: OrderTypes) {
        const payload = {
            ...this.payloadHeader,
            referenceID: containerId
        }

        const response = await this.client.GetContentPreviewAsync(payload)
            .then(valArray => valArray[0].GetContentPreviewResult);

        if (!response.content_file_thumbnails || !response.content_file_thumbnails.base64Binary ||
            !response.content_file_pdf) {
            throw Error('Content preview not generated properly');
        }

        const result = {
            status: response.status,
            thumbnails: response.content_file_thumbnails.base64Binary,
            pdf: response.content_file_pdf,
            numPages: response.pageCount,
            numColorPages: response.colorpageCount
        }

        return result;
    }

    async clearContent(containerId: string) {
        const payload = {
            ...this.payloadHeader,
            referenceID: containerId
        }

        return this.client.DeleteContentAsync(payload);
    }

    async uploadContent(containerId: string, file: Readable, metadata: ByothFile, index: number): Promise<ByothFile> {
        const retValue = JSON.parse(JSON.stringify(metadata));

        return readable2Base64(file)
            .then(fileContent => Promise.resolve({
                    ...this.payloadHeader,
                    referenceID: containerId,
                    sortPriority: index + 1,
                    contentFile: fileContent
                }))
            .then(payload => this.client.UploadContentAsync(payload))
            .then(valArray => valArray && valArray[0].UploadContentResult)
            .then(response => {
                if (response) {
                    retValue.numPages = response.pageCount;
                    retValue.numColorPages = response.colorpageCount;
                } else {
                    throw Error('No file upload response from printshop');
                }

                return retValue;
            });
    }

    async validateContent(containerId: string, metadata: ByothFile, index: number) {
        const retValue = JSON.parse(JSON.stringify(metadata)) as ByothFile;

        const payload = {
            ...this.payloadHeader,
            referenceID: containerId,
            sortPriority: index + 1
        };

        return this.client.ValidateContentAsync(payload)
            .then(valArray => valArray[0] && valArray[0].ValidateContentResult)
            .then(response => {
                if (!response) {
                    throw Error('No file validation response from printshop');
                }
                return response;
            })
            .then(async response => {
                switch (response.print_quality) {
                    case 0:
                        retValue.status = FileStatus.ERROR;
                        break;
                    case 50:
                        retValue.status = FileStatus.WARNING;
                        break;
                    case 100:
                    default:
                        retValue.status = FileStatus.OK;
                }

                if (response.print_report_pdf) {
                    const report = await this.fileAdapter.postFileFromStream(containerId, {
                        stream: base642Readable(response.print_report_pdf),
                        mimetype: 'application/pdf',
                        originalname: 'validation-report-' + (index + 1) + '-' + metadata.filename
                    });

                    retValue.hasValidationReport = report['@id'];
                    report.isValidationReportFor = retValue['@id'];

                    await this.fileAdapter.updateFileMetadata(report['@id'], report);
                }

                if (response.corrected_pdf) {
                    const correction = await this.fileAdapter.postFileFromStream(containerId, {
                        stream: base642Readable(response.corrected_pdf),
                        mimetype: 'application/pdf',
                        originalname: 'corrected-' + metadata.filename
                    });

                    retValue[index].hasCorrection = correction['@id'];
                    correction.isCorrectionFor = retValue[index]['@id'];

                    await this.fileAdapter.updateFileMetadata(correction['@id'], correction);
                }

                return retValue;
            });
    }

    async getPrice(containerId: string, order: Order, type: OrderTypes) {
        const inputFiles = await this.fileAdapter.getInputFiles(containerId);

        // Our printshop can not calculate price if there are no files.
        if (!inputFiles.items || inputFiles.items.length === 0) {
            return 'No price yet';
        }

        const payload = {
            ...this.payloadHeader,
            referenceID: containerId,
            colorPrint: order.offer.printColor,
            tesbook: order.offer.printProofs,
            orderQuantity: order.offer.quantity,
            separatorPages: false
        }

        const response = await this.client.GetPriceAsync(payload)
            .then(valArray => valArray[0] && valArray[0].GetPriceResult);

        return response.price + ' NOK';
    }

    async submitOrder(containerId: string, order: Order, type: OrderTypes) {
        const retValue: Order = JSON.parse(JSON.stringify(order));

        const payload = {
            ...this.payloadHeader,
            referenceID: containerId,
            comment: order.delivery.description,
            contactperson: order.customer.name,
            contactemail: order.customer.email,
            contactphone: order.customer.telephone,
            delivery_location: order.delivery.address.name,
            delivery_address: order.delivery.address.streetAddress,
            delivery_postalcode: order.delivery.address.postalCode,
            delivery_postalplace: order.delivery.address.city,
            colorPrint: order.offer.printColor,
            orderQuantity: order.offer.quantity,
            committeeVersion: type === OrderTypes.EVALUATION,
            testSubmission: order.isTest,
            testbook: order.printProofs,
            separatorPages: false
        };

        if (process.env.NODE_ENV !== 'production') {
            payload.testSubmission = true;
        }

        if (order.delivery.proofAddress) {
            payload.testbook_delivery_location = order.delivery.proofAddress.name;
            payload.testbook_delivery_address = order.delivery.proofAddress.streetAddress;
            payload.testbook_delivery_postalcode = order.delivery.proofAddress.postalCode;
            payload.testbook_delivery_postalplace = order.delivery.proofAddress.city;
        }

        const response = await this.client.SubmitOrderAsync(payload)
            .then(valArray => valArray[0] && valArray[0].SubmitOrderResult)

        if (!response) {
            throw Error('No order submission response from printshop');
        }

        retValue.offer.price = response.price.toString();
        retValue.offer.currency = 'NOK';
        retValue.orderNumber = response.order_id.toString();

        setOrderStatus(retValue, response.status);

        return retValue;
    }

    async cancelOrder(containerId: string, orderId: string) {
        return 'cancelled';
    }
}
