import * as chai from 'chai';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';

import { MANDATORY_COPIES, OrderTypes } from 'model/order';
import { FileStatus } from 'model/byoth-file';
import { ThesisTypes, PrintFormats, DegreeTypes, PublishThesis } from 'model/thesis';

import { validateOrder } from './validate-order';

const expect = chai.expect;

describe('validation', () => {
    let sandbox: SinonSandbox;

    const id = 'foo';
    let thesis;
    let candidate;
    let defense;
    let inputFiles;
    let order;

    const mockContainer = {
        getProperty: (containerId, property) => Promise.resolve({}),
        getContainer: () => Promise.resolve({}),
    }

    const mockFileAdapter = {
        getInputFiles: () => Promise.resolve({})
    };

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        thesis = {
            title: 'foo bar',
            subtitle: '123',
            isbn: '123',
            publish: PublishThesis.WITH_EMBARGO,
            license: 'http://license.com',
            embargo: 6,
            thesisType: ThesisTypes.COLLECTION,
            degreeType: DegreeTypes.PHD,
            printFormat: PrintFormats.B17x24,
            abstract: {
                en: 'An abstract'
            }
        };
        candidate = { familyName: 'Foo', givenName: 'Bar' };
        defense = { start: '1999' };
        inputFiles = { items: [{ errors: [], status: FileStatus.OK }]};
        order = {
            offer: { quantity: MANDATORY_COPIES + 1 },
            coverPreviewAccepted: true,
            delivery: {
                address: {
                    name: 'Foo Bar',
                    postalCode: '1234',
                    city: 'Baz',
                    streetAddress: 'Foo-street 1'
                }
            },
            customer: {
                name: 'Foo Bar',
                telephone: '1234',
                email: 'foo@bar.com'
            }
        };
    });

    afterEach(() => {
        sandbox.restore();
    });

    function setupOrder() {
        sandbox.stub(mockContainer, 'getProperty')
        .withArgs(id, 'thesis').resolves(thesis)
        .withArgs(id, 'candidate').resolves(candidate)
        .withArgs(id, 'defense').resolves(defense)
        .withArgs(id, 'finalOrder').resolves(order)
        .withArgs(id, 'evaluationOrder').resolves(order);

        sandbox.stub(mockFileAdapter, 'getInputFiles')
        .withArgs(id).resolves(inputFiles);
    }

    // basically verifies that schema is correct
    describe('validateOrder()', () => {
        it('should accept complete final order', async () => {
            setupOrder();
            const result = await validateOrder(mockContainer as any, mockFileAdapter as any, id, OrderTypes.FINAL);
            expect(result).to.have.property('valid', true);
        });

        it('should reject an incomplete final order', async () => {
            thesis.title = undefined;
            setupOrder();

            const result = await validateOrder(mockContainer as any, mockFileAdapter as any, id, OrderTypes.FINAL);

            expect(result).to.have.property('valid', false);
            expect(result).to.have.property('errors').that.has.length(1);
            expect(result.errors[0]).to.have.property('dataPath', '.thesis');
            expect(result.errors[0]).to.have.property('params').that.has.property('missingProperty', 'title');
        });

        it('should report all errors', async () => {
            thesis.title = undefined;
            thesis.isbn = undefined;
            defense.start = undefined;
            candidate.familyName = undefined;
            setupOrder();

            const result = await validateOrder(mockContainer as any, mockFileAdapter as any, id, OrderTypes.FINAL);

            expect(result).to.have.property('valid', false);
            expect(result).to.have.property('errors').that.has.length(4);
            expect(result.errors[0]).to.have.property('dataPath', '.thesis');
            expect(result.errors[0]).to.have.property('params').that.has.property('missingProperty', 'title');
        });

        it('should accept complete evaluation order', async () => {
            setupOrder();
            const result = await validateOrder(mockContainer as any, mockFileAdapter as any, id, OrderTypes.EVALUATION);
            expect(result).to.have.property('valid', true);
        });

        it('should reject an incomplete evaluation order', async () => {
            thesis.title = undefined;
            setupOrder();

            const result = await validateOrder(mockContainer as any, mockFileAdapter as any, id, OrderTypes.EVALUATION);

            expect(result).to.have.property('valid', false);
            expect(result).to.have.property('errors').that.has.length(1);
            expect(result.errors[0]).to.have.property('dataPath', '.thesis');
            expect(result.errors[0]).to.have.property('params').that.has.property('missingProperty', 'title');
        });
    });
});
