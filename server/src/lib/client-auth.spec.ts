import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';
import { ClientAuth } from './client-auth';
import { getModel, getInternalModel } from 'model/index';
import { NotFoundError, AuthorizationError, DatabaseError } from './errors';

chai.use(chaiAsPromised);
const expect = chai.expect;

/* istanbul ignore next */
const mockLdp = {
    get: () => Promise.resolve({}),
    post: () => Promise.resolve({}),
    update: (arg1, arg2, doc) => Promise.resolve(doc),
    delete: () => Promise.resolve({}),
    exists: () => Promise.resolve(true),
    rootUrl: 'foo',
}

const mockConfig = {
    get: () => 'foo',
    required: () => {}
}

describe('ClientAuth', () => {
    let sandbox: SinonSandbox;
    const model = getModel(mockLdp.rootUrl);
    const internalModel = getInternalModel(mockLdp.rootUrl);
    let clientAuth;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        clientAuth = new ClientAuth(mockConfig as any, mockLdp as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    // Passport strategy will be tested in integration tests only

    describe('init()', () => {
        // there is not much logic here to test with mocked LDP, but will at least detect regression.
        it('creates required entities', async () => {
            sandbox.stub(mockLdp, 'exists').resolves(false);
            const post = sandbox.spy(mockLdp, 'post');
            const update = sandbox.spy(mockLdp, 'update');

            await clientAuth.init();

            const postCalls = post.getCalls();
            const updateCalls = update.getCalls();

            expect(postCalls.length).to.equal(5);

            expect(postCalls[0].args[0]).to.equal('');
            expect(postCalls[0].args[1]).to.have.property('expandedType', model.ldpSet.expandedType);
            expect(postCalls[0].args[2]).to.equal('authorization');

            expect(postCalls[1].args[0]).to.equal('');
            expect(postCalls[1].args[1]).to.have.property('expandedType', model.ldpSet.expandedType);
            expect(postCalls[1].args[2]).to.equal('client');

            const createAuthCalls = postCalls.filter(call => call.args[0] === 'authorization/');
            expect(updateCalls).to.have.length(3);
            expect(createAuthCalls).to.have.length(3);

            createAuthCalls.forEach(call => {
                expect(call.args[1]).to.have.property('expandedType', internalModel.authorization.expandedType);
                expect(call.args[2]).to.be.oneOf(['readExport', 'readProof', 'writeProof']);
            });

            const updatePaths = updateCalls.map(call => call.args[0]);
            expect(updatePaths).to.have.members(['authorization/readExport', 'authorization/readProof', 'authorization/writeProof']);
        });

        it('does nothing if already set up', async () => {
            sandbox.stub(mockLdp, 'exists').resolves(true);
            const post = sandbox.spy(mockLdp, 'post');
            const update = sandbox.spy(mockLdp, 'update');

            await clientAuth.init();

            expect(post.getCalls().length).to.equal(0);
            expect(update.getCalls().length).to.equal(0);
        });
    });

    describe('verify()', () => {
        it('should return client if payload has valid sub', (done) => {
            sandbox.stub(mockLdp, 'get').resolves({ '@id': 'foo', name: 'bar' })

            clientAuth.verify({ sub: 'client/foo' }, (err, client) => {
                expect(err).to.equal(null);
                expect(client).to.have.property('id', 'foo');
                expect(client).to.have.property('name', 'bar');
                done();
            });
        });

        it('should give AuthorizationError if payload has no sub', (done) => {
            sandbox.stub(mockLdp, 'get').resolves({ '@id': 'foo', name: 'bar' })

            clientAuth.verify({ }, (err, client) => {
                expect(err).to.be.instanceOf(AuthorizationError);
                done();
            });
        });

        it('should give AuthorizationError if sub does not start with \'client\'', (done) => {
            sandbox.stub(mockLdp, 'get').resolves({ '@id': 'foo', name: 'bar' })

            clientAuth.verify({ sub: 'foo' }, (err, client) => {
                expect(err).to.be.instanceOf(AuthorizationError);
                done();
            });
        });

        it('should give NotFoundError LDP returns null client', (done) => {
            sandbox.stub(mockLdp, 'get').resolves(null);

            clientAuth.verify({ sub: 'client/foo' }, (err, client) => {
                expect(err).to.be.instanceOf(NotFoundError);
                done();
            });
        });

        it('should pass on any other errors from LDP', (done) => {
            sandbox.stub(mockLdp, 'get').rejects(new DatabaseError('fooErr'));

            clientAuth.verify({ sub: 'client/foo' }, (err, client) => {
                expect(err).to.be.instanceof(DatabaseError);
                done();
            });
        })
    });

    describe('createClient()', () => {
        it('creates a new client with given name', async () => {
            const name = 'foo';
            const mockEmptyClient = { '@id': 'bar' };
            const mockClient = { ...mockEmptyClient, name: name };
            const post = sandbox.stub(mockLdp, 'post').resolves(mockEmptyClient);
            const update = sandbox.spy(mockLdp, 'update');

            const client = await clientAuth.createClient(name);

            expect(client).to.deep.equal(mockClient);
            expect(post.getCalls().length).to.equal(1);
            expect(update.getCalls().length).to.equal(1);
        });

        it('throws RangeError if client name missing', async () => {
            await expect(clientAuth.createClient(undefined)).to.be.eventually.rejectedWith(RangeError);
        });
    });

    describe('clientHasAuthorization()', () => {
        it('returns true if client has requested authorization', async () => {
            const auth = 'foo';
            const clientId = 'bar';
            sandbox.stub(mockLdp, 'get').resolves({agent: [clientId]});

            const hasAuth = await clientAuth.clientHasAuthorization(clientId, auth as any);

            expect(hasAuth).to.equal(true);
        });

        it('returns false if client does not have requested authorization', async () => {
            const auth = 'foo';
            const clientId = 'bar';
            sandbox.stub(mockLdp, 'get').resolves({agent: []});

            const hasAuth = await clientAuth.clientHasAuthorization(clientId, auth as any);

            expect(hasAuth).to.equal(false);
        });

        it('throws NotFoundError if client does not exist', async () => {
            const auth = 'foo';
            const clientId = 'bar';
            sandbox.stub(mockLdp, 'exists').withArgs([clientId]).resolves(false);

            await expect(clientAuth.clientHasAuthorization(clientId, auth as any))
            .to.be.eventually.rejectedWith(NotFoundError);
        });

        it('throws NotFoundError if authorization does not exist', async () => {
            const auth = 'foo';
            const clientId = 'bar';
            sandbox.stub(mockLdp, 'exists')
            .withArgs(clientId).resolves(true)
            .withArgs(auth).resolves(false);

            await expect(clientAuth.clientHasAuthorization(clientId, auth as any))
            .to.be.eventually.rejectedWith(NotFoundError);
        });
    });

    describe('modifyClientPermission()', () => {
        it('should grant permission if called with \'grant\'', async () => {
            const auth = 'foo';
            const clientId = 'bar';
            sandbox.stub(mockLdp, 'get').resolves({agent: []});
            const update = sandbox.spy(mockLdp, 'update');

            await clientAuth.modifyClientPermission(clientId, auth as any, 'grant');

            expect(update.getCall(0).args[2]).to.have.property('agent').that.includes(clientId);
        });

        it('should revoke permission if called with \'revoke\'', async () => {
            const auth = 'foo';
            const clientId = 'bar';
            sandbox.stub(mockLdp, 'get').resolves({agent: [clientId]});
            const update = sandbox.spy(mockLdp, 'update');

            await clientAuth.modifyClientPermission(clientId, auth as any, 'revoke');

            expect(update.getCall(0).args[2]).to.have.property('agent').that.not.includes(clientId);
        });

        it('throws RangeError if called with argument other than \'grant\' or \'revoke\'', async () => {
            const auth = 'foo';
            const clientId = 'bar';

            await expect(clientAuth.modifyClientPermission(clientId, auth as any, 'foo' as any))
            .to.be.eventually.rejectedWith(RangeError);
        });

        it('throws NotFoundError if client does not exist', async () => {
            const auth = 'foo';
            const clientId = 'bar';

            sandbox.stub(mockLdp, 'exists')
            .withArgs(clientId).resolves(false);

            await expect(clientAuth.modifyClientPermission(clientId, auth as any, 'grant'))
            .to.be.eventually.rejectedWith(NotFoundError);
        });

        it('throws NotFoundError if authorization does not exist', async () => {
            const auth = 'foo';
            const clientId = 'bar';

            sandbox.stub(mockLdp, 'exists')
            .withArgs(clientId).resolves(true)
            .withArgs(auth).resolves(false);

            await expect(clientAuth.modifyClientPermission(clientId, auth as any, 'grant'))
            .to.be.eventually.rejectedWith(NotFoundError);
        });
    });
});
