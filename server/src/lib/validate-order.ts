import * as Ajv from 'ajv';
import { ErrorObject } from 'ajv';
import { JSONSchema7 } from 'json-schema';

import { ThesisContainerAdapter } from './thesis-container-adapter';
import { ThesisFileAdapter } from './thesis-file-adapter';

import { FINAL_ORDER_SCHEMA, EVALUATION_ORDER_SCHEMA } from 'model/schemas';
import { CompactedDocument } from 'model/ld-model';
import { OrderTypes } from 'model/order';

const ajv = new Ajv({allErrors: true});

export interface ValidationReport {
    valid: boolean,
    errors: ErrorObject[],
    document: CompactedDocument
}

export function validateDocument(document: CompactedDocument, schema: JSONSchema7): ValidationReport {
    const valid = ajv.validate(schema, document) as boolean;
    return {
        valid: valid,
        errors: ajv.errors,
        document: document
    };
}

async function validateContainer(
        containerAdapter: ThesisContainerAdapter,
        fileAdapter: ThesisFileAdapter,
        containerId: string,
        properties: string[],
        schema: JSONSchema7): Promise<ValidationReport> {
    const container = await containerAdapter.getContainer(containerId);

    return Promise.all(
        properties.map(property => {
            if (property === 'inputFiles') {
                return fileAdapter.getInputFiles(containerId)
                    .then(document => container['inputFiles'] = document);
            } else {
                return containerAdapter.getProperty(containerId, property as any)
                    .then(document => container[property] = document)
            }
        })
    ).then(() => validateDocument(container, schema));
}

export async function validateOrder(containerAdapter: ThesisContainerAdapter,
                                    fileAdapter: ThesisFileAdapter,
                                    containerId: string,
                                    type: OrderTypes): Promise<ValidationReport> {
    switch (type) {
        case OrderTypes.FINAL:
            return validateContainer(
                containerAdapter,
                fileAdapter,
                containerId,
                ['thesis', 'candidate', 'defense', 'inputFiles', 'finalOrder'],
                FINAL_ORDER_SCHEMA
            );
        case OrderTypes.EVALUATION:
            return validateContainer(
                containerAdapter,
                fileAdapter,
                containerId,
                ['thesis', 'candidate', 'inputFiles', 'evaluationOrder'],
                EVALUATION_ORDER_SCHEMA
            );
        default:
            throw Error('Unknown order type');
    }
}
