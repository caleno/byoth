import axios from 'axios';
import { Provider } from 'nconf';

import { DatabaseError } from './errors';
import { ThesisContainerAdapter } from './thesis-container-adapter';
import { Thesis } from 'model/thesis';

/**
 * Connects to ISBN service (GrOIN: https://gitlab.com/ubbdev/groin).
 *
 * Used to assign ISBN numbers to a thesis by requesting them from an external service
 * and storing them in the thesis document.
 */
export class ISBNHandler {
    private config: { protocol: string, host: string, port: string, token: string };
    private url: string;
    private headers;

    /** Constructor */
    constructor(config: Provider, private containerAdapter: ThesisContainerAdapter) {
        config.required(['byoth:isbn:host', 'byoth:isbn:port', 'byoth:isbn:protocol', 'byoth:isbn:token']);
        this.config = config.get('byoth:isbn');
        this.url = this.config.protocol + '://' + this.config.host + ':' + this.config.port;
        this.headers = { authorization: 'Bearer ' + this.config.token };
    }

    private async reserveNumber(): Promise<string> {
        return axios.post(this.url + '/api/isbn/reserve', null, { headers: this.headers } )
            .catch(err => {
                throw new DatabaseError('Problems connecting to ISBN server: ' + err.response.statusText, err.response.status);
            })
            .then(res => res.data.isbn);
    }

    private async releaseNumber(number: string) {
        return axios.post(this.url + '/api/isbn/release/' + number, null, { headers: this.headers })
            .catch(err => {
                 throw new DatabaseError('Problems connecting to ISBN server: ' + err.response.statusText, err.response.status);
             });
    }

    /**
     * Assign an ISBN  to a given thesis container.
     *
     * Returns the unchanged thesis if the thesis already has an ISBN.
     *
     * @param containerId Thesis container id.
     * @returns A `Thesis` document with the new ISBN.
     */
    async assignISBN(containerId: string): Promise<Thesis> {
        const thesis = await this.containerAdapter.getProperty(containerId, 'thesis') as Thesis;

        if (thesis.isbn) {
            return thesis;
        }

        thesis.isbn = await this.reserveNumber();
        return await this.containerAdapter.putProperty(containerId, 'thesis', thesis) as Thesis;
    }

    /**
     * Unassign the ISBN from a given thesis container.
     *
     * Returns the unchanged thesis if the thesis doesn't have an ISBN.
     *
     * @param containerId Thesis container id.
     * @returns A `Thesis` document without ISBN.
     */
    async unassignISBN(containerId: string): Promise<Thesis> {
        const thesis = await this.containerAdapter.getProperty(containerId, 'thesis') as Thesis;

        if (!thesis.isbn) {
            return thesis;
        }

        await this.releaseNumber(thesis.isbn);

        delete thesis.isbn;

        return await this.containerAdapter.putProperty(containerId, 'thesis', thesis) as Thesis;
    }
}
