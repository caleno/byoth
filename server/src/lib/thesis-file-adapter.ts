import { Response, Request } from 'express';
import * as Redlock from 'redlock';
import { Readable } from 'stream';

import { ThesisContainerAdapter } from './thesis-container-adapter';
import { LdpClient, Ldp } from './ldp-client';
import { LockError, InputError, NotFoundError } from './errors';

import { ByothModel } from 'model/index';
import { ByothFile } from 'model/byoth-file';
import { LdpSet } from 'model/ldp-set';
import { replaceOrderedSetItem, appendOrderedSet, deleteOrderedSetMember, getOrderedSetArray, setOrderedSetArray } from 'model/ordered-set';

export class ThesisFileAdapter {
    private model: ByothModel;

    constructor(private containerAdapter: ThesisContainerAdapter, private ldp: LdpClient, private redlock: Redlock) {
        this.model = containerAdapter.model;
    }

    private async getFileMetadataArray(fileIds: string[]): Promise<ByothFile[]> {
        const tasks = fileIds.map(fileId => this.ldp.getFileMetadata(fileId, this.model.file));

        return Promise.all(tasks)
        .then(res => res.map(file => <ByothFile>file));
    }

    /**
     * Get metadata for all files in the thesis container.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param req An Express request (used to create better error messages).
     * @returns An array of compacted JSON-LD documents describing the the files.
     * @throws Errors from the LDP.
     */
    async getInputFiles(containerId: string): Promise<LdpSet> {
        const inputFiles = await this.containerAdapter.getProperty(containerId, 'inputFiles').then(res => <LdpSet>res);

        inputFiles.items = await this.getFileMetadataArray(getOrderedSetArray(inputFiles))
        return inputFiles;
    }

    /**
     * Define order of files by passing ordered array of ids.
     */
    async setInputFileOrder(containerId: string, orderedFileIds: string[]): Promise<string[]> {
        let lock;

        try {
            lock = await this.redlock.lock('inputFiles:' + containerId, 2000);
        } catch (err) {
            throw new LockError(err.message);
        }

        try {
            const inputFiles = await this.containerAdapter.getProperty(containerId, 'inputFiles');

            const files = getOrderedSetArray(inputFiles);

            // verify that all file Ids are valid
            orderedFileIds.forEach(id => {
                if (files.indexOf(id) === -1) {
                    lock.unlock().catch(console.error); // don't really need to await
                    throw new InputError(`File ID '${id}' not known`, 422);
                }
            });

            setOrderedSetArray(inputFiles, orderedFileIds);

            const result = await this.containerAdapter.putProperty(containerId, 'inputFiles', inputFiles)
            .then(res => getOrderedSetArray(res));

            return result;
        } finally {
            await lock.unlock().catch(console.error);
        }
    }

    /**
     * Upload a new input file to the thesis container from an express request.
     *
     * Only upload files here that are intended to be part of the actual thesis.
     * The uploaded file will be appended to the ordered list of input files.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param req An Express request containing the file to be uploaded.
     * @returns File metadata as a compacted JSON-LD document.
     */
    async postInputFileFromRequest(containerId: string, req: Request): Promise<ByothFile> {
        let lock;

        try {
            lock = await this.redlock.lock('inputFiles:' + containerId, 2000);
        } catch (err) {
            throw new LockError(err.message);
        }

        try {
            const doc = await this.postFileFromRequest(containerId, req);

            const inputFiles = await this.containerAdapter.getProperty(containerId, 'inputFiles');

            appendOrderedSet(inputFiles, doc['@id']);
            await this.containerAdapter.putProperty(containerId, 'inputFiles', inputFiles);

            return doc;
        } finally {
            await lock.unlock().catch(console.error);
        }
    }

    /**
     * Upload a new file to the thesis container from an express request.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param req An Express request containing the file to be uploaded.
     * @returns File metadata as a compacted JSON-LD document.
     */
    async postFileFromRequest(containerId: string, req: Request): Promise<ByothFile> {
        return this.ldp.postFileFromRequest(containerId + '/files', this.model.file, req)
        .then(res => <ByothFile>res);
    }

    /**
     * Upload a new file to the thesis container from an Readable stream.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param file An `Ldp.File` container the file stream, mimetype and original filename.
     * @returns File metadata as a compacted JSON-LD document.
     */
    async postFileFromStream(containerId: string, file: Ldp.File): Promise<ByothFile> {
        return this.ldp.postFileFromStream(containerId + '/files', this.model.file, file)
        .then(res => <ByothFile>res);
    }

    /**
     * Upload a new file to the thesis container from a Buffer.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param file An object thatn contains the buffer, mimetype and original filename.
     * @returns File metadata as a compacted JSON-LD document.
     */
    async postFileFromBuffer(containerId: string, file: { buffer: Buffer, mimetype: string, originalname: string }) {
        const stream = new Readable();
        stream._read = () => {};
        stream.push(file.buffer);
        stream.push(null);

        return this.postFileFromStream(containerId, { stream: stream, mimetype: file.mimetype, originalname: file.originalname });
    }

    /**
     * Remove any corrections or validation reports associated with a file
     *
     * Deletes the files and nulls the corresponding fields in the parent file metadata.
     *
     * @param fileId the file to update
     * @returns The updated file metadata as a compacted JSON-LD document.
     */
    async clearAssociatedFiles(fileId: string): Promise<ByothFile> {
        const fileMetadata = await this.getFileMetadata(fileId);

        if (!(fileMetadata.hasCorrection || fileMetadata.hasValidationReport)) {
            return fileMetadata;
        }

        if (fileMetadata.hasCorrection) {
            await this.deleteFile(fileMetadata.hasCorrection);
        }

        if (fileMetadata.hasValidationReport) {
            await this.deleteFile(fileMetadata.hasValidationReport);
        }

        fileMetadata.hasCorrection = fileMetadata.hasValidationReport = null;

        return await this.ldp.updateFileMetadata(fileId, this.model.file, fileMetadata) as ByothFile;
    }

    /**
     * Replace a file in the input file set.
     *
     * Deletes the old file and any associated files.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param fileId The file to replace.
     * @param req An Express request containing the file to be uploaded.
     * @returns File metadata as a compacted JSON-LD document.
     * @throws Errors from the LDP and RangeError if fileId is not an input file.
     */
    async replaceInputFile(containerId: string, fileId: string, req: Request): Promise<ByothFile> {
        let lock;

        try {
            lock = await this.redlock.lock('inputFiles:' + containerId, 2000);
        } catch (err) {
            throw new LockError(err.message);
        }

        try {
            await this.clearAssociatedFiles(fileId);

            const newFileMetadata = await this.ldp.postFileFromRequest(containerId + '/files', this.model.file, req) as ByothFile;
            const inputFileSet = await this.containerAdapter.getProperty(containerId, 'inputFiles') as LdpSet;

            try {
                replaceOrderedSetItem(inputFileSet, fileId, newFileMetadata['@id']);
            } catch (RangeError) {
                throw new NotFoundError('File is not in input file list', req);
            }

            await this.containerAdapter.putProperty(containerId, 'inputFiles', inputFileSet);
            await this.deleteFile(fileId);

            return newFileMetadata;
        } finally {
            await lock.unlock().catch(console.error);
        }
    }

    /**
     * Get a file.
     *
     * The file is piped through the requested response.
     *
     * @param fileId The ID of the file relative to the root container.
     * @param res An Express respsonse to use to send the file
     */
    async getFile(fileId: string, res: Response) {
        const fileResponse = await this.ldp.getFile(fileId);

        res.status(200)
        .type(fileResponse.headers['content-type'])
        .set('content-disposition', fileResponse.headers['content-disposition'])
        .set('content-length', fileResponse.headers['content-length']);

        fileResponse.data.pipe(res);
    }

    /**
     * Get file as a stream.
     *
     * @param fileId The ID of the file relative to the root container.
     * @returns A readable stream of the file.
     */
    async getFileStream(fileId: string): Promise<Readable> {
         const fileResponse = await this.ldp.getFile(fileId);
         return fileResponse.data;
    }

    /**
     * Get file as a buffer.
     *
     * @param fileId The ID of the file relative to the root container.
     * @returns A file buffer.
     */
    async getFileBuffer(fileId: string): Promise<Buffer> {
        const stream = await this.getFileStream(fileId);

        return new Promise<Buffer>((resolve, reject) => {
            const buffers = []
            stream.on('data', chunk => buffers.push(chunk));
            stream.on('end', () => resolve(Buffer.concat(buffers)));
            stream.on('error', reject);
        });
    }

    /**
     * Get file metadata
     *
     * @param fileId The ID of the file relative to the root container.
     * @returns File metadata as a compacted JSON-LD document.
     */
    async getFileMetadata(fileId: string): Promise<ByothFile> {
        return this.ldp.getFileMetadata(fileId, this.model.file).then(res => <ByothFile>res);
    }

    /**
     * Update file metadata
     *
     * @param fileId The ID of the file relative to the root container.
     * @param document JSON-LD document containing the new metadata.
     * @returns File metadata as a compacted JSON-LD document.
     */
    async updateFileMetadata(fileId: string, document: ByothFile): Promise<ByothFile> {
        return this.ldp.updateFileMetadata(fileId, this.model.file, document).then(res => <ByothFile>res);
    }

    /**
     * Set file name
     *
     * Can not change file extension.
     *
     * @param fileId The ID of the file relative to the root container.
     * @param filename The new file name.
     * @returns File metadata as a compacted JSON-LD document.
     * @throws InputError if filename invalide
     */
    async setFileName(fileId: string, filename: string): Promise<ByothFile> {
        if (!filename) {
            throw new InputError('Filename not defined');
        }

        const file = await this.getFileMetadata(fileId);

        const newExtension = filename.split('.').pop();
        const oldExtension = file.filename.split('.').pop();

        if (newExtension !== oldExtension) {
            throw new InputError('Can not change file extension', 422);
        }

        file.filename = filename;

        return this.updateFileMetadata(fileId, file);
    }

    /**
     * Delete a file.
     *
     * If it is an input file, i.e. in the ordered input file set, use deleteInputFile instead
     * @param fileId The ID of the file relative to the root container.
     * @returns The status code returned by the LDP (204 for sucess).
     * @throws Errors from the LDP.
     */
    async deleteFile(fileId: string): Promise<number> {
        return await this.ldp.delete(fileId);
    }

    /**
     * Delete an input file.
     *
     * In addition to deleting the file it is deleted from the list of input files.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param fileId The ID of the file relative to the root container.
     * @returns The status code returned by the LDP (204 for sucess).
     * @throws Errors from the LDP.
     */
    async deleteInputFile(containerId: string, fileId: string): Promise<number> {
        let lock;

        try {
            lock = await this.redlock.lock('inputFiles:' + containerId, 2000);
        } catch (err) {
            throw new LockError(err.message);
        }

        try {
            await this.clearAssociatedFiles(fileId);

            const inputFiles = await this.getInputFiles(containerId);

            deleteOrderedSetMember(inputFiles, fileId);

            const status = await this.ldp.delete(fileId);

            await this.containerAdapter.putProperty(containerId, 'inputFiles', inputFiles);

            return Promise.resolve(status);
        } finally {
            await lock.unlock().catch(console.error);
        }
    }
}
