import { RedisClient } from 'redis';
import { Provider } from 'nconf';
import axios from 'axios';
import { Institution, Place, Faculty, Department, Room, Area, Building } from './institution';
import * as async from 'async';
import { parseString } from 'xml2js';
import { Iconv } from 'iconv';

import { DatabaseError } from './errors';

const CACHE_TTL = 604800; // (s), 1 week

const FACULTY_KEY_PREFIX = 'uib:faculty:';
const DEPARTMENT_KEY_PREFIX = 'uib:department:';
const PLACE_KEY_PREFIX = 'uib:sted:';

function isFaculty(place: Place) {
    return !!place.id.match(/(0[1-9]|[1-9]0|[1-9]{2})0000/);
}

function isDepartment(place: Place) {
    return !!place.id.match(/(0[1-9]|[1-9]0|[1-9]{2}){2}00/);
}

function getOrgUnit(groupid: string): string {
    if (!groupid.match(/^fc:org:uib\.no:unit:\d{6}$/)) {
        throw RangeError('Invalid group id');
    }

    return groupid.replace('fc:org:uib\.no:unit:', '');
}

const iconv = new Iconv('ISO-8859-1', 'UTF-8');

async function toUtfString(val: Buffer): Promise<string> {
    return iconv.convert(val).toString();
}

async function parseXml(xml: string): Promise<any> {
    return new Promise<any>((resolve, reject) =>
        parseString(xml, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        })
    );
}

/**
 * UiB implementation of the interface Institution.
 *
 * The class has a bunch of code very specific to UiB.
 * The faculty/department/group list is fetched as one big blob, and is hence cached in Redis.
 * The Areas/Buildings/Rooms has a more sophisticated API, and is hence not cached.
 *
 * The groupids are assumed to come from Dataporten, and are then matched to the six digit codes that describe faculties, etc.
 */
export class UiBInstitution implements Institution {
    private sebraUrl: string;
    private roomsUrl: string;
    private sebraToken: string;

    constructor(config: Provider, private redis: RedisClient) {
        config.required([
            'byoth:uib:sebra:url'
        ]);

        this.sebraUrl = config.get('byoth:uib:sebra:url');
        this.sebraToken = config.get('byoth:uib:sebra:token');
        this.roomsUrl = config.get('byoth:uib:rooms:url') + config.get('byoth:uib:rooms:token') + '/ws/room/2.0';

    }

    private async cachePlaces(): Promise<any> {
        const placesXml = await axios.get(
                this.sebraUrl + 'places',
                {
                    responseType: 'arraybuffer',
                    headers: { authorization: 'Bearer ' + this.sebraToken }
                })
            .then(response => toUtfString(response.data));

        const places: Place[] = [];

        await parseXml(placesXml)
        .then(data => data.places && data.places.place)
        .then(sebraPlaces => {
            let promise: Promise<void> = Promise.resolve(null);
            sebraPlaces.forEach(shortPlace => {
                if (shortPlace.code && shortPlace.code[0].match(/00$/)) {
                    promise = promise.then(() =>
                        // axios 0.18 has problems with non utf-8 text, therefore getting arraybuffer and converting to utf manually
                        axios.get(
                            this.sebraUrl + 'place?code=' + shortPlace.code,
                            {
                                responseType: 'arraybuffer',
                                headers: { authorization: 'Bearer ' + this.sebraToken }
                            })
                        .then(response => toUtfString(response.data))
                        .then(xml => parseXml(xml))
                        .then(data => data.place)
                        .then(place => {
                            const nbName = place.name && place.name[0] || null;
                            const enName = place.name_en && place.name_en[0] || nbName;
                            places.push({
                                id: shortPlace.code[0],
                                name: {en: enName, nb: nbName || enName },
                                address: {
                                    postalCode: place.postnr && place.postnr[0],
                                    city: place.postcode && place.postcode[0],
                                    streetAddress: place.address && place.address[0],
                                    name: nbName || enName
                                }
                            });
                        })
                    );
                }
            });

            return promise;
        });

        const faculties: Faculty[] = places.filter(place => isFaculty(place));

        const departments: Department[] = places.filter(place => isDepartment(place));

        // perhaps wasteful, but not a lot of data anyway
        return new Promise((resolve, reject) => {
            async.parallel([
                done => async.each(places,
                    (place, placeDone) => {
                        const key = PLACE_KEY_PREFIX + place.id;
                        this.redis.multi()
                        .set(key, JSON.stringify(place))
                        .expire(key, CACHE_TTL)
                        .exec(placeDone);
                    },
                    done),
                done => async.each(faculties,
                    (faculty, facultyDone) => {
                        const key = FACULTY_KEY_PREFIX + faculty.id;
                        this.redis.multi()
                        .set(key, JSON.stringify(faculty))
                        .expire(key, CACHE_TTL)
                        .exec(facultyDone);
                    },
                    done),
                done => async.each(departments,
                    (dep, depDone) => {
                        const key = DEPARTMENT_KEY_PREFIX + dep.id;
                        this.redis.multi()
                        .set(key, JSON.stringify(dep))
                        .expire(key, CACHE_TTL)
                        .exec(depDone);
                    },
                    done),
                ], (err: Error) => {
                    if (err) {
                        return reject(new DatabaseError(err.message));
                    }

                    resolve();
                }
            );
        });
    }

    private async cacheRooms(): Promise<void> {
        return null;
    }

    // crude
    private hasCache(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.redis.keys(FACULTY_KEY_PREFIX + '*', (err: Error, keys) => {
                if (err) {
                    return reject(new DatabaseError(err.message));
                }

                if (keys.length > 0) {
                    return resolve(true);
                }

                resolve(false);
            })
        });
    }

    private async cacheIfNecessary(): Promise<void> {
        if (! await this.hasCache()) {
            await this.init()
        }
    }

    private async get(key: string): Promise<any> {
        await this.cacheIfNecessary();

        return new Promise<Faculty>((resolve, reject) =>
            this.redis.get(key, (err: Error, result) => {
                if (err) {
                    return reject(new DatabaseError(err.message));
                }

                resolve(JSON.parse(result));
            })
        );
    }

    private async getKeys(keyPattern: string): Promise<Place[]> {
        await this.cacheIfNecessary();

        return new Promise<Place[]>((resolve, reject) => {
            const values: Place[] = [];
            this.redis.keys(keyPattern, (keysErr: Error, keys) => {
                if (keysErr) {
                    return reject(new DatabaseError(keysErr.message));
                }
                async.eachSeries(keys, (key, done) => {
                    this.redis.get(key, (getErr: Error, result) => {
                        if (getErr) {
                            return done(new DatabaseError(getErr.message));
                        }
                        values.push(JSON.parse(result))
                        done();
                    })
                }, err => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(values);
                });
            });
        });
    }

    /** Initialize cache, can be called at server startup, otherwise it will be called on the first request where necessary. */
    async init(): Promise<void> {
        await this.cachePlaces();
        await this.cacheRooms();
    }

    async getFaculties(): Promise<Faculty[]> {
        return this.getKeys(FACULTY_KEY_PREFIX + '*');
    }

    /**
     * Get the faculty a specific group ID corresponds to.
     *
     * @param groupid The group ID, has to match `/^fc:org:uib\.no:unit:\d{6}$/`.
     */
    async getFaculty(groupId: string): Promise<Faculty> {
        const orgunit = getOrgUnit(groupId);
        return this.get(FACULTY_KEY_PREFIX + orgunit.substring(0, 2) + '0000');
    }

    /**
     * Get the departmens at a given faculty
     *
     * @param facultyId A six digit string.
     */
    async getDepartments(facultyId: string): Promise<Department[]> {
        return this.getKeys(DEPARTMENT_KEY_PREFIX + facultyId.substring(0, 2) + '*');
    }

    /**
     * Get the department a specific group ID corresponds to
     *
     * @param groupid The group ID, has to match `/^fc:org:uib\.no:unit:\d{6}$/`.
     */
    async getDepartment(groupId: string): Promise<Department> {
        const orgunit = getOrgUnit(groupId);

        return this.get(DEPARTMENT_KEY_PREFIX + orgunit.substring(0, 4) + '00');
    }

    /**
     * Get the place instance for a given ID.
     *
     * @param placeId A six digit string.
     */
    async getPlace(placeId: string): Promise<Place> {
        return this.get(PLACE_KEY_PREFIX + placeId);
    }

    async getAreas(): Promise<Area[]> {
        return axios.get(this.roomsUrl + '/areas.php').then(res => <Area[]>res.data.data);
    }

    async getBuildings(areaId: string): Promise<Building[]> {
        return axios.get(this.roomsUrl + '/buildings.php', { params: { id: areaId } }).then(res => <Building[]>res.data.data);
    }

    async getRooms(buildingId: string): Promise<Room[]> {
        return axios.get(this.roomsUrl + '/rooms.php', { params: { id: buildingId } }).then(res => <Room[]>res.data.data);
    }
}
