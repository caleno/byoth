import { Response, Request } from 'express';
import { ThesisContainerAdapter } from './thesis-container-adapter';
import { LdpClient } from './ldp-client';
import { ThesisFileAdapter } from './thesis-file-adapter';

const shortid = require('shortid');

import { ByothFile } from 'model/byoth-file';
import { LdpSet } from 'model/ldp-set';
import { Article } from 'model/article';

export class ThesisArticleAdapter {
    private model;

    constructor(private containerAdapter: ThesisContainerAdapter, private ldp: LdpClient, private fileAdapter: ThesisFileAdapter) {
        this.model = containerAdapter.model;
    }

    /**
     * Get an article
     *
     * @param articleId The ID of the article relative to the root container.
     * @returns The article.
     * @throws Errors from the LDP.
     */
    async getArticle(articleId: string): Promise<Article> {
        return this.ldp.get(articleId, this.model.article);
    }

    /**
     * Get all articles in a thesis container
     *
     * @param containerId: The ID of the container relative to the root container.
     * @param req An Express request (used to create better error messages).
     * @returns An array of `Article`.
     * @throws Errors from the LDP.
     */
    async getArticles(containerId: string, req?: Request): Promise<LdpSet> {
        const articles = await this.containerAdapter.getProperty(containerId, 'articles', req).then(res => <LdpSet>res);

        const tasks = articles.items.map(item => this.ldp.get( item['@id'], this.model.article));

        articles.items = await Promise.all(tasks)
        .then(res => res.map(article => <Article>article));

        return articles;
    }

    /**
     * Create a new article under a container
     *
     * @param containerId The ID of the container relative to the root container.
     * @returns A compacted JSON-LD document representing the created article.
     * @throws Errors from the LDP.
     */
    async postArticle(containerId: string): Promise<Article> {
        if (! await this.containerAdapter.existsProperty(containerId, 'articles')) {
            await this.containerAdapter.addProperty(containerId, 'articles', this.model.articles);
        }

        return this.ldp.post(containerId + '/articles', this.model.article, shortid.generate())
        .then(res => <Article>res);
    }

    /**
     * Replace an article
     *
     * Only values present in the Article model frame will be kept.
     *
     * @param articleId The ID of the article relative to the root container.
     * @param data Document to replace it with.
     * @returns A compacted JSON-LD document representing the updated article.
     * @throws Errors from the LDP.
     */
    async putArticle(articleId: string, data: Article): Promise<Article> {
        return this.ldp.update(articleId, this.model.article, data)
        .then(res => <Article>res);
    }

    /**
     * Delete an article
     *
     * @param articleId The ID of the article relative to the root container.
     * @returns The status code returned by the LDP (204 for sucess).
     * @throws Errors from the LDP.
     */
    async deleteArticle(articleId: string): Promise<number> {
        const article = await this.getArticle(articleId);

        if (article.hasFile) {
            await this.deleteArticleFile(articleId);
        }

        return this.ldp.delete(articleId);
    }

    /**
     * Upload a file associated with an article
     *
     * This will be accessible as the proeprty .hasFile, not to be confused with .inFile,
     * which should be used for files uploaded elsewhere. This distinction allows us
     * to delete article files when the article .hasFile but not when it is .inFile.
     *
     * If the article already has a file, it will be replaced.
     * If the article has the .inFile relation, it will be nulled.
     *
     * @param articleId articleId The ID of the article relative to the root container.
     * @param req An Express request containing the file to be uploaded.
     * @returns A compacted JSON-LD document describing the file.
     * @throws Errors from the LDP.
     */
    async uploadArticleFile(containerId: string, articleId: string, req: Request): Promise<ByothFile> {
        const article = await this.getArticle(articleId);

        if (article.hasFile) {
            await this.deleteArticleFile(articleId);
        }

        const file = await this.fileAdapter.postFileFromRequest(containerId, req) as ByothFile;

        article.hasFile = file['@id'];
        article.inFile = null;

        await this.putArticle(articleId, article);

        return file;
    }

    /**
     * Delete an article file.
     *
     * If the article has a .hasFile it will be deleted and unlinked,
     * if it has .inFile the file will only be unlinked.
     *
     * @param articleId articleId The ID of the article relative to the root container.
     * @returns A compacted JSON-LD document representing the updated article.
     * @throws Errors from the LDP.
     */
    async deleteArticleFile(articleId: string): Promise<Article> {
        const article = await this.getArticle(articleId);

        if (article.hasFile) {
            await this.ldp.delete(article.hasFile)
        }

        article.inFile = null;
        article.hasFile = null;

        return this.putArticle(articleId, article);
    }

    /**
     * Get the article file.
     *
     * Returns either .hasFile or .inFile, depending on what's available.
     *
     * Pipes the file to the provided Express response.
     *
     * @param articleId The ID of the article relative to the root container.
     * @param res An Express respsonse to use to send the file.
     * @throws Errors from the LDP.
     */
    async getArticleFile(articleId: string, res: Response) {
        const article = await this.getArticle(articleId);

        if (article.hasFile) {
            this.fileAdapter.getFile(article.hasFile, res);
        } else if (article.inFile) {
            this.fileAdapter.getFile(article.inFile, res);
        } else {
            res.sendStatus(404);
        }
    }
}
