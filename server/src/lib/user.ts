import { Request } from 'express';
import { ClientProperties } from 'model/client';

interface User {
    name: string,
    email: string,
}

/** Internal information about a user */
export interface InternalUser extends User {
    groupid: string;
    id: string;
}

/** An external agent (i.e. non-human client) */
export type ExternalAgent = ClientProperties;

/** Helper function for handling requests, users and clients */
export const userFromRequest = {
    getUser: (req: Request): User => {
        const user = req.user.data;
        return {
            name: user.name,
            email: user.email,
        };
    },
    getUserId: (req: Request): string => {
        return req.user.data.id;
    },
    getUserGroupId: (req: Request): string => {
        return req.user.data.groupid;
    },
    getInternalUser: (req: Request): InternalUser => {
        return req.user.data;
    },
    getExternalAgent(req: Request): ExternalAgent {
        return req.user.data;
    }
}
