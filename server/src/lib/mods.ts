import { ThesisContainerAdapter } from './thesis-container-adapter';
import { Thesis } from 'model/thesis';
import { Candidate } from 'model/candidate';
import { Defense } from 'model/defense';

const context = {
    modsrdf: 'http://www.loc.gov/mods/rdf/v1#',
    madsrdf: 'http://www.loc.gov/mads/rdf/v1#',
    relators: 'http://id.loc.gov/vocabulary/relators/',
    identifiers: 'http://id.loc.gov/vocabulary/identifiers/',
    languages: 'http://id.loc.gov/vocabulary/languages/',
    ri: 'http://id.loc.gov/ontologies/RecordInfo/',
    rdfs: 'http://www.w3.org/2000/01/rdf-schema#',
    xsd: 'http://www.w3.org/2001/XMLSchema#'
};

// This function is experimental and the results are probably not valid mods, at the moment.
export async function mapToMods(adapter: ThesisContainerAdapter, containerId: string) {
    const defense = await adapter.getProperty(containerId, 'defense').then(doc => <Defense>doc)
    const thesis = await adapter.getProperty(containerId, 'thesis').then(doc => <Thesis>doc);
    const candidate = await adapter.getProperty(containerId, 'candidate').then(doc => <Candidate>doc);

    let issuance = 'unknown';
    if (thesis.thesisType === 'byoth:monographThesis') {
        issuance = 'monographic';
    } else if (thesis.thesisType === 'byoth:collectionThesis') {
        issuance = 'collection';
    }

    const mods = {
        '@context': context,
        '@id': containerId.split('/').pop(),
        'modsrdf:name': {
            '@id': '_:b1',
            '@type': 'madsrdf:Name',
            'rdfs:label': candidate.familyName + ', ' + candidate.givenName,
        },
        'relators:cre': { '@id': '_:b1' },
        'modsrdf:titlePrincipal': {
            '@id': '_:b2',
            '@type': 'madsrdf:Title',
            'rdfs:label': thesis.title + ': ' + thesis.subtitle
        },
        'modsrdf:placeOfOrigin': [
            {
                '@id': '_:b3',
                '@type': 'madsrdf:Country',
                'rdfs:label': 'Norge'
            },
            {
                '@id': '_:b4',
                '@type': 'madsrdf:City',
                'rdfs:label': 'Bergen'
            }
        ],
        'modsrdf:publisher': 'Universitetet i Bergen',
        'modsrdf:dateIssued': defense.start,
        'modsrdf:issuance': issuance,
        'modsrdf:form': 'electronic',
        'identifier:isbn': thesis.isbn,
        'modsrdf:languageOfResource': thesis.language,
        'modsrdf:genre': {
            '@id': '_:b5',
            '@type': 'madsrdf:GenreForm',
            'rdfs:label': 'doctoral thesis'
        }
    }

    return mods;
}
