import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';
import { InputError } from '../lib/errors';
import { ThesisFileAdapter } from './thesis-file-adapter';

import { getOrderedSetArray } from 'model/index';

chai.use(chaiAsPromised);
const expect = chai.expect;

const mockLock = {
    lock: () => Promise.resolve(mockLock),
    unlock: () => Promise.resolve()
}

/* istanbul ignore next */
const mockLdp = {
    get: () => Promise.resolve({}),
    getFile: () => Promise.resolve({}),
    getFileMetadata: () => Promise.resolve({}),
    post: () => Promise.resolve({}),
    postFileFromRequest: () => Promise.resolve({}),
    postFileFromStream: () => Promise.resolve({}),
    update: () => Promise.resolve({}),
    updateFileMetadata: () => Promise.resolve({}),
    delete: () => Promise.resolve({}),
    exists: () => Promise.resolve(true),
    storeSnapshot: () => Promise.resolve({}),
    snapshotLabelExists: () => Promise.resolve(false)
}

/* istanbul ignore next */
const mockContainer = {
    getProperty: () => Promise.resolve({}),
    putProperty: () => Promise.resolve({}),
    model: { file: {} }
}

describe('ThesisFileAdapter', () => {
    let sandbox: SinonSandbox;
    let adapter: ThesisFileAdapter;

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        adapter = new ThesisFileAdapter(<any>mockContainer, <any>mockLdp, <any>mockLock);
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('getInputFiles()', () => {
        it('should return empty array if file container has nothing', async () => {
            const files = await adapter.getInputFiles('bar');

            expect(files.items).to.be.instanceof(Array);
            expect(files.items.length).to.equal(0);
        });

        it('should return all files in file container as an array of file metadata', async () => {
            const id1 = 'foo1';
            const id2 = 'foo2';

            const file1 = { '@id': id1 };
            const file2 = { '@id': id2 };

            sandbox.stub(mockContainer, 'getProperty')
            .resolves(
                {
                    first: 'proxy1',
                    last: 'proxy2',
                    proxies: [
                        {
                            '@id': 'proxy1',
                            '@type': 'ore:Proxy',
                            next: 'proxy2',
                            proxyFor: id1
                        },
                        {
                            '@id': 'proxy2',
                            '@type': 'ore:Proxy',
                            prev: 'proxy1',
                            proxyFor: id2
                        },
                    ]
                }
            );

            const ldpget = sandbox.stub(mockLdp, 'getFileMetadata')
            ldpget.onCall(0).resolves(file1);
            ldpget.onCall(1).resolves(file2);

            const files = await adapter.getInputFiles('foo');

            expect(files.items).to.include(file1);
            expect(files.items).to.include(file2);
        });
    });

    describe('setInputFileOrder()', () => {
        it('should set order of files to provided array', async () => {
            const data = {
                '@id': 'foo',
                first: 'proxy1',
                last: 'proxy2',
                proxies: [
                    {
                        '@id': 'proxy1',
                        '@type': 'ore:Proxy',
                        next: 'proxy2',
                        proxyFor: 'f1'
                    },
                    {
                        '@id': 'proxy2',
                        '@type': 'ore:Proxy',
                        prev: 'proxy1',
                        proxyFor: 'f2'
                    },
                ]
            }

            sandbox.stub(mockContainer, 'getProperty').resolves(data);

            const spy = sandbox.spy(mockContainer, 'putProperty');

            await adapter.setInputFileOrder('foo', [ 'f2', 'f1' ]);
            expect(spy.calledOnce).to.equal(true);
            expect(getOrderedSetArray(spy.getCall(0).args[2])).to.deep.equal([ 'f2', 'f1' ]);
        });

        it('should fail if file array contains unknown file id', async () => {
            sandbox.stub(mockContainer, 'getProperty').resolves({ '@id': 'foo', first: null, last: null, proxies: [] });

            await expect(adapter.setInputFileOrder('foo', [ 'f1' ])).to.eventually.be.rejectedWith(InputError);
        });
    });

    describe('postInputFileFromRequest()', () => {
        it('should post a file to ldp', async () => {
            const postSpy = sandbox.spy(mockLdp, 'postFileFromRequest');
            sandbox.stub(mockContainer, 'getProperty').resolves({ '@id': 'foo', proxies: []});

            await adapter.postInputFileFromRequest('foo', {} as any);

            expect(postSpy.calledOnce, 'should call ldp postFile').to.equal(true);
            expect(postSpy.getCall(0).args[0], 'should use correct url').to.equal('foo/files');
            expect(postSpy.getCall(0).args[1], 'should use model.file').to.equal(mockContainer.model.file);
            expect(postSpy.getCall(0).args[2], 'should generate uuid').to.match(/\w+/); // should we be more specific?
        });

        it('should create inputFile-list if missing', async () => {
            const postSpy = sandbox.spy(mockLdp, 'postFileFromRequest');
            const propertySpy = sandbox.spy(mockContainer, 'putProperty');
            const document = { '@id': 'foo' };
            sandbox.stub(mockContainer, 'getProperty').resolves(document);

            await adapter.postInputFileFromRequest('foo', { } as any);

            expect(postSpy.calledOnce, 'should call ldp postFile').to.equal(true);

            expect(propertySpy.calledOnce).to.equal(true);
            expect(propertySpy.getCall(0).args[2]['@id']).to.equal(document['@id']);
        });
    });

    describe('postFileFromRequest()', () => {
        // this test is only there for coverage, basically
        it('should pass request to ldp client', async () => {
            const postSpy = sandbox.spy(mockLdp, 'postFileFromRequest');

            await adapter.postFileFromRequest('foo', { } as any);

            expect(postSpy.calledOnce).to.equal(true);
        });
    });

    describe('postFileFromStream()', () => {
        // This test is only there for coverage, basically.
        // Integration tests do the actual testing
        it('should pass stream to ldp client', async () => {
            const postSpy = sandbox.spy(mockLdp, 'postFileFromStream');

            await adapter.postFileFromStream('foo', {} as any);

            expect(postSpy.calledOnce).to.equal(true);
        });
    });

    describe('getFile()', () => {
        it('should get a file from ldp', async () => {
            const stub = sandbox.stub(mockLdp, 'getFile').resolves({headers: {}, data: { pipe: () => {}}});

            const mockResponse = { status: () => mockResponse, type: () => mockResponse, set: () => mockResponse};

            await adapter.getFile('foo', mockResponse as any);

            expect(stub.calledOnce, 'should call ldp getFile').to.equal(true);
            expect(stub.getCall(0).args[0], 'should use correct url').to.equal('foo');
        });
    });

    describe('getFileStream()', () => {
        it('should get a file stream from ldp', async () => {
            const stub = sandbox.stub(mockLdp, 'getFile').resolves({headers: {}, data: { foo: 'bar' } } as any);

            const file: any = await adapter.getFileStream('foo');

            expect(stub.calledOnce, 'should call ldp getFile').to.equal(true);
            expect(stub.getCall(0).args[0], 'should use correct url').to.equal('foo');
            expect(file.foo, 'should return file stream from LDP').to.equal('bar');
        });
    });

    describe('getFileMedata()', () => {
        it('should get a file stream from ldp', async () => {
            const stub = sandbox.stub(mockLdp, 'getFileMetadata').resolves({foo: 'bar'} as any);

            const data: any = await adapter.getFileMetadata('foo');

            expect(stub.calledOnce, 'should call ldp getFileMetadata').to.equal(true);
            expect(stub.getCall(0).args[0], 'should use correct url').to.equal('foo');
            expect(data.foo, 'should return data from LDP').to.equal('bar');
        });
    });

    describe('updateFileMedata()', () => {
        it('should get a file stream from ldp', async () => {
            const stub = sandbox.stub(mockLdp, 'updateFileMetadata').resolves({foo: 'bar'} as any);

            const data: any = await adapter.updateFileMetadata('foo', {foo2: 'bar'} as any);

            expect(stub.calledOnce, 'should call ldp updateFileMetadata').to.equal(true);
            expect(stub.getCall(0).args[0], 'should use correct url').to.equal('foo');

            // In reality we should get the document we sent back (provided it's correct),
            // but use different values here for testing purposes
            expect(data.foo, 'should return data from LDP').to.equal('bar');
        });
    });

    describe('setFileName()', () => {
        it('should change filename of file', async () => {
            const oldFilename = 'foo.bar';
            const newFilename = 'baz.bar';
            const fileId = 'foobar';

            sandbox.stub(adapter, 'getFileMetadata').resolves({filename: oldFilename});
            const updateSpy = sandbox.stub(adapter, 'updateFileMetadata').resolves({filename: newFilename});

            await adapter.setFileName(fileId, newFilename);

            expect(updateSpy.getCall(0).args[1]).to.have.property('filename', newFilename);
        });

        it('throws InputError if filename empty', async () => {
            await expect(adapter.setFileName('foo', null)).to.be.eventually.rejectedWith(InputError);
        });

        it('throws InputError if file extension changed', async () => {
            const oldFilename = 'foo.bar';
            const newFilename = 'foo.baz';

            sandbox.stub(adapter, 'getFileMetadata').resolves({filename: oldFilename});

            await expect(adapter.setFileName('foo', newFilename)).to.be.eventually.rejectedWith(InputError);
        });
    });

    describe('deleteFile()', () => {
        it('should delete a file from the LDP', async () => {
            const spy = sandbox.spy(mockLdp, 'delete');
            await adapter.deleteFile('foo');

            expect(spy.calledOnce, 'shuold call ldp delete').to.equal(true);
            expect(spy.getCall(0).args[0], 'shuold use correct url').to.equal('foo');
        });
    });

    describe('deleteInputFile()', () => {
        it('should delete a file from the LDP, and from the input file list', async () => {
            const deleteSpy = sandbox.spy(mockLdp, 'delete');
            const clearSpy = sandbox.spy(adapter, 'clearAssociatedFiles');
            sandbox.stub(adapter, 'getInputFiles')
            .resolves({
                items: [{'@id': 'i1'}, {'@id': 'i2'}, {'@id': 'i3'}],
                proxies:
                    [
                        { '@id': 'p1', '@type': 'ore:Proxy', proxyFor: 'i1', prev: null, next: 'p2' },
                        { '@id': 'p2', '@type': 'ore:Proxy', proxyFor: 'i2', prev: 'p1', next: 'p3' },
                        { '@id': 'p3', '@type': 'ore:Proxy', proxyFor: 'i3', prev: 'p2', next: null },
                    ],
                first: 'p1',
                last: 'p3'
            });

            await adapter.deleteInputFile('foo', 'i2');

            expect(deleteSpy.calledOnce, 'should call ldp delete').to.equal(true);
            expect(deleteSpy.getCall(0).args[0], 'should use correct url').to.equal('i2');
            expect(clearSpy.calledOnce, 'should clear associated files').to.equal(true);
        });
    });

    describe('clearAssociatedFiles()', () => {
        it('should delete any associated corrections and validation reports', async () => {
            const deleteSpy = sandbox.spy(mockLdp, 'delete');
            const updateSpy = sandbox.spy(mockLdp, 'updateFileMetadata');

            sandbox.stub(adapter, 'getFileMetadata')
            .resolves({'@id': 'i1', hasCorrection: 'c1', hasValidationReport: 'v1'});

            await adapter.clearAssociatedFiles('i1');
            expect(deleteSpy.calledTwice, 'should delete two files').to.equal(true);
            expect(updateSpy.calledOnce, 'should update file metadata').to.equal(true);
            expect(updateSpy.getCall(0).args[2]['@id']).to.equal('i1');
            expect(!updateSpy.getCall(0).args[2].hasCorrection);
            expect(!updateSpy.getCall(0).args[2].hasValidationReport);
        });

        it('should do nothing if there are no associated files', async () => {
            const deleteSpy = sandbox.spy(mockLdp, 'delete');
            const updateSpy = sandbox.spy(mockLdp, 'updateFileMetadata');

            sandbox.stub(adapter, 'getFileMetadata')
            .resolves({'@id': 'i1' });

            await adapter.clearAssociatedFiles('i1');

            expect(deleteSpy.called, 'should not delete any files').to.equal(false);
            expect(updateSpy.called, 'should not udate file metadata').to.equal(false);
        });
    });
});
