import { Readable } from 'stream'
import { spawn } from 'child_process';
import * as byline from 'byline';

import { ByothFile, FileStatus } from 'model/byoth-file';

/**
 * A simple GhostScript-based PDF-analyzer.
 * Calculates page count, guesses color-page count and reports any serious errors.
 */
export async function analyzePdf(file: Readable, metadata: ByothFile): Promise<ByothFile> {
    return new Promise<ByothFile>((resolve, reject) => {
        const output: ByothFile = JSON.parse(JSON.stringify(metadata));
        output.numPages = output.numColorPages = 0;
        output.status = FileStatus.OK;

        let gs;

        try {
            gs = spawn('gs', ['-q', '-o', '-', '-sDEVICE=inkcov', '-'], { stdio: 'pipe'});
        } catch (err) {
            return reject(err);
        }

        const lineStream = byline(gs.stdout);

        file.pipe(gs.stdin);

        // if file causes GhostScript to crash
        gs.stdin.on('error', reject);

        lineStream.on('data', data => {
            const line = data.toString();

            const error = line.match(/^(\s|\*)*Error: (.*)/);
            if (error) {
                if (!output.errors) {
                    output.errors = [];
                }
                output.errors.push(error[2]);
                output.status = FileStatus.ERROR;
            }

            const inkcov = line.match(/^\s*(\d\.\d+\s+){4}CMYK/);
            if (inkcov) {
                output.numPages++;
                if (!line.match(/^\s*(0\.0+\s+){3}/)) {
                    output.numColorPages++;
                }
            }
        });

        lineStream.on('error', reject);

        lineStream.on('end', () => resolve(output));
    });
}

// for testing
if (require.main === module) {
    analyzePdf(process.stdin as any, {} as any).then(console.log, console.error);
}
