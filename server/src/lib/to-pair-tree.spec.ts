import * as chai from 'chai';

import { InputError } from './errors';
import { toPairTree, toFedoraPairTree } from './to-pair-tree';

const expect = chai.expect;

describe('Container adapter helper functions', () => {
    describe('toPairTree()', () => {
        it('should choke on too short strings', () => {
            expect(() => toPairTree('1', 2)).to.throw(InputError);
            expect(() => toPairTree('123', 2, 2)).to.throw(InputError);
        });

        it('should convert string to correct pair tree string', () => {
            expect(toPairTree('1', 0)).to.equal('1');
            expect(toPairTree('1', 1, 1)).to.equal('1/1');
            expect(toPairTree('12', 1, 1)).to.equal('1/12');
            expect(toPairTree('1234', 2, 2)).to.equal('12/34/1234');
        });
    });

    describe('toFedoraPairTree()', () => {
        it('should choke on too short strings', () => {
            expect(() => toFedoraPairTree('123456')).to.throw(InputError);
        });

        it('should convert string to correct pair tree string', () => {
            expect(toFedoraPairTree('123456789')).to.equal('12/34/56/78/123456789');
        });
    });
});
