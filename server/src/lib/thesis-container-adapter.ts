import { Request } from 'express';
import * as Redlock from 'redlock';

import { LdpClient } from './ldp-client';
import { Institution } from './institution';

import { InternalUser, userFromRequest } from './user';
import { toPairTree } from './to-pair-tree';

import {
    createModelDocument,
    ThesisContainer,
    User,
    Candidate,
    Department,
    LdpSet,
    CompactedDocument,
    DocumentModel,
    getModel, getInternalModel,
    ByothModel, ByothInternalModel
} from 'model/index';

import { NotFoundError, LockError, Conflict } from './errors';

type Properties =  'articles' | 'candidate' | 'defense' | 'department' |  'evaluationOrder' |
    'files' |  'finalOrder' | 'inputFiles' | 'thesis' | 'trialLecture';


/**
 * Class for CRUD operations on a thesis container. Uses an `LdpClient` for storage.
 *
 * The class keeps a root container uri, all IDs (`containerId`, `articleId`, `fileId`)
 * should be relative to this.
 *
 * All complex data will be returned as JSON-LD compacted documents (`CompactedDocument`),
 * or derivatives there of. Likewise, updates will expect data as `CompactedDocument`.
 * The root thesis container is represented by `ThesisContainer`.
 *
 * The current model only supports one container per user.
 */
export class ThesisContainerAdapter {
    public model: ByothModel;
    private internalModel: ByothInternalModel;

    /**
     * @param ldp An LdpClient to be used for storage.
     * @param root The url to the root LDP container to be used, e.g. `"http://localhost:8080/rest/phd/"`.
     *        Needed for setting the right `@base` in the JSON-LD contexts.
     */
    constructor(private ldp: LdpClient, private redlock: Redlock, private institution: Institution) {
        this.model = getModel(ldp.rootUrl);
        this.internalModel = getInternalModel(ldp.rootUrl);
    }

    private get containerSlug() {
        return 'container';
    }

    private get personSlug() {
        return 'person';
    }

    /**
     * Setup the associated LDP to have the containers needed to store thesis-containers.
     */
    async setupLDP() {
        if (! await this.ldp.exists(this.containerSlug)) {
            await this.ldp.post('', this.model.thesis, this.containerSlug);
        }

        if (! await this.ldp.exists(this.personSlug)) {
            await this.ldp.post('', this.internalModel.user, this.personSlug);
        }
    }

    /**
     *  Convert userId to pair-tree: 123456 -> 12/34/123456.
     */
    private userToPairTree(id: string): string {
        return toPairTree(id, 2, 2);
    }

    /**
     * Get the URI for a user in the given LDP.
     *
     * @param user The user.
     * @returns The user URI.
     */
    personId(user: InternalUser) {
        return this.personSlug + '/' + this.userToPairTree(user.id);
    }

    /**
     * Get the owner for a given thesis container.
     *
     * @param containerId The container ID relative to the root container.
     * @returns The owner URI.
     */
    async getContainerOwner(containerId: string): Promise<string> {
        return this.ldp.get(containerId, this.model.thesisContainer)
        .then(doc =>  doc.creator);
    }

    /**
     * Get the ID of a given user's thesis container
     *
     * @param req An Express request containing user session data
     * @returns The ID of the user's container or null, if the user has none.
     * @throws Errors from the LDP
     */
    async getUserContainerId(req: Request): Promise<string|null> {
        const personId = this.personId(userFromRequest.getInternalUser(req));

        try {
            const person = await this.ldp.get(personId, this.internalModel.user)
            return person.owns;
        } catch (err) {
            if (err.status === 404) {
                return null;
            } else {
                throw err;
            }
        }
    }

    /**
     * Create new user based on user session information.
     *
     * Simply returns existing user, if one exists.
     *
     * @param user User session information.
     * @returns The created user as a JSON-LD document.
     */
    private async putUser(user: InternalUser): Promise<User> {
        const personId = this.personId(user);
        const personExists = await this.ldp.exists(personId);

        let promise;

        if (!personExists) {
            promise = this.ldp.post(this.personSlug, this.internalModel.user, this.userToPairTree(user.id));
        } else {
            promise = this.ldp.get(personId, this.internalModel.user);
        }

        return promise.then(res => <User>res);
    }

    /**
     * Create an empty thesis container.
     *
     * @param userDocument User information needed to register ownership.
     * @returns A JSON-LD document describing the newly created thesis container.
     */
    private async createEmptyContainer(userDocument: User): Promise<ThesisContainer> {
        const container = await this.ldp.post(this.containerSlug, this.model.thesisContainer);
        const containerId = container['@id'];
        const candidate = await this.ldp.post(containerId, this.model.candidate, 'candidate');
        const department = await this.ldp.post(containerId, this.model.department, 'department');
        await this.ldp.post(containerId, this.model.ldpSet, 'files');

        const containerDocument = <ThesisContainer>createModelDocument(this.model.thesisContainer, containerId, {
            creator: userDocument['@id'],
            candidate: {'@id': candidate['@id']},
            department: {'@id': department['@id']},
        });

        return this.ldp.update(containerId, this.model.thesisContainer, containerDocument)
        .then(res => <ThesisContainer>res);
    }

    /**
     * Register the owner of a thesis container.
     *
     * @param container A document describing the the thesis container.
     * @param user User session information.
     * @param userDocument A JSON-LD document describing the user in the LDP.
     */
    private async registerContainerOwner(container: ThesisContainer, user: InternalUser, userDocument: User) {
        userDocument.name = user.name;
        userDocument.email = user.email;
        userDocument.owns = container['@id'];

        return this.ldp.update(this.personId(user), this.internalModel.user, userDocument);
    }

    /**
     * Copy user session info to a `Candidate`.
     *
     * @param container The thesis container containing the candidate.
     * @param user User session information.
     */
    private async registerUserCandidate(container: ThesisContainer, user: InternalUser): Promise<Candidate> {
        const candidate = createModelDocument(this.model.candidate, container.candidate['@id'], {
            email: user.email
        }) as Candidate;

        if (user.name) {
            const names = user.name.split(' ');
            candidate.familyName = names.pop();
            candidate.givenName = names.join(' ');
        }

        return this.putProperty(container['@id'], 'candidate', candidate)
        .then(res => <Candidate>res);
    }

    /**
     * Get user department from session info and populate a `Department`.
     *
     * The department is taken from the user's group and generated using the institution interface.
     * If the user has no group information stored, the department will be empty.
     *
     * @param container The thesis container containing the department.
     * @param user User session information.
     * @returns The updated department. Null if user has no groupid,
     *   or the groupid was not recognized by the institution interface.
     */
    private async registerDepartment(container: ThesisContainer, user: InternalUser): Promise<Department> {
        const orgunit = user.groupid;

        if (orgunit) {
            let dep;
            let fac;

            try {
                dep = await this.institution.getDepartment(orgunit);
                fac = await this.institution.getFaculty(orgunit);
            } catch (err) {
                // this is not essential functionality, so we should fail silently here
                return null;
            }

            const department = createModelDocument(this.model.department, container.department['@id'], {
                departmentName: dep.name,
                departmentId: dep.id,
                facultyName: fac.name,
                facultyId: fac.id,
            });

            return this.putProperty(container['@id'], 'department', department)
            .then(res => <Department>res);
        } else {
            return null;
        }
    }

    /**
     * Create a fresh container for a user.
     *
     * If the user already has a thesis container, the existing one will be returned.
     * Ideally one should check first if the user already has a container.
     *
     * @param req An Express request containing user session data.
     * @returns The created thesis container.
     * @throws Errors from the LDP
     */
    async createContainer(req: Request): Promise<ThesisContainer> {
        const user = userFromRequest.getInternalUser(req);

        const userDocument = await this.putUser(user);

        let containerId = userDocument.owns;

        if (!containerId) {
            const container = await this.createEmptyContainer(userDocument);

            containerId = container['@id'];

            await this.registerContainerOwner(container, user, userDocument);

            await this.registerUserCandidate(container, user);

            await this.registerDepartment(container, user);
        }

        return this.getContainer(containerId);
    }

    /**
     * Get a given thesis container.
     *
     * @param containerId The ID of the container relative to the root container.
     * @returns The thesis container as a compacted JSON-LD document.
     * @throws Errors from the LDP
     */
    async getContainer(containerId: string): Promise<ThesisContainer> {
        return this.ldp.get(containerId, this.model.thesisContainer)
        .then(res => <ThesisContainer>res);
    }

    /**
     * Update a given container.
     *
     * @param containerid The ID of the container relative to the root container.
     * @param container A JSON-LD document describing the updated thesis container.
     * @returns The updated thesis container as a compacted JSON-LD document.
     * @throws Errors from the LDP
     */
    async putContainer(containerId: string, container: ThesisContainer) {
        return this.ldp.update(containerId, this.model.thesisContainer, container)
        .then(res => <ThesisContainer>res);
    }

    /**
     * Delete a given thesis container.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param req An Express request containing user session data.
     * @returns The status code returned by the LDP (204 for success).
     * @throws Errors from the LDP
     */
    async deleteContainer(containerId: string, req: Request): Promise<number> {
        const personId = this.personId(userFromRequest.getInternalUser(req));

        await this.ldp.update(personId, this.internalModel.user,
            createModelDocument(this.internalModel.user, personId, userFromRequest.getUser(req)));

        return this.ldp.delete(containerId);
    }

    /**
     * Get array of all thesis container IDs.
     *
     * @returns Array of IDs relative to the root container.
     * @throws Errors from the LDP.
     */
    async getAllContainerIds(): Promise<string[]> {
        return this.ldp.get(this.containerSlug, this.model.ldpSet)
        .then(res => <LdpSet>res)
        .then(res => res.items.map(item => item['@id']));
    }

    /**
     * Get the document model for a given property.
     *
     * @param property The property.
     * @returns the document model, or null if the property is not recognized.
     */
    private getModel(property: Properties): DocumentModel|null {
        return this.model[property];
    }

    /**
     * Check if a property exists on the container
     *
     * @param containerId The ID of the container relative to the root container.
     * @param property The name of the property.
     * @returns Wether the property exists.
     * @throws Any errors from the LDP
     */
    async existsProperty(containerId: string, property: Properties): Promise<boolean> {
        return this.ldp.exists(containerId + '/' + property);
    }

    /**
     * Add a property to the container by both creating the property and adding the mention in the container.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param property The name of the property.
     * @param model The document model to use to create the property.
     * @returns A compacted JSON-LD document describing the property.
     * @throws Any errors from the LDP
     */
    async addProperty(containerId: string, property: Properties, model: DocumentModel): Promise<CompactedDocument> {
        const document = await this.ldp.post(containerId, model, property);

        const container = await this.getContainer(containerId);
        container[property] = { '@id': document['@id'] };
        await this.putContainer(containerId, container);

        return document;
    }

    /**
     * Get a property (or subcontainer) of the thesis container.
     *
     * If the property is valid but has not been created yet, an empty one will be created and returned.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param property The name of the property
     * @param req An Express request (used to create better error messages).
     * @returns A compacted JSON-LD document describing the property.
     * @throws Any errors from the LDP and `NotFoundError` if the property is not recognized
     */
    async getProperty(containerId: string, property: Properties, req?: Request): Promise<CompactedDocument> {
        // need a lock to avoid conflict with putProperty (if addProperty is called)
        let lock;
        try {
            lock = await this.redlock.lock('property:' + containerId + ':' + property, 2000);
        } catch (err) {
            throw new LockError(err.message);
        }

        try {
            const model = this.getModel(property);

            let document: CompactedDocument;

            if (model) {
                if (! await this.ldp.exists(containerId + '/' + property)) {
                    document = await this.addProperty(containerId, property, model);
                } else {
                    document = await this.ldp.get(containerId + '/' + property, model);
                }
            } else {
                await lock.unlock().catch(console.error);
                throw new NotFoundError(`Property '${property}' unknown`, req);
            }

            return document;
        } finally {
            await lock.unlock().catch(console.error);
        }
    }

    /**
     * Update a property of the thesis container.
     *
     * The provided JSON-LD document must be compatible with the model and the request,
     * i.e. it must have a correct ID and type and be frameable to the corresponding model.
     *
     * @param containerId: The ID of the container relative to the root container.
     * @param property The name of the property
     * @param data A JSON-LD document with updated values.
     * @param req An Express request (used to create better error messages).
     * @returns A compacted JSON-LD document describing the updated property.
     * @throws Any errors from the LDP and `NotFoundError` if the property is not recognized
     */
    async putProperty(containerId: string, property: Properties, data: CompactedDocument, req?: Request): Promise<CompactedDocument> {
        // because getProperty can also potentially write to a property, if it is unitialized, we need this lock
        let lock;
        try {
            lock = await this.redlock.lock('property:' + containerId + ':' + property, 2000);
        } catch (err) {
            throw new LockError(err.message);
        }

        try {
            // We have to use LDP PATCH and not PUT (because things get complicated with properties that are managed by the LDP).
            // We therefore first check if the property exists and add an empty one if necessary, before the update.
            const model = this.getModel(property);

            let document: CompactedDocument;

            if (model) {
                // first create the property if it doesn't exist
                if (! await this.ldp.exists(containerId + '/' + property)) {
                    await this.addProperty(containerId, property, model);
                }

                document = await this.ldp.update(containerId + '/' + property, model, data);
            } else {
                await lock.unlock().catch(console.error);
                throw new NotFoundError(`Property '${property}' unknown`, req);
            }

            return document;
        } finally {
            await lock.unlock().catch(console.error);
        }
    }

    /**
     * Store a snapshot of thesis container.
     *
     * The adapter does not provide a method to restore snapshots, the snapshots are only
     * intended for archiving.
     *
     * @param containerId The ID of the container relative to the root container.
     * @param label Label for snapshot.
     * @returns The status code returned by the LDP (201 for success).
     * @throws Errors from the LDP.
     */
    async storeSnapshot(containerId: string, label: string): Promise<number> {
        const exists = await this.ldp.snapshotLabelExists(containerId, label);

        if (exists) {
            throw new Conflict('Version \'' + label + '\' already exists');
        }

        return this.ldp.storeSnapshot(containerId, label);
    }
}
