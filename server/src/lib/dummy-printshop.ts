import { Readable } from 'stream';
import { PrintShopClient, PreviewResponse } from './printshop-client';
import { Thesis } from 'model/thesis';
import { Candidate } from 'model/candidate';
import { ByothFile } from 'model/byoth-file';
import { Order, setOrderStatus, OrderTypes } from 'model/order';
import { ThesisContainerAdapter } from './thesis-container-adapter';
import * as PDFDocument from 'pdfkit';

import { analyzePdf } from './analyze-pdf';

/**
 * Printshop that can be used for testing.
 * Both for automated tests or to evaluate the application
 * one's actual printshop has been integrated.
 *
 * Uses pdfkit and ghostscript.
 */
export class DummyPrintShop implements PrintShopClient {
    constructor(private containerAdapter: ThesisContainerAdapter) {}

    private generateFrontCover(pdf: PDFDocument, thesis: Thesis, candidate: Candidate, type: OrderTypes): void {
        pdf.fontSize(20).fillColor('grey').text('FRONT');
        pdf.fillColor('black');
        pdf.fontSize(20).text(candidate.givenName + ' ' + candidate.familyName, 100, 200, {align: 'center', width: 400});
        pdf.fontSize(30).text(thesis.title, 100, 400, {align: 'center', width: 400});
        pdf.fontSize(20).text(thesis.subtitle, 100, 450, {align: 'center', width: 400});

        if (type === OrderTypes.EVALUATION) {
            pdf.fontSize(20).fillColor('grey').text('EVALUATION COPY', 100, 550, {align: 'center', width: 400});
        }
        pdf.restore();
    }

    private generateBackCover(pdf: PDFDocument, thesis: Thesis, candidate: Candidate): void {
        pdf.fontSize(20).fillColor('grey').text('BACK');
        pdf.fillColor('black');
        pdf.fontSize(10).text('ISBN: ' + thesis.isbn, 100, 650, {align: 'center'});
        pdf.restore();
    }

    private generateSpine(pdf: PDFDocument, thesis: Thesis, candidate: Candidate): void {
        pdf.fontSize(20).fillColor('grey').text('SPINE');
        pdf.fillColor('black');
        pdf.save().rotate(90, {origin: [320, 60]});
        pdf.fontSize(30).text(candidate.familyName + ':' + thesis.title, 320, 60, {width: 700});
        pdf.restore();
    }

    private generateTitlePage(pdf: PDFDocument, thesis: Thesis, candidate: Candidate): void {
        pdf.fontSize(20).fillColor('grey').text('TITLE PAGE');
        pdf.fillColor('black');
        pdf.fontSize(25).text(thesis.title, 100, 150, {align: 'center'});
        pdf.fontSize(25).text(thesis.subtitle, 100, 250, {align: 'center'});
        pdf.fontSize(25).text(candidate.givenName + ' ' + candidate.familyName, 100, 400, {align: 'center'});
        pdf.fontSize(15).text(new Date().getFullYear(), 100, 650, {align: 'center'});
        pdf.restore();
    }

    async getCoverPreview(containerId: string, type: OrderTypes) {
        const thesis = await this.containerAdapter.getProperty(containerId, 'thesis') as Thesis;
        const candidate = await this.containerAdapter.getProperty(containerId, 'candidate') as Candidate;

        return new Promise<PreviewResponse>((resolve, reject) => {
            const pdf = new PDFDocument();
            const buffers: Buffer[] = [];
            pdf.on('data', buffers.push.bind(buffers));
            pdf.on('end', () => resolve({ pdf: Buffer.concat(buffers).toString('base64'), status: '', thumbnails: []}));
            pdf.on('error', err => reject(err));


            this.generateFrontCover(pdf, thesis, candidate, type);
            pdf.addPage();
            this.generateSpine(pdf, thesis, candidate);
            pdf.addPage();
            this.generateTitlePage(pdf, thesis, candidate);
            pdf.addPage();
            this.generateBackCover(pdf, thesis, candidate);
            pdf.end();
        });
    }

    async getContentPreview(containerId: string, type: OrderTypes) {
        return this.getCoverPreview(containerId, type);
    }

    async clearContent(containerId: string) {
        return Promise.resolve();
    }

    async uploadContent(containerId: string, file: Readable, metadata: ByothFile, index: number) {
        return analyzePdf(file, metadata);
    }

    async validateContent(containerId: string, metadata: ByothFile, index: number) {
        return metadata;
    }

    async getPrice(containerId: string, order: Order, type: OrderTypes) {
        let price = 0;

        if (order.offer) {
            price = order.offer.quantity * 50;
            price += order.offer.printProofs ? 300 : 0;
            price *= order.offer.printColor ? 1.5 : 1;
        }

        return price.toString() + ' NOK';
    }

    async submitOrder(containerId: string, order: Order, type: OrderTypes) {
        const output: Order = JSON.parse(JSON.stringify(order));
        output.offer.price = '100 NOK';
        output.orderNumber = '1';
        setOrderStatus(output, 'ok');
        return output;
    }

    async cancelOrder(containerId: string, orderId: string) {
        return 'cancelled';
    }
}
