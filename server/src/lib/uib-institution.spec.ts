import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';
import axios from 'axios';
import MockAxios from 'axios-mock-adapter';
import * as redis from 'redis-mock';

import { DatabaseError } from './errors';
import { UiBInstitution } from './uib-institution';

chai.use(chaiAsPromised);
const expect = chai.expect;

const mockConfig = {
    get: () => 'foo',
    required: () => {}
}

const sebraPlaces = `
<?xml version="1.0" encoding="iso-8859-1"?>
<places>
    <place>
        <code>110000</code>
        <name>Det humanistiske fakultet</name>
    </place>
    <place>
        <code>110100</code>
        <name>HF-Fak. Sekr.</name>
    </place>
    <place>
        <code>110110</code>
        <name>Det humanistiske fakultet, sekretariatet</name>
    </place>
    <place>
        <code>112000</code>
        <name>Institutt for fremmedspråk</name>
    </place>
    <place>
        <code>112100</code>
        <name>Institutt for lingvistiske, litterære og estetiske studier</name>
    </place>
    <place>
        <code>112150</code>
        <name>Datalingvistikk</name>
    </place>
    <place>
        <code>112151</code>
        <name>Digital kultur</name>
    </place>
    <place>
        <code>112200</code>
        <name>Institutt for arkeologi, historie, kultur- og religionsvitskap</name>
    </place>
    <place>
        <code>120000</code>
        <name>Det matematisk-naturvitenskapelige fakultet</name>
    </place>
    <place>
        <code>130000</code>
        <name>Det medisinske fakultet</name>
    </place>
</places>
`;

const sebraPlace = `
<?xml version="1.0" encoding="iso-8859-1"?>
<place>
    <code>110000</code>
    <name>Det humanistiske fakultet</name>
    <name_en>Faculty of Humanities</name_en>
    <address>Harald Hårfagresgt. 1</address>
    <postnr>5020</postnr>
    <postcode>BERGEN</postcode>
    <tlf>55589380</tlf>
    <fax>55589383</fax>
    <alias>HF-fak.</alias>
    <maildomain>uib.no</maildomain>
    <status>aktiv</status>
    <postboks>7805</postboks>
    <email>post@hf.uib.no</email>
</place>
`;

describe('UiBInstitution', () => {
    let mockAxios;
    let mockRedis;
    let sandbox: SinonSandbox;
    let institution: UiBInstitution;

    beforeEach(() => {
        mockAxios = new MockAxios(axios);
        mockRedis = redis.createClient();
        sandbox = sinon.createSandbox();
        institution = new UiBInstitution(mockConfig as any, mockRedis);
    });

    afterEach((done) => {
        sandbox.restore();
        mockAxios.reset();
        mockRedis.flushall(done);
    });

    describe('Caching', () => {
        it('should load from remote on first request and then use cache on subsequent requests', async () => {
            const spy = sandbox.spy()
            mockAxios.onGet(/places/).reply(() => {
                spy();
                return Promise.resolve([200, sebraPlaces]);
            });
            mockAxios.onGet(/place\?code/).reply(200, sebraPlace);

            const faculties = await institution.getFaculties();

            await institution.getDepartments(faculties[0].id);

            expect(spy.calledOnce, 'should load only once').to.equal(true);
        });
    });

    describe('Faculty/Department/Place', () => {
        beforeEach(() => mockAxios.onGet(/places/).reply(200, sebraPlaces));
        beforeEach(() => mockAxios.onGet(/place\?code/).reply(200, sebraPlace));

        it('should give correct departments for a faculty', async () => {
            const faculties = await institution.getFaculties();
            expect(faculties).to.have.length(3);

            let departments = await institution.getDepartments(faculties[0].id);
            expect(departments).to.have.length(4);
            expect(departments[0]).to.have.property('id', '110100');

            departments = await institution.getDepartments(faculties[1].id);
            expect(departments).to.have.length(0);
        });

        it('should give correct place for a place id', async () => {
            const place = await institution.getPlace('112000');
            expect(place).to.have.property('id', '112000');
        });

        it('should give correct faculty and department for a groupid', async () => {
            const faculty = await institution.getFaculty('fc:org:uib.no:unit:110110');
            expect(faculty).to.have.property('id', '110000');

            const department = await institution.getDepartment('fc:org:uib.no:unit:112150');
            expect(department).to.have.property('id', '112100');
        });

        it('should throw RangeError for malformatted groupid', async () => {
            await expect(institution.getFaculty('fc:org:unit:110110')).to.be.rejectedWith(RangeError);
        });
    });

    describe('Database errors', () => {
        beforeEach(() => mockAxios.onGet(/places/).reply(200, sebraPlaces));
        beforeEach(() => mockAxios.onGet(/place\?code/).reply(200, sebraPlace));

        it('should convert redis errors to DatabaseError in cache initialization', async () => {
            sandbox.stub(mockRedis, 'keys').yields(Error('foo'));

            await expect(institution.getFaculties()).to.be.rejectedWith(DatabaseError);

            sandbox.restore();
            sandbox.stub(mockRedis, 'multi').returns({ set: () => { return { expire: () => {return { exec: cb => cb(Error('foo'))}}}}});

            await expect(institution.init()).to.be.rejectedWith(DatabaseError);
        });

        it('should convert redis errors to DatabaseErrors when getting keys', async () => {
            sandbox.stub(mockRedis, 'keys')
                .onCall(0).callThrough()
                .onCall(1).yields(Error('foo'))
                .callThrough();
            await expect(institution.getFaculties()).to.be.rejectedWith(DatabaseError);

            sandbox.restore();
            sandbox.stub(mockRedis, 'get').yields(Error('foo'));
            await expect(institution.getFaculties()).to.be.rejectedWith(DatabaseError);
            await expect(institution.getFaculty('fc:org:uib.no:unit:110110')).to.be.rejectedWith(DatabaseError);
        });
    });

    // no real logic to test here, just here for coverage
    describe('Area/Building/Room', () => {
        let spy;
        beforeEach(() => {
            spy = sandbox.spy()
            mockAxios.onGet().reply(config => {
                spy(config.params);
                return Promise.resolve([200, {}]);
            });
        });

        it('should get Areas from remote', async() => {
            await institution.getAreas();
            expect(spy.calledOnce).to.equal(true);
        });

        it('should get Buildings from remote', async() => {
            await institution.getBuildings('foo');
            expect(spy.calledOnce).to.equal(true);
            expect(spy.args[0][0]).to.have.property('id', 'foo');
        });

        it('should get Rooms from remote', async() => {
            await institution.getRooms('foo');
            expect(spy.calledOnce).to.equal(true);
            expect(spy.args[0][0]).to.have.property('id', 'foo');
        });
    });
});
