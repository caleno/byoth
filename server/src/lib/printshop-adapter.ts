import * as Redlock from 'redlock';
import { RedisClient } from 'redis';

import { ByothFile } from 'model/byoth-file';
import { Order, initOrderOffer, OrderTypes } from 'model/order';
import { Thesis } from 'model/thesis';
import { ByothModel } from 'model/index';

import { ThesisContainerAdapter } from './thesis-container-adapter';
import { ThesisFileAdapter } from './thesis-file-adapter';
import { Conflict, LockError } from './errors';
import { validateOrder } from './validate-order';

import { PrintShopClient, PreviewResponse } from './printshop-client';

function orderProperty(type: OrderTypes) {
    return type === OrderTypes.FINAL ? 'finalOrder' : 'evaluationOrder';
}

/**
 * Uses a PrintShop to get cover previews, validate PDFs, get price and submit and cancel orders.
 */
export class PrintShopAdapter {
    public model: ByothModel;
    private redlock: Redlock;

    /**
     * Constructor.
     *
     * @param printshop The PrintShop to use.
     * @param ldp The LDP Connector to use.
     */
    constructor(private printshop: PrintShopClient,
                private containerAdapter: ThesisContainerAdapter,
                private fileAdapter: ThesisFileAdapter,
                redis: RedisClient) {
        this.redlock = new Redlock([redis], {retryCount: 0});
    }

    /**
     * Get Order document for a container.
     *
     * @param containerId The ID of the thesis container relative to the root container.
     * @returns The Order document
     */
    private async getOrder(containerId: string, type: OrderTypes): Promise<Order> {
        return this.containerAdapter.getProperty(containerId, orderProperty(type), null).then(res => <Order>res);
    }

    /**
     * Patch Order document for a container
     * @param containerId The ID of the thesis container relative to the root container.
     * @param order The updated order
     * @returns The Order document
     */
    private async patchOrder(containerId: string, order: Order, type: OrderTypes): Promise<Order> {
        return this.containerAdapter.putProperty(containerId, orderProperty(type), order, null).then(res => <Order>res);
    }

    /**
     * Has an order been submitted
     *
     * @param containerId The ID of the thesis container relative to the root container.
     * @param type Type of order (final/evaluation).
     * @returns Boolean
     */
    async isSubmitted(containerId: string, type: OrderTypes): Promise<boolean> {
        const order = await this.getOrder(containerId, type);

        return !!order.orderNumber;
    }
    /**
     * Submit order to printshop
     *
     * @param containerId The ID of the thesis container relative to the root container.
     * @param type Is the order for evaluation or for final thesis.
     * @param isTest This order is only a test.
     * @returns The updated Order document.
     */
    async submitOrder(containerId: string, type: OrderTypes, isTest = false): Promise<Order> {
        const report = await validateOrder(this.containerAdapter, this.fileAdapter, containerId, type);

        if (!report.valid) {
            throw new Conflict('Order is incomplete');
        }

        let order: Order = await this.getOrder(containerId, type);
        order.isTest = isTest;

        if (order.orderNumber) {
            throw new Conflict('Order has already been sent, it has to be cancelled before a new order can be sent');
        }

        const response: Order = await this.printshop.submitOrder(containerId, order, type);
        const today = new Date();

        order = await this.patchOrder(containerId, response, type);

        if (order.orderNumber) {
            // log that we have a submission
            const container = await this.containerAdapter.getContainer(containerId);
            container.submissionDate = today.toISOString();
            await this.containerAdapter.putContainer(containerId, container);
            await this.containerAdapter.storeSnapshot(containerId, 'PrintSubmission-' + type + '-' + order.orderNumber);

            return order;
        } else {
            throw Error('Failed to send order: ' + order.status.description);
        }
    }

    /**
     * Cancel order
     *
     * @param containerId The ID of the thesis container relative to the root container.
     * @param type Type of order (final/evaluation)
     * @returns The updated Order document.
     */
    async cancelOrder(containerId: string, type: OrderTypes): Promise<Order> {
        const order: Order = await this.getOrder(containerId, type);

        if (!order.orderNumber) {
            throw new Conflict('Can not cancel order without order number');
        }

        await this.printshop.cancelOrder(containerId, order.orderNumber);
        order.orderNumber = null;

        return Promise.all([
            this.patchOrder(containerId, order, type),
        ])
        .then(val => val[0]);
    }

    /**
     * Get a price for a given order.
     *
     * @param containerId The ID of the thesis container relative to the root container.
     * @param type Type of order (final/evaulation)
     * @returns The updated Order document with price included.
     */
    async getPrice(containerId: string, type: OrderTypes): Promise<Order> {
        const order: Order = await this.getOrder(containerId, type);

        if (!order.offer) {
            initOrderOffer(order);
        }

        const price = await this.printshop.getPrice(containerId, order, type);

        order.offer.price = price;

        return this.patchOrder(containerId, order, type);
    }

    /**
     * Upload and validate all inputFiles to printshop
     *
     * Will clear all previously updated files.
     * Can check for errors, count pages and count color pages.
     *
     * @param containerId The ID of the thesis container relative to the root container.
     * @param type The order type, for content preview generation.
     * @returns File metadata including validation information.
     */
    async uploadInputFiles(containerId: string, type: OrderTypes): Promise<ByothFile[]> {
        let lock;

        try {
            lock = await this.redlock.lock('upload:' + containerId, 180000); // validation may take a while
        } catch (err) {
            throw new LockError('Already uploading');
        }

        try {
            await this.printshop.clearContent(containerId);

            const fileMetadata: ByothFile[] = await this.fileAdapter.getInputFiles(containerId)
                .then(set => set.items);

            let promise: Promise<any> = Promise.resolve(null);

            const result = [];

            // do it one file at a time to avoid conflict
            fileMetadata.forEach((doc, index) => {
                promise = promise.then(() =>
                    this.fileAdapter.clearAssociatedFiles(doc['@id'])
                        .then(() => this.fileAdapter.getFileStream(doc['@id']))
                        .then(file => this.printshop.uploadContent(containerId, file, doc, index))
                        .then(uploadResult => this.printshop.validateContent(containerId, uploadResult, index))
                        .then(validationResult => this.fileAdapter.updateFileMetadata(validationResult['@id'], validationResult))
                        .then(updatedDoc => result[index] = updatedDoc)
                        .then(async () => {
                            try {
                                await lock.extend(180000);
                            } catch { } // in the rare case that this takes very very long, we don't want to trigger an error here.
                        })
                );
            });

            await promise;

            // we don't use the result of the preview generation, but by calling this we store an updated value in the cache.
            await this.getContentPreview(containerId, type, true);

            return result;
        } finally {
            await lock.unlock().catch(console.error);
        }
    }

    async isUploading(containerId: string): Promise<boolean> {
        try {
            // Won't work if TTL is too short. Why?
            const lock = await this.redlock.lock('upload:' + containerId, 50);
            await lock.unlock();
            return false;
        } catch (err) {
            return true;
        }
    }

    private async cacheObject(containerId: string, property: string, filename: string, object: any) {
        const buffer = Buffer.from(JSON.stringify(object));

        const coverFile = await this.fileAdapter.postFileFromBuffer(containerId,
            { buffer: buffer, mimetype: 'text/plain', originalname: filename });
        const container = await this.containerAdapter.getContainer(containerId);

        // clean up
        if (container[property]) {
            await this.fileAdapter.deleteFile(container[property]['@id']);
        }

        container[property] = { '@id': coverFile['@id'] };

        await this.containerAdapter.putContainer(containerId, container);
    }

    private async getObjectCache(containerId: string, property: string) {
        const container = await this.containerAdapter.getContainer(containerId);

        if (container[property]) {
            const buffer = await this.fileAdapter.getFileBuffer(container[property]['@id']);
            return JSON.parse(buffer.toString());
        } else { // fallback to regeneration
            return null;
        }
    }

    /**
     * Generate cover preview given thesis metadata.
     *
     * @param containerId The ID of the thesis container relative to the root container.
     * @param type Type of order (final/evaulation)
     * @returns The cover preview.
     */
    async getCoverPreview(containerId: string, type: OrderTypes, regenerate: boolean): Promise<PreviewResponse> {
        const property = type === OrderTypes.FINAL ? 'finalCoverPreview' : 'evaluationCoverPreview';

        if (regenerate) {
            const cover = await this.printshop.getCoverPreview(containerId, type);

            await this.cacheObject(containerId, property, 'cover-preview', cover);

            return cover;
        } else {
            const cover = await this.getObjectCache(containerId, property);

            return cover || this.getCoverPreview(containerId, type, true);
        }
    }

    async getContentPreview(containerId: string, type: OrderTypes, regenerate: boolean): Promise<PreviewResponse> {
        const property = type === OrderTypes.FINAL ? 'finalContentPreview' : 'evaluationContentPreview';

        if (regenerate) {
            const content = await this.printshop.getContentPreview(containerId, type);

            await this.cacheObject(containerId, property, 'content-preview', content);

            const thesis = await this.containerAdapter.getProperty(containerId, 'thesis') as Thesis;
            thesis.numPages = content.numPages;
            thesis.numColorPages = content.numColorPages;
            await this.containerAdapter.putProperty(containerId, 'thesis', thesis);

            return content;
        } else {
            const content = await this.getObjectCache(containerId, property);

            return content || this.getContentPreview(containerId, type, regenerate);
        }
    }

    async clearContent(containerId: string): Promise<void> {
        await this.printshop.clearContent(containerId);
        const fileMetadata: ByothFile[] = await this.fileAdapter.getInputFiles(containerId)
            .then(set => set.items);

        await Promise.all(
            fileMetadata.map(doc => {
                let promise = Promise.resolve() as Promise<any>;
                promise = promise
                .then(() => this.fileAdapter.clearAssociatedFiles(doc['@id']))
                .then(doc => {
                    if (doc.status) {
                        delete doc.status;
                        return this.fileAdapter.updateFileMetadata(doc['@id'], doc);
                    } else {
                        return Promise.resolve(doc);
                    }
                });

                return promise;
            })
        );
    }
}
