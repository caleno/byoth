import { InternalUser } from './user';
import axios from 'axios';
import * as OAuth2Strategy from 'passport-oauth2';

export const DATAPORTEN_PROFILE_URL = 'https://auth.dataporten.no/userinfo';
export const DATAPORTEN_GROUP_URL = 'https://groups-api.dataporten.no/groups/me/groups';

/** Passport strategy for Dataporten */
export class DataportenStrategy extends OAuth2Strategy {
    constructor(options, verify) {
        super(options, verify);
    }

    userProfile(accessToken, done) {
        axios.get(DATAPORTEN_PROFILE_URL, { headers: { 'Authorization': 'Bearer ' + accessToken } })
        .then(response => {
            const user = response.data.user;

            let feideId;

            if (user.userid_sec && user.userid_sec.length) {
                const feideIdElement = user.userid_sec.find(uid => uid.match(/^feide\:/));
                if (feideIdElement) {
                    feideId = feideIdElement.replace(/feide\:(\w+)\@.*$/, '$1');
                }
            }

            if (!feideId) {
                throw Error('Feide ID missing from Oauth2 provider.');
            }

            const profile: InternalUser = {
                id: feideId,
                name: user.name,
                email: user.email,
                groupid: null
            }

            return profile;
        })
        .then(profile => {
            return axios.get(DATAPORTEN_GROUP_URL, { headers: { 'Authorization': 'Bearer ' + accessToken}})
            .then(response => {
                if (response.data && response.data.length) {
                    const group = response.data.find(g => g.type && g.type === 'fc:orgunit');
                    profile.groupid = group && group.id
                }
                return profile;
            });
        })
        .then(profile => done(null, profile), done);
    }
}
