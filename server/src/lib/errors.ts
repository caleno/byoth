// adapted from https://gist.github.com/slavafomin/b164e3e710a6fc9352c934b9073e7216
import { Request } from 'express';

/** General error class, primarily use derived classes*/
export class ByothError extends Error {
    name: string;
    status: number;
    code: any;
    request: any;

    /**
     * Constructor
     *
     * @param message Error message
     * @param status HTTP status code
     */
    constructor (message: string, status?: any) {
        super(message);

        this.name = this.constructor.name;

        Error.captureStackTrace(this, this.constructor);

        this.status = status || 500;
    }

    /**
     * Append error
     *
     * Allows us to add context when moving up the stack trace.
     * Concatenates error strings, uses the status code of the
     * error appended to if exist, otherwise that of the argument error.
     *
     * @param error Error to append
     */
    append(error: Error) {
        this.message += ': ';
        this.message += error.name ? error.name + ': ' : ''
        this.message += error.message || '';

        // If we have no code or status, use the one from the appended error.
        this.code = this.code || (<ByothError>error).code;
        this.status = this.status || (<ByothError>error).status;

        return this;
    }
}

/** User input is invalid (usually has status 422) */
export class InputError extends ByothError {
    constructor(message: string, status?: number) {
        super(message, status);
    }
}

/** A JSON-LD document can't be framed, usually a special case of InputError */
export class FramingError extends ByothError {
    constructor(message: string, status?: number) {
        super(message);
    }
}

/** Remote error from anything considered a database */
export class DatabaseError extends ByothError {
    constructor(message: string, status?: number) {
        super(message, status);
    }
}

/** Error in filehandling */
export class FileError extends ByothError {
    constructor(message: string, status?: number) {
        super(message, status);
    }
}

/** Conflict. Data has logical inconsistencies, mandatory fields missing, etc. Status 409. */
export class Conflict extends ByothError {
    constructor(message: string) {
        super(message, 409);
    }
}

/** Resource locked, try again later. Status 423. */
export class LockError extends ByothError {
    constructor(message: string) {
        super(message, 423);
    }
}

/**
 * An error that needs access to the request method and url for it's message.
 * For example authorization or not found errors.
 */
export class RequestError extends ByothError {
    constructor(message: string, status: number, request?: Request) {
        super(message, status);

        if (request) {
            this.request = request.method + ' ' + request.originalUrl;
        }
    }
}

/** Authorization error. Status 401. */
export class AuthorizationError extends RequestError {
    constructor(request?: Request) {
        super('Unauthorized', 401, request);
    }
}

/** Resource not found. Status 404. */
export class NotFoundError extends RequestError {
    constructor(message: string, request?: Request) {
        super(message, 404, request);
    }
}
