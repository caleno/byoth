import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';
import { NotFoundError, DatabaseError, Conflict } from '../lib/errors';
import { InternalUser } from '../lib/user';
import { ThesisContainerAdapter } from './thesis-container-adapter';

import {
    createModelDocument,
    ByothModel,
    ByothInternalModel } from 'model/index';

chai.use(chaiAsPromised);
const expect = chai.expect;

const mockLock = {
    lock: () => Promise.resolve(mockLock),
    unlock: () => Promise.resolve()
}

/* istanbul ignore next */
const mockLdp = {
    get: () => Promise.resolve({}),
    getFile: () => Promise.resolve({}),
    getFileMetadata: () => Promise.resolve({}),
    post: () => Promise.resolve({}),
    postFileFromRequest: () => Promise.resolve({}),
    postFileFromStream: () => Promise.resolve({}),
    update: () => Promise.resolve({}),
    updateFileMetadata: () => Promise.resolve({}),
    delete: () => Promise.resolve({}),
    exists: () => Promise.resolve(true),
    storeSnapshot: () => Promise.resolve({}),
    snapshotLabelExists: () => Promise.resolve(false)
}

/* istanbul ignore next */
const mockInstitution = {
    getFaculty: orgunit => Promise.resolve({}),
    getDepartment: orgunit => Promise.resolve({}),
}

describe('Container adapter', () => {
    const sandbox: SinonSandbox = sinon.sandbox.create();
    let adapter: ThesisContainerAdapter;
    let model: ByothModel;
    let internalModel: ByothInternalModel;

    const url = 'http://example.com/';
    const containerId = 'container/foo';
    const containerUri = url + containerId;
    const user: InternalUser = { name: 'Foo Bar', email: 'foo.bar@example.com', id: 'foobar', groupid: '123456' };
    const request: any = { user: { data: user }};
    let personId;

    beforeEach(() => {
        adapter = new ThesisContainerAdapter(<any>mockLdp, <any>mockLock, <any>mockInstitution);
        personId = adapter.personId(user);
        model = adapter.model;
        internalModel = adapter['internalModel'];
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('getContainerOwner()', () => {
        it('should return uri of container creator', async () => {
            sandbox.stub(mockLdp, 'get').resolves(createModelDocument(model.thesisContainer, containerUri, { creator: personId }));

            const uri = await adapter.getContainerOwner(request);

            expect(uri).to.equal(personId);
        });
    });

    describe('getUserContainerId()', () => {
        it('should return uri of container if user has one', async () => {
            sandbox.stub(mockLdp, 'get').resolves(createModelDocument(internalModel.user, personId, { owns: containerId}));

            const uri = await adapter.getUserContainerId(request);

            expect(uri).to.equal(containerId);
        });

        it('should return null user does not exist', async () => {
            sandbox.stub(mockLdp, 'get').throws({status: 404});

            const uri = await adapter.getUserContainerId(request);

            expect(uri).to.equal(null);
        });

        it('should throw if ldp gives other errors', async () => {
            sandbox.stub(mockLdp, 'get').throws({status: 400});

            await expect(adapter.getUserContainerId(request))
            .to.eventually.be.rejected
            .and.have.property('status', 400);
        });
    });

    // Mocking gets so complicated here that we let integration tests cover this case
    // describe('createContainer()', () => { });

    describe('setupLDP()', () => {
        it('should create thesis and person subcontainers if they do not exist', async () => {
            sandbox.stub(mockLdp, 'exists').resolves(false);
            const spy = sandbox.spy(mockLdp, 'post');

            await adapter.setupLDP();

            expect(spy.calledTwice, 'should create two subcontainers').to.equal(true);
        });

        it('should not create subcontainers if they exist', async () => {
            sandbox.stub(mockLdp, 'exists').resolves(true);
            const spy = sandbox.spy(mockLdp, 'post');

            await adapter.setupLDP();

            expect(spy.called, 'should create two subcontainers').to.equal(false);
        });

    });

    describe('getContainer()', () => {
        it('should return container from ldp', async () => {
            const doc = {foo: 'bar'};
            sandbox.stub(mockLdp, 'get').resolves(doc);

            const container = await adapter.getContainer('foo');

            expect(container, 'should be right document').to.equal(doc);
        });
    });

    describe('deleteContainer()', () => {
        it('should delete both container, and user ownership', async () => {
            const personSpy = sandbox.stub(mockLdp, 'update').withArgs(personId, internalModel.user);
            const containerSpy = sandbox.stub(mockLdp, 'delete').withArgs(containerId);

            await adapter.deleteContainer(containerId, request);

            expect(personSpy.calledOnce, 'should update user').to.equal(true);
            expect(personSpy.getCall(0).args[2], 'updated user should not have owns field').to.not.have.property('owns');
            expect(containerSpy.calledOnce, 'should delete container').to.equal(true);
        });
    });

    describe('getAllContainerIds()', () => {
        it('should return an array of container IDs', async () => {
            sandbox.stub(mockLdp, 'get')
                .onFirstCall().resolves({items: []})
                .onSecondCall().resolves({items: [ {'@id': 'foo'}, {'@id': 'bar'} ] });

            let ids = await adapter.getAllContainerIds();
            expect(ids).to.have.length(0);

            ids = await adapter.getAllContainerIds();
            expect(ids).to.have.length(2);
            expect(ids).to.include('foo');
            expect(ids).to.include('bar');
        });
    });

    describe('getProperty()', () => {
        it('should throw when called with unknown property', async () => {
            await expect(adapter.getProperty('foo', 'bar' as any, request))
            .to.eventually.be.rejectedWith(NotFoundError);
        });

        it('should fail if LDP fails', async () => {
            sandbox.stub(mockLdp, 'exists').rejects(new DatabaseError('', 500));

            await expect(adapter.getProperty('foo', 'candidate', request)).to.eventually.be.rejectedWith(DatabaseError);
        });

        it('should create a known property that has not been created yet', async () => {
            sandbox.stub(mockLdp, 'exists').resolves(false);
            sandbox.stub(mockLdp, 'get').withArgs('foo', model.thesisContainer).resolves({});

            const postSpy = sandbox.stub(mockLdp, 'post').resolves({'@id': 'foo/candidate'});

            await adapter.getProperty('foo', 'candidate', request);
            expect(postSpy.getCall(0).args[1], 'should create with model.candidate').to.equal(model.candidate);
            expect(postSpy.getCall(0).args[2], 'should create with slug \'candidate\'').to.equal('candidate');
        });

        it('should get existing Candidate when called with \'candidate\'', async () => {
            const spy = sandbox.stub(mockLdp, 'get').resolves({'candidate': 'id' });
            await adapter.getProperty('foo', 'candidate', request);
            expect(spy.getCall(0).args[1], 'should use model.candidate').to.equal(model.candidate);
        });

        it('should get existing Thesis when called with \'thesis\'', async () => {
            const spy = sandbox.stub(mockLdp, 'get').resolves({'thesis': 'id' });
            await adapter.getProperty('foo', 'thesis', request);
            expect(spy.getCall(0).args[1], 'should use model.thesis').to.equal(model.thesis);
        });

        it('should get existing Defense when called with \'defense\'', async () => {
            const spy = sandbox.stub(mockLdp, 'get').resolves({'defense': 'id' });
            await adapter.getProperty('foo', 'defense', request);
            expect(spy.getCall(0).args[1], 'should use model.defense').to.equal(model.defense);
        });

        it('should get existing TrialLecture when called with \'trial-lecture\'', async () => {
            const spy = sandbox.stub(mockLdp, 'get').resolves({'trial-lecture': 'id' });
            await adapter.getProperty('foo', 'trialLecture', request);
            expect(spy.getCall(0).args[1], 'should use model.trialLecture').to.equal(model.trialLecture);
        });

        it('should get existing Department when called with \'department\'', async () => {
            const spy = sandbox.stub(mockLdp, 'get').resolves({'department': 'id' });
            await adapter.getProperty('foo', 'department', request);
            expect(spy.getCall(0).args[1], 'should use model.department').to.equal(model.department);
        });

        it('should get existing LpdSet when called with \'articles\'', async () => {
            const spy = sandbox.stub(mockLdp, 'get').resolves({'articles': 'id' });
            await adapter.getProperty('foo', 'articles', request);
            expect(spy.getCall(0).args[1], 'should use model.ldpSet').to.equal(model.ldpSet);
        });

        it('should get existing LdpSet when called with \'files\'', async () => {
            const spy = sandbox.stub(mockLdp, 'get').resolves({'files': 'id' });
            await adapter.getProperty('foo', 'files', request);
            expect(spy.getCall(0).args[1], 'should use model.orderedSet').to.equal(model.ldpSet);
        });

        it('should get existing Order when called with \'finalOrder\'', async () => {
            const spy = sandbox.stub(mockLdp, 'get').resolves({'order': 'id' });
            await adapter.getProperty('foo', 'finalOrder', request);
            expect(spy.getCall(0).args[1], 'should use model.orderedSet').to.equal(model.finalOrder);
        });

        it('should get existing Order when called with \'evaluationOrder\'', async () => {
            const spy = sandbox.stub(mockLdp, 'get').resolves({'order': 'id' });
            await adapter.getProperty('foo', 'evaluationOrder', request);
            expect(spy.getCall(0).args[1], 'should use model.orderedSet').to.equal(model.evaluationOrder);
        });
    });

    describe('putProperty', () => {
        let doc;

        beforeEach(() => {
            doc = createModelDocument(model.candidate);
        });

        it('should throw when property doesn\'t exist', async () => {
            await expect(adapter.putProperty('foo', 'bar' as any, doc, request))
            .to.eventually.be.rejectedWith(NotFoundError);
        });

        it('should fail when LDP fails', async () => {
            sandbox.stub(mockLdp, 'exists').rejects(new DatabaseError('', 400));
            await expect(adapter.putProperty('foo', 'candidate', doc, request))
            .to.eventually.be.rejectedWith(DatabaseError);
        });

        it('should update correct property', async () => {
            const spy = sandbox.stub(mockLdp, 'update').resolves(doc);
            const result = await adapter.putProperty('foo', 'candidate', doc, request);
            expect(spy.calledOnce, 'should update ldp').to.equal(true);
            expect(spy.getCall(0).args[1], 'should update with correct model').to.equal(model.candidate);
            expect(result, 'should get result back').to.equal(doc);
        });

        it('should first create empty property if necessary', async () => {
            sandbox.stub(mockLdp, 'exists').resolves(false);
            const updateSpy = sandbox.stub(mockLdp, 'update').resolves(doc);
            const addSpy = sandbox.stub(adapter as any, 'addProperty').resolves({});

            const result = await adapter.putProperty('foo', 'candidate', doc, request);

            expect(addSpy.calledOnce, 'should add property').to.equal(true);
            expect(updateSpy.calledOnce, 'should update ldp').to.equal(true);
            expect(updateSpy.getCall(0).args[1], 'should update with correct model').to.equal(model.candidate);
            expect(result, 'should get result back').to.equal(doc);
        });
    });

    describe('storeSnapshot()', () => {
        it('should store a snapshot in the ldp', async () => {
            const spy = sandbox.stub(mockLdp, 'storeSnapshot').resolves(201);
            const result = await adapter.storeSnapshot('foo', 'bar');
            expect(result).to.equal(201);
            expect(spy.calledOnce).to.equal(true);
        });

        it('should throw Conflict if snapshot already exists', async () => {
            sandbox.stub(mockLdp, 'snapshotLabelExists').resolves(true);
            await expect(adapter.storeSnapshot('foo', 'bar')).to.eventually.be.rejectedWith(Conflict);
        });
    });
});
