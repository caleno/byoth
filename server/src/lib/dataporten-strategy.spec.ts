import * as chai from 'chai';
import axios from 'axios';
import MockAxios from 'axios-mock-adapter';

import { DataportenStrategy, DATAPORTEN_PROFILE_URL, DATAPORTEN_GROUP_URL } from './dataporten-strategy';

const expect = chai.expect;

const options = {
    authorizationURL: 'http://example.com',
    tokenURL: 'http://example/com',
    clientID: '123',
    clientSecret: '456'
};

const token = '123';

describe('DataportenStrategy', () => {
    let mockAxios: MockAxios;

    beforeEach(() => {
        mockAxios = new MockAxios(axios);
    });

    afterEach(() => {
        mockAxios.reset();
    });

    it('should get profile and group from dataporten', (done) => {
        const user = {
            userid_sec: [ 'feide:abc123'],
            name: 'foo',
            email: 'foo@bar.baz'
        }

        const groups = [ { type: 'fc:orgunit', id: 'fooGroup' } ];

        mockAxios.onGet(DATAPORTEN_PROFILE_URL).reply(200, { user: user });
        mockAxios.onGet(DATAPORTEN_GROUP_URL).reply(200, groups);

        const strategy = new DataportenStrategy(options, () => {});

        strategy.userProfile(token, (err, profile) => {
            expect(!!err, 'should not have error').to.equal(false);
            expect(profile).to.have.property('id', user.userid_sec[0]);
            expect(profile).to.have.property('name', user.name);
            expect(profile).to.have.property('email', user.email);
            expect(profile).to.have.property('groupid', groups[0].id);
            done();
        });
    });

    it('should fail if user has no Secondary ID', (done) => {
        const user = {
            userid_sec: null,
            name: 'foo',
            email: 'foo@bar.baz'
        }

        mockAxios.onGet(DATAPORTEN_PROFILE_URL).replyOnce(200, { user: user })

        const strategy = new DataportenStrategy(options, () => {});

        strategy.userProfile(token, (err, profile) => {
            expect(!!err, 'should have error').to.equal(true);
            done();
        });
    });

    it('should fail if user has no Feide ID among secondary IDs', (done) => {
        const user = {
            userid_sec: [ 'abc123'],
            name: 'foo',
            email: 'foo@bar.baz'
        }

        mockAxios.onGet(DATAPORTEN_PROFILE_URL).replyOnce(200, { user: user })

        const strategy = new DataportenStrategy(options, () => {});

        strategy.userProfile(token, (err, profile) => {
            expect(!!err, 'should have error').to.equal(true);
            done();
        });
    });

    it('should leave groupid field empty if no groups found', (done) => {
        const user = {
            userid_sec: [ 'feide:abc123' ],
            name: 'foo',
            email: 'foo@bar.baz'
        }

        const groups = [ ];

        mockAxios.onGet(DATAPORTEN_PROFILE_URL).replyOnce(200, { user: user });
        mockAxios.onGet(DATAPORTEN_GROUP_URL).reply(200, groups);

        const strategy = new DataportenStrategy(options, () => {});

        strategy.userProfile(token, (err, profile) => {
            expect(!!profile.groupid, 'should not groupid').to.equal(false);
            done();
        });
    });

    it('should leave groupid field empty if no relevant groups found', (done) => {
        const user = {
            userid_sec: [ 'feide:abc123' ],
            name: 'foo',
            email: 'foo@bar.baz'
        }

        const groups = [ { type: 'bar', id: 'fooGroup' } ];

        mockAxios.onGet(DATAPORTEN_PROFILE_URL).replyOnce(200, { user: user });
        mockAxios.onGet(DATAPORTEN_GROUP_URL).reply(200, groups);

        const strategy = new DataportenStrategy(options, () => {});

        strategy.userProfile(token, (err, profile) => {
            expect(!!profile.groupid, 'should not have groupid').to.equal(false);
            done();
        });
    });
});
