import { RedisClient } from 'redis';
import { ThesisContainer } from 'model/thesis-container';

export class ApprovalIndex {
    constructor(private redis: RedisClient, private key = 'final_order_index') { }

    async insert(container: ThesisContainer): Promise<void> {
        if (!container.approvalDate) {
            return Promise.resolve();
        }

        const date = new Date(container.approvalDate);

        return new Promise<void>((resolve, reject) =>
            this.redis.zadd(this.key, date.valueOf().toString(), container['@id'], (err, reply) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        );
    }

    async delete(container: ThesisContainer): Promise<void> {
        return new Promise<void>((resolve, reject) =>
            this.redis.zrem(this.key, container['@id'], (err, reply) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        );
    }

    async clearIndex(): Promise<void> {
        return new Promise<void>((resolve, reject) =>
            this.redis.del(this.key, (err, reply) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        );
    }

    async getAllAfterDate(date: Date): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) =>
            this.redis.zrangebyscore(this.key, date.valueOf(), Date.now().valueOf(), (err, reply) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(reply);
                }
            })
        );
    }
}
