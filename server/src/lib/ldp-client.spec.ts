import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import * as sinon from 'sinon';
import { SinonSandbox } from 'sinon';
import axios from 'axios';
import MockAxios from 'axios-mock-adapter';
import * as express from 'express';
import * as request from 'supertest';
import * as stream from 'stream';
import { Readable } from 'stream';

import { FramingError, DatabaseError, NotFoundError, LockError } from './errors';
import { LdpClient } from './ldp-client';

const mockLock = {
    lock: () => Promise.resolve(mockLock),
    unlock: () => Promise.resolve()
}

chai.use(chaiAsPromised);
const expect = chai.expect;

describe('LdpClient', () => {
    let ldp: LdpClient;
    let sandbox: SinonSandbox;
    let mockAxios: MockAxios;
    const root = 'http://example.com/rest/';

    beforeEach(done => {
        sandbox = sinon.sandbox.create();
        ldp = new LdpClient(mockLock as any, {root: root, authorization: 'dummy authorization'});
        mockAxios = new MockAxios(axios);
        done();
    });

    afterEach(() => {
        sandbox.restore();
    });

    afterEach(() => {
        mockAxios.reset();
    });

    it('should append trailing slash to root url if missing', () => {
        ldp = new LdpClient(mockLock as any, {root: 'foo', authorization: ''});
        expect(ldp.rootUrl).to.equal('foo/')
    });

    describe('getTriples()', () => {
        it('should return raw data from backend', async () =>  {
            const data = 'foobar';
            mockAxios.onGet().reply(200, data);

            await expect((<any>ldp).getTriples('foo')).to.eventually.equal(data);
        });

        it('should throw DatabaseError on 4xx response from backend', async () => {
            mockAxios.onGet().reply(400);

            await expect((<any>ldp).getTriples('foo'))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 400);
        });

        it('should throw DatabaseError on network error when connecting to backend', async () => {
            mockAxios.onGet().networkError();

            await expect((<any>ldp).getTriples('foo'))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 500);
        });

        it('should throw DatabaseError if backend times out', async () => {
            mockAxios.onGet().timeout();

            await expect((<any>ldp).getTriples('foo'))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 500);
        });
    });

    describe('exists()', () => {
        it('should return true if backend finds uri', async () => {
            mockAxios.onHead().reply(200);

            await expect(ldp.exists('foo')).to.eventually.be.true;
        });

        it('should return false if backend returns 404', async () => {
            mockAxios.onHead().reply(404);

            await expect(ldp.exists('foo')).to.eventually.be.false;
        });

        it('should throw DatabaseError if backend returns other 4xx error', async () => {
            mockAxios.onHead().reply(400);

            await expect(ldp.exists('foo'))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 400);
        });

        it('should throw DatabaseError on network error when connecting to backend', async () => {
            mockAxios.onHead().networkError();

            await expect(ldp.exists('foo'))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 500);
        });

        it('should throw DatabaseError if backend times out', async () => {
            mockAxios.onHead().timeout();

            await expect(ldp.exists('foo'))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 500);
        });
    });

    describe('get()', () => {
        let frame;
        beforeEach(done => {
            frame = { '@context': { 'foo': 'http://foo.org/' }, '@type': 'foo:baz', '@explicit': true };
            done();
        });

        it('should return framed and compacted JSON-LD document if query result and model match', async () => {
            const triples = '<http://foo.org/bar> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://foo.org/baz> .';
            sandbox.stub(ldp as any, 'getTriples').resolves(triples);

            await expect(ldp.get('foo', { frame: frame } as any))
            .to.eventually.include({'@type': 'foo:baz', '@id': 'foo:bar'})
            .and.not.have.property('@graph');
        });

        it('should throw FramingError if model does not match data', async () => {
            const triples = '<http://foo.org/bar> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://foo.org/baz> .';
            frame['@type'] = 'foo:foobar';
            sandbox.stub(ldp as any, 'getTriples').resolves(triples);

            await expect(ldp.get('foo', { frame: frame } as any))
            .to.eventually.be.rejectedWith(FramingError);
        });

        it('should throw FramingError if triples invalid', async () => {
            const triples = '<http://foo.org/bar> a <http://foo.org/baz> .';
            sandbox.stub(ldp as any, 'getTriples').resolves(triples);

            await expect(ldp.get('foo', { frame: frame } as any))
            .to.eventually.be.rejectedWith(FramingError, /Input triples/);
        });

        it('should throw FramingError if frame invalid', async () => {
            const triples = '<http://foo.org/bar> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://foo.org/baz> .';
            frame['@type'] = 1;
            sandbox.stub(ldp as any, 'getTriples').resolves(triples);

            await expect(ldp.get('foo', { frame: frame } as any))
            .to.eventually.be.rejectedWith(FramingError);
        });

        it('should throw DatabaseError if there are request errors', async () => {
            mockAxios.onGet().reply(400);

            await expect(ldp.get('foo', {} as any))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 400);
        });
    });

    describe('post()', () => {
        const model = {
            frame: { '@context': { 'foo': 'http://foo.org/' }, '@type': 'foo:baz', '@explicit': true },
            expandedType: 'http://foo.org/baz'
        }

        it('should return JSON-LD representation of newly created document', async () => {
            const triples = '<http://foo.org/bar> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://foo.org/baz> .';
            mockAxios.onPost().reply(201, { data: 'foo/bar'});
            sandbox.stub(ldp as any, 'getTriples').resolves(triples);

            await expect(ldp.post('foo', model as any, 'bar'))
            .to.eventually.include({'@type': 'foo:baz', '@id': 'foo:bar'});
        });

        it('should throw DatabaseError if post to backend fails', async () => {
            mockAxios.onPost().reply(400);

            await expect(ldp.post('foo', model as any, 'bar'))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 400);
        });

        it('should throw DatabaseError if get from backend fails', async () => {
            mockAxios.onPost().reply(201, { data: 'foo/123-456'});
            sandbox.stub(ldp as any, 'getTriples').rejects(new DatabaseError('', 400));

            await expect(ldp.post('foo', model as any))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 400);
        });
    });

    describe('update()', () => {
        let model;
        let doc;

        beforeEach(done => {
            model = {
                frame: {
                    '@context': { 'foo': 'http://foo.org/' },
                    '@type': 'foo:baz',
                    '@explicit': true,
                    'foo:bar': { '@default': null }
                },
                expandedType: 'http://foo.org/baz'
            };

            doc = {
                '@context': { 'foo': 'http://foo.org/' },
                '@type': 'foo:baz',
                '@id': 'foo:1'
            };

            done();
        });


        const typeLine = '<http://foo.org/1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://foo.org/baz> .';
        const foobarLine = '<http://foo.org/1> <http://foo.org/bar> "foobar" .';

        it('should send an update query to ldp backend based on input and model', async () => {
            const expected = doc;
            expected['foo:bar'] = 'foobar';

            const oldTriples = typeLine;
            const newTriples = `${typeLine}\n${foobarLine}`;

            const triplesStub = sandbox.stub(ldp as any, 'getTriples')
            triplesStub.onCall(0).resolves(oldTriples);
            triplesStub.onCall(1).resolves(newTriples);

            mockAxios.onPatch().reply(200);
            const axiosSpy = sandbox.spy(axios, 'patch');

            await expect(ldp.update('foo', model as any, expected))
            .to.eventually.deep.equal(expected);
            expect(axiosSpy.calledOnce).to.equal(true);

            const spyQuery = axiosSpy.getCall(0).args[1];
            expect(spyQuery).to.have.string(`DELETE {\n${oldTriples}\n}`);
            const alt1 = `INSERT {\n${newTriples}\n}`;
            const alt2 = `INSERT {\n${foobarLine}\n${typeLine}\n}`
            if (!(spyQuery.includes(alt1) || spyQuery.includes(alt2))) {
                return Promise.reject(`Query: '${spyQuery}' does not include '${alt1}' or '${alt2}'`);
            }
        });

        it('should throw if failing to lock instance', async () => {
            sandbox.stub(mockLock, 'lock').throws(new LockError('lock-err'));

            await expect(ldp.update('foo', model as any, {} as any))
            .to.eventually.be.rejectedWith(LockError)
            .that.has.property('message', 'lock-err');
        });

        it('should print error to console if failing to unlock instance', async () => {
            sandbox.stub(ldp as any, 'getTriples').resolves(typeLine);
            mockAxios.onPatch().reply(200);
            sandbox.stub(mockLock, 'unlock').rejects(Error('unlock-err'));
            const logSpy = sandbox.stub(console, 'error');

            await ldp.update('foo', model as any, doc);

            expect(logSpy.getCall(0).args[0].message).to.equal('unlock-err');
        });

        it('should not modify triples that are not in model frame', async () => {
            const offendingTriple = '<http://foo.org/1> <http://foo.org/barbaz> "barbaz" .';
            const triples = `${typeLine}\n${offendingTriple}`;
            sandbox.stub(ldp as any, 'getTriples').resolves(triples);
            mockAxios.onPatch().reply(200);

            const axiosSpy = sandbox.spy(axios, 'patch');

            await ldp.update('foo', model as any, doc);

            expect(axiosSpy.getCall(0).args[1]).to.not.have.string(offendingTriple);
        });

        it('should throw FramingError if model does not match stored triples', async () => {
            model.frame['@type'] = 'foo:barbaz';

            sandbox.stub(ldp as any, 'getTriples').resolves(typeLine);

            await expect(ldp.update('foo', model as any, doc))
            .to.eventually.be.rejectedWith(FramingError)
            .and.have.property('message')
            .that.have.string('not compatible');
        });

        it('should throw FramingError if doc does not match model', async () => {
            doc['@type'] = 'foo:barbaz';

            sandbox.stub(ldp as any, 'getTriples').resolves(typeLine);

            await expect(ldp.update('foo', model as any, doc))
            .to.eventually.be.rejectedWith(FramingError)
            .and.have.property('message')
            .that.have.string('not compatible');
        });

        it('should throw DatabaseError if patch to ldp backend fails', async () => {
            sandbox.stub(ldp as any, 'getTriples').resolves(typeLine);
            mockAxios.onPatch().reply(400);

            await expect(ldp.update('foo', model as any, doc))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 400);
        });
    });

    describe('delete()', () => {
        it('should delete document from ldp backend', async () => {
            mockAxios.onDelete().reply(204);
            await expect(ldp.delete('foo')).to.eventually.equal(204);
        });

        it('should throw DatabaseError if ldp backend responds with error', async () => {
            mockAxios.onDelete().reply(400);
            await expect(ldp.delete('foo'))
            .to.eventually.be.rejectedWith(DatabaseError)
            .and.have.property('status', 400);
        });
    });

    describe('File interface', () => {
        let typeLine;
        let model;
        let fileDoc;

        beforeEach(() => {
            typeLine = '<http://foo.org/1> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://foo.org/fileType> .';

            model = {
                frame: {
                    '@context': { 'foo': 'http://foo.org/' },
                    '@type': 'foo:fileType',
                    '@explicit': true,
                    'foo:bar': { '@default': null }
                },
                expandedType: 'http://foo.org/fileType'
            };

            fileDoc = {
                '@context': { 'foo': 'http://foo.org/' },
                '@type': 'foo:fileType',
                '@id': 'foo:1'
            }
        });

        describe('postFileFromRequest()', () => {
            // easier to create skeleton app and supertest than to mock Express requests.
            const app = express();

            app.post('/', (req, res, next) => {
                ldp.postFileFromRequest('/foo', model as any, req)
                .then(response => res.send(response), next)
            });

            app.post('/:slug', (req, res, next) => {
                ldp.postFileFromRequest('/foo', model as any, req, req.params.slug)
                .then(response => res.send(response), next)
            });

            app.use((err, req, res, next) => {
                res.status(err.status || 500);
                res.type('text/plain').send(err.name || 'Error');
            });

            const agent = request(app);

            function mockPost() {
                mockAxios.onPost().reply(config => {
                    config.data.emit('end');
                    return [201, '/foo/bar'];
                });
            }

            it('should post file to ldp backend', async () => {
                const content = Buffer.from('foo bar');
                mockPost();
                mockAxios.onPatch().reply(200);
                sinon.stub(ldp as any, 'getTriples').resolves(typeLine);

                await agent.post('/')
                .attach('file', content, {filename: 'test.txt', contentType: 'text/plain'})
                .expect(200)
                .expect(res => {
                    expect(res.body).to.have.property('@id', 'foo:1');
                });
            });

            // not really checking anything here, just making sure this path executes too
            it('should post file to ldp backend with slug', async () => {
                const content = Buffer.from('foo bar');
                mockPost();
                mockAxios.onPatch().reply(200);
                sinon.stub(ldp as any, 'getTriples').resolves(typeLine);

                await agent.post('/slug')
                .attach('file', content, {filename: 'test.txt', contentType: 'text/plain'})
                .expect(200)
                .expect(res => {
                    expect(res.body).to.have.property('@id', 'foo:1');
                });
            });

            it('should throw DatabaseError if post to ldp fails', async () => {
                const content = Buffer.from('foo bar');
                mockAxios.onPost().reply(400);
                mockAxios.onPatch().reply(200);
                sinon.stub(ldp as any, 'getTriples').resolves(typeLine);

                await agent.post('/')
                .attach('file', content, {filename: 'test.txt', contentType: 'text/plain'})
                .expect(400)
                .expect(res => {
                    expect(res.text).to.have.string('DatabaseError');
                });
            });

            it('should throw DatabaseError if post to ldp fails', async () => {
                const content = Buffer.from('foo bar');
                mockPost();
                mockAxios.onPatch().reply(400);
                sinon.stub(ldp as any, 'getTriples').resolves(typeLine);

                await agent.post('/')
                .attach('file', content, {filename: 'test.txt', contentType: 'text/plain'})
                .expect(400)
                .expect(res => {
                    expect(res.text).to.have.string('DatabaseError');
                });
            });
        });

        describe('postFileFromStream()', () => {
            it('should send file stream to LDP', async () => {
                mockAxios.onPost().reply(config => {
                    config.data.emit('end');
                    return [201, '/foo/bar'];
                });

                const fileStream = new Readable();

                fileStream._read = () => {}
                fileStream.push('abc\n');
                fileStream.push(null);

                mockAxios.onPatch().reply(200, typeLine);
                sinon.stub(ldp as any, 'getTriples').resolves(typeLine);

                await expect(ldp.postFileFromStream('/', model as any, {
                        stream: fileStream,
                        mimetype: 'text/plain',
                        originalname: 'foo' }
                )).to.eventually.have.property('@id', 'foo:1');
            });
        });

        describe('getFileMetadata()', () => {
            it('should return file metadata', async () => {
                mockAxios.onGet().reply(200, typeLine);

                await expect(ldp.getFileMetadata('foo', model as any))
                .to.eventually.deep.equal(fileDoc);
            });

            it('should throw FramingError if file model is incompatible with stored metadata', async () => {
                model.frame['@type'] = 'foo:bar';
                mockAxios.onGet().reply(200, typeLine);

                await expect(ldp.getFileMetadata('foo', model as any))
                .to.eventually.be.rejectedWith(FramingError);
            });

            it('should throw DatabaseError if ldp backend errors', async () => {
                mockAxios.onGet().reply(400);

                await expect(ldp.getFileMetadata('foo', model as any))
                .to.eventually.be.rejectedWith(DatabaseError);
            });
        });

        describe('getFile()', () => {
            // this test is somewhat phony
            it('should pass file stream from ldp backend', async () => {
                const data = new stream.PassThrough();
                data.write(Buffer.from('foo bar'));
                // mockAxios does funny stuff to the stream
                sandbox.stub(axios, 'get').resolves({status: 200, data: data });

                const response = await ldp.getFile('foo');
                expect(response.data.read().toString()).to.equal('foo bar');
             });

            it('should throw DatabaseError if ldp backend errors', async () => {
                mockAxios.onGet().reply(400);

                await expect(ldp.getFile('foo'))
                .to.eventually.be.rejectedWith(DatabaseError);
            });
        });

        describe('patchFileMetadata()', () => {
            // most relevant tests are under ldp.update()
            it('should path file metadata', async () => {
                sandbox.stub(ldp, 'update').resolves('foo');
                await expect(ldp.updateFileMetadata('bar', model, fileDoc))
                .to.eventually.equal('foo');
            });
        });
    });

    describe('Versioning', () => {
        describe('storeSnapshot()', () => {
            it('should post to ldp backend', async () => {
                mockAxios.onPost().reply(201);
                await expect(ldp.storeSnapshot('foo', 'bar')).to.eventually.equal(201);
            });

            it('should throw DatabaseError if ldp backend errors', async () => {
                mockAxios.onPost().reply(400);
                await expect(ldp.storeSnapshot('foo', 'bar')).to.eventually.be.rejectedWith(DatabaseError);
            });
        });

        describe('deleteSnapshot()', () => {
            it('should delete from ldp backend', async () => {
                mockAxios.onDelete().reply(204);
                await expect(ldp.deleteSnapshot('foo', 'bar')).to.eventually.equal(204);
            });

            it('should throw NotFoundError if ldp returns 404', async () => {
                mockAxios.onDelete().reply(404);
                await expect(ldp.deleteSnapshot('foo', 'bar')).to.eventually.be.rejectedWith(NotFoundError);
            });

            it('should throw DatabaseError if ldp throws non-404 error', async () => {
                mockAxios.onDelete().reply(400);
                await expect(ldp.deleteSnapshot('foo', 'bar')).to.eventually.be.rejectedWith(DatabaseError);
            });
        });

        describe('getSnapshotLabels()', () => {
            it('should return list of version labels from ldp', async () => {
                // adapted from fedora docs
                /* tslint:disable */
                const triples = `
<http://localhost:8080/rest/path/to/resource> <http://fedora.info/definitions/v4/repository#hasVersion> <http://localhost:8080/rest/path/to/resource/fcr:versions/0> .
<http://localhost:8080/rest/path/to/resource/fcr:versions/0> <http://fedora.info/definitions/v4/repository#hasVersionLabel> "v0"^^<http://www.w3.org/2001/XMLSchema#string> .
<http://localhost:8080/rest/path/to/resource/fcr:versions/0> <http://fedora.info/definitions/v4/repository#created> "2014-12-03T23:55:38.47Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> .

<http://localhost:8080/rest/path/to/resource> <http://fedora.info/definitions/v4/repository#hasVersion> <http://localhost:8080/rest/path/to/resource/fcr:versions/1> .
<http://localhost:8080/rest/path/to/resource/fcr:versions/1> <http://fedora.info/definitions/v4/repository#hasVersionLabel> "v1"^^<http://www.w3.org/2001/XMLSchema#string> .
<http://localhost:8080/rest/path/to/resource/fcr:versions/1> <http://fedora.info/definitions/v4/repository#created> "2014-12-03T23:56:12.863Z"^^<http://www.w3.org/2001/XMLSchema#dateTime> .`
                /* tslint:enable */

                mockAxios.onGet().reply(200, triples);
                const labels = await ldp.getSnapshotLabels('foo');
                expect(labels).to.have.length(2);
                expect(labels).to.include('v0');
                expect(labels).to.include('v1');
            });

            it('should return empty list if there are no versions', async () => {
                mockAxios.onGet().reply(404);
                const labels = await ldp.getSnapshotLabels('foo');
                expect(labels).to.have.length(0);
            });

            it('should throw DatabaseError if ldp backend errors', async () => {
                mockAxios.onGet().reply(400);
                await expect(ldp.getSnapshotLabels('foo')).to.eventually.be.rejectedWith(DatabaseError);
            });

            it('should throw FramingError if ldp data is not compatible with frame', async () => {
                mockAxios.onGet().reply(200, '');
                await expect(ldp.getSnapshotLabels('foo')).to.eventually.be.rejectedWith(FramingError);
            });
        });

        describe('snapshotLabelExists', () => {
            it('should return true if label exists', async () => {
                sandbox.stub(ldp, 'getSnapshotLabels').resolves(['foo']);
                const exists = await ldp.snapshotLabelExists('foo', 'foo');
                expect(exists).to.equal(true);
            });

            it('should return false if label does not exist', async () => {
                sandbox.stub(ldp, 'getSnapshotLabels').resolves(['foo']);
                const exists = await ldp.snapshotLabelExists('foo', 'bar');
                expect(exists).to.equal(false);
            });
        });
    });
});
