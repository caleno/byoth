import 'source-map-support/register';
import * as http from 'http';
import * as express from 'express';
import { Express } from 'express';
import * as helmet from 'helmet';
import * as appRoot from 'app-root-path';
import * as logger from 'morgan';
import * as bodyParser from 'body-parser';

import { Provider } from 'nconf';

import { initNconf } from './boot/nconf';
import { initAuth } from './boot/auth';
import { initErrorHandler } from './boot/error-handler';

import { setupApiRoutes } from './routes/setup-api-routes';
import { initServices } from './services';

if (process.env.NODE_ENV && !process.env.NODE_ENV.match(/(test|development|production)/)) {
    console.error('BYOTh only supports the NODE_ENV values test, development or production');
    process.exit(1);
}

const config = initNconf();

config.required(['byoth:host', 'byoth:port']);

const host = config.get('byoth:host');
const port = config.get('byoth:port');

function initExpress(config: Provider) {
    const app = express();

    app.set('host', host);
    app.set('env', process.env.NODE_ENV);

    app.set('views', appRoot + '/server/views');
    app.set('view engine', 'pug');

    if (process.env.NODE_ENV !== 'test') {
        app.use(logger('dev'));
    }

    app.use(bodyParser.json({ limit: '1mb'}));
    app.use(bodyParser.json({ limit: '1mb', type: 'application/ld+json'}));
    app.use(bodyParser.urlencoded({ extended: false }));

    app.use(helmet({frameguard: false}));

    return app;
}

export async function createApp() {
    const app = initExpress(config);

    const services = await initServices(config);
    await initAuth(config, app, services.ldp).catch(console.error);
    await setupApiRoutes(app, services);
    initErrorHandler(app);

    return app;
}

export async function start(app: Express) {
    let server;

    server = http.createServer(app)
        .listen(port, () => console.log(`BYOTh running on http://${host}:${port}`));

    server.on('error', error => {
        console.error('UNCAUGHT ERROR:', error);
    });

    return server;
}

