import * as jwt from 'jsonwebtoken';
import { initNconf } from '../boot/nconf';
import { initRedis } from '../boot/redis';
import { initRedlock } from '../boot/redlock';
import { initLdp } from '../boot/ldp';

const argv = require('minimist')(process.argv.slice(2));

const config = initNconf();

config.required(['byoth:jwtSecret']);

const secret = config.get('byoth:jwtSecret');

if (!argv['sub'] || argv.length > 1) {
    console.error(`Usage: create-jwt.js --sub <clientId>`);
    process.exit(0);
}

const redis = initRedis(config);
const ldp = initLdp(config, initRedlock(config, redis));
ldp.exists(argv['sub'])
.then(exists => {
    if (!exists) {
        throw new Error('Unknown userid: ' + argv['sub']);
    }
    console.log('Token: ' + jwt.sign({sub: argv['sub']}, secret));
    process.exit(0);
})
.catch(err => {
    console.error(err.message);
    process.exit(1);
});
