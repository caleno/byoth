import { initNconf } from '../boot/nconf';
import { initRedis } from '../boot/redis';
import { initRedlock } from '../boot/redlock';
import { initLdp } from '../boot/ldp';
import { UiBInstitution } from '../lib/uib-institution';
import { ThesisContainerAdapter } from '../lib/thesis-container-adapter';
import { ApprovalIndex } from '../lib/approval-index';

const config = initNconf();
const redis = initRedis(config);
const redlock = initRedlock(config, redis);
const ldp = initLdp(config, redlock);
const approvalIndex = new ApprovalIndex(redis);
const institution = new UiBInstitution(config, redis);
const containerAdapter = new ThesisContainerAdapter(ldp, redlock, institution);

containerAdapter.getAllContainerIds()
    // update index of which theses have been approved
    .then(ids => {
        let promise = Promise.resolve();

        console.log('Indexing', ids.length, 'theses');

        // do it sequentially in order not to overload database
        ids.forEach(id =>
            promise = promise
                .then(() => containerAdapter.getContainer(id))
                .then(container => approvalIndex.insert(container))
        );

        return promise;
    })
    // update cache of institution data
    .then(() => institution.init())
    .then(() => process.exit(0))
    .catch(err => {
        console.log('ERROR', err);
        process.exit(1);
    });
