import { ClientAuth } from '../lib/client-auth';
import { initNconf } from '../boot/nconf';
import { initRedis } from '../boot/redis';
import { initRedlock } from '../boot/redlock';
import { initLdp } from '../boot/ldp';

const argv = require('minimist')(process.argv.slice(2));

if (!argv['client'] || !argv['permission'] ||
    !(argv['grant'] || argv['revoke']) ||
    argv.length > 3 ||
    !argv['permission'].match(/(readProof|writeProof|readExport)/)) {
    console.error('Usage: node modify-client-permission.js ' +
        '--client <client id>' +
        ' --permission <readProof|writeProof|readExport> ' +
        '[ --grant | --revoke ]');
    process.exit(1);
}

const config = initNconf();
const redis = initRedis(config);
const ldp = initLdp(config, initRedlock(config, redis));
const auth = new ClientAuth(config, ldp);

const verb = argv['grant'] ? 'grant' : 'revoke';

auth.modifyClientPermission(argv['client'], argv['permission'], verb)
.then(() => process.exit(0))
.catch(err => {
    console.error(err.message);
    process.exit(1);
});
