import { ClientAuth } from '../lib/client-auth';
import { initNconf } from '../boot/nconf';
import { initRedis } from '../boot/redis';
import { initRedlock } from '../boot/redlock';
import { initLdp } from '../boot/ldp';

if (process.argv.length !== 3) {
    console.error('Usage: create-client.js <client-name>');
    process.exit(1);
}

const name = process.argv[2];

const config = initNconf();
const redis = initRedis(config);
const ldp = initLdp(config, initRedlock(config, redis));
const auth = new ClientAuth(config, ldp);

auth.createClient(name)
.then(client => {
    console.log('Client id: ', client['@id']);
    process.exit(0);
})
.catch(err => {
    console.error(err.message);
    process.exit(1);
});
