MOCHA="$(npm bin)/mocha --opts ./mocha.opts"

export BYOTH_CONFIG_FILE="../config.test.json"

RUN_UNIT=0
RUN_INTEGRATION=0
args=("$@")

if [ $# -eq 0 ]; then
    RUN_UNIT=1
    RUN_INTEGRATION=1
elif [ $# -gt 0 ]; then
    if [ "${args[0]}" == "unit" ]; then
        RUN_UNIT=1
    elif [ "${args[0]}" == "integration" ]; then
        RUN_INTEGRATION=1
    else
        echo "Invalid argument '$@'"
        exit 1
    fi
fi

tsc -p ./tsconfig.json

RET=$?
if [ $RET -ne 0 ]; then
    exit $RET
fi

if [ $RUN_UNIT -eq 1 ]; then
    echo
    echo "## Unit tests ##"
    echo
    ${MOCHA} './**/*.spec.ts' '../model/**/*.spec.ts'
    RET=$?
    if [ $RET -ne 0 ]; then
        exit $RET
    fi
fi

if [ $RUN_INTEGRATION -eq 1 ]; then
    echo
    echo "## Integration tests ##"
    echo
    ${MOCHA} --timeout 5000 './integration-tests/**/*.ts'
    RET=$?
    if [ $RET -ne 0 ]; then
        exit $RET
    fi
fi

