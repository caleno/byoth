import * as chai from 'chai';
import * as request from 'supertest';
import * as sinon  from 'sinon';
import * as base64url from 'base64-url';
import * as uuid from 'uuid/v4';

import { _helpers } from '../src/middleware/authorize';
import { userFromRequest } from '../src/lib/user';
import { setup } from './setup';

const expect = chai.expect;

describe('User API', () => {
    let server;
    let sandbox;
    let user = { id: 'foobar', name: 'Foo Bar' };

    before(() => {
        server = setup.server;
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        user = { id: uuid(), name: 'Foo Bar' };
        sandbox.stub(userFromRequest, 'getUser').returns(JSON.parse(JSON.stringify(user)));
        sandbox.stub(userFromRequest, 'getInternalUser').returns(JSON.parse(JSON.stringify(user)));
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('user/', () => {
        const path = '/api/user';

        it('should deny unauthenticated requests', async () => {
            await request(server).head(path).expect(401);
            await request(server).get(path).expect(401);
            await request(server).post(path).expect(401);
            await request(server).put(path).expect(401);
            await request(server).delete(path).expect(401);
        });

        it('should deny unauthenticated requests to sub-paths', async () => {
            await request(server).get(path + '/foo').expect(401);
            await request(server).get(path + '/container').expect(401);
        });

        it('GET should return current user', async () => {
            sandbox.stub(_helpers, 'isAuthenticated').yields(null);

            await request(server).get(path)
            .expect(200)
            .expect(res => {
                expect(res.body).to.have.property('id', user.id);
                expect(res.body).to.not.have.property('containerId');
            });
        });
    });

    describe('user/container', () => {
        const path = '/api/user/container';

        beforeEach(() => {
            sandbox.stub(_helpers, 'isAuthenticated').yields(null);
        });

        it('HEAD should return 204 if user has no container', async () => {
            await request(server).head(path).expect(204);
        });

        it('GET should return 404 if user has no container', async () => {
            await request(server).get(path).expect(404);
        });

        it('POST should create new container', async () => {
            let id;
            let location

            await request(server).post(path)
            .expect(201)
            .expect(res =>{
                expect(res.body).to.have.property('containerId');
                id = res.body.containerId;
                location = base64url.encode(id);
            });

            await request(server).head(path)
            .expect(200);

            await request(server).get(path)
            .expect(200)
            .expect(res => expect(res.text).to.equal(id));

            await request(server).get('/api/user')
            .expect(200)
            .expect(res => expect(res.body).to.have.property('containerId', id));

            await request(server).delete('/api/container/' + location).expect(204);

            await request(server).head(path).expect(204);

            await request(server).get('/api/user')
            .expect(200)
            .expect(res => expect(res.body).not.to.have.property('containerId'));
        });
    });
});
