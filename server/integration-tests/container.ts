import * as chai from 'chai';
import * as request from 'supertest';
import * as sinon  from 'sinon';

import * as fs from 'fs';
import * as config from 'nconf';
import * as base64url from 'base64-url';
import * as btoa from 'btoa';
import * as uuid from 'uuid/v4';

import { _helpers } from '../src/middleware/authorize';
import { userFromRequest } from '../src/lib/user';

import { getModel, createModelDocument } from 'model/index';
import { initOrderOffer, initOrderCustomer, initOrderDelivery, MANDATORY_COPIES } from 'model/order';
import { PrintFormats, PublishThesis, DegreeTypes, ThesisTypes } from 'model/thesis';

import { setup } from './setup';

const expect = chai.expect;

/** File that is accepted by PDF validation mockup */
const workingFile = __dirname + '/data/four-pages-one-color.pdf';

/** File that is not accepted by PDF validation mockup */
const brokenFile = __dirname + '/data/not-a-pdf.pdf';

const model = getModel(config.get('byoth:ldp:root'));

async function setupISBN() {
    const isbnConfig = config.get('byoth:isbn');
    const url = isbnConfig.protocol + '://' + isbnConfig.host + ':' + isbnConfig.port;
    const list = [ '9780596520687', '0596520689' ];

    await Promise.all(
        list.map(isbn => {
            return request(url).post('/api/isbn/release/' + isbn)
                .set('Authorization', 'Bearer ' + isbnConfig.token)
                // We release all isbns, also those that haven't been used, so that we don't get problems when tests fail.
                // Which is why we catch 404s here when releasing unused numbers.
                .catch(console.error);
        }
    ));

    await request(url).put('/api/isbn/')
    .set('Authorization', 'Bearer ' + isbnConfig.token)
    .send(list)
    .expect(200);
}

describe('Container API', () => {
    let server;
    let sandbox;
    let getUser;
    let getInternalUser;
    let user;

    before(() => {
        server = setup.server;
    });

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        user = { id: uuid(), name: 'Foo Bar' };

        getUser = sandbox.stub(userFromRequest, 'getUser').returns(user);
        getInternalUser = sandbox.stub(userFromRequest, 'getInternalUser').returns(user);
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe('/api/container/[:id]', () => {
        const path = '/api/container';

        it('should deny unauthenticated HEAD/GET/POST/PUT/DELETE requests', async () => {
            await request(server).head(path).expect(401);
            await request(server).get(path).expect(401);
            await request(server).post(path).expect(401);
            await request(server).put(path).expect(401);
            await request(server).delete(path).expect(401);
        });

        it('should deny unauthenticated GET requests to subpaths', async () => {
            await request(server).get(path + '/foo').expect(401);
        });

        it('should allow authenticated user to POST/GET/HEAD/DELETE container', async () => {
            sandbox.stub(_helpers, 'isAuthenticated').yields(null);
            let encodedContainerId: string;

            await request(server).post(path)
            .expect(201)
            .expect(res => {
                expect(res.body.creator).to.contain(user.id);
                encodedContainerId = base64url.encode(res.body['@id']);
            });

            await request(server).get(path + '/' + encodedContainerId)
            .expect(200)
            .expect(res => {
                expect(encodedContainerId).to.equal(base64url.encode(res.body['@id']), 'Should get the right document');
            });

            await request(server).delete(path + '/' + encodedContainerId).expect(204);
            await request(server).get(path + '/' + encodedContainerId).expect(410);
        });

        it('should return same container on repeated POSTs', async () => {
            sandbox.stub(_helpers, 'isAuthenticated').yields(null);
            let id;

            await request(server).post(path)
            .expect(201)
            .then(res => {
                id = res.body['@id'];
            });

            await request(server).post(path)
            .expect(201)
            .expect(res => {
                expect(res.body).to.have.property('@id', id);
            });
        });

        it('should return 404 for PATCH/PUT', async () => {
            sandbox.stub(_helpers, 'isAuthenticated').yields(null);

            let encodedId: string;

            await request(server).post(path)
            .expect(201)
            .then(res => {
                encodedId = base64url.encode(res.body['@id']);
            });

            await request(server).patch(path + '/' + encodedId).expect(404);
            await request(server).put(path + '/' + encodedId).expect(404);

            await request(server).delete(path + '/' + encodedId).expect(204);
        });

        it('should deny access to non-owner requests', async () => {
            sandbox.stub(_helpers, 'isAuthenticated').yields(null);
            let encodedContainerId: string;

            await request(server).post(path)
            .expect(201)
            .expect(res => encodedContainerId = base64url.encode(res.body['@id']));

            const otherUser = { id: uuid(), name: 'Bar Foo' };
            getUser.returns(otherUser);
            getInternalUser.returns(otherUser);

            await request(server).head(path + '/' + encodedContainerId).expect(401);
            await request(server).get(path + '/' + encodedContainerId).expect(401);
            await request(server).delete(path + '/' + encodedContainerId).expect(401);

            getUser.returns(user);
            getInternalUser.returns(user);
            await request(server).delete(path + '/' + encodedContainerId).expect(204);
        });
    })
    describe('/api/container/:id', () => {
        let encodedContainerId;
        let path;

        beforeEach(async () => {
            sandbox.stub(_helpers, 'isAuthenticated').yields(null);

            await request(server).post('/api/container')
            .expect(201)
            .then(res => {
                encodedContainerId = base64url.encode(res.body['@id']);
                path = '/api/container/' + encodedContainerId;
            });
        });

        afterEach(async () => {
            await request(server).delete(path).expect(204);
            encodedContainerId = null;
            path = null;
        });

        ['candidate', 'defense', 'department', 'files', 'finalOrder', 'evaluationOrder', 'thesis', 'trialLecture' ]
        .forEach(property => {
            describe('/' + property + '/', () => {
                it('GET/PUT should deny access to non-owners', async() => {
                    const otherUser = { id: uuid(), name: 'Bar Foo' };
                    getUser.returns(otherUser);
                    getInternalUser.returns(otherUser);

                    await request(server).get(path + '/' + property).expect(401);
                    await request(server).put(path + '/' + property).send({}).expect(401);

                    getUser.returns(user);
                    getInternalUser.returns(user);
                });

                it('GET should return document of right type', async () => {
                    await request(server).get(path + '/' + property)
                    .expect(200)
                    .expect(res => {
                        expect(res.body['@type']).to.contain(model[property].frame['@type']);
                    });
                });

                it('should allow writing document to pristine container', async () => {
                    const document = createModelDocument(model[property], base64url.decode(encodedContainerId) + '/' + property);

                    await request(server).put(path + '/' + property)
                    .send(document)
                    .expect(200)
                    .expect(res => {
                        expect(res.body['@type']).to.contain(model[property].frame['@type']);
                    });
                });

                it('PUT should fail if document[@id] does not match request path', async () => {
                    const document = createModelDocument(model[property], 'foo');

                    await request(server).put(path + '/' + property)
                    .send(document)
                    .expect(403);
                });

                it('PUT should ignore fields not defined in frame', async () => {
                    const res = await request(server).get(path + '/' + property).expect(200);
                    const document = res.body;
                    document.foo = 'bar';

                    await request(server).put(path + '/' + property)
                    .send(document)
                    .expect(200)
                    .expect(res => {
                        expect(res.body).not.to.have.property('foo');
                    });
                });

                it('PUT should choke on documents with wrong type', async () => {
                    const document = createModelDocument(model[property], base64url.decode(encodedContainerId) + '/' + property);
                    document['@type'] = 'byoth:foo';

                    await request(server)
                    .put(path + '/' + property)
                    .send(document)
                    .expect(422);
                });
            });
        });

        it('/foo GET/POST/PUT/DELETE should give 404', async () => {
            await request(server).get(path + '/foo').expect(404);
            await request(server).post(path + '/foo').expect(404);
            await request(server).put(path + '/foo').expect(404);
            await request(server).delete(path + '/foo').expect(404);
        });

        describe('/articles/[:articleId]', () => {
            it('should return empty list for GET requests to pristine container', async () => {
                await request(server).get(path + '/articles')
                .expect(200)
                .expect(res => {
                    expect(res.body.items).to.have.length(0);
                });
            });

            it('POST should allow adding articles', async () => {
                await request(server).post(path + '/articles')
                .expect(201);
            });

            it('PUT/DELETE should allow updating and deleting articles', async () => {
                let encodedId;
                let article;

                await request(server).post(path + '/articles')
                .expect(201)
                .expect(res => article = res.body);

                article.title = 'foo';
                encodedId = base64url.encode(article['@id']);

                await request(server).put(path + '/articles/' + encodedId)
                .send(article)
                .expect(200)
                .expect(res => {
                    expect(res.body.title).to.equal(article.title);
                });

                await request(server).get(path + '/articles')
                .expect(200)
                .expect(res => {
                    expect(res.body.items[0].title).to.equal(article.title);
                });

                await request(server).delete(path + '/articles/' + encodedId)
                .expect(204);
            });

            it('POST should allow posting several articles', async () => {
                await request(server).post(path + '/articles').expect(201);
                await request(server).post(path + '/articles').expect(201);
                await request(server).post(path + '/articles').expect(201);

                await request(server).get(path + '/articles')
                .expect(200)
                .expect(res => {
                    expect(res.body.items.length).to.equal(3);
                });
            });

            it('PUT should ignore fields that are not in the article model', async () => {
                let article;
                await request(server).post(path + '/articles')
                .expect(201)
                .expect(res => article = res.body);

                const encodedId = base64url.encode(article['@id']);

                article.foo = 'bar';

                await request(server).put(path + '/articles/' + encodedId)
                .send(article)
                .expect(200)
                .expect(res => {
                    expect(res.body).to.not.have.property('foo');
                });
            });

            describe('/file', () => {
                it('POST/DELETE should upload file for article and remove it', async () => {
                    let encodedArticleId;

                    await request(server).post(path + '/articles')
                    .expect(201)
                    .expect(res => encodedArticleId = base64url.encode(res.body['@id']));

                    await request(server).post(path + '/articles/' + encodedArticleId + '/file')
                    .attach('file', workingFile)
                    .expect(201);

                    await request(server).get(path + '/articles/' + encodedArticleId)
                    .expect(200)
                    .expect(res => expect(res.body).to.have.property('hasFile'));

                    await request(server).delete(path + '/articles/' + encodedArticleId + '/file').expect(204);

                    await request(server).get(path + '/articles/' + encodedArticleId)
                    .expect(200)
                    .expect(res => expect(res.body).to.not.have.property('hasFile'));
                });

                it('POST should replace file for article if it already has one', async () => {
                    let articleId, firstFile;

                    await request(server).post(path + '/articles')
                    .expect(201)
                    .expect(res => {
                        articleId = res.body['@id'];
                    });

                    const encodedArticleId = base64url.encode(articleId);

                    await request(server).post(path + '/articles/' + encodedArticleId + '/file')
                    .attach('file', workingFile)
                    .expect(201);

                    await request(server).get(path + '/articles/' + encodedArticleId)
                    .expect(200)
                    .expect(res => firstFile = res.body.hasFile);

                    await request(server).post(path + '/articles/' + encodedArticleId + '/file')
                    .attach('file', workingFile)
                    .expect(201);

                    await request(server).get(path + '/articles/' + encodedArticleId)
                    .expect(200)
                    .expect(res => {
                        expect(res.body).to.have.property('hasFile').that.does.not.equal(firstFile);
                    });

                    await request(server).get(path + '/files/' + base64url.encode(firstFile)).expect(410);
                });

                it('POST should upload file and deleting article should also delete file', async () => {
                    let articleId, fileId;

                    await request(server).post(path + '/articles')
                    .expect(201)
                    .expect(res => {
                        articleId = res.body['@id'];
                    });

                    const encodedArticleId = base64url.encode(articleId);

                    await request(server).post(path + '/articles/' + encodedArticleId + '/file')
                    .attach('file', workingFile)
                    .expect(201);

                    await request(server).get(path + '/articles/' + encodedArticleId)
                    .expect(200)
                    .expect(res => {
                        expect(res.body).to.have.property('hasFile');
                        fileId = res.body.hasFile;
                    });

                    await request(server).get(path + '/files/' + base64url.encode(fileId)).expect(200);

                    await request(server).delete(path + '/articles/' + encodedArticleId).expect(204);

                    await request(server).get(path + '/files/' + base64url.encode(fileId)).expect(410);
                });

                it('DELETE for linked file should only remove link', async () => {
                    let fileId, article;

                    await request(server).post(path + '/inputFiles')
                    .attach('file', workingFile)
                    .expect(201)
                    .expect(res => fileId = res.body['@id']);

                    await request(server).post(path + '/articles')
                    .expect(201)
                    .expect(res => article = res.body);

                    const encodedId = base64url.encode(article['@id']);
                    article.inFile = fileId;

                    await request(server).put(path + '/articles/' + encodedId).send(article).expect(200);

                    await request(server).delete(path + '/articles/' + encodedId + '/file').expect(204)

                    await request(server).delete(path + '/articles/' + encodedId).expect(204);
                });
            });
        });

        describe('/inputFiles/[:fileId]', () => {
            async function postInputFile(testFilePath): Promise<any[]> {
                return request(server).post(path + '/inputFiles')
                .attach('file', testFilePath)
                .expect(201)
                .then(res => {
                    return [base64url.encode(res.body['@id']), res.body];
                });
            }

            async function testFile(testFilePath: string) {
                const testFile = fs.readFileSync(testFilePath);

                const res = await postInputFile(testFilePath);
                const fileId = res[0];
                const metadata = res[1];

                await request(server).get(path + '/inputFiles')
                .expect(200)
                .expect(res => {
                    expect(res.body.items.length).to.equal(1, 'should have metadata for one file');
                    expect(res.body.items[0]['@id']).to.equal(metadata['@id'], 'should be metadata for correct file');
                });

                await request(server).get(path + '/files/' + fileId)
                .expect(200)
                .expect(res => {
                    res.body.length === testFile.length;
                });

                await request(server).delete(path + '/inputFiles/' + fileId).expect(204);
            }

            it('should allow posting file, getting it back and deleting it', async () => {
                await testFile(workingFile);
            });

            it('should allow posting broken file, getting it back and deleting it', async () => {
                await testFile(brokenFile);
            });

            it('/:fileId DELETE should delete file and remove it from input file list', async () => {
                const [encodedId] = await postInputFile(workingFile);

                await request(server).delete(path + '/inputFiles/' + encodedId).expect(204);

                await request(server).get(path + '/inputFiles')
                .expect(200)
                .expect(res => {
                    expect(res.body.items).to.have.length(0);
                });
            });

            it('/order should allow reordering', async () => {
                const first = await postInputFile(workingFile);
                const second = await postInputFile(brokenFile);

                await request(server).get(path + '/inputFiles')
                .expect(200)
                .expect(res => {
                    expect(res.body.items.length).to.equal(2);
                    expect(res.body.items[0]['@id']).to.equal(first[1]['@id']);
                    expect(res.body.items[1]['@id']).to.equal(second[1]['@id']);
                });

                await request(server).put(path + '/inputFiles/order')
                .send([second[1]['@id'], first[1]['@id']])
                .expect(200)

                await request(server).get(path + '/inputFiles')
                .expect(200)
                .expect(res => {
                    expect(res.body.items.length).to.equal(2);
                    expect(res.body.items[0]['@id']).to.equal(second[1]['@id']);
                    expect(res.body.items[1]['@id']).to.equal(first[1]['@id']);
                });
            });

            it('/order should fail if input has unknown ids', async () => {
                const first = await postInputFile(workingFile);
                const second = await postInputFile(brokenFile);

                await request(server).get(path + '/inputFiles')
                .expect(200)
                .expect(res => {
                    expect(res.body.items.length).to.equal(2);
                    expect(res.body.items[0]['@id']).to.equal(first[1]['@id']);
                    expect(res.body.items[1]['@id']).to.equal(second[1]['@id']);
                });

                await request(server).put(path + '/inputFiles/order')
                .send([second[1]['@id'], first[1]['@id'], 'foo'])
                .expect(422);

                await request(server).get(path + '/inputFiles')
                .expect(200)
                .expect(res => {
                    expect(res.body.items.length).to.equal(2);
                    expect(res.body.items[0]['@id'], 'order unchanged').to.equal(first[1]['@id']);
                    expect(res.body.items[1]['@id'], 'order unchanged').to.equal(second[1]['@id']);
                });
            });

            it('/submit should return validation report', async () => {
                await postInputFile(workingFile);
                await postInputFile(brokenFile);

                await request(server).post(path + '/inputFiles/submit?type=final')
                .expect(200)
                .expect(res => {
                    expect(res.body).to.have.length(2);
                    expect(res.body[0]).to.have.property('numPages', 4);
                    expect(res.body[0]).to.have.property('numColorPages', 1);
                    expect(res.body[0]).to.have.property('status', 'byoth:fileOk');
                    expect(res.body[0].errors).to.have.length(0, 'should not have errors');
                    expect(res.body[0].warnings).to.have.length(0, 'should not have warnings');
                    expect(res.body[1]).to.have.property('status', 'byoth:fileError');
                    expect(res.body[1].errors).to.have.length(1, 'should have errors');
                    expect(res.body[1].warnings).to.have.length(0, 'should not have warnings');
                });
            });

            it('/submit should require type parameter', async () => {
                await request(server).post(path + '/inputFiles/submit?type=foo')
                .expect(400);

                await request(server).post(path + '/inputFiles/submit')
                .expect(400);
            });

            it('/:fileId/replace should replace file', async () => {
                let newId;
                const [oldEncodedLocation, oldMetadata] = await postInputFile(workingFile);
                const [ , otherMetadata] = await postInputFile(workingFile);

                const originalFileListResponse = await request(server).get(path + '/inputFiles');
                const originalItems = originalFileListResponse.body.items;

                // check ordering of original files
                expect(originalItems[0]).to.have.property('@id', oldMetadata['@id']);
                expect(originalItems[1]).to.have.property('@id', otherMetadata['@id']);

                await request(server).post(path + '/inputFiles/' + oldEncodedLocation + '/replace')
                .attach('file', workingFile)
                .expect(201)
                .expect(res => {
                    newId = res.body['@id'];
                });

                const updatedFileListResponse = await request(server).get(path + '/inputFiles');
                const updatedItems = updatedFileListResponse.body.items;

                // check ordering of replaced files
                expect(updatedItems[0]).to.have.property('@id', newId);
                expect(updatedItems[1]).to.have.property('@id', otherMetadata['@id']);
            });
        });

        describe('/final[Order]/', () => {
            let thesis, order, candidate, defense;

            beforeEach(() => {
                order = createModelDocument(
                    model.finalOrder,
                    base64url.decode(encodedContainerId) + '/finalOrder',
                    { coverPreviewAccepted: true }
                );
                initOrderOffer(order);
                initOrderCustomer(order);
                initOrderDelivery(order);
                order.offer.quantity = MANDATORY_COPIES + 1;
                order.customer = {
                    ...order.customer,
                    name: 'Foo Bar',
                    telephone: '1234',
                    email: 'foo@bar.com'
                };
                order.delivery.address = {
                    ...order.delivery.address,
                    name: 'Foo Bar',
                    postalCode: '1234',
                    city: 'Baz',
                    streetAddress: 'Foo-street 1'
                };

                thesis = createModelDocument(
                    model.thesis,
                    base64url.decode(encodedContainerId) + '/thesis',
                    {
                        title: 'foo bar',
                        subtitle: '123',
                        publish: PublishThesis.WITH_EMBARGO,
                        license: 'http://license.com',
                        embargo: 6,
                        thesisType: ThesisTypes.COLLECTION,
                        degreeType: DegreeTypes.PHD,
                        printFormat: PrintFormats.B17x24,
                        abstract: { en: 'English abstract' }
                    }
                );


                candidate = createModelDocument(
                    model.candidate,
                    base64url.decode(encodedContainerId) + '/candidate',
                    { familyName: 'Foo', givenName: 'Bar' }
                );

                defense = createModelDocument(
                    model.defense,
                    base64url.decode(encodedContainerId) + '/defense',
                    { start: '1999' }
                );
            });

            async function setupContainer() {
                await setupISBN();

                await Promise.all([
                    request(server).put(path + '/candidate').send(candidate).expect(200),
                    request(server).put(path + '/defense').send(defense).expect(200),
                    request(server).put(path + '/finalOrder').send(order).expect(200),
                    request(server).put(path + '/thesis').send(thesis).expect(200),
                    request(server).post(path + '/inputFiles').attach('file', workingFile).expect(201)
                ]);

                await Promise.all([
                    request(server).post(path + '/inputFiles/submit?type=final').expect(200),
                    request(server).post(path + '/thesis/assign-isbn').expect(200),
                    request(server).post(path + '/final/price-request').send(order).expect(200),
                ]);
            }

            it('should have no offer information on a pristine container', async () => {
                await request(server).get(path + '/finalOrder')
                .expect(200)
                .expect(res => {
                    expect(res.body).not.to.have.property('offer');
                });
            });

            it('/price-request should give a price estimate, and it should be stored in the order for future requests', async () => {
                let price;

                await request(server).post(path + '/final/price-request')
                .send(order)
                .expect(200)
                .expect(res => {
                    expect(res.body).to.have.property('offer');
                    expect(res.body.offer).to.have.property('price').that.matches(/\d+ \w+/);
                    price = res.body.offer.price;
                });

                await request(server).get(path + '/finalOrder')
                .expect(200)
                .expect(res => {
                    expect(res.body).to.have.property('offer');
                    expect(res.body.offer).to.have.property('price', price);
                    expect(res.body.offer).not.to.have.property('submitted');
                    expect(res.body.offer).not.to.have.property('submissionDate');
                });
            });

            describe('/validate', () => {
                it('POST should succeed on valid order', async () => {
                    await setupContainer();

                    await request(server).post(path + '/final/validate')
                    .expect(200)
                    .expect(res => expect(res.body).to.have.property('valid', true));
                }).timeout(10000); // sometimes slow in CI

                it('POST should fail on invalid order', async () => {
                    thesis.title = '';
                    await setupContainer();

                    await request(server).post(path + '/final/validate')
                    .expect(200)
                    .expect(res => expect(res.body).to.have.property('valid', false));
                });
            });

            describe('/submit', () => {
                it('POST should fail if order invalid', async () => {
                    order.quantity = 0;
                    thesis.title = '';
                    await setupContainer();
                    await request(server).post(path + '/final/submit').expect(409);
                });

                it('POST should succeed and set confirmation number if order complete', async () => {
                    await setupContainer();

                    await request(server).post(path + '/final/submit')
                    .expect(200)
                    .expect(res => {
                        expect(res.body).to.have.property('offer');
                        expect(res.body).to.have.property('orderNumber');
                    });
                }).timeout(10000); // sometimes slow in CI

                it('DELETE should fail if order does not have a order number', async () => {
                    await setupContainer();
                    await request(server).delete(path + '/final/submit').expect(409);
                });

                it('DELETE should unset order number upon success', async () => {
                    await setupContainer();

                    await request(server).post(path + '/final/submit')
                    .expect(200);

                    await request(server).delete(path + '/final/submit')
                    .expect(204)
                    .expect(res => {
                        expect(res.body).to.not.have.property('orderNumber');
                    });
                }).timeout(10000); // sometimes slow in CI

                it('/submit?isTest=true POST should send test submission', async () => {
                    await setupContainer();

                    await request(server).post(path + '/final/submit')
                    .query({isTest: true})
                    .expect(200)
                    .expect(res => {
                        expect(res.body).to.have.property('isTest', true);
                    });
                }).timeout(10000); // sometimes slow in CI
            });
        });

        describe('/final/cover-preview', async () => {
            it('/pdf should generate a response with a base64-encoded file', async () => {
                await request(server).get(path + '/final/cover-preview/pdf?regenerate=true')
                .expect(200)
                .expect(res => {
                    try {
                        btoa(res.body);
                    } catch(err) {
                        console.error(err);
                        expect(false, 'should be able to decode string').to.equal(true);
                    }
                });
            });

            it('/pdf should give a different output if thesis title changes', async () => {
                let pdf;

                await request(server).get(path + '/final/cover-preview/pdf?regenerate=true')
                .expect(200)
                .then(res => pdf = res.body);

                await request(server).get(path + '/thesis')
                .expect(200)
                .then(res => {
                    const thesis = res.body;
                    thesis.title = 'Foo';
                    return request(server).put(path + '/thesis')
                    .send(thesis)
                    .expect(200);
                });

                await request(server).get(path + '/final/cover-preview/pdf?regenerate=true')
                .expect(200)
                .expect(res => {
                    expect(res.body).not.to.equal(pdf);
                });
            });

            it('/png should generate a response with 4 base64-encoded files', async () => {
                await request(server).get(path + '/final/cover-preview/png?regenerate=true')
                .expect(200);;
            });

            it('/png should give a different output if thesis title changes');
        });

        describe('/thesis/[(un)assign-isbn]', () => {
            beforeEach(async () => {
                setupISBN();
            });

            it('/assign-isbn POST should return a thesis with an ISBN', async () => {
                await request(server).post(path + '/thesis/assign-isbn')
                .expect(200)
                .expect(res => expect(res.body).to.have.property('isbn').that.matches(/(\d){10,13}/))
            });

            it('/assign-isbn POST should not request new ISBN if thesis already has one', async () => {
                let number;

                await request(server).post(path + '/thesis/assign-isbn')
                .expect(200)
                .expect(res => number = res.body.isbn);

                await request(server).post(path + '/thesis/assign-isbn')
                .expect(200)
                .expect(res => expect(res.body).to.have.property('isbn', number));
            });

            it('/unassign-isbn POST should release thesis ISBN if possible', async () => {
                await request(server).post(path + '/thesis/assign-isbn')
                .expect(200);

                await request(server).post(path + '/thesis/unassign-isbn')
                .expect(200)
                .expect(res => expect(res.body).not.to.have.property('isbn'));
            });

            it('/unassign-isbn POST should do nothing if thesis does not have ISBN', async () => {
                await request(server).post(path + '/thesis/unassign-isbn')
                .expect(200)
                .expect(res => expect(res.body).not.to.have.property('isbn'));
            });
        });
    });
});
