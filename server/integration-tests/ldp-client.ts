import * as chai from 'chai';
import * as chaiAsPromised from 'chai-as-promised';
import { Readable } from 'stream';
import * as uuid from 'uuid/v4';

import { LdpClient } from '../src/lib/ldp-client';
import { initRedis } from '../src/boot/redis';
import { initRedlock } from  '../src/boot/redlock';
import { initLdp } from '../src/boot/ldp';
import { initNconf } from '../src/boot/nconf';

import { DatabaseError } from '../src/lib/errors';

import { getModel, ByothModel } from 'model/index';

chai.use(chaiAsPromised);
const expect = chai.expect;

describe('LdpClient', () => {
    let ldp: LdpClient;
    let id: string;
    let model: ByothModel;

    before(() => {
        const config = initNconf();
        const redis = initRedis(config);
        const redlock = initRedlock(config, redis);
        ldp = initLdp(config, redlock);
        model = getModel(ldp.rootUrl);
    });

    beforeEach(async () => {
        const doc = await ldp.post('', model.thesisContainer);
        id = doc['@id'];
    });

    describe('versioning', () => {
        it('should report empty set of snapshots if there are no snapshots', async () => {
            const labels = await ldp.getSnapshotLabels(id);
            expect(labels).to.have.length(0);

            const exists = await ldp.snapshotLabelExists(id, 'foo');
            expect(exists).to.equal(false);
        });

        it('should allowing storing labelled snapshot', async () => {
            await ldp.storeSnapshot(id, 'foo');

            const labels = await ldp.getSnapshotLabels(id);
            expect(labels).to.have.length(1);
            expect(labels).to.include('foo');

            const exists = await ldp.snapshotLabelExists(id, 'foo');
            expect(exists).to.equal(true);
        });

        it('should allowing storing several labelled snapshots', async () => {
            await ldp.storeSnapshot(id, 'foo');
            await ldp.storeSnapshot(id, 'bar');

            const labels = await ldp.getSnapshotLabels(id);
            expect(labels).to.have.length(2);
            expect(labels).to.include('foo');
            expect(labels).to.include('bar');
        });

        it('should not allow storing snapshot with reused label', async () => {
            await ldp.storeSnapshot(id, 'foo');

            await expect(ldp.storeSnapshot(id, 'foo')).to.eventually.be.rejectedWith(DatabaseError);

            const labels = await ldp.getSnapshotLabels(id);
            expect(labels).to.have.length(1);
            expect(labels).to.include('foo');
        });

        it('should allow deleting snapshot', async () => {
            await ldp.storeSnapshot(id, 'foo');
            await ldp.storeSnapshot(id, 'bar');

            let labels = await ldp.getSnapshotLabels(id);
            expect(labels).to.have.length(2);

            await ldp.deleteSnapshot(id, 'foo');

            labels = await ldp.getSnapshotLabels(id);
            expect(labels).to.have.length(1);
            const exists = await ldp.snapshotLabelExists(id, 'foo');
            expect(exists).to.equal(false);
        });
    });

    describe('file handling', () => {
        it('should allow posting file from stream', async () => {
            const stream = new Readable();

            stream._read = () => {}
            stream.push('abc\n');
            stream.push(null);

            const file = {
                stream: stream,
                mimetype: 'text/plain',
                originalname: 'foo'
            };

            const response = await ldp.postFileFromStream('', model.file, file);

            expect(response).to.have.property('size', '4');
        });

        it('should allow posting file from stream with slug', async () => {
            const stream = new Readable();
            const slug = uuid(); // use uuid to avoid tombstones if test is run several times on same LDP

            stream._read = () => {}
            stream.push('abc\n');
            stream.push(null);

            const file = {
                stream: stream,
                mimetype: 'text/plain',
                originalname: 'foo'
            };

            const response = await ldp.postFileFromStream('', model.file, file, slug);

            expect(response).to.have.property('size', '4');
            expect(response).to.have.property('@id', slug);
        });

        it('should transform special characters in filename before storing', async () => {
            const stream = new Readable();

            stream._read = () => {}
            stream.push('abc\n');
            stream.push(null);

            const file = {
                stream: stream,
                mimetype: 'text/plain',
                originalname: 'åäøü.pdf'
            };

            const response = await ldp.postFileFromStream('', model.file, file);

            expect(response).to.have.property('filename').that.matches(/[a-z\-]\.pdf/);
        })
    });
});
