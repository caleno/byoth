import * as chai from 'chai';
import * as request from 'supertest';
import * as sinon  from 'sinon';
import { SinonSandbox } from 'sinon';
import * as config from 'nconf';
import * as uuid from 'uuid/v4';
import * as base64url from 'base64-url';

import { initRedis } from '../src/boot/redis';
import { initRedlock } from '../src/boot/redlock';
import { initLdp } from '../src/boot/ldp';

import { ClientAuth } from '../src/lib/client-auth';
import { ApprovalIndex } from '../src/lib/approval-index';

import { _helpers } from '../src/middleware/authorize';
import { userFromRequest } from '../src/lib/user';

import { setup } from './setup';

const expect = chai.expect;

describe('Approval/Export API', () => {
    let server;
    let token;
    let sandbox: SinonSandbox;
    let user;
    let redis;

    beforeEach(() => {
        server = setup.server;
        sandbox = sinon.createSandbox();
        sandbox.stub(_helpers, 'isAuthenticated').yields(null);
        user = { id: uuid(), name: 'Foo Bar' };

        sandbox.stub(userFromRequest, 'getUser').returns(user);
        sandbox.stub(userFromRequest, 'getInternalUser').returns(user);
    });

    afterEach(() => {
        sandbox.restore();
    });

    before    (async () => {
        redis = initRedis(config);
        const ldp = initLdp(config, initRedlock(config, redis));
        const auth = new ClientAuth(config, ldp);
        // auth.init() not necessary, already done when initializing server
        const client = await auth.createClient('testClient');
        await auth.modifyClientPermission(client['@id'], 'writeProof', 'grant');
        await auth.modifyClientPermission(client['@id'], 'readExport', 'grant');
        token = auth.generateToken(client['@id']);
    });

    beforeEach(async () => {
        const index = new ApprovalIndex(redis);
        await index.clearIndex();
    });

    it('/api/export should deny unauthenticated requests', async () => {
        await request(server).get('/api/export').expect(401);
        await request(server).get('/api/export/thesis').expect(401);
    });

    it('/api/export/thesis should require since query', async () => {
        await request(server).get('/api/export/thesis')
        .set('Authorization', 'Bearer ' + token)
        .expect(422);
    });

    it('/api/export/thesis should give empty list if theses', async () => {
        await request(server).get('/api/export/thesis?since=2018-01-01')
        .set('Authorization', 'Bearer ' + token)
        .expect(200)
        .expect(res => {
            expect(res.body).to.have.length(0);
        });
    });

    describe('Approving', () => {
        let encodedContainerId: string;

        beforeEach(async () => {
            await request(server).post('/api/container')
            .expect(201)
            .expect(res => encodedContainerId = base64url.encode(res.body['@id']));
        });

        afterEach(async () => {
            await request(server).delete('/api/container/' + encodedContainerId).expect(204);
        });

        it('/api/export should give empty list if no approved theses', async () => {
            await request(server).get('/api/export/thesis?since=2018-01-01')
            .set('Authorization', 'Bearer ' + token)
            .expect(200)
            .expect(res => {
                expect(res.body).to.have.length(0);
            });
        });

        it('/api/approve POST should give results in /api/export/', async () => {
            await request(server).post('/api/approve/' + encodedContainerId)
            .set('Authorization', 'Bearer ' + token)
            .expect(200);

            await request(server).get('/api/export/thesis?since=2018-01-01')
            .set('Authorization', 'Bearer ' + token)
            .expect(200)
            .expect(res => {
                expect(res.body).to.have.length(1);
                expect(res.body).to.include(encodedContainerId);
            });
        });
    });
});
