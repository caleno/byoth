import { start as startServer, createApp } from '../src/app';

/* istanbul ignore file */

export const setup = {
    server: null
}

before(async () => {
    console.log('Starting server');
    const app = await createApp();

    try {
        setup.server = await startServer(app);
    } catch (error) {
        console.error(error);
        process.exit(1);
    }

    await new Promise((resolve, reject) => {
        if (setup.server.address()) {
            resolve();
        } else {
            setup.server.on('listening', resolve);
        }
    });
});

after(() => {
    setup.server.close();
});
