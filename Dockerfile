FROM node:10-alpine

# add ghostscript and build tools for npm modules
RUN apk update
RUN apk add ghostscript python make g++ gcc

WORKDIR /app
ENV NODE_ENV=production

# install modules
COPY package* ./
COPY model model
COPY client client
COPY server server

RUN npm install --unsafe-perm

# Building client is much slower than server, so do it first to get caching where it matters the most
RUN npm run build:client:prod

# Build server
RUN npm run build:server

COPY index.js .

EXPOSE 3000

USER node

# start application
CMD [ "npm", "start" ]
